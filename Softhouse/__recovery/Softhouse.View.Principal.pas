unit Softhouse.View.Principal;

interface

uses
  Winapi.Windows,
  Winapi.Tlhelp32,
  Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.ExtCtrls, Vcl.StdCtrls, Vcl.Mask,
  Vcl.DBCtrls, Vcl.ComCtrls, Data.DB, FireDAC.Stan.Intf, FireDAC.Stan.Option,
  FireDAC.Stan.Param, FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf,
  FireDAC.DApt.Intf, FireDAC.Stan.Async, FireDAC.DApt, FireDAC.UI.Intf,
  FireDAC.Stan.Def, FireDAC.Stan.Pool, FireDAC.Phys, FireDAC.Phys.FB,
  FireDAC.Phys.FBDef, FireDAC.VCLUI.Wait, FireDAC.Comp.Client,
  FireDAC.Comp.DataSet, FireDAC.Phys.IBBase, FireDAC.Comp.UI,
  FireDAC.Phys.MySQLDef, FireDAC.Phys.MySQL, Vcl.Themes, Vcl.Buttons, Vcl.Grids,
  Vcl.DBGrids, acPNG, FireDAC.Comp.ScriptCommands, FireDAC.Stan.Util,
  FireDAC.Comp.Script, System.ImageList, Vcl.ImgList, frxBarcode, System.IniFiles;

type
  TFormPrincipal = class(TForm)
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    TabSheet2: TTabSheet;
    DBEdit26: TDBEdit;
    Label109: TLabel;
    DBEdit32: TDBEdit;
    Label108: TLabel;
    DBEdit31: TDBEdit;
    DBEdit30: TDBEdit;
    Label107: TLabel;
    DBEdit28: TDBEdit;
    Label105: TLabel;
    DBEdit34: TDBEdit;
    Label137: TLabel;
    Label138: TLabel;
    DBEdit37: TDBEdit;
    DBEdit29: TDBEdit;
    Label106: TLabel;
    Label104: TLabel;
    DBEdit27: TDBEdit;
    Label103: TLabel;
    DBEdit12: TDBEdit;
    Label102: TLabel;
    Button12: TButton;
    GroupBox7: TGroupBox;
    Label279: TLabel;
    Label280: TLabel;
    Label281: TLabel;
    edtDataBase_Li: TEdit;
    edtSenha_LI: TEdit;
    edtUsuario_LI: TEdit;
    GroupBox6: TGroupBox;
    Label276: TLabel;
    Label277: TLabel;
    Label278: TLabel;
    edtUsuario_APP: TEdit;
    edtSenha_APP: TEdit;
    edtDataBase_APP: TEdit;
    edtServidor: TEdit;
    Label275: TLabel;
    Panel1: TPanel;
    Panel2: TPanel;
    EdtCaminho: TEdit;
    BtnSair: TButton;
    BtnCancelar: TButton;
    Button3: TButton;
    Label1: TLabel;
    Button1: TButton;
    Conexao: TFDConnection;
    Transacao: TFDTransaction;
    WaitCursor: TFDGUIxWaitCursor;
    FBDriver: TFDPhysFBDriverLink;
    dsSistema: TDataSource;
    OpenDialog1: TOpenDialog;
    Panel3: TPanel;
    qryParametro: TFDQuery;
    qryParametroVERSAO: TIntegerField;
    qryParametroDATA_SCRIPT: TDateField;
    qryParametroEMAIL_SUPORTE: TStringField;
    qryParametroTITULO_SISTEMA: TStringField;
    qryParametroSUB_TITULO_SISTEMA: TStringField;
    qryParametroSITE: TStringField;
    qryParametroFONE1: TStringField;
    qryParametroFONE2: TStringField;
    qryParametroCONTATO: TStringField;
    qryParametroESTILO: TStringField;
    qryParametroLINK_TREINAMENTO: TStringField;
    qryParametroICONE: TBlobField;
    qryParametroSPLASH: TBlobField;
    qryParametroSERVIDOR_APP: TStringField;
    qryParametroUSUARIO_APP: TStringField;
    qryParametroSENHA_APP: TStringField;
    qryParametroDATABASE_APP: TStringField;
    qryParametroDATABASE_LI: TStringField;
    qryParametroTELA_FUNDO: TStringField;
    qryParametroMENU_MAXIMIZADO: TStringField;
    qryParametroSENHA_LI: TStringField;
    qryParametroTELA_ABERTURA: TStringField;
    qryParametroTELA_FUNDO_ECF: TStringField;
    qryParametroUSUARIO_LI: TStringField;
    qryParametroFTP_PASTA: TStringField;
    qryParametroFTP_SERVIDOR: TStringField;
    qryParametroFTP_USUARIO: TStringField;
    qryParametroFTP_SENHA: TStringField;
    qryParametroFTP_ARQUIVO: TStringField;
    qryParametroEMPRESA: TStringField;
    qryParametroLINK_VENDA: TStringField;
    qryParametroCAMINHO_LOGO_FPG: TStringField;
    MysqlChave: TFDPhysMySQLDriverLink;
    ConexaoChave: TFDConnection;
    Button2: TButton;
    ConexaoAPP: TFDConnection;
    MysqlAPP: TFDPhysMySQLDriverLink;
    TabSheet3: TTabSheet;
    Panel4: TPanel;
    PageControl2: TPageControl;
    TabSheet4: TTabSheet;
    qryParametroCNPJ: TStringField;
    Panel5: TPanel;
    Label2: TLabel;
    Label3: TLabel;
    DBText2: TDBText;
    Label26: TLabel;
    DBText1: TDBText;
    Label5: TLabel;
    DBCheckBox37: TDBCheckBox;
    Label110: TLabel;
    DBComboBox9: TDBComboBox;
    qryParametroBLOQUEAR_PERSONALIZACAO: TStringField;
    qryParametroTELA_FUNDO_LOGIN: TStringField;
    qryParametroDATA_ATUALIZACAO: TSQLTimeStampField;
    qryEmpresa: TFDQuery;
    dsEmpresa: TDataSource;
    Panel6: TPanel;
    Label4: TLabel;
    Label6: TLabel;
    Label7: TLabel;
    DBText3: TDBText;
    DBText4: TDBText;
    DBText5: TDBText;
    qryEmpresaFANTASIA: TStringField;
    qryEmpresaRAZAO: TStringField;
    qryEmpresaCNPJ: TStringField;
    Label93: TLabel;
    grVisiveis: TGroupBox;
    DBCheckBox1: TDBCheckBox;
    DBCheckBox2: TDBCheckBox;
    DBCheckBox3: TDBCheckBox;
    DBCheckBox4: TDBCheckBox;
    DBCheckBox5: TDBCheckBox;
    DBCheckBox6: TDBCheckBox;
    DBCheckBox7: TDBCheckBox;
    DBCheckBox8: TDBCheckBox;
    DBCheckBox9: TDBCheckBox;
    DBCheckBox10: TDBCheckBox;
    DBCheckBox11: TDBCheckBox;
    TabSheet16: TTabSheet;
    TabSheet17: TTabSheet;
    TabSheet18: TTabSheet;
    Panel20: TPanel;
    Panel21: TPanel;
    Panel22: TPanel;
    DBEdit38: TDBEdit;
    btnSplash: TButton;
    Label140: TLabel;
    DBEdit40: TDBEdit;
    Button5: TButton;
    DBEdit33: TDBEdit;
    Label18: TLabel;
    Button6: TButton;
    Label116: TLabel;
    TabSheet19: TTabSheet;
    Panel23: TPanel;
    qryEmpresaCODIGO: TIntegerField;
    qryEmpresaTIPO: TStringField;
    qryEmpresaIE: TStringField;
    qryEmpresaIM: TStringField;
    qryEmpresaENDERECO: TStringField;
    qryEmpresaNUMERO: TStringField;
    qryEmpresaCOMPLEMENTO: TStringField;
    qryEmpresaBAIRRO: TStringField;
    qryEmpresaCIDADE: TStringField;
    qryEmpresaUF: TStringField;
    qryEmpresaCEP: TStringField;
    qryEmpresaFONE: TStringField;
    qryEmpresaFAX: TStringField;
    qryEmpresaSITE: TStringField;
    qryEmpresaLOGOMARCA: TBlobField;
    qryEmpresaFUNDACAO: TDateField;
    qryEmpresaUSU_CAD: TSmallintField;
    qryEmpresaUSU_ATU: TSmallintField;
    qryEmpresaNSERIE: TStringField;
    qryEmpresaCSENHA: TStringField;
    qryEmpresaNTERM: TStringField;
    qryEmpresaID_PLANO_TRANSFERENCIA_C: TIntegerField;
    qryEmpresaID_PLANO_TRANSFERENCIA_D: TIntegerField;
    qryEmpresaID_CAIXA_GERAL: TIntegerField;
    qryEmpresaBLOQUEAR_ESTOQUE_NEGATIVO: TStringField;
    qryEmpresaID_CIDADE: TIntegerField;
    qryEmpresaCRT: TSmallintField;
    qryEmpresaID_UF: TSmallintField;
    qryEmpresaID_PLANO_VENDA: TIntegerField;
    qryEmpresaOBSFISCO: TMemoField;
    qryEmpresaCFOP: TStringField;
    qryEmpresaCSOSN: TStringField;
    qryEmpresaCST_ICMS: TStringField;
    qryEmpresaALIQ_ICMS: TFMTBCDField;
    qryEmpresaCST_ENTRADA: TStringField;
    qryEmpresaCST_SAIDA: TStringField;
    qryEmpresaALIQ_PIS: TFMTBCDField;
    qryEmpresaALIQ_COF: TFMTBCDField;
    qryEmpresaCST_IPI: TStringField;
    qryEmpresaALIQ_IPI: TFMTBCDField;
    qryEmpresaIMP_F5: TStringField;
    qryEmpresaIMP_F6: TStringField;
    qryEmpresaMOSTRA_RESUMO_CAIXA: TStringField;
    qryEmpresaLIMITE_DIARIO: TFMTBCDField;
    qryEmpresaPRAZO_MAXIMO: TSmallintField;
    qryEmpresaID_PLA_CONTA_FICHA_CLI: TIntegerField;
    qryEmpresaID_PLANO_CONTA_RETIRADA: TIntegerField;
    qryEmpresaUSA_PDV: TStringField;
    qryEmpresaRECIBO_VIAS: TStringField;
    qryEmpresaID_PLANO_TAXA_CARTAO: TIntegerField;
    qryEmpresaOBS_CARNE: TMemoField;
    qryEmpresaCAIXA_UNICO: TStringField;
    qryEmpresaCAIXA_RAPIDO: TStringField;
    qryEmpresaEMPRESA_PADRAO: TSmallintField;
    qryEmpresaID_PLANO_CONTA_DEVOLUCAO: TIntegerField;
    qryEmpresaN_INICIAL_NFCE: TIntegerField;
    qryEmpresaN_INICIAL_NFE: TIntegerField;
    qryEmpresaCHECA_ESTOQUE_FISCAL: TStringField;
    qryEmpresaDESCONTO_PROD_PROMO: TStringField;
    qryEmpresaDATA_CADASTRO: TStringField;
    qryEmpresaDATA_VALIDADE: TStringField;
    qryEmpresaFLAG: TStringField;
    qryEmpresaLANCAR_CARTAO_CREDITO: TStringField;
    qryEmpresaFILTRAR_EMPRESA_LOGIN: TStringField;
    qryEmpresaENVIAR_EMAIL_NFE: TStringField;
    qryEmpresaTRANSPORTADORA: TStringField;
    qryEmpresaTABELA_PRECO: TStringField;
    qryEmpresaTAXA_VENDA_PRAZO: TFMTBCDField;
    qryEmpresaEMAIL_CONTADOR: TStringField;
    qryEmpresaAUTOPECAS: TStringField;
    qryEmpresaATUALIZA_PR_VENDA: TStringField;
    qryEmpresaINFORMAR_GTIN: TStringField;
    qryEmpresaRECOLHE_FCP: TStringField;
    qryEmpresaDIFAL_ORIGEM: TFMTBCDField;
    qryEmpresaDIFAL_DESTINO: TFMTBCDField;
    qryEmpresaEXCLUI_PDV: TStringField;
    qryEmpresaVENDA_SEMENTE: TStringField;
    qryEmpresaCHECA: TStringField;
    qryEmpresaEMAIL: TStringField;
    qryEmpresaULTIMONSU: TStringField;
    qryEmpresaULTIMO_PEDIDO: TIntegerField;
    qryEmpresaTIPO_CONTRATO: TIntegerField;
    qryEmpresaTIPO_EMPRESA: TIntegerField;
    qryEmpresaQTD_MESAS: TSmallintField;
    qryEmpresaBLOQUEAR_PRECO: TStringField;
    qryEmpresaEXIBE_RESUMO_CAIXA: TStringField;
    qryEmpresaID_PLANO_COMPRA: TIntegerField;
    qryEmpresaRESPONSAVEL_TECNICO: TStringField;
    qryEmpresaCARENCIA_JUROS: TIntegerField;
    qryEmpresaPESQUISA_REFERENCIA: TStringField;
    qryEmpresaRESTAURANTE: TStringField;
    qryEmpresaEXIBE_F3: TStringField;
    qryEmpresaEXIBE_F4: TStringField;
    qryEmpresaLER_PESO: TStringField;
    qryEmpresaDESCONTO_MAXIMO: TFMTBCDField;
    qryEmpresaCHECA_LIMITE: TStringField;
    qryEmpresaEMITE_ECF: TStringField;
    qryEmpresaLOJA_ROUPA: TStringField;
    qryEmpresaTIPO_JUROS: TStringField;
    qryEmpresaJUROS_DIA: TFMTBCDField;
    qryEmpresaJUROS_MES: TFMTBCDField;
    qryEmpresaFARMACIA: TStringField;
    qryEmpresaPAGAMENTO_DINHEIRO: TStringField;
    qryEmpresaRESPONSAVEL_EMPRESA: TStringField;
    qryEmpresaHABILITA_DESCONTO_PDV: TStringField;
    qryEmpresaPUXA_CFOP_PRODUTO: TStringField;
    qryEmpresaLANCAR_CARTAO_CR: TStringField;
    qryEmpresaUSA_BLUETOOTH_RESTA: TStringField;
    qryEmpresaCFOP_EXTERNO: TStringField;
    qryEmpresaCNAE: TStringField;
    qryEmpresaCODIGO_PAIS: TIntegerField;
    qryEmpresaOBSNFCE: TMemoField;
    qryEmpresaHABILITA_ACRESCIMO: TStringField;
    qryEmpresaMULTI_IDIOMA: TStringField;
    qryEmpresaCSOSN_EXTERNO: TStringField;
    qryEmpresaCST_EXTERNO: TStringField;
    qryEmpresaALIQ_ICMS_EXTERNO: TFMTBCDField;
    qryEmpresaCOD_FPG_DINHEIRO: TIntegerField;
    qryEmpresaSEGUNDA_VIA_NFCE: TStringField;
    qryEmpresaREPLICAR_DADOS: TStringField;
    qryEmpresaID_PLANO_BOLETO: TIntegerField;
    qryEmpresaBLOQUEAR_PERSONALIZACAO: TStringField;
    qryEmpresaBLOQUEAR_CPF: TStringField;
    qryEmpresaDESCONTO_ITEM_PDV: TStringField;
    qryEmpresaRATEAR_FRETE: TStringField;
    qryEmpresaUSA_CREDITO_SIMPLES: TStringField;
    qryEmpresaUSA_TEF: TStringField;
    qryEmpresaAUTO_ATUALIZA: TStringField;
    qryEmpresaPESQUISA_POR_PARTE: TStringField;
    qryEmpresaBAIXAR_ESTOQUE_COMPOSICAO: TStringField;
    qryEmpresaPRE_VISUALIZAR_IMPRESSAO: TStringField;
    qryEmpresaMENSAGEM_COBRANCA: TMemoField;
    qryEmpresaALIQUOTA_SIMPLES: TFMTBCDField;
    qryEmpresaSERVIDOR_SMTP_PROPRIO: TStringField;
    qryEmpresaTRANSMITIR_CARTAO_AUTO: TStringField;
    qryEmpresaLUCRO_PADRAO: TFMTBCDField;
    qryEmpresaNFE_SERIE: TIntegerField;
    qryEmpresaOCULTAR_SALDO_ANTERIOR: TStringField;
    qryEmpresaBAIXAR_ESTOQUE_NFE: TStringField;
    qryEmpresaEXIBE_ESTOQUE_FISCAL: TStringField;
    qryEmpresaNFCE_PRODUTO_SUBSTITUTO: TStringField;
    qryEmpresaAUTO_CADASTRO_PRODUTO: TStringField;
    qryEmpresaCFOP_ENTRADA_PADRAO: TStringField;
    qryEmpresaCFOP_ENTRADA_PADRO_E: TStringField;
    qryEmpresaPUXAR_CFOP_ENTRADA: TStringField;
    qryEmpresaID_PLANO_ABERTURA: TIntegerField;
    Panel7: TPanel;
    Panel8: TPanel;
    DBGrid1: TDBGrid;
    Panel9: TPanel;
    Panel10: TPanel;
    Panel11: TPanel;
    bbAlterar: TSpeedButton;
    bbNovo: TSpeedButton;
    SpeedButton1: TSpeedButton;
    SpeedButton2: TSpeedButton;
    SpeedButton3: TSpeedButton;
    SpeedButton4: TSpeedButton;
    SpeedButton5: TSpeedButton;
    SpeedButton7: TSpeedButton;
    SpeedButton8: TSpeedButton;
    SpeedButton9: TSpeedButton;
    SpeedButton10: TSpeedButton;
    Panel12: TPanel;
    GroupBox1: TGroupBox;
    DBCheckBox12: TDBCheckBox;
    DBCheckBox13: TDBCheckBox;
    DBCheckBox14: TDBCheckBox;
    DBCheckBox15: TDBCheckBox;
    GroupBox2: TGroupBox;
    DBCheckBox16: TDBCheckBox;
    DBCheckBox17: TDBCheckBox;
    DBCheckBox18: TDBCheckBox;
    DBCheckBox20: TDBCheckBox;
    GroupBox3: TGroupBox;
    DBCheckBox21: TDBCheckBox;
    GroupBox4: TGroupBox;
    DBCheckBox22: TDBCheckBox;
    DBCheckBox23: TDBCheckBox;
    DBCheckBox24: TDBCheckBox;
    GroupBox5: TGroupBox;
    DBCheckBox25: TDBCheckBox;
    DBCheckBox26: TDBCheckBox;
    DBCheckBox27: TDBCheckBox;
    DBCheckBox28: TDBCheckBox;
    DBCheckBox29: TDBCheckBox;
    qryConfigAcesso: TFDQuery;
    dsConfigAcesso: TDataSource;
    qryConfigAcessoCODIGO: TIntegerField;
    qryConfigAcessoBLOQ_CAD_EMPRESA: TStringField;
    qryConfigAcessoBLOQ_CAD_USUARIO: TStringField;
    qryConfigAcessoBLOQ_PERMISSOES: TStringField;
    qryConfigAcessoBLOQ_CONFIG: TStringField;
    qryConfigAcessoBLOQ_CONFIG_TERM: TStringField;
    qryConfigAcessoBLOQ_CAD_CONTADOR: TStringField;
    qryConfigAcessoBLOQ_IBPT: TStringField;
    qryConfigAcessoBLOQ_ICMS: TStringField;
    qryConfigAcessoBLOQ_CFOP: TStringField;
    qryConfigAcessoBLOQ_CAD_CAIXAS: TStringField;
    qryConfigAcessoBLOQ_CAD_MESAS: TStringField;
    qryConfigAcessoRETAGUARDA_BLOQUEADO: TStringField;
    qryConfigAcessoPDV_BLOQUEADO: TStringField;
    qryConfigAcessoPREVENDA_BLOQUEADO: TStringField;
    qryConfigAcessoZAP_BLOQUEADO: TStringField;
    qryConfigAcessoBLOQ_NFE: TStringField;
    qryConfigAcessoBLOQ_CTE: TStringField;
    qryConfigAcessoBLOQ_MDFE: TStringField;
    qryConfigAcessoBLOQ_ORCAMENTO: TStringField;
    qryConfigAcessoPRODUTO_BLOQ_IMPOSTOS: TStringField;
    qryConfigAcessoCLIENTE_BLOQ_ADICIONAIS: TStringField;
    qryConfigAcessoCLIENTE_BLOQ_FOTO: TStringField;
    qryConfigAcessoCLIENTE_BLOQ_CONTATOS: TStringField;
    qryConfigAcessoMENU_BLOQ_BOLETO: TStringField;
    qryConfigAcessoMENU_BLOQ_FISCAL: TStringField;
    qryConfigAcessoMENU_BLOQ_TRANSP: TStringField;
    qryConfigAcessoMENU_BLOQ_OS: TStringField;
    qryConfigAcessoMENU_BLOQ_REL_FISCAL: TStringField;
    Image1: TImage;
    Image2: TImage;
    TabSheet6: TTabSheet;
    MemoSql: TMemo;
    Panel13: TPanel;
    lblMsg: TLabel;
    Button4: TButton;
    Panel14: TPanel;
    qryConsulta: TFDQuery;
    IBScript: TFDScript;
    DBCheckBox30: TDBCheckBox;
    qryConfigAcessoBLOQ_SCRIPT: TStringField;
    ImageList1: TImageList;
    Label8: TLabel;
    frxBarCodeObject1: TfrxBarCodeObject;
    DBText6: TDBText;
    DBEdit1: TDBEdit;
    RadioGroup1: TRadioGroup;
    edtTerminal: TEdit;
    btnAplicar: TButton;
    Label10: TLabel;
    btnTeste: TButton;
    RadioButton1: TRadioButton;
    procedure Button3Click(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure BtnSairClick(Sender: TObject);
    procedure BtnCancelarClick(Sender: TObject);
    procedure dsSistemaStateChange(Sender: TObject);
    procedure qryParametroBeforePost(DataSet: TDataSet);
    procedure qryParametroAfterOpen(DataSet: TDataSet);
    procedure FormShow(Sender: TObject);
    procedure edtServidorChange(Sender: TObject);
    procedure Button12Click(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure DBComboBox9Change(Sender: TObject);
    procedure Button6Click(Sender: TObject);
    procedure Button5Click(Sender: TObject);
    procedure btnSplashClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure DBGrid1DblClick(Sender: TObject);
    procedure DBGrid1DrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumn; State: TGridDrawState);
    procedure DBGrid1KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure bbNovoClick(Sender: TObject);
    procedure bbAlterarClick(Sender: TObject);
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure SpeedButton4Click(Sender: TObject);
    procedure SpeedButton5Click(Sender: TObject);
    procedure SpeedButton6Click(Sender: TObject);
    procedure SpeedButton7Click(Sender: TObject);
    procedure SpeedButton8Click(Sender: TObject);
    procedure SpeedButton9Click(Sender: TObject);
    procedure SpeedButton10Click(Sender: TObject);
    procedure Button4Click(Sender: TObject);
    procedure GravaLicenca;
    procedure LeLicenca;
    procedure btnTesteClick(Sender: TObject);
    procedure RadioGroup1Click(Sender: TObject);
  private
    { Private declarations }
    function Crypt(Action, Src: String): String;
    procedure ExecuteScript;
    function ChamaLogin: Boolean;
  public
    Estilo: String;
    procedure ConfiguraSistema;
    { Public declarations }
  end;

var
  FormPrincipal: TFormPrincipal;

implementation

{$R *.dfm}

uses
Udados, uDadosWeb, uSplash, uAcessoconfig, uEmpresa, UUsuarios, uPermissoes, uConfig,
uTerminais, uIBPT, uICMS, UCFOP, uContas, uMesas, U_Backup, uConexaoBD;

function IsFireBirdRunning: Boolean;
const
  PROCESS_TERMINATE = $0001;
var
  Co: BOOL;
  FS: THandle;
  FP: TProcessEntry32;
  s: string;
begin
  FS := CreateToolhelp32Snapshot(TH32CS_SNAPPROCESS, 0);
  FP.dwSize := Sizeof(FP);
  Co := Process32First(FS, FP);
  while Integer(Co) <> 0 do
        begin
        s := s + FP.szExeFile + #13;
        Co := Process32Next(FS, FP);
        end;
  CloseHandle(FS);
  if pos('fbserver', s) > 0 then
     Result := True
  else
     Result := False;
end;

function TFormPrincipal.ChamaLogin: Boolean;
var
  CriaUsuario: Boolean;
begin
  CriaUsuario := false;

  Dados.qryConsulta.Close;
  Dados.qryConsulta.SQL.Text := 'select * from usuarios';
  Dados.qryConsulta.Open;
  if Dados.qryConsulta.IsEmpty then
    CriaUsuario := true;
  Result := CriaUsuario;
  if not CriaUsuario then
  begin
    try
      frmAcessoConfig := TfrmAcessoconfig.Create(Application);
      frmAcessoconfig.ShowModal;
    finally
      frmAcessoconfig.Release;
    end;
  end
  else
  begin
    try
      frmUsuarios := TfrmUsuarios.Create(Application);
      frmUsuarios.Tela := 'Novo';
      frmUsuarios.ShowModal;
    finally
      frmUsuarios.Release;
    end;
  end;

end;

procedure TFormPrincipal.bbAlterarClick(Sender: TObject);
begin
   try
    bbAlterar.Enabled := false;
    FrmEmpresa := TFrmEmpresa.Create(Application);

    if qryEmpresa.IsEmpty then
      exit;

    FrmEmpresa.qryEmpresa.Close;
    FrmEmpresa.qryEmpresa.Params[0].AsInteger := qryEmpresaCODIGO.AsInteger;
    FrmEmpresa.qryEmpresa.Open;

    FrmEmpresa.qryEmpresa.edit;

    FrmEmpresa.Showmodal;
    Application.ProcessMessages;
  finally
    FrmEmpresa.Release;

    bbAlterar.Enabled := true;

  end;
end;

procedure TFormPrincipal.bbNovoClick(Sender: TObject);
begin
    try
    bbNovo.Enabled := false;
    FrmEmpresa := TFrmEmpresa.Create(Application);
    FrmEmpresa.qryEmpresa.Close;
    FrmEmpresa.qryEmpresa.Params[0].Value := -1;
    FrmEmpresa.qryEmpresa.Open;
    FrmEmpresa.qryEmpresa.Insert;
    FrmEmpresa.qryEmpresaCODIGO.Value := Dados.Numerador('EMPRESA', 'CODIGO',
      'N', '', '');
    FrmEmpresa.Showmodal;
    Application.ProcessMessages;
  finally

    FrmEmpresa.Release;
    bbNovo.Enabled := true;

  end;
end;

procedure TFormPrincipal.BtnCancelarClick(Sender: TObject);
begin
  try
    if qryParametro.State in [dsinsert, dsedit] then
      qryParametro.Cancel;
  Conexao.RollbackRetaining;
  except

  end;
end;

procedure TFormPrincipal.BtnSairClick(Sender: TObject);
begin
  Close;
end;

procedure TFormPrincipal.btnSplashClick(Sender: TObject);
begin
    OpenDialog1.Title := 'Selecione o Logo';
  OpenDialog1.DefaultExt := '*.*';
  OpenDialog1.Filter := 'PNG|*.png';
  OpenDialog1.InitialDir := ExtractFileDir(application.ExeName);
  OpenDialog1.Execute;
  if OpenDialog1.FileName <> '' then
  begin
    if not(qryParametro.State in dsEditModes) then
      qryParametro.Edit;
    qryParametroTELA_ABERTURA.Value := OpenDialog1.FileName;
  end;
end;

procedure TFormPrincipal.Button12Click(Sender: TObject);
begin
  try
    MysqlChave.VendorLib := ExtractFilePath(Application.ExeName) + 'libmysql.dll';
    ConexaoChave.Params.Values['Server']    := edtServidor.Text;
    ConexaoChave.Params.Values['Database']  := edtDataBase_Li.Text;
    ConexaoChave.Params.Values['User_Name'] := edtUsuario_LI.Text;
    ConexaoChave.Params.Values['Password']  := edtSenha_LI.Text;
    ConexaoChave.Params.Values['Port']      := '3306';
    ConexaoChave.Connected                  := True;
    ShowMessage('Teste de conexão ao servidor de licença com sucesso!');
  except
    ShowMessage('Não foi possivel conectar ao servidor de licença!');
  end;
end;

procedure TFormPrincipal.Button1Click(Sender: TObject);
begin
  if not IsFireBirdRunning then
     begin
     ShowMessage('Firebird não esta ativo!');
     exit;
     end;


 // OpenDialog1.Title      := 'Selecione o Banco de Dados';
 // OpenDialog1.DefaultExt := '*.fdb';
// OpenDialog1.Filter     := 'Arquivos Firebird (*.fdb)|*.fdb|Todos os Arquivos (*.*)|*.*';
 // OpenDialog1.InitialDir := ExtractFileDir(application.ExeName);
 // if OpenDialog1.Execute then
  if Conexao.Connected = False then
     begin
     try
       //FBDriver.VendorLib := ExtractFilePath(Application.ExeName) + 'fbclient.dll';
       //FBDriver.Embedded  := True;
       Conexao.Connected  := False;
       Conexao.Params.Values['DriverID'] := 'FB';
       Conexao.Params.Values['Server']   := 'localhost';
       Conexao.Params.Values['Database'] := 'C:\GerenteErp\Dados\DADOS.FDB';//OpenDialog1.FileName;
       Conexao.Connected := True;
       qryParametro.Open;
       qryEmpresa.Open;
       qryConfigAcesso.Open;
       EdtCaminho.Text := Conexao.Params.Values['Database'];//OpenDialog1.FileName;
       PageControl1.Visible := True;
       Panel12.Visible := False;
       Image1.Visible := True;
       Image2.Visible := False;
       Button1.Caption := 'Conectado';
       Button1.Enabled := False;
     except
       ShowMessage('Não foi possivel conectar na base de dados!');
     end;
     end;
end;

procedure TFormPrincipal.Button2Click(Sender: TObject);
begin
    try
    MysqlAPP.VendorLib := ExtractFilePath(Application.ExeName) + 'libmysql.dll';
    ConexaoAPP.Params.Values['Server']    := edtServidor.Text;
    ConexaoAPP.Params.Values['Database']  := edtDataBase_APP.Text;
    ConexaoAPP.Params.Values['User_Name'] := edtUsuario_APP.Text;
    ConexaoAPP.Params.Values['Password']  := edtSenha_APP.Text;
    ConexaoAPP.Params.Values['Port']      := '3306';
    ConexaoAPP.Connected                  := True;
    ShowMessage('Teste de conexão ao servidor de APP com sucesso!');
  except
    ShowMessage('Não foi possivel conectar ao servidor de APP!');
  end;
end;

procedure TFormPrincipal.Button3Click(Sender: TObject);
begin
  try
    if qryParametro.State in [dsinsert, dsedit] then
      qryParametro.Post;

    if qryEmpresa.State in [dsinsert, dsedit] then
      qryEmpresa.Post;

    if qryConfigAcesso.State in [dsinsert, dsedit] then
      qryConfigAcesso.Post;

    GravaLicenca;

  Conexao.CommitRetaining;
  except

  end;
end;

procedure TFormPrincipal.Button4Click(Sender: TObject);
begin
  try
    lblMsg.Caption := '';
    frmBackup := TfrmBackup.Create(Application);
    frmBackup.ShowModal;
  finally
    frmBackup.Release;
    ExecuteScript;
  end;
end;

procedure TFormPrincipal.ExecuteScript;
begin
  try
    IBScript.ExecuteScript(MemoSql.Lines);
    Dados.Conexao.CommitRetaining;
    ShowMessage('SQL executado com sucesso!');
  except
    on e: exception do
      ShowMessage(e.Message);
  end;
end;

procedure TFormPrincipal.Button5Click(Sender: TObject);
begin
    OpenDialog1.Title := 'Selecione o Logo';
  OpenDialog1.DefaultExt := '*.*';
  OpenDialog1.Filter := 'PNG|*.png';
  OpenDialog1.InitialDir := ExtractFileDir(application.ExeName);
  OpenDialog1.Execute;
  if OpenDialog1.FileName <> '' then
  begin
    if not(qryParametro.State in dsEditModes) then
      qryParametro.Edit;
    qryParametroTELA_FUNDO_ECF.Value := OpenDialog1.FileName;
  end;
end;

procedure TFormPrincipal.Button6Click(Sender: TObject);
begin
  OpenDialog1.Title := 'Selecione o Logo';
  OpenDialog1.DefaultExt := '*.*';
  OpenDialog1.Filter := 'PNG|*.png';
  OpenDialog1.InitialDir := ExtractFileDir(application.ExeName);
  OpenDialog1.Execute;
  if OpenDialog1.FileName <> '' then
  begin
    if not(qryParametro.State in dsEditModes) then
      qryParametro.Edit;
    qryParametroTELA_FUNDO.Value := OpenDialog1.FileName;
  end;
end;

procedure TFormPrincipal.btnTesteClick(Sender: TObject);
var
  iArq: TIniFile;
  nTentativas: word;
begin

  try

    nTentativas := 1;
    iArq := TIniFile.Create(ExtractFilePath(Application.ExeName) + 'Banco.ini');

    Conexao.Params.Values['DriverID'] := 'FB';
    Conexao.Params.Values['Server'] := edtTerminal.Text;
    Conexao.Params.Values['Database'] := iArq.ReadString('BD', 'Path', '');
    FBDriver.VendorLib := ExtractFilePath(Application.ExeName) + 'fbclient.dll';

    while nTentativas <= 12 do
    begin
      if not IsFireBirdRunning then
      begin
        nTentativas := nTentativas + 1;
        if frmConexaoBD = nil then
        begin
          frmConexaoBD := TfrmConexaoBD.Create(Application);
          frmConexaoBD.Show;
        end;
        Application.ProcessMessages;
        sleep(10000);
      end
      else
        nTentativas := 13;
    end;

    if nTentativas = 13 then
    begin
      if frmConexaoBD <> nil then
        frmConexaoBD.Close;
    end;

    try
      Conexao.Connected := true;
    Except
      ShowMessage('Não foi possivel conectar na base de dados!');
      //Dados.vFechaPrograma := true;
      //Application.Terminate;
    end;

  Finally
    iArq.Free;
  end;

end;

function TFormPrincipal.Crypt(Action, Src: String): String;
Label Fim;
var
  KeyLen: Integer;
  KeyPos: Integer;
  OffSet: Integer;
  Dest, Key, KeyNew: String;
  SrcPos: Integer;
  SrcAsc: Integer;
  TmpSrcAsc: Integer;
  Range: Integer;
begin
  if (Src = '') Then                             // NOVA FUNÇÃO DE ATIVAÇÃO ONLINE
  begin
    Result := '';
    Goto Fim;
  end;

  Key := 'XNGREXCPAJHKQWERYTUIOP98756LKJHASFGMNBVCAXZ13450';

  KeyNew := 'PRODOXCPAJHKQWERYTUIOP98765LKJHASFGMNBVCAXZ01234';

  Dest := '';
  KeyLen := Length(Key);
  KeyPos := 0;
  SrcPos := 0;
  SrcAsc := 0;
  Range := 128;
  if (Action = UpperCase('C')) then
  begin
    // Randomize;
    OffSet := Range;
    Dest := Format('%1.2x', [OffSet]);
    for SrcPos := 1 to Length(Src) do
    begin
      Application.ProcessMessages;
      SrcAsc := (Ord(Src[SrcPos]) + OffSet) Mod 255;
      if KeyPos < KeyLen then
        KeyPos := KeyPos + 1
      else
        KeyPos := 1;

      SrcAsc := SrcAsc Xor Ord(Key[KeyPos]);
      Dest := Dest + Format('%1.2x', [SrcAsc]);
      OffSet := SrcAsc;
    end;
  end
  Else if (Action = UpperCase('D')) then
  begin
    OffSet := StrToInt('$' + copy(Src, 1, 2));
    // <--------------- adiciona o $ entra as aspas simples
    SrcPos := 3;
    repeat
      SrcAsc := StrToInt('$' + copy(Src, SrcPos, 2));
      // <--------------- adiciona o $ entra as aspas simples
      if (KeyPos < KeyLen) Then
        KeyPos := KeyPos + 1
      else
        KeyPos := 1;
      TmpSrcAsc := SrcAsc Xor Ord(Key[KeyPos]);
      if TmpSrcAsc <= OffSet then
        TmpSrcAsc := 255 + TmpSrcAsc - OffSet
      else
        TmpSrcAsc := TmpSrcAsc - OffSet;
      Dest := Dest + Chr(TmpSrcAsc);
      OffSet := SrcAsc;
      SrcPos := SrcPos + 2;
    until (SrcPos >= Length(Src));
  end;
  Result := Dest;
Fim:


end;

procedure TFormPrincipal.dsSistemaStateChange(Sender: TObject);
begin
  if qryParametro.State in [dsinsert, dsedit] then
     BtnCancelar.Enabled := true;


end;

procedure TFormPrincipal.edtServidorChange(Sender: TObject);
begin
  if qryParametro.Active then
     qryParametro.Edit;

    if qryEmpresa.Active then
      qryEmpresa.Edit;

  if qryConfigAcesso.Active then
      qryConfigAcesso.Edit;
end;

procedure TFormPrincipal.FormActivate(Sender: TObject);
begin
  Dados.vForm := nil;
  Dados.vForm := SELF;
  Dados.GetComponentes;
end;

procedure TFormPrincipal.FormCreate(Sender: TObject);
begin
  Conexao.Connected  := false;

end;

procedure TFormPrincipal.ConfiguraSistema;
begin

  Dados.qryParametro.Close;
  Dados.qryParametro.Open;

end;

procedure TFormPrincipal.FormDestroy(Sender: TObject);
begin
  Conexao.Connected  := False;
end;

procedure TFormPrincipal.FormShow(Sender: TObject);
begin

   if ChamaLogin then
    exit;

  //button1.Click;
  PageControl1.ActivePage := TabSheet1;
  Panel12.Align := alClient;
  LeLicenca;
end;

procedure TFormPrincipal.GravaLicenca;
begin
   if not(Dados.qryParametro.State in dsEditModes) then
    Dados.qryParametro.Edit;
  try
    Dados.qryParametroSERVIDOR_APP.Value := edtServidor.Text;
    //Dados.qryParametroFTP_SERVIDOR.Value := edtFTP_Caminho.Text;
    Dados.qryParametroUSUARIO_APP.Value :=
      Dados.Crypt('C', edtUsuario_APP.Text);
    Dados.qryParametroSENHA_APP.Value := Dados.Crypt('C', edtSenha_APP.Text);
    Dados.qryParametroDATABASE_APP.Value :=
      Dados.Crypt('C', edtDataBase_APP.Text);

    Dados.qryParametroUSUARIO_LI.Value := Dados.Crypt('C', edtUsuario_LI.Text);
    Dados.qryParametroSENHA_LI.Value := Dados.Crypt('C', edtSenha_LI.Text);
    Dados.qryParametroDATABASE_LI.Value :=
      Dados.Crypt('C', edtDataBase_Li.Text);
  except
    Dados.qryParametroSENHA_APP.Clear;
    Dados.qryParametroDATABASE_APP.Clear;
    Dados.qryParametroUSUARIO_APP.Clear;

    Dados.qryParametroUSUARIO_LI.Clear;
    Dados.qryParametroSENHA_LI.Clear;
    Dados.qryParametroDATABASE_LI.Clear;

  end;
  Dados.qryParametro.Post;
end;

procedure TFormPrincipal.LeLicenca;
begin
  try
    edtServidor.Text := Dados.qryParametroSERVIDOR_APP.Value;

    //edtFTP_Caminho.Text := Dados.qryParametroFTP_SERVIDOR.Value;

    edtSenha_APP.Text := Dados.Crypt('D', Dados.qryParametroSENHA_APP.Value);

    edtSenha_LI.Text := Dados.Crypt('D', Dados.qryParametroSENHA_LI.Value);

    edtDataBase_APP.Text := Dados.Crypt('D',
      Dados.qryParametroDATABASE_APP.Value);

    edtDataBase_Li.Text := Dados.Crypt('D',
      Dados.qryParametroDATABASE_LI.Value);

    edtUsuario_APP.Text := Dados.Crypt('D',
      Dados.qryParametroUSUARIO_APP.Value);

    edtUsuario_LI.Text := Dados.Crypt('D', Dados.qryParametroUSUARIO_LI.Value);

  Except
    edtSenha_APP.Text := '';
    edtSenha_LI.Text := '';
    edtDataBase_APP.Text := '';
    edtDataBase_Li.Text := '';
    edtUsuario_APP.Text := '';
    edtUsuario_LI.Text := '';
  end;
end;

procedure TFormPrincipal.qryParametroAfterOpen(DataSet: TDataSet);
begin
//  try
//    edtServidor.Text     := qryParametroSERVIDOR_APP.AsString;
//
//    edtSenha_APP.Text    := Crypt('D', qryParametroSENHA_APP.Value);
//
//    edtSenha_LI.Text     := Crypt('D', qryParametroSENHA_LI.Value);
//
//    edtDataBase_APP.Text := Crypt('D', qryParametroDATABASE_APP.Value);
//
//    edtDataBase_Li.Text  := Crypt('D', qryParametroDATABASE_LI.Value);
//
//    edtUsuario_APP.Text  := Crypt('D', qryParametroUSUARIO_APP.Value);
//
//    edtUsuario_LI.Text   := Crypt('D', qryParametroUSUARIO_LI.Value);
//  Except
////    edtServidor.Text     := EmptyStr;
////    edtUsuario_APP.Text  := EmptyStr;
////    edtUsuario_LI.Text   := EmptyStr;
////    edtDataBase_APP.Text := EmptyStr;
////    edtDataBase_Li.Text  := EmptyStr;
////    edtSenha_APP.Text    := EmptyStr;
////    edtSenha_LI.Text     := EmptyStr;
//  end;
end;

procedure TFormPrincipal.qryParametroBeforePost(DataSet: TDataSet);
begin
//  try
//    qryParametroSERVIDOR_APP.AsString := edtServidor.Text;
//    qryParametroSENHA_APP.Value       := Crypt('C', edtSenha_APP.Text);
//    qryParametroSENHA_LI.Value        := Crypt('C', edtSenha_LI.Text);
//    qryParametroDATABASE_APP.Value    := Crypt('C', edtDataBase_APP.Text);
//    qryParametroDATABASE_LI.Value     := Crypt('C', edtDataBase_Li.Text);
//    qryParametroUSUARIO_APP.Value     := Crypt('C', edtUsuario_APP.Text);
//    qryParametroUSUARIO_LI.Value      := Crypt('C', edtUsuario_LI.Text);
//  Except
////    qryParametroSERVIDOR_APP.AsString := EmptyStr;
////    qryParametroSENHA_APP.Value       := EmptyStr;
////    qryParametroSENHA_LI.Value        := EmptyStr;
////    qryParametroDATABASE_APP.Value    := EmptyStr;
////    qryParametroDATABASE_LI.Value     := EmptyStr;
////    qryParametroUSUARIO_APP.Value     := EmptyStr;
////    qryParametroUSUARIO_LI.Value      := EmptyStr;
//  end;
end;

procedure TFormPrincipal.RadioGroup1Click(Sender: TObject);
begin
  if RadioGroup1.ItemIndex = 1 then
    edtTerminal.Text := 'localhost';
end;

procedure TFormPrincipal.SpeedButton10Click(Sender: TObject);
begin
     try
    frmMesas := TfrmMesas.Create(Application);
    frmMesas.ShowModal;
  finally
    frmMesas.Release;
  end;
end;

procedure TFormPrincipal.SpeedButton1Click(Sender: TObject);
begin
     try
    frmContas := TfrmContas.Create(Application);
    frmContas.ShowModal;
  finally
    frmContas.Release;
  end;
end;

procedure TFormPrincipal.SpeedButton2Click(Sender: TObject);
begin
   try
    FrmUsuarios := TFrmUsuarios.Create(Application);
    FrmUsuarios.ShowModal;
  finally
    FrmUsuarios.Release;
  end;
end;

procedure TFormPrincipal.SpeedButton3Click(Sender: TObject);
begin
  try
    frmPermissoes := TfrmPermissoes.Create(Application);
    frmPermissoes.ShowModal;
  finally
    frmPermissoes.Release;
  end;
end;

procedure TFormPrincipal.SpeedButton4Click(Sender: TObject);
begin
   try
    frmConfig := TfrmConfig.Create(Application);
    frmConfig.ShowModal;
  finally
    frmConfig.Release;
  end;
end;

procedure TFormPrincipal.SpeedButton5Click(Sender: TObject);
begin
     try
    frmTerminais := TfrmTerminais.Create(Application);
    frmTerminais.ShowModal;
  finally
    frmTerminais.Release;
  end;
end;

procedure TFormPrincipal.SpeedButton6Click(Sender: TObject);
begin
 {  try
    frmContador := TfrmContador.Create(Application);
    frmContador.btnImp.Visible := False;
    frmContador.ShowModal;
  finally
    frmContador.Release;
  end;}
end;

procedure TFormPrincipal.SpeedButton7Click(Sender: TObject);
begin
   try
    frmIBPT := TfrmIBPT.Create(Application);
    frmIBPT.ShowModal;
  finally
    frmIBPT.Release;
  end;
end;

procedure TFormPrincipal.SpeedButton8Click(Sender: TObject);
begin
   try
    FrmICMS := TFrmICMS.Create(Application);
    FrmICMS.ShowModal;
  finally
    FrmICMS.Release;
  end;
end;

procedure TFormPrincipal.SpeedButton9Click(Sender: TObject);
begin
   try
    frmCFOP := TfrmCFOP.Create(Application);
    frmCFOP.ShowModal;
  finally
    frmCFOP.Release;
  end;
end;

procedure TFormPrincipal.DBComboBox9Change(Sender: TObject);
begin
  try

    if Estilo = 'Amethyst Kamri' then
      TStyleManager.TrySetStyle('Amethyst Kamri')
    else

      if Estilo = 'Aqua Light Slate' then
      TStyleManager.TrySetStyle('Aqua Light Slate')
    else

      if Estilo = 'Luna' then
      TStyleManager.TrySetStyle('Luna')
    else

      if Estilo = 'Cyan Dusk' then
      TStyleManager.TrySetStyle('Cyan Dusk')
    else

      if Estilo = 'Emerald Light Slate' then
      TStyleManager.TrySetStyle('Emerald Light Slate')
    else

      if Estilo = 'Iceberg Classico' then
      TStyleManager.TrySetStyle('Iceberg Classico')
    else

      if Estilo = 'Lavender Classico' then
      TStyleManager.TrySetStyle('Lavender Classico')
    else

      if Estilo = 'Light' then
      TStyleManager.TrySetStyle('Light')
    else

      if Estilo = 'Luna' then
      TStyleManager.TrySetStyle('Luna')
    else

      if Estilo = 'Sapphire Kamri' then
      TStyleManager.TrySetStyle('Sapphire Kamri')
    else

      if Estilo = 'Silver' then
      TStyleManager.TrySetStyle('Silver')
    else

      if Estilo = 'Glossy' then
      TStyleManager.TrySetStyle('Glossy')
    else

      if Estilo = 'Onyx Blue' then
      TStyleManager.TrySetStyle('Onyx Blue')
    else

     if Estilo = 'Windows 10 Dark' then
      TStyleManager.TrySetStyle('Windows 10 Dark')
    else

     if Estilo = 'Windows 10 Blue' then
      TStyleManager.TrySetStyle('Windows 10 Blue')
    else

      if Estilo = 'Sky' then
      TStyleManager.TrySetStyle('Sky')
    else

      if Estilo = 'Slate Classico' then
      TStyleManager.TrySetStyle('Slate Classico')
    else

      if Estilo = 'Smokey Quartz Kamri' then
      TStyleManager.TrySetStyle('Smokey Quartz Kamri')
    else
      TStyleManager.TrySetStyle('Sky');
  except
    TStyleManager.TrySetStyle('Sky');
  end;
end;

procedure TFormPrincipal.DBGrid1DblClick(Sender: TObject);
begin
  bbAlterarClick(self);
end;

procedure TFormPrincipal.DBGrid1DrawColumnCell(Sender: TObject;
  const Rect: TRect; DataCol: Integer; Column: TColumn; State: TGridDrawState);
begin
    if odd((Sender as TDBGrid).DataSource.DataSet.RecNo) then
    (Sender as TDBGrid).Canvas.Brush.Color := $00F1ECE7
  else
    (Sender as TDBGrid).Canvas.Brush.Color := $00FAF8F5;

  if (gdSelected in State) then
  begin
    (Sender as TDBGrid).Canvas.Font.Color := clWhite;
    (Sender as TDBGrid).Canvas.Brush.Color := clGray;
    DBGrid1.Canvas.Font.Style := [fsbold];
  end
  else
    DBGrid1.Canvas.Font.Style := [];

  (Sender as TDBGrid).DefaultDrawColumnCell(Rect, DataCol, Column, State);
end;

procedure TFormPrincipal.DBGrid1KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
    if Key = vk_f2 then
  begin
    bbNovoClick(self);
    abort;
  end;
  if Key = vk_f3 then
  begin
    bbAlterarClick(self);
    abort;
  end;
end;

end.
