program Softhouse;

uses
  Vcl.Forms,
  Softhouse.View.Principal in 'Softhouse.View.Principal.pas' {FormPrincipal},
  Vcl.Themes,
  Vcl.Styles,
  Udados in '..\Model\Udados.pas' {Dados: TDataModule},
  uDadosWeb in '..\Model\uDadosWeb.pas' {DadosWeb: TDataModule},
  Serial in '..\View\Serial.pas',
  uConexaoBD in '..\View\uConexaoBD.pas' {frmConexaoBD},
  uSplash in '..\View\uSplash.pas' {frmSplash},
  uAcessoConfig in '..\View\uAcessoConfig.pas' {frmAcessoconfig},
  uChave in '..\View\uChave.pas' {frmChave},
  uEmpresa in '..\View\uEmpresa.pas' {frmEmpresa},
  uRotinasComuns in '..\View\uRotinasComuns.pas' {DMRotinas: TDataModule},
  UUsuarios in '..\View\UUsuarios.pas' {FrmUsuarios},
  uPermissoes in '..\View\uPermissoes.pas' {frmPermissoes},
  uConfig in '..\View\uConfig.pas' {frmConfig},
  frExibeMensagem in '..\View\frExibeMensagem.pas' {FormExibeMensagem},
  uTerminais in '..\View\uTerminais.pas' {frmTerminais},
  uIBPT in '..\View\uIBPT.pas' {frmIBPT},
  uICMS in '..\View\uICMS.pas' {FrmICMS},
  UCFOP in '..\View\UCFOP.pas' {frmCFOP},
  uContas in '..\View\uContas.pas' {frmContas},
  uSupervisor in '..\View\uSupervisor.pas' {FrmSupervisor},
  uMesas in '..\View\uMesas.pas' {frmMesas},
  U_Backup in '..\View\U_Backup.pas' {frmBackup};

{$R *.res}

begin
  Application.Initialize;
  Application.MainFormOnTaskbar := True;
  TStyleManager.TrySetStyle('Smokey Quartz Kamri');
  Application.CreateForm(TFormPrincipal, FormPrincipal);
  Application.CreateForm(TDados, Dados);
  Application.CreateForm(TDadosWeb, DadosWeb);
  Application.CreateForm(TfrmEmpresa, frmEmpresa);
  Application.CreateForm(TDMRotinas, DMRotinas);
  Application.CreateForm(TFrmUsuarios, FrmUsuarios);
  Application.CreateForm(TfrmPermissoes, frmPermissoes);
  Application.CreateForm(TfrmConfig, frmConfig);
  Application.CreateForm(TfrmTerminais, frmTerminais);
  Application.CreateForm(TfrmIBPT, frmIBPT);
  Application.CreateForm(TFrmICMS, FrmICMS);
  Application.CreateForm(TfrmCFOP, frmCFOP);
  Application.CreateForm(TfrmContas, frmContas);
  Application.CreateForm(TFrmSupervisor, FrmSupervisor);
  Application.CreateForm(TfrmMesas, frmMesas);
  Application.CreateForm(TfrmBackup, frmBackup);
  Application.Run;
end.
