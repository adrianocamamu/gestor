object Principal_FV: TPrincipal_FV
  Left = 0
  Top = 0
  BorderIcons = [biSystemMenu, biMinimize]
  Caption = 'PrincipalFV'
  ClientHeight = 125
  ClientWidth = 384
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Label27: TLabel
    Left = 191
    Top = 8
    Width = 177
    Height = 13
    Caption = 'Ultima Sinconiza'#231#227'o de Pedidos'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = [fsBold, fsUnderline]
    ParentFont = False
  end
  object DBText27: TDBText
    Left = 207
    Top = 95
    Width = 145
    Height = 17
    Alignment = taCenter
    DataField = 'SINCRO_BD'
    DataSource = dsSincro_FV
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Label28: TLabel
    Left = 191
    Top = 69
    Width = 179
    Height = 13
    Caption = 'Ultima Sincroniza'#231#227'o dos Dados'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = [fsBold, fsUnderline]
    ParentFont = False
  end
  object DBText20: TDBText
    Left = 207
    Top = 32
    Width = 145
    Height = 17
    Alignment = taCenter
    DataField = 'SINCRO_PEDIDO'
    DataSource = dsSincro_FV
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Label1: TLabel
    Left = 8
    Top = 57
    Width = 116
    Height = 13
    Caption = 'Intervalo Sincroniza'#231#227'o:'
  end
  object BtnSicronizar: TButton
    Left = 98
    Top = 91
    Width = 79
    Height = 25
    Caption = 'Sincronizar BD'
    TabOrder = 0
    OnClick = BtnSicronizarClick
  end
  object BtnPedidoWeb: TButton
    Left = 8
    Top = 91
    Width = 87
    Height = 25
    Caption = 'Pedido Externo'
    TabOrder = 1
    OnClick = BtnPedidoWebClick
  end
  object DBRadioGroup1: TDBRadioGroup
    Left = 8
    Top = 8
    Width = 169
    Height = 40
    Caption = 'Sicroniza'#231#227'o do For'#231'a de Ativa:'
    Columns = 2
    DataField = 'SINCRONIZA_FV'
    DataSource = dsConfigAcesso
    Items.Strings = (
      'Sim'
      'N'#227'o')
    TabOrder = 2
    Values.Strings = (
      'S'
      'N')
  end
  object DBEdit1: TDBEdit
    Left = 136
    Top = 54
    Width = 41
    Height = 21
    DataField = 'INTERVALO'
    DataSource = dsSincro_FV
    TabOrder = 3
  end
  object qrySincro_FV: TFDQuery
    Connection = Dados.Conexao
    SQL.Strings = (
      'select * from sincro_fv')
    Left = 353
    Top = 5
    object qrySincro_FVINTERVALO: TIntegerField
      FieldName = 'INTERVALO'
      Origin = 'INTERVALO'
    end
    object qrySincro_FVSINCRO_PEDIDO: TSQLTimeStampField
      FieldName = 'SINCRO_PEDIDO'
      Origin = 'SINCRO_PEDIDO'
    end
    object qrySincro_FVSINCRO_BD: TSQLTimeStampField
      FieldName = 'SINCRO_BD'
      Origin = 'SINCRO_BD'
    end
  end
  object dsSincro_FV: TDataSource
    DataSet = qrySincro_FV
    Left = 225
    Top = 5
  end
  object dsConfigAcesso: TDataSource
    DataSet = qryConfigAcesso
    Left = 185
    Top = 5
  end
  object qryConfigAcesso: TFDQuery
    Connection = Dados.Conexao
    SQL.Strings = (
      'select * from CONFIG_ACESSOS')
    Left = 305
    Top = 5
    object qryConfigAcessoCODIGO: TIntegerField
      FieldName = 'CODIGO'
      Origin = 'CODIGO'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
    end
    object qryConfigAcessoBLOQ_CAD_EMPRESA: TStringField
      FieldName = 'BLOQ_CAD_EMPRESA'
      Origin = 'BLOQ_CAD_EMPRESA'
      Size = 1
    end
    object qryConfigAcessoBLOQ_CAD_USUARIO: TStringField
      FieldName = 'BLOQ_CAD_USUARIO'
      Origin = 'BLOQ_CAD_USUARIO'
      Size = 1
    end
    object qryConfigAcessoBLOQ_PERMISSOES: TStringField
      FieldName = 'BLOQ_PERMISSOES'
      Origin = 'BLOQ_PERMISSOES'
      Size = 1
    end
    object qryConfigAcessoBLOQ_CONFIG: TStringField
      FieldName = 'BLOQ_CONFIG'
      Origin = 'BLOQ_CONFIG'
      Size = 1
    end
    object qryConfigAcessoBLOQ_CONFIG_TERM: TStringField
      FieldName = 'BLOQ_CONFIG_TERM'
      Origin = 'BLOQ_CONFIG_TERM'
      Size = 1
    end
    object qryConfigAcessoBLOQ_CAD_CONTADOR: TStringField
      FieldName = 'BLOQ_CAD_CONTADOR'
      Origin = 'BLOQ_CAD_CONTADOR'
      Size = 1
    end
    object qryConfigAcessoBLOQ_IBPT: TStringField
      FieldName = 'BLOQ_IBPT'
      Origin = 'BLOQ_IBPT'
      Size = 1
    end
    object qryConfigAcessoBLOQ_ICMS: TStringField
      FieldName = 'BLOQ_ICMS'
      Origin = 'BLOQ_ICMS'
      Size = 1
    end
    object qryConfigAcessoBLOQ_CFOP: TStringField
      FieldName = 'BLOQ_CFOP'
      Origin = 'BLOQ_CFOP'
      Size = 1
    end
    object qryConfigAcessoBLOQ_CAD_CAIXAS: TStringField
      FieldName = 'BLOQ_CAD_CAIXAS'
      Origin = 'BLOQ_CAD_CAIXAS'
      Size = 1
    end
    object qryConfigAcessoBLOQ_CAD_MESAS: TStringField
      FieldName = 'BLOQ_CAD_MESAS'
      Origin = 'BLOQ_CAD_MESAS'
      Size = 1
    end
    object qryConfigAcessoRETAGUARDA_BLOQUEADO: TStringField
      FieldName = 'RETAGUARDA_BLOQUEADO'
      Origin = 'RETAGUARDA_BLOQUEADO'
      Size = 1
    end
    object qryConfigAcessoPDV_BLOQUEADO: TStringField
      FieldName = 'PDV_BLOQUEADO'
      Origin = 'PDV_BLOQUEADO'
      Size = 1
    end
    object qryConfigAcessoPREVENDA_BLOQUEADO: TStringField
      FieldName = 'PREVENDA_BLOQUEADO'
      Origin = 'PREVENDA_BLOQUEADO'
      Size = 1
    end
    object qryConfigAcessoZAP_BLOQUEADO: TStringField
      FieldName = 'ZAP_BLOQUEADO'
      Origin = 'ZAP_BLOQUEADO'
      Size = 1
    end
    object qryConfigAcessoBLOQ_NFE: TStringField
      FieldName = 'BLOQ_NFE'
      Origin = 'BLOQ_NFE'
      Size = 1
    end
    object qryConfigAcessoBLOQ_CTE: TStringField
      FieldName = 'BLOQ_CTE'
      Origin = 'BLOQ_CTE'
      Size = 1
    end
    object qryConfigAcessoBLOQ_MDFE: TStringField
      FieldName = 'BLOQ_MDFE'
      Origin = 'BLOQ_MDFE'
      Size = 1
    end
    object qryConfigAcessoBLOQ_ORCAMENTO: TStringField
      FieldName = 'BLOQ_ORCAMENTO'
      Origin = 'BLOQ_ORCAMENTO'
      Size = 1
    end
    object qryConfigAcessoPRODUTO_BLOQ_IMPOSTOS: TStringField
      FieldName = 'PRODUTO_BLOQ_IMPOSTOS'
      Origin = 'PRODUTO_BLOQ_IMPOSTOS'
      Size = 1
    end
    object qryConfigAcessoCLIENTE_BLOQ_ADICIONAIS: TStringField
      FieldName = 'CLIENTE_BLOQ_ADICIONAIS'
      Origin = 'CLIENTE_BLOQ_ADICIONAIS'
      Size = 1
    end
    object qryConfigAcessoCLIENTE_BLOQ_FOTO: TStringField
      FieldName = 'CLIENTE_BLOQ_FOTO'
      Origin = 'CLIENTE_BLOQ_FOTO'
      Size = 1
    end
    object qryConfigAcessoCLIENTE_BLOQ_CONTATOS: TStringField
      FieldName = 'CLIENTE_BLOQ_CONTATOS'
      Origin = 'CLIENTE_BLOQ_CONTATOS'
      Size = 1
    end
    object qryConfigAcessoMENU_BLOQ_BOLETO: TStringField
      FieldName = 'MENU_BLOQ_BOLETO'
      Origin = 'MENU_BLOQ_BOLETO'
      Size = 1
    end
    object qryConfigAcessoMENU_BLOQ_FISCAL: TStringField
      FieldName = 'MENU_BLOQ_FISCAL'
      Origin = 'MENU_BLOQ_FISCAL'
      Size = 1
    end
    object qryConfigAcessoMENU_BLOQ_TRANSP: TStringField
      FieldName = 'MENU_BLOQ_TRANSP'
      Origin = 'MENU_BLOQ_TRANSP'
      Size = 1
    end
    object qryConfigAcessoMENU_BLOQ_OS: TStringField
      FieldName = 'MENU_BLOQ_OS'
      Origin = 'MENU_BLOQ_OS'
      Size = 1
    end
    object qryConfigAcessoMENU_BLOQ_REL_FISCAL: TStringField
      FieldName = 'MENU_BLOQ_REL_FISCAL'
      Origin = 'MENU_BLOQ_REL_FISCAL'
      Size = 1
    end
    object qryConfigAcessoBLOQ_SCRIPT: TStringField
      FieldName = 'BLOQ_SCRIPT'
      Origin = 'BLOQ_SCRIPT'
      Size = 1
    end
    object qryConfigAcessoCHEC_MODULOS: TStringField
      FieldName = 'CHEC_MODULOS'
      Origin = 'CHEC_MODULOS'
      Size = 1
    end
    object qryConfigAcessoSINCRONIZA_FV: TStringField
      FieldName = 'SINCRONIZA_FV'
      Origin = 'SINCRONIZA_FV'
      Size = 1
    end
  end
  object TimerFV: TTimer
    Enabled = False
    OnTimer = TimerFVTimer
    Left = 345
    Top = 48
  end
  object ApplicationEvents1: TApplicationEvents
    OnMinimize = ApplicationEvents1Minimize
    Left = 185
    Top = 53
  end
  object ctiPrincipal: TTrayIcon
    Hint = 'Servidor Mobile | Restaurante'
    PopupMenu = pmMenu
    Left = 225
    Top = 53
  end
  object pmMenu: TPopupMenu
    Left = 297
    Top = 53
    object RestaurarAplicao1: TMenuItem
      Caption = 'Exibir Aplica'#231#227'o'
      OnClick = RestaurarAplicao1Click
    end
    object SairdaAplicao1: TMenuItem
      Caption = 'Sair da Aplica'#231#227'o'
      OnClick = SairdaAplicao1Click
    end
  end
end
