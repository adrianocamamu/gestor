unit uProducao;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants,
  System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Data.DB, Vcl.Grids, Vcl.DBGrids,
  Vcl.StdCtrls, Vcl.ExtCtrls, Vcl.Buttons, FireDAC.Stan.Intf,
  FireDAC.Stan.Option, FireDAC.Stan.Param, FireDAC.Stan.Error, FireDAC.DatS,
  FireDAC.Phys.Intf, FireDAC.DApt.Intf, FireDAC.Stan.Async, FireDAC.DApt,
  FireDAC.Comp.DataSet, FireDAC.Comp.Client, Vcl.ComCtrls, Vcl.DBCtrls,
  Vcl.Mask,
  Vcl.Tabs, Vcl.ExtDlgs, JPeg, frxClass, frxDBSet, frxExportPDF,
  frxExportBaseDialog, frxExportXLS, System.Actions, Vcl.ActnList;

type
  TfrmProducao = class(TForm)
    dsProducao: TDataSource;
    Panel2: TPanel;
    Panel3: TPanel;
    btnProducaoParcial: TSpeedButton;
    btnProducaoTotal: TSpeedButton;
    frxReport: TfrxReport;
    dsEmpresa: TDataSource;
    Panel4: TPanel;
    GroupBox1: TGroupBox;
    edtLoc: TEdit;
    PageControl1: TPageControl;
    ActionList1: TActionList;
    actIDX: TAction;
    btnBuscar: TSpeedButton;
    qryProducao: TFDQuery;
    Panel1: TPanel;
    btnFinalizar: TSpeedButton;
    frxDBProducaoItens: TfrxDBDataset;
    frxDBEmpresa: TfrxDBDataset;
    frxDBCliente: TfrxDBDataset;
    frxDBProducao: TfrxDBDataset;
    frxDBVendedor: TfrxDBDataset;
    qryVendedor: TFDQuery;
    qryCliente: TFDQuery;
    btnImpProd: TSpeedButton;
    qryQtdConcluido: TFDQuery;
    TabSheet2: TTabSheet;
    DBGrid2: TDBGrid;
    qryPV: TFDQuery;
    qryPVCODIGO: TIntegerField;
    qryPVDATA_EMISSAO: TDateField;
    qryPVDATA_SAIDA: TDateField;
    qryPVID_CLIENTE: TIntegerField;
    qryPVFK_USUARIO: TIntegerField;
    qryPVFK_CAIXA: TIntegerField;
    qryPVFK_VENDEDOR: TIntegerField;
    qryPVTIPO_DESCONTO: TStringField;
    qryPVTIPO: TStringField;
    qryPVNECF: TIntegerField;
    qryPVFKORCAMENTO: TIntegerField;
    qryPVFKEMPRESA: TIntegerField;
    qryPVOBSERVACOES: TMemoField;
    qryPVCPF_NOTA: TStringField;
    qryPVSITUACAO: TStringField;
    qryPVVIRTUAL_SITUACAO: TStringField;
    qryPVSUBTOTAL: TFMTBCDField;
    qryPVDESCONTO: TFMTBCDField;
    qryPVTROCO: TFMTBCDField;
    qryPVDINHEIRO: TFMTBCDField;
    qryPVTOTAL: TFMTBCDField;
    qryPVPERCENTUAL: TFMTBCDField;
    qryPVLOTE: TIntegerField;
    qryPVGERA_FINANCEIRO: TStringField;
    qryPVPERCENTUAL_ACRESCIMO: TFMTBCDField;
    qryPVACRESCIMO: TFMTBCDField;
    qryPVFK_TABELA: TIntegerField;
    qryPVPEDIDO: TStringField;
    qryPVFORMA_PAGAMENTO: TStringField;
    qryPVFLAG_NFCE: TStringField;
    qryPVVIRTUAL_TIPO: TStringField;
    qryPVVENDEDOR: TStringField;
    qryPVVIRTUAL_VENDEDOR: TStringField;
    qryPVNOME: TStringField;
    qryPVRAZAO: TStringField;
    qryPVVIRTUAL_CLIENTE: TStringField;
    qryPVMOTIVO: TStringField;
    qryPVTTOTAL: TAggregateField;
    dsPedido: TDataSource;
    btnNovaProducao: TSpeedButton;
    dsItens: TDataSource;
    qryPV_Itens: TFDQuery;
    qryPV_ItensCODIGO: TIntegerField;
    qryPV_ItensFKVENDA: TIntegerField;
    qryPV_ItensID_PRODUTO: TIntegerField;
    qryPV_ItensITEM: TSmallintField;
    qryPV_ItensSITUACAO: TStringField;
    qryPV_ItensQTD: TFMTBCDField;
    qryPV_ItensID_MODELO: TIntegerField;
    DBGrid1: TDBGrid;
    Panel5: TPanel;
    DBGrid4: TDBGrid;
    qryPV_ItensLARGURA: TFMTBCDField;
    qryPV_ItensCOMPRIMENTO: TFMTBCDField;
    qryPV_ItensDESCRICAO_SL: TStringField;
    qryProducaoPR_CODIGO: TIntegerField;
    qryProducaoFIL_CODIGO: TIntegerField;
    qryProducaoVEN_CODIGO: TIntegerField;
    qryProducaoFK_CLIENTE: TIntegerField;
    qryProducaoPR_DATA: TDateField;
    qryProducaoPR_HORA: TTimeField;
    qryProducaoPR_STATUS: TStringField;
    qryProducaoUSR_USUARIO: TStringField;
    qryProducaoFK_VENDEDOR: TIntegerField;
    qryProducaoRAZAO: TStringField;
    qryPV_ItensFABRICADO: TStringField;
    qryPVSTATUS_PRODUCAO: TStringField;
    qryProducaoItens: TFDQuery;
    dsProducaoItems: TDataSource;
    dbgProducaoItens: TDBGrid;
    qryProducaoItensPRI_CODIGO: TIntegerField;
    qryProducaoItensPRO_CODIGO: TStringField;
    qryProducaoItensPRI_QTD_PRODUCAO: TFloatField;
    qryProducaoItensPRI_QTD_FALTA: TFloatField;
    qryProducaoItensPRI_QTD_CONCLUIDO: TFloatField;
    qryProducaoItensDESCRICAO: TStringField;
    qryProducaoItensPR_CODIGO: TIntegerField;
    qryProducaoItensPRI_ITEM_VENDA: TIntegerField;
    qryQtdVendido: TFDQuery;
    qryQtdConcluidoCONCLUIDO: TFloatField;
    qryQtdVendidoVENDIDO: TFMTBCDField;
    procedure edtLocChange(Sender: TObject);
    procedure DBGrid1DrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumn; State: TGridDrawState);
    procedure cbTemploChange(Sender: TObject);
    procedure edtLocKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure DBGrid1TitleClick(Column: TColumn);
    procedure edtLocKeyPress(Sender: TObject; var Key: Char);
    procedure FormShow(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure TabSet2Click(Sender: TObject);
    procedure DBGrid2DrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumn; State: TGridDrawState);
    procedure DBGrid2TitleClick(Column: TColumn);
    procedure actIDXExecute(Sender: TObject);
    procedure btnBuscarClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure DBGrid1CellClick(Column: TColumn);
    procedure btnProducaoTotalClick(Sender: TObject);
    procedure btnFinalizarClick(Sender: TObject);
    procedure dbgProducaoItensDrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumn; State: TGridDrawState);
    procedure btnProducaoParcialClick(Sender: TObject);
    procedure btnImpOEClick(Sender: TObject);
    procedure btnImpReciboClick(Sender: TObject);
    procedure btnProducaoClick(Sender: TObject);
    procedure btnNovaProducaoClick(Sender: TObject);
    procedure btnImpProdClick(Sender: TObject);
    procedure DBGrid4DrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumn; State: TGridDrawState);
    procedure DBGrid2CellClick(Column: TColumn);

    

  private
    filtro, filtro1, filtro2: string;
    parte, ordem: String;
    tipoBaixa : Integer;
    procedure localiza;
    procedure tamanho;
    procedure LocalizaProduto;

    { Private declarations }
  public
    idx: Integer;
    vOrdem: string[5];
    vSql, vSqlConsulta: String;
    { Public declarations }
  end;

var
  frmProducao: TfrmProducao;

implementation

{$R *.dfm}

uses Udados, uCadProduto, uSupervisor, uEtiquetas;

procedure TfrmProducao.actIDXExecute(Sender: TObject);
var
i: Integer;
begin
  idx := idx + 1;
  GroupBox1.Caption := 'F6 | Localizar <<' + DBGrid2.Columns[idx]
    .Title.Caption + '>>';



  for i := 0 to DBGrid2.Columns.Count - 1 do
    DBGrid2.Columns[i].Title.Font.Style := [];

  DBGrid2.Columns[idx].Title.Font.Style := [fsbold];

   DBGrid2.Columns[idx].Title.Caption := '>>' + DBGrid2.Columns[idx]
    .Title.Caption;

  edtLoc.SetFocus;

  localiza;

end;

procedure TfrmProducao.btnProducaoParcialClick(Sender: TObject);
var
  i : Integer;
begin
    btnProducaoParcial.Enabled := false;
    btnProducaoTotal.Enabled := false;

   if application.messagebox('Confirma o envio para produ��o?', 'Confirma��o', mb_yesno) = mrYes then
    begin
     qryProducaoItens.First;
     while not qryProducaoItens.Eof do
      begin
        if dbgProducaoItens.Fields[4].IsNull then
        begin
          if not(qryProducaoItens.State in dsEditModes) then
              qryProducaoItens.Edit;
          dbgProducaoItens.Fields[4].Value := 0;

          Dados.Conexao.CommitRetaining;
        end;

       if dbgProducaoItens.Fields[4].Value >= 0 then
        begin

             if dbgProducaoItens.Fields[4].Value <= dbgProducaoItens.Fields[3].Value then
             begin
               if not(qryProducaoItens.State in dsEditModes) then
              qryProducaoItens.Edit;

               dbgProducaoItens.Fields[5].Value := dbgProducaoItens.Fields[3].Value - dbgProducaoItens.Fields[4].Value;

            end
            else
            begin

              ShowMessage('Quantidade n�o pode ser maior que a quantidade A PRODUZIR');
              if not(qryProducaoItens.State in dsEditModes) then
              qryProducaoItens.Edit;
              qryProducaoItensPRI_QTD_CONCLUIDO.Value := 0;

              exit;
            end;
          end
        else
        begin
              ShowMessage('Quantidade a PRODUZIR n�o pode ser menor que 0');
              if not(qryProducaoItens.State in dsEditModes) then
                qryProducaoItens.Edit;
              qryProducaoItensPRI_QTD_CONCLUIDO.Value := 0;
              exit;
        end;

               Dados.qryExecute.SQL.Clear;
               Dados.qryExecute.SQL.Add('UPDATE PRODUCAO_ITENS SET ');
               Dados.qryExecute.SQL.Add('PRI_QTD_CONCLUIDO = :CONCLUIDO,');
               Dados.qryExecute.SQL.Add('PRI_QTD_FALTA = :FALTA ');
               Dados.qryExecute.SQL.Add('WHERE');
               Dados.qryExecute.SQL.Add('PR_CODIGO = :CODIGO AND ');
               Dados.qryExecute.SQL.Add('PRI_ITEM_VENDA = :ITEM');

               Dados.qryExecute.ParamByName('CONCLUIDO').Value := dbgProducaoItens.Fields[4].Value;
               Dados.qryExecute.ParamByName('FALTA').Value := dbgProducaoItens.Fields[5].Value;
               Dados.qryExecute.ParamByName('CODIGO').Value := qryProducaoItensPR_CODIGO.Value;
               Dados.qryExecute.ParamByName('ITEM').Value := qryProducaoItensPRI_ITEM_VENDA.Value;

               Dados.qryExecute.ExecSQL;
               Dados.Conexao.CommitRetaining;

      qryProducaoItens.Next;
      end;

      btnImpProd.Enabled := true;
      btnFinalizar.Enabled := true;

      if not(qryPV.State in dsEditModes) then
      qryPV.Edit;
      qryPVSTATUS_PRODUCAO.Value := 'EM PRODUCAO';
      qryPV.Refresh;

      if not(qryProducao.State in dsEditModes) then
      qryProducao.Edit;
      qryProducaoPR_STATUS.Value := 'EM PRODUCAO';
      qryProducao.Refresh;


    end
    else
    begin
      btnProducaoParcial.Enabled := true;
      btnProducaoTotal.Enabled   := true;
    end;


end;

procedure TfrmProducao.btnProducaoTotalClick(Sender: TObject);
begin

    btnProducaoParcial.Enabled     := false;
    btnProducaoTotal.Enabled := false;

   if application.messagebox('Confirma o envio de todos os itens para produ��o?', 'Confirma��o', mb_yesno) = mrYes then
    begin
     qryProducaoItens.First;
     while not qryProducaoItens.Eof do
      begin

               if not(qryProducaoItens.State in dsEditModes) then
              qryProducaoItens.Edit;

               dbgProducaoItens.Fields[4].Value := dbgProducaoItens.Fields[3].Value;
               dbgProducaoItens.Fields[5].Value := 0;


               Dados.qryExecute.SQL.Clear;
               Dados.qryExecute.SQL.Add('UPDATE PRODUCAO_ITENS SET ');
               Dados.qryExecute.SQL.Add('PRI_QTD_CONCLUIDO = :CONCLUIDO,');
               Dados.qryExecute.SQL.Add('PRI_QTD_FALTA = :FALTA ');
               Dados.qryExecute.SQL.Add('WHERE');
               Dados.qryExecute.SQL.Add('PR_CODIGO = :CODIGO AND ');
               Dados.qryExecute.SQL.Add('PRI_ITEM_VENDA = :ITEM');

               Dados.qryExecute.ParamByName('CONCLUIDO').Value := dbgProducaoItens.Fields[4].Value;
               Dados.qryExecute.ParamByName('FALTA').Value := dbgProducaoItens.Fields[5].Value;
               Dados.qryExecute.ParamByName('CODIGO').Value := qryProducaoItensPR_CODIGO.Value;
               Dados.qryExecute.ParamByName('ITEM').Value := qryProducaoItensPRI_ITEM_VENDA.Value;

               Dados.qryExecute.ExecSQL;
               Dados.Conexao.CommitRetaining;

      qryProducaoItens.Next;
      end;

      btnImpProd.Enabled := true;
      btnFinalizar.Enabled := true;

      if not(qryPV.State in dsEditModes) then
      qryPV.Edit;
      qryPVSTATUS_PRODUCAO.Value := 'EM PRODUCAO';
      qryPV.Refresh;

      if not(qryProducao.State in dsEditModes) then
      qryProducao.Edit;
      qryProducaoPR_STATUS.Value := 'EM PRODUCAO';
      qryProducao.Refresh;


    end
    else
    begin
      btnProducaoParcial.Enabled := true;
      btnProducaoParcial.Enabled := true;
    end;


end;

procedure TfrmProducao.btnBuscarClick(Sender: TObject);
begin
  localiza;
end;

procedure TfrmProducao.btnFinalizarClick(Sender: TObject);
var
   i : Integer;
begin
 try
    btnFinalizar.Enabled  := false;

    if application.messagebox('Deseja finalizar essa produ��o?', 'Confirma��o', mb_yesno) = mrYes then
  begin

    if not(qryProducao.State in dsEditModes) then
    qryProducao.Edit;
    qryProducaoPR_STATUS.Value := 'FINALIZADO';
    qryProducao.Refresh;

    qryPRODUCAO.First;
    while not qryPRODUCAO.Eof do
    begin
     if (qryPRODUCAO.FieldByName('PR_STATUS').Value = 'EM PRODUCAO') OR (qryPRODUCAO.FieldByName('PR_STATUS').Value = 'AGUARDANDO') then
      begin
        i := i + 1;
      end;
    qryPRODUCAO.Next;
    end;

    if i = 0 then
     begin

      if not(qryPV.State in dsEditModes) then
      qryPV.Edit;
      qryPVSTATUS_PRODUCAO.Value := 'FINALIZADO';
      qryPV.Refresh;
     end;

    btnFinalizar.Enabled  := false;
  end
  else
  begin
    btnFinalizar.Enabled  := true;
  end;

  finally

 end;

end;

procedure TfrmProducao.cbTemploChange(Sender: TObject);
begin
  localiza;
end;

procedure TfrmProducao.dbgProducaoItensDrawColumnCell(Sender: TObject;
  const Rect: TRect; DataCol: Integer; Column: TColumn; State: TGridDrawState);
begin
   if odd((Sender as TDBGrid).DataSource.DataSet.RecNo) then
    (Sender as TDBGrid).Canvas.Brush.Color := $00F1ECE7
  else
    (Sender as TDBGrid).Canvas.Brush.Color := clWhite;

  if (gdSelected in State) then
  begin
    (Sender as TDBGrid).Canvas.Font.Color := clBlack;
    (Sender as TDBGrid).Canvas.Brush.Color := clSilver;
    dbgProducaoItens.Canvas.Font.Style := [fsbold];
  end
  else
    dbgProducaoItens.Canvas.Font.Style := [];

  (Sender as TDBGrid).DefaultDrawColumnCell(Rect, DataCol, Column, State);
end;

procedure TfrmProducao.DBGrid1CellClick(Column: TColumn);
begin

   qryProducaoItens.Close;
   qryProducaoItens.Params[0].AsInteger := DBGrid1.Fields[0].Value;
   qryProducaoItens.Open;

   if not(qryProducaoItens.State in dsEditModes) then
    qryProducaoItens.Edit;

    if (qryproducaoPR_STATUS.Value = 'EM PRODUCAO') then
     begin
      dbgProducaoItens.Enabled       := false;
      btnProducaoTotal.Enabled       := false;
      btnProducaoParcial.Enabled     := false;
      btnFinalizar.Enabled           := true;
      btnImpProd.Enabled             := true;
     end
    else if (qryproducaoPR_STATUS.Value = 'AGUARDANDO') then
     begin
      dbgProducaoItens.Enabled       := true;
      btnProducaoTotal.Enabled       := true;
      btnProducaoParcial.Enabled     := true;
      btnFinalizar.Enabled           := false;
      btnImpProd.Enabled             := false;
     end
    else
     begin
      dbgProducaoItens.Enabled       := false;
      btnProducaoTotal.Enabled       := false;
      btnProducaoParcial.Enabled     := false;
      btnFinalizar.Enabled           := false;
      btnImpProd.Enabled             := true;
     end;

end;

procedure TfrmProducao.DBGrid1DrawColumnCell(Sender: TObject; const Rect: TRect;
  DataCol: Integer; Column: TColumn; State: TGridDrawState);
begin
  if qryProducaoPR_STATUS.AsString= 'AGUARDANDO' then
    TDBGrid(Sender).Canvas.Font.Color:=clRed
  else if qryProducaoPR_STATUS.AsString= 'EM PRODUCAO' then
    TDBGrid(Sender).Canvas.Font.Color:=clBlue
  else if qryProducaoPR_STATUS.AsString= 'FINALIZADO' then
    TDBGrid(Sender).Canvas.Font.Color:=clGreen
        else TDBGrid(Sender).Canvas.Font.Color:=clBlack;

  if odd((Sender as TDBGrid).DataSource.DataSet.RecNo) then
    (Sender as TDBGrid).Canvas.Brush.Color := $00F1ECE7
  else
    (Sender as TDBGrid).Canvas.Brush.Color := clWhite;

  if (gdSelected in State) then
  begin
    (Sender as TDBGrid).Canvas.Font.Color := clBlack;
    (Sender as TDBGrid).Canvas.Brush.Color := clSilver;
    DBGrid2.Canvas.Font.Style := [fsbold];
  end
  else
    DBGrid2.Canvas.Font.Style := [];

  (Sender as TDBGrid).DefaultDrawColumnCell(Rect, DataCol, Column, State);
end;

procedure TfrmProducao.DBGrid1TitleClick(Column: TColumn);
var
  i: Integer;
begin

  edtLoc.Clear;

  if idx = Column.Index then
  begin
    if vOrdem = ' ASC' then
      vOrdem := ' DESC'
    else
      vOrdem := ' ASC';
  end
  else
    vOrdem := ' ASC';

  idx := Column.Index;

  DBGrid2.Columns[0].Title.Caption := 'VENDA';
  DBGrid2.Columns[1].Title.Caption := 'DATA';
  DBGrid2.Columns[2].Title.Caption := 'RAZ�O';
  DBGrid2.Columns[3].Title.Caption := 'TOTAL';
  DBGrid2.Columns[4].Title.Caption := 'STATUS';


  GroupBox1.Caption := 'F6 | Localizar <<' + DBGrid2.Columns[idx]
    .Title.Caption + '>>';

  DBGrid2.Columns[idx].Title.Caption := '>>' + DBGrid2.Columns[idx]
    .Title.Caption;

  for i := 0 to DBGrid2.Columns.Count - 1 do
    DBGrid2.Columns[i].Title.Font.Style := [];

  DBGrid2.Columns[idx].Title.Font.Style := [fsbold];

  edtLoc.SetFocus;

  localiza;

end;

procedure TfrmProducao.DBGrid2CellClick(Column: TColumn);
begin
    qryPV_Itens.Close;
    qryPV_Itens.Params[0].AsInteger := DBGrid2.Fields[0].Value;
    qryPV_Itens.Open;

    qryProducao.Close;
    qryProducao.Params[0].AsInteger := DBGrid2.Fields[0].Value;
    qryProducao.Open;

    qryProducaoItens.Close;

     if DBGrid2.Fields[4].Value = 'FINALIZADA' then
      begin
        btnNovaProducao.Enabled := false;
      end
     else
      begin
        btnNovaProducao.Enabled := true;
      end;

end;

procedure TfrmProducao.DBGrid2DrawColumnCell(Sender: TObject; const Rect: TRect;
  DataCol: Integer; Column: TColumn; State: TGridDrawState);
begin
  if odd((Sender as TDBGrid).DataSource.DataSet.RecNo) then
    (Sender as TDBGrid).Canvas.Brush.Color := $00F1ECE7
  else
    (Sender as TDBGrid).Canvas.Brush.Color := clWhite;

  if (gdSelected in State) then
  begin
    (Sender as TDBGrid).Canvas.Font.Color := clBlack;
    (Sender as TDBGrid).Canvas.Brush.Color := clSilver;
    DBGrid2.Canvas.Font.Style := [fsbold];
  end
  else
    DBGrid2.Canvas.Font.Style := [];

  (Sender as TDBGrid).DefaultDrawColumnCell(Rect, DataCol, Column, State);
end;

procedure TfrmProducao.DBGrid2TitleClick(Column: TColumn);
var
  i: Integer;
begin

  edtLoc.Clear;

  if idx = Column.Index then
  begin
    if vOrdem = ' ASC' then
      vOrdem := ' DESC'
    else
      vOrdem := ' ASC';
  end
  else
    vOrdem := ' ASC';

  idx := Column.Index;

  if idx <= 1 then
  begin
    //DBGrid1.Columns[0].Title.Caption := 'Produto';


    GroupBox1.Caption := 'F6 | Localizar <<' + DBGrid2.Columns[idx]
      .Title.Caption + '>>';

    DBGrid2.Columns[idx].Title.Caption := '>>' + DBGrid2.Columns[idx]
      .Title.Caption;

    for i := 0 to DBGrid2.Columns.Count - 1 do
      DBGrid2.Columns[i].Title.Font.Style := [];

    DBGrid2.Columns[idx].Title.Font.Style := [fsbold];

    edtLoc.SetFocus;

    localiza;
  end;
end;






procedure TfrmProducao.DBGrid4DrawColumnCell(Sender: TObject; const Rect: TRect;
  DataCol: Integer; Column: TColumn; State: TGridDrawState);
begin
  if odd((Sender as TDBGrid).DataSource.DataSet.RecNo) then
    (Sender as TDBGrid).Canvas.Brush.Color := $00F1ECE7
  else
    (Sender as TDBGrid).Canvas.Brush.Color := clWhite;

  if (gdSelected in State) then
  begin
    (Sender as TDBGrid).Canvas.Font.Color := clBlack;
    (Sender as TDBGrid).Canvas.Brush.Color := clSilver;
    DBGrid4.Canvas.Font.Style := [fsbold];
  end
  else
    DBGrid4.Canvas.Font.Style := [];

  (Sender as TDBGrid).DefaultDrawColumnCell(Rect, DataCol, Column, State);
end;

procedure TfrmProducao.edtLocChange(Sender: TObject);
begin
   //localiza;
end;

procedure TfrmProducao.edtLocKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key = vk_up then
    qryProducao.Prior;
  if Key = VK_DOWN then
    qryProducao.Next;

  if Key = vk_return then
      localiza;
end;

procedure TfrmProducao.edtLocKeyPress(Sender: TObject; var Key: Char);
begin
  if PageControl1.ActivePageIndex = 0 then
  begin

    if idx = 0 then
    begin
      if not(Key in ['0' .. '9', #8, #9, #13, #27]) then
        Key := #0;
    end;

    if (idx in [6 .. 8]) then
    begin
      if not(Key in [',', '0' .. '9', #8, #9, #13, #27]) then
        Key := #0;
    end;

  end;

end;

procedure TfrmProducao.FormActivate(Sender: TObject);
begin
  dados.vForm := nil;
  dados.vForm := self;
  dados.GetComponentes;
end;

procedure TfrmProducao.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  dados.qryProdutos.Close;

end;

procedure TfrmProducao.FormShow(Sender: TObject);
begin

  idx := 2;
  vOrdem := ' ASC';

  edtLoc.SetFocus;

  tamanho;

  DBGrid1TitleClick(DBGrid2.Columns[idx]);

  qryPV.Close;
  qryPV.Open;

//  if Dados.qryEmpresaMARMORARIA.Value = 'S' then
//    begin
//    dbgProducaoItens.Columns[7].Visible := true;
//    btnProducao.Visible        := true;
//
//    end;

end;

procedure TfrmProducao.tamanho;
begin
  DBGrid2.Columns[0].Width := round(Screen.Width * 0.04);
  DBGrid2.Columns[1].Width := round(Screen.Width * 0.04);
  DBGrid2.Columns[2].Width := round(Screen.Width * 0.19);
  DBGrid2.Columns[3].Width := round(Screen.Width * 0.05);
  DBGrid2.Columns[4].Width := round(Screen.Width * 0.05);

  DBGrid4.Columns[0].Width := round(Screen.Width * 0.02);
  DBGrid4.Columns[1].Width := round(Screen.Width * 0.03);
  DBGrid4.Columns[2].Width := round(Screen.Width * 0.19);
  DBGrid4.Columns[3].Width := round(Screen.Width * 0.05);
  

end;

procedure TfrmProducao.LocalizaProduto;
  var
  filtro, filtro1, filtro2, ordem: string;
begin
  filtro := '';
  filtro1 := '';
  filtro2 := '';
  ordem := '';

  if vSql = '' then
    vSql := qryPV.SQL.Text;

  filtro := ' WHERE pv.fkempresa=' + Dados.qryEmpresaCODIGO.AsString;



  case idx of
    0:
      begin
        if (Trim(edtLoc.Text) <> '') then
          filtro := filtro + ' and pv.codigo=' + edtLoc.Text;
      end;
    2:
      begin
        if (Trim(edtLoc.Text) <> '') then
          filtro := filtro + ' and PES.RAZAO like ' +
            QuotedStr(edtLoc.Text + '%');
      end;
    3:
      begin
        if (Trim(edtLoc.Text) <> '') then
          filtro := filtro + ' and PV.Nome like ' +
            QuotedStr(edtLoc.Text + '%');
      end;
    4:
      begin
        if (Trim(edtLoc.Text) <> '') then
          filtro := filtro + ' and PV.total >= ' + StringReplace(edtLoc.Text,
            ',', '.', []);
      end;
    5:
      begin
        if (Trim(edtLoc.Text) <> '') then
          filtro := filtro + ' and PV.SITUACAO =' +
            QuotedStr(copy(edtLoc.Text, 1, 1));
      end;

  end;

  case TabSheet2.TabIndex of

    0:
      filtro1 := ' and PV.situacao=''F''';

  end;

//  case Pedidos.TabIndex of
//    0:
//      filtro2 := ' and not(PV.necf>0)';
//
//    1:
//      filtro2 := ' and pv.necf>=1';
//
//  end;

  case idx of
    0:
      ordem := ' order by PV.codigo' + vOrdem;
    1:
      ordem := ' order by PV.data_emissao' + vOrdem;
    2:
      ordem := ' order by PES.Razao' + vOrdem;
    3:
      ordem := ' order by ve.nome' + vOrdem;
    4:
      ordem := ' order by PV.total' + vOrdem;

  end;

  qryPV.close;

  qryPV.SQL.Text := vSql;
  qryPV.SQL.Text := StringReplace(qryPV.SQL.Text, '/*where*/',
    filtro + filtro1 + filtro2 + ordem, [rfReplaceAll]);
  qryPV.Open;


  end;



procedure TfrmProducao.btnNovaProducaoClick(Sender: TObject);
var codigo_pr, codigo_pri, ultima_producao, i : Integer;
begin

    qryProducao.Close;
    qryProducao.Params[0].AsInteger := DBGrid2.Fields[0].Value;
    qryProducao.Open;

    qryQtdConcluido.Close;
    qryQtdConcluido.Params[0].AsInteger := DBGrid2.Fields[0].Value;
    qryQtdConcluido.Open;

    qryQtdVendido.Close;
    qryQtdVendido.Params[0].AsInteger := DBGrid2.Fields[0].Value;
    qryQtdVendido.Open;

    i := 0;

    qryPV_ITENS.First;
  while not qryPV_ITENS.Eof do
    begin
     if qryPV_ITENS.FieldByName('fabricado').Value = 'S' then
      begin
        i := i + 1;
      end;
    qryPV_ITENS.Next;
    end;

    if qryQtdConcluido.FieldByName('CONCLUIDO').Value = qryQtdVendido.FieldByName('VENDIDO').Value then
      begin
        MessageDlg('Nova produ��o n�o permitida, pois, todos os itens j� foram para produ��o.',mtInformation,[mbOk],0);
        abort;
      end;


    //VERIFICA SE J� EXISTE ALGUMA PRODU��O FEITA PARA VENDA SELECIONADA
  if i <> 0 then
  begin
    if qryProducao.RecordCount = 0 then
    begin

    qryPV_Itens.Close;
    qryPV_Itens.Params[0].AsInteger := DBGrid2.Fields[0].Value;
    qryPV_Itens.Open;

    Dados.qryExecute.SQL.Clear;
    Dados.qryExecute.SQL.Add('INSERT INTO PRODUCAO_MASTER (');
    Dados.qryExecute.SQL.Add('PR_CODIGO,');
    Dados.qryExecute.SQL.Add('FIL_CODIGO,');
    Dados.qryExecute.SQL.Add('VEN_CODIGO,');
    Dados.qryExecute.SQL.Add('FK_CLIENTE,');
    Dados.qryExecute.SQL.Add('PR_DATA,');
    Dados.qryExecute.SQL.Add('PR_HORA,');
    Dados.qryExecute.SQL.Add('PR_STATUS,');
    Dados.qryExecute.SQL.Add('USR_USUARIO,');
    Dados.qryExecute.SQL.Add('FK_VENDEDOR');
    Dados.qryExecute.SQL.Add(')');
    Dados.qryExecute.SQL.Add('VALUES');
    Dados.qryExecute.SQL.Add('(');
    Dados.qryExecute.SQL.Add(':PR_CODIGO,');
    Dados.qryExecute.SQL.Add(':FIL_CODIGO,');
    Dados.qryExecute.SQL.Add(':VEN_CODIGO,');
    Dados.qryExecute.SQL.Add(':ID_CLIENTE,');
    Dados.qryExecute.SQL.Add(':PR_DATA,');
    Dados.qryExecute.SQL.Add(':PR_HORA,');
    Dados.qryExecute.SQL.Add(':PR_STATUS,');
    Dados.qryExecute.SQL.Add(':USR_USUARIO,');
    Dados.qryExecute.SQL.Add(':FK_VENDEDOR');
    Dados.qryExecute.SQL.Add(');');

    Dados.qryExecute.ParamByName('FIL_CODIGO').Value :=
      Dados.qryEmpresaCODIGO.Value;
    Dados.qryExecute.ParamByName('VEN_CODIGO').Value :=
      qryPVCODIGO.Value;
    Dados.qryExecute.ParamByName('ID_CLIENTE').Value :=
      qryPVID_CLIENTE.Value;
    Dados.qryExecute.ParamByName('PR_DATA').Value := date;
    Dados.qryExecute.ParamByName('PR_HORA').Value := time;
    Dados.qryExecute.ParamByName('PR_STATUS').Value := 'AGUARDANDO';
    Dados.qryExecute.ParamByName('USR_USUARIO').Value := Dados.idUsuario;
    Dados.qryExecute.ParamByName('FK_VENDEDOR').Value := qryPVFK_VENDEDOR.Value;

    codigo_pr := Dados.Numerador('PRODUCAO_MASTER', 'PR_CODIGO', 'N', '', '');

    Dados.qryExecute.ParamByName('PR_CODIGO').Value := codigo_pr;
    Dados.qryExecute.ExecSQL;
    Dados.Conexao.CommitRetaining;

    codigo_pri := 0;

  qryPV_ITENS.First;
  while not qryPV_ITENS.Eof do
    begin

    if qryPV_ITENS.FieldByName('fabricado').Value = 'S' then
      begin

            Dados.qryExecute.SQL.Clear;
            Dados.qryExecute.SQL.Add('INSERT INTO PRODUCAO_ITENS (');
            Dados.qryExecute.SQL.Add('FIL_CODIGO,');
            Dados.qryExecute.SQL.Add('VEN_CODIGO,');
            Dados.qryExecute.SQL.Add('PR_CODIGO,');
            Dados.qryExecute.SQL.Add('PRI_CODIGO,');
            Dados.qryExecute.SQL.Add('PRO_CODIGO,');
            Dados.qryExecute.SQL.Add('PRI_QTD_PRODUCAO,');
            Dados.qryExecute.SQL.Add('PRI_ITEM_VENDA');
            Dados.qryExecute.SQL.Add(')');
            Dados.qryExecute.SQL.Add('VALUES');
            Dados.qryExecute.SQL.Add('(');
            Dados.qryExecute.SQL.Add(':FIL_CODIGO,');
            Dados.qryExecute.SQL.Add(':VEN_CODIGO,');
            Dados.qryExecute.SQL.Add(':PR_CODIGO,');
            Dados.qryExecute.SQL.Add(':PRI_CODIGO,');
            Dados.qryExecute.SQL.Add(':PRO_CODIGO,');
            Dados.qryExecute.SQL.Add(':PRI_QTD_PRODUCAO,');
            Dados.qryExecute.SQL.Add(':PRI_ITEM_VENDA');
            Dados.qryExecute.SQL.Add(');');

            Dados.qryExecute.ParamByName('FIL_CODIGO').Value :=
              Dados.qryEmpresaCODIGO.Value;
            Dados.qryExecute.ParamByName('VEN_CODIGO').Value :=
              qryPVCODIGO.Value;

            Dados.qryExecute.ParamByName('PR_CODIGO').Value :=
              codigo_pr;
            Dados.qryExecute.ParamByName('PRO_CODIGO').Value :=
              qryPV_ITENS.FieldByName('ID_PRODUTO').Value;
            Dados.qryExecute.ParamByName('PRI_QTD_PRODUCAO').Value :=
              qryPV_ITENS.FieldByName('QTD').Value;
            Dados.qryExecute.ParamByName('PRI_ITEM_VENDA').Value :=
              qryPV_ITENS.FieldByName('ITEM').Value;

            codigo_pri := codigo_pri + 1;

            Dados.qryExecute.ParamByName('PRI_CODIGO').Value := codigo_pri;
            Dados.qryExecute.ExecSQL;
            Dados.Conexao.CommitRetaining;




      end;
      qryPV_ITENS.Next;
    end;

    if not(qryPV.State in dsEditModes) then
      qryPV.Edit;

    qryPVSTATUS_PRODUCAO.Value := 'AGUARDANDO';

    qryPV.Refresh;
    qryProducao.Refresh;
  end
  else   //cria uma nova producao usando uma producao j� ativada
  begin

      Dados.qryExecute.Close;
      Dados.qryExecute.SQL.Clear;
      Dados.qryExecute.SQL.Add('SELECT MAX(PR_CODIGO) FROM producao_master pm where pm.ven_codigo = :codigo');
      Dados.qryExecute.Params[0].Value := qryPVCODIGO.Value;
      Dados.qryExecute.Open;

      ultima_producao := Dados.qryExecute.Fields[0].AsInteger;

      Dados.qryExecute.Close;
      Dados.qryExecute.SQL.Clear;
      Dados.qryExecute.SQL.Add('SELECT pm.pr_status FROM producao_master pm where pm.pr_codigo = :codigo');
      Dados.qryExecute.Params[0].Value := ultima_producao;
      Dados.qryExecute.Open;

     if Dados.qryExecute.Fields[0].Value = 'AGUARDANDO' then
      begin

        MessageDlg('Existe produ��o com status - AGUARDANDO.',mtInformation,[mbOk],0);
        abort;
      end;


    qryProducao.Close;
    qryProducao.Params[0].AsInteger := DBGrid2.Fields[0].Value;
    qryProducao.Open;

    Dados.qryExecute.SQL.Clear;
    Dados.qryExecute.SQL.Add('INSERT INTO PRODUCAO_MASTER (');
    Dados.qryExecute.SQL.Add('PR_CODIGO,');
    Dados.qryExecute.SQL.Add('FIL_CODIGO,');
    Dados.qryExecute.SQL.Add('VEN_CODIGO,');
    Dados.qryExecute.SQL.Add('FK_CLIENTE,');
    Dados.qryExecute.SQL.Add('PR_DATA,');
    Dados.qryExecute.SQL.Add('PR_HORA,');
    Dados.qryExecute.SQL.Add('PR_STATUS,');
    Dados.qryExecute.SQL.Add('USR_USUARIO,');
    Dados.qryExecute.SQL.Add('FK_VENDEDOR');
    Dados.qryExecute.SQL.Add(')');
    Dados.qryExecute.SQL.Add('VALUES');
    Dados.qryExecute.SQL.Add('(');
    Dados.qryExecute.SQL.Add(':PR_CODIGO,');
    Dados.qryExecute.SQL.Add(':FIL_CODIGO,');
    Dados.qryExecute.SQL.Add(':VEN_CODIGO,');
    Dados.qryExecute.SQL.Add(':ID_CLIENTE,');
    Dados.qryExecute.SQL.Add(':PR_DATA,');
    Dados.qryExecute.SQL.Add(':PR_HORA,');
    Dados.qryExecute.SQL.Add(':PR_STATUS,');
    Dados.qryExecute.SQL.Add(':USR_USUARIO,');
    Dados.qryExecute.SQL.Add(':FK_VENDEDOR');
    Dados.qryExecute.SQL.Add(');');

    Dados.qryExecute.ParamByName('FIL_CODIGO').Value :=
      Dados.qryEmpresaCODIGO.Value;
    Dados.qryExecute.ParamByName('VEN_CODIGO').Value :=
      qryPVCODIGO.Value;
    Dados.qryExecute.ParamByName('ID_CLIENTE').Value :=
      qryPVID_CLIENTE.Value;
    Dados.qryExecute.ParamByName('PR_DATA').Value := date;
    Dados.qryExecute.ParamByName('PR_HORA').Value := time;
    Dados.qryExecute.ParamByName('PR_STATUS').Value := 'AGUARDANDO';
    Dados.qryExecute.ParamByName('USR_USUARIO').Value := Dados.idUsuario;
    Dados.qryExecute.ParamByName('FK_VENDEDOR').Value := qryPVFK_VENDEDOR.Value;

    codigo_pr := Dados.Numerador('PRODUCAO_MASTER', 'PR_CODIGO', 'N', '', '');

    Dados.qryExecute.ParamByName('PR_CODIGO').Value := codigo_pr;
    Dados.qryExecute.ExecSQL;
    Dados.Conexao.CommitRetaining;

    codigo_pri := 0;

   qryProducaoItens.Close;
   qryProducaoItens.Params[0].AsInteger := ultima_producao;
   qryProducaoItens.Open;

  qryProducaoItens.First;
  while not qryProducaoItens.Eof do
    begin
          if qryProducaoItens.FieldByName('PRI_QTD_FALTA').Value > 0 then
           begin
            Dados.qryExecute.SQL.Clear;
            Dados.qryExecute.SQL.Add('INSERT INTO PRODUCAO_ITENS (');
            Dados.qryExecute.SQL.Add('FIL_CODIGO,');
            Dados.qryExecute.SQL.Add('VEN_CODIGO,');
            Dados.qryExecute.SQL.Add('PR_CODIGO,');
            Dados.qryExecute.SQL.Add('PRI_CODIGO,');
            Dados.qryExecute.SQL.Add('PRO_CODIGO,');
            Dados.qryExecute.SQL.Add('PRI_QTD_PRODUCAO,');
            Dados.qryExecute.SQL.Add('PRI_ITEM_VENDA');
            Dados.qryExecute.SQL.Add(')');
            Dados.qryExecute.SQL.Add('VALUES');
            Dados.qryExecute.SQL.Add('(');
            Dados.qryExecute.SQL.Add(':FIL_CODIGO,');
            Dados.qryExecute.SQL.Add(':VEN_CODIGO,');
            Dados.qryExecute.SQL.Add(':PR_CODIGO,');
            Dados.qryExecute.SQL.Add(':PRI_CODIGO,');
            Dados.qryExecute.SQL.Add(':PRO_CODIGO,');
            Dados.qryExecute.SQL.Add(':PRI_QTD_PRODUCAO,');
            Dados.qryExecute.SQL.Add(':PRI_ITEM_VENDA');
            Dados.qryExecute.SQL.Add(');');

            Dados.qryExecute.ParamByName('FIL_CODIGO').Value :=
              Dados.qryEmpresaCODIGO.Value;
            Dados.qryExecute.ParamByName('VEN_CODIGO').Value :=
              qryPVCODIGO.Value;

            Dados.qryExecute.ParamByName('PR_CODIGO').Value :=
              codigo_pr;
            Dados.qryExecute.ParamByName('PRO_CODIGO').Value :=
              qryProducaoItens.FieldByName('PRO_CODIGO').Value;
            Dados.qryExecute.ParamByName('PRI_QTD_PRODUCAO').Value :=
              qryProducaoItens.FieldByName('PRI_QTD_FALTA').Value;
            Dados.qryExecute.ParamByName('PRI_ITEM_VENDA').Value :=
              qryProducaoItens.FieldByName('PRI_ITEM_VENDA').Value;

            codigo_pri := codigo_pri + 1;

            Dados.qryExecute.ParamByName('PRI_CODIGO').Value := codigo_pri;
            Dados.qryExecute.ExecSQL;
            Dados.Conexao.CommitRetaining;
          end;

      qryProducaoItens.Next;
    end;

    qryProducao.Refresh;
  end;
 end
 else
 begin
    MessageDlg('N�o existe produtos para serem fabricado neste pedido de venda.',mtInformation,[mbOk],0);
   abort;
 end;


end;

procedure TfrmProducao.btnImpOEClick(Sender: TObject);
begin

    qryCliente.Close;
    qryCliente.Params[0].Value := qryProducao.FieldByName('FK_CLIENTE').Value;
    qryCliente.Open;

    qryVendedor.Close;
    qryVendedor.Params[0].Value := qryProducao.FieldByName('FK_VENDEDOR').Value;
    qryVendedor.Open;


    frxReport.LoadFromFile(ExtractFilePath(Application.ExeName) +
    '\Relatorio\RelOrdemEntrega.fr3');
    frxReport.ShowReport;

end;

procedure TfrmProducao.btnImpProdClick(Sender: TObject);
begin
    qryCliente.Close;
    qryCliente.Params[0].Value := qryProducao.FieldByName('FK_CLIENTE').Value;
    qryCliente.Open;

    qryVendedor.Close;
    qryVendedor.Params[0].Value := qryProducao.FieldByName('FK_VENDEDOR').Value;
    qryVendedor.Open;


    frxReport.LoadFromFile(ExtractFilePath(Application.ExeName) +
    '\Relatorio\RelOrdemProducao.fr3');
    frxReport.ShowReport;
end;

procedure TfrmProducao.btnImpReciboClick(Sender: TObject);
begin

    qryCliente.Close;
    qryCliente.Params[0].Value := qryProducao.FieldByName('FK_CLIENTE').Value;
    qryCliente.Open;

    qryVendedor.Close;
    qryVendedor.Params[0].Value := qryProducao.FieldByName('FK_VENDEDOR').Value;
    qryVendedor.Open;

    frxReport.LoadFromFile(ExtractFilePath(Application.ExeName) +
    '\Relatorio\RelOrdemEntregaRecibo.fr3');
    frxReport.ShowReport;

end;



procedure TfrmProducao.btnProducaoClick(Sender: TObject);
begin


  dbgProducaoItens.Enabled := true;

  qryProducaoItens.First;
  dbgProducaoItens.SelectedIndex := 4;
  dbgProducaoItens.SetFocus;

  dbgProducaoItens.Columns[3].ReadOnly := true;
  dbgProducaoItens.Columns[5].ReadOnly := true;

  if not(qryProducaoItens.State in dsEditModes) then
        qryProducaoItens.Edit;

  DBGrid2.Enabled             := false;
  btnProducaoParcial.Enabled     := false;
  btnProducaoTotal.Enabled       := false;

  btnFinalizar.Enabled           := true;


end;

procedure TfrmProducao.localiza;
begin

      LocalizaProduto;

end;

procedure TfrmProducao.TabSet2Click(Sender: TObject);
begin
  localiza;
end;

end.
