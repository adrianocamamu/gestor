object frmGeraEntrega: TfrmGeraEntrega
  Left = 0
  Top = 0
  BorderStyle = bsNone
  Caption = 'Controle de Entrega'
  ClientHeight = 439
  ClientWidth = 968
  Color = 13018760
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poDesktopCenter
  OnKeyDown = FormKeyDown
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object btnGravarOrdem: TSpeedButton
    Left = 8
    Top = 333
    Width = 177
    Height = 90
    Caption = 'Gravar | F12'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -16
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
    OnClick = btnGravarOrdemClick
  end
  object btnSairOrdem: TSpeedButton
    Left = 778
    Top = 333
    Width = 177
    Height = 90
    Caption = 'Sair | ESC'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -16
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
    OnClick = btnSairOrdemClick
  end
  object DBGrid1: TDBGrid
    Left = 10
    Top = 8
    Width = 945
    Height = 312
    DataSource = dsItemVenda
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
    TabOrder = 0
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'Tahoma'
    TitleFont.Style = []
    OnKeyPress = DBGrid1KeyPress
    Columns = <
      item
        Expanded = False
        FieldName = 'ITEM'
        Title.Caption = 'Item'
        Width = 35
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'FKVENDA'
        Visible = False
      end
      item
        Expanded = False
        FieldName = 'ID_PRODUTO'
        Title.Caption = 'C'#243'digo'
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'DESCRICAO'
        Title.Caption = 'Descri'#231#227'o'
        Width = 430
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'UNIDADE'
        Title.Caption = 'Un'
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'QTD'
        Title.Caption = 'Vendido'
        Width = 110
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'ENTREGUE'
        Title.Caption = 'Entregue'
        Width = 110
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'SALDO'
        Title.Caption = 'A Entregar'
        Width = 110
        Visible = True
      end>
  end
  object qryItemVenda: TFDQuery
    AfterOpen = qryItemVendaAfterOpen
    Connection = Dados.Conexao
    SQL.Strings = (
      'SELECT VD.*, PRO.DESCRICAO FROM VENDAS_DETALHE VD, PRODUTO PRO'
      'where'
      'PRO.codigo = VD.ID_PRODUTO AND FKVENDA= :CODIGO'
      'ORDER BY ITEM;')
    Left = 352
    Top = 88
    ParamData = <
      item
        Name = 'CODIGO'
        DataType = ftInteger
        ParamType = ptInput
      end>
    object qryItemVendaITEM: TSmallintField
      FieldName = 'ITEM'
      Origin = 'ITEM'
      ReadOnly = True
    end
    object qryItemVendaFKVENDA: TIntegerField
      FieldName = 'FKVENDA'
      Origin = 'FKVENDA'
      ReadOnly = True
      Required = True
      Visible = False
    end
    object qryItemVendaID_PRODUTO: TIntegerField
      FieldName = 'ID_PRODUTO'
      Origin = 'ID_PRODUTO'
      Required = True
    end
    object qryItemVendaDESCRICAO: TStringField
      AutoGenerateValue = arDefault
      FieldName = 'DESCRICAO'
      Origin = 'DESCRICAO'
      ProviderFlags = []
      ReadOnly = True
      Required = True
      Size = 100
    end
    object qryItemVendaUNIDADE: TStringField
      FieldName = 'UNIDADE'
      Origin = 'UNIDADE'
      ReadOnly = True
      Size = 3
    end
    object qryItemVendaQTD: TFMTBCDField
      FieldName = 'QTD'
      Origin = 'QTD'
      ReadOnly = True
      Precision = 18
      Size = 3
    end
    object qryItemVendaENTREGUE: TFloatField
      FieldKind = fkInternalCalc
      FieldName = 'ENTREGUE'
      Precision = 2
    end
    object qryItemVendaSALDO: TFloatField
      FieldKind = fkInternalCalc
      FieldName = 'SALDO'
    end
    object qryItemVendaLARGURA: TFMTBCDField
      FieldName = 'LARGURA'
      Origin = 'LARGURA'
      Precision = 18
      Size = 2
    end
    object qryItemVendaCOMPRIMENTO: TFMTBCDField
      FieldName = 'COMPRIMENTO'
      Origin = 'COMPRIMENTO'
      Precision = 18
      Size = 2
    end
    object qryItemVendaID_MODELO: TIntegerField
      FieldName = 'ID_MODELO'
      Origin = 'ID_MODELO'
    end
    object qryItemVendaQTD_MODELO: TFMTBCDField
      FieldName = 'QTD_MODELO'
      Origin = 'QTD_MODELO'
      Precision = 18
      Size = 2
    end
  end
  object dsItemVenda: TDataSource
    DataSet = qryItemVenda
    Left = 344
    Top = 152
  end
  object qryVenda: TFDQuery
    Connection = Dados.Conexao
    SQL.Strings = (
      'select * from VENDAS_MASTER'
      'where'
      'codigo=:cod')
    Left = 181
    Top = 80
    ParamData = <
      item
        Name = 'COD'
        DataType = ftInteger
        ParamType = ptInput
        Value = Null
      end>
    object qryVendaCODIGO: TIntegerField
      FieldName = 'CODIGO'
      Origin = 'CODIGO'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
    end
    object qryVendaID_CLIENTE: TIntegerField
      FieldName = 'ID_CLIENTE'
      Origin = 'ID_CLIENTE'
    end
    object qryVendaFK_USUARIO: TIntegerField
      FieldName = 'FK_USUARIO'
      Origin = 'FK_USUARIO'
    end
    object qryVendaFK_VENDEDOR: TIntegerField
      FieldName = 'FK_VENDEDOR'
      Origin = 'FK_VENDEDOR'
    end
    object qryVendaFKEMPRESA: TIntegerField
      FieldName = 'FKEMPRESA'
      Origin = 'FKEMPRESA'
      Required = True
    end
  end
  object dsVenda: TDataSource
    DataSet = qryVenda
    Left = 168
    Top = 168
  end
  object frxDBEmpresa: TfrxDBDataset
    UserName = 'frxDBEmpresa'
    CloseDataSource = False
    FieldAliases.Strings = (
      'CODIGO=CODIGO'
      'FANTASIA=FANTASIA'
      'RAZAO=RAZAO'
      'CNPJ=CNPJ'
      'IE=IE'
      'IM=IM'
      'ENDERECO=ENDERECO'
      'NUMERO=NUMERO'
      'COMPLEMENTO=COMPLEMENTO'
      'BAIRRO=BAIRRO'
      'CIDADE=CIDADE'
      'UF=UF'
      'CEP=CEP'
      'FONE=FONE'
      'FAX=FAX'
      'EMAIL=EMAIL'
      'SITE=SITE'
      'LOGOMARCA=LOGOMARCA'
      'ID_PLANO_TRANSFERENCIA_C=ID_PLANO_TRANSFERENCIA_C'
      'ID_PLANO_TRANSFERENCIA_D=ID_PLANO_TRANSFERENCIA_D'
      'ID_CAIXA_GERAL=ID_CAIXA_GERAL'
      'BLOQUEAR_ESTOQUE_NEGATIVO=BLOQUEAR_ESTOQUE_NEGATIVO'
      'ID_CIDADE=ID_CIDADE'
      'CRT=CRT'
      'ID_UF=ID_UF'
      'ID_PLANO_VENDA=ID_PLANO_VENDA'
      'OBSFISCO=OBSFISCO'
      'CFOP=CFOP'
      'CSOSN=CSOSN'
      'CST_ICMS=CST_ICMS'
      'ALIQ_ICMS=ALIQ_ICMS'
      'CST_ENTRADA=CST_ENTRADA'
      'CST_SAIDA=CST_SAIDA'
      'ALIQ_PIS=ALIQ_PIS'
      'ALIQ_COF=ALIQ_COF'
      'CST_IPI=CST_IPI'
      'ALIQ_IPI=ALIQ_IPI')
    DataSet = Dados.qryEmpresa
    BCDToCurrency = False
    Left = 592
    Top = 56
  end
  object frxDBOrdemItens: TfrxDBDataset
    UserName = 'frxDBOrdemItens'
    CloseDataSource = False
    FieldAliases.Strings = (
      'FIL_CODIGO=FIL_CODIGO'
      'VEN_CODIGO=VEN_CODIGO'
      'VE_CODIGO=VE_CODIGO'
      'PEI_CODIGO=PEI_CODIGO'
      'PRO_CODIGO=PRO_CODIGO'
      'VEI_QTD=VEI_QTD'
      'VEI_QTD_RESTANTE=VEI_QTD_RESTANTE'
      'VEI_QTD_FALTA=VEI_QTD_FALTA'
      'DESCRICAO=DESCRICAO')
    DataSet = qryItemOrdem
    BCDToCurrency = False
    Left = 727
    Top = 64
  end
  object frxDBCliente: TfrxDBDataset
    UserName = 'frxDBCliente'
    CloseDataSource = False
    FieldAliases.Strings = (
      'CODIGO=CODIGO'
      'RAZAO=RAZAO'
      'CNPJ=CNPJ'
      'ENDERECO=ENDERECO'
      'NUMERO=NUMERO'
      'BAIRRO=BAIRRO'
      'MUNICIPIO=MUNICIPIO'
      'UF=UF'
      'CEP=CEP'
      'FONE1=FONE1'
      'CELULAR1=CELULAR1')
    DataSet = qryCliente
    BCDToCurrency = False
    Left = 592
    Top = 136
  end
  object frxDBOrdem: TfrxDBDataset
    UserName = 'frxDBOrdem'
    CloseDataSource = False
    FieldAliases.Strings = (
      'VE_CODIGO=VE_CODIGO'
      'FIL_CODIGO=FIL_CODIGO'
      'VEN_CODIGO=VEN_CODIGO'
      'FK_CLIENTE=FK_CLIENTE'
      'VE_DATA=VE_DATA'
      'VE_HORA=VE_HORA'
      'VE_STATUS=VE_STATUS'
      'USR_USUARIO=USR_USUARIO'
      'VE_VEICULO_PLACA=VE_VEICULO_PLACA'
      'VE_CODIGO_RASTREIO=VE_CODIGO_RASTREIO'
      'VE_PROCURAR=VE_PROCURAR'
      'VE_ONDE_ENTREGAR=VE_ONDE_ENTREGAR')
    DataSet = qryOrdem
    BCDToCurrency = False
    Left = 751
    Top = 144
  end
  object frxReport: TfrxReport
    Tag = 1
    Version = '6.8.6'
    DotMatrixReport = False
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Padr'#227'o'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 42203.460160023100000000
    ReportOptions.LastChange = 44556.660048750000000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      ''
      'begin'
      ''
      'end.')
    OnReportPrint = 'frxReportOnReportPrint'
    Left = 616
    Top = 216
    Datasets = <
      item
        DataSet = frxDBCliente
        DataSetName = 'frxDBCliente'
      end
      item
        DataSet = frxDBEmpresa
        DataSetName = 'frxDBEmpresa'
      end
      item
        DataSet = frxDBOrdem
        DataSetName = 'frxDBOrdem'
      end
      item
        DataSet = frxDBOrdemItens
        DataSetName = 'frxDBOrdemItens'
      end
      item
        DataSet = frxDBVendedor
        DataSetName = 'frxDBVendedor'
      end>
    Variables = <
      item
        Name = ' New Category1'
        Value = Null
      end
      item
        Name = 'Endereco'
        Value = Null
      end>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -13
      Font.Name = 'COlonna MT'
      Font.Style = []
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LeftMargin = 7.500000000000000000
      RightMargin = 7.500000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      Frame.Typ = []
      MirrorMode = []
      OnBeforePrint = 'Page1OnBeforePrint'
      object MasterData1: TfrxMasterData
        FillType = ftBrush
        Frame.Typ = []
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = []
        Height = 83.149660000000000000
        ParentFont = False
        Top = 124.724490000000000000
        Width = 737.008350000000000000
        DataSet = frxDBOrdem
        DataSetName = 'frxDBOrdem'
        RowCount = 0
        object Memo84: TfrxMemoView
          Align = baClient
          AllowVectorExport = True
          Width = 737.008350000000000000
          Height = 83.149660000000000000
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
        end
        object Memo116: TfrxMemoView
          AllowVectorExport = True
          Left = 87.144960000000000000
          Top = 3.000000000000000000
          Width = 166.299320000000000000
          Height = 15.118120000000000000
          DataField = 'VEN_CODIGO'
          DataSet = frxDBOrdem
          DataSetName = 'frxDBOrdem'
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Courier New'
          Font.Style = [fsBold]
          Frame.Typ = []
          Memo.UTF8W = (
            '[frxDBOrdem."VEN_CODIGO"]')
          ParentFont = False
        end
        object Memo117: TfrxMemoView
          AllowVectorExport = True
          Left = 452.805010000000000000
          Top = 7.502350000000000000
          Width = 66.595300000000000000
          Height = 15.118120000000000000
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Courier New'
          Font.Style = []
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            'DATA:')
          ParentFont = False
        end
        object Memo5: TfrxMemoView
          AllowVectorExport = True
          Left = 16.759060000000000000
          Top = 41.795287800000000000
          Width = 66.595300000000000000
          Height = 15.118120000000000000
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Courier New'
          Font.Style = []
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            'CLIENTE:')
          ParentFont = False
        end
        object Memo6: TfrxMemoView
          AllowVectorExport = True
          Left = 8.559060000000000000
          Top = 60.669303540000000000
          Width = 76.195300000000000000
          Height = 15.118120000000000000
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Courier New'
          Font.Style = []
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            'VENDEDOR:')
          ParentFont = False
        end
        object frxDBPVRAZAO: TfrxMemoView
          AllowVectorExport = True
          Left = 88.000000000000000000
          Top = 42.795287800000000000
          Width = 331.388930000000000000
          Height = 15.420470000000000000
          DataField = 'RAZAO'
          DataSet = frxDBCliente
          DataSetName = 'frxDBCliente'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Courier New'
          Font.Style = [fsBold]
          Frame.Typ = []
          Memo.UTF8W = (
            '[frxDBCliente."RAZAO"]')
          ParentFont = False
        end
        object frxDBPVNOME: TfrxMemoView
          AllowVectorExport = True
          Left = 88.559060000000000000
          Top = 60.669303540000000000
          Width = 195.325850000000000000
          Height = 15.420470000000000000
          DataField = 'NOME'
          DataSet = frxDBVendedor
          DataSetName = 'frxDBVendedor'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Courier New'
          Font.Style = [fsBold]
          Frame.Typ = []
          Memo.UTF8W = (
            '[frxDBVendedor."NOME"]')
          ParentFont = False
        end
        object frxDBPVDATA_EMISSAO: TfrxMemoView
          AllowVectorExport = True
          Left = 521.472790000000000000
          Top = 7.559060000000000000
          Width = 205.379530000000000000
          Height = 15.420470000000000000
          DataField = 'VE_DATA'
          DataSet = frxDBOrdem
          DataSetName = 'frxDBOrdem'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Courier New'
          Font.Style = [fsBold]
          Frame.Typ = []
          Memo.UTF8W = (
            '[frxDBOrdem."VE_DATA"]')
          ParentFont = False
        end
        object Memo118: TfrxMemoView
          AllowVectorExport = True
          Left = 14.338590000000000000
          Top = 2.779530000000000000
          Width = 66.595300000000000000
          Height = 15.118120000000000000
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Courier New'
          Font.Style = []
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            'PEDIDO:')
          ParentFont = False
        end
        object Memo19: TfrxMemoView
          AllowVectorExport = True
          Left = 15.118120000000000000
          Top = 21.897650000000000000
          Width = 66.595300000000000000
          Height = 15.118120000000000000
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Courier New'
          Font.Style = []
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            'OE N'#186':')
          ParentFont = False
        end
        object Memo20: TfrxMemoView
          AllowVectorExport = True
          Left = 86.929190000000000000
          Top = 22.677180000000000000
          Width = 166.299320000000000000
          Height = 18.897650000000000000
          DataField = 'VE_CODIGO'
          DataSet = frxDBOrdem
          DataSetName = 'frxDBOrdem'
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Courier New'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            '[frxDBOrdem."VE_CODIGO"]')
          ParentFont = False
        end
        object Memo21: TfrxMemoView
          AllowVectorExport = True
          Left = 453.543600000000000000
          Top = 30.236240000000000000
          Width = 66.595300000000000000
          Height = 15.118120000000000000
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Courier New'
          Font.Style = []
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            'HOR'#193'RIO:')
          ParentFont = False
        end
        object Memo22: TfrxMemoView
          AllowVectorExport = True
          Left = 522.211380000000000000
          Top = 30.292950000000000000
          Width = 205.379530000000000000
          Height = 15.420470000000000000
          DataField = 'VE_HORA'
          DataSet = frxDBOrdem
          DataSetName = 'frxDBOrdem'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Courier New'
          Font.Style = [fsBold]
          Frame.Typ = []
          Memo.UTF8W = (
            '[frxDBOrdem."VE_HORA"]')
          ParentFont = False
        end
      end
      object DetailData1: TfrxDetailData
        FillType = ftBrush
        Frame.Typ = []
        Height = 22.677180000000000000
        Top = 275.905690000000000000
        Width = 737.008350000000000000
        OnAfterPrint = 'DetailData1OnAfterPrint'
        DataSet = frxDBOrdemItens
        DataSetName = 'frxDBOrdemItens'
        RowCount = 0
        object frxDBItensID_PRODUTO: TfrxMemoView
          AllowVectorExport = True
          Left = -62.031540000000000000
          Width = 41.574830000000000000
          Height = 15.118120000000000000
          DataField = 'ID_PRODUTO'
          DataSet = FrmBalcao.frxDBItens
          DataSetName = 'frxDBItens'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            '[frxDBItens."ID_PRODUTO"]')
          ParentFont = False
        end
        object Memo10: TfrxMemoView
          AllowVectorExport = True
          Top = 2.559060000000000000
          Width = 37.795300000000000000
          Height = 15.118120000000000000
          DataField = 'PEI_CODIGO'
          DataSet = frxDBOrdemItens
          DataSetName = 'frxDBOrdemItens'
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Courier New'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDBOrdemItens."PEI_CODIGO"]')
          ParentFont = False
        end
        object Memo13: TfrxMemoView
          AllowVectorExport = True
          Left = 45.174830000000000000
          Top = 2.559060000000000000
          Width = 463.445950000000000000
          Height = 15.118120000000000000
          DataField = 'DESCRICAO'
          DataSet = frxDBOrdemItens
          DataSetName = 'frxDBOrdemItens'
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Courier New'
          Font.Style = [fsBold]
          Frame.Typ = []
          Memo.UTF8W = (
            '[frxDBOrdemItens."DESCRICAO"]')
          ParentFont = False
        end
        object Memo18: TfrxMemoView
          AllowVectorExport = True
          Left = 522.616710000000000000
          Top = 2.559060000000000000
          Width = 210.217440000000000000
          Height = 15.118120000000000000
          DataField = 'VEI_QTD_RESTANTE'
          DataSet = frxDBOrdemItens
          DataSetName = 'frxDBOrdemItens'
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Courier New'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDBOrdemItens."VEI_QTD_RESTANTE"]')
          ParentFont = False
        end
        object Line10: TfrxLineView
          AllowVectorExport = True
          Left = 41.040940000000000000
          Top = -0.220470000000000000
          Height = 23.040000000000000000
          Color = clBlack
          Frame.Typ = []
          Diagonal = True
        end
        object Line12: TfrxLineView
          AllowVectorExport = True
          Left = 517.897650000000000000
          Top = -0.220470000000000000
          Height = 23.040000000000000000
          Color = clBlack
          Frame.Typ = []
          Diagonal = True
        end
        object Line2: TfrxLineView
          AllowVectorExport = True
          Left = 737.008350000000000000
          Height = 23.040000000000000000
          Color = clBlack
          Frame.Typ = []
          Diagonal = True
        end
        object Line3: TfrxLineView
          AllowVectorExport = True
          Top = 3.779530000000000000
          Height = 23.040000000000000000
          Color = clBlack
          Frame.Typ = []
          Diagonal = True
        end
      end
      object Header1: TfrxHeader
        FillType = ftBrush
        Frame.Typ = []
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = []
        Height = 23.040000000000000000
        ParentFont = False
        Top = 230.551330000000000000
        Width = 737.008350000000000000
        object Memo11: TfrxMemoView
          AllowVectorExport = True
          Top = 4.779530000000000000
          Width = 37.795300000000000000
          Height = 15.118120000000000000
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Courier New'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            'ITEM')
          ParentFont = False
        end
        object Memo12: TfrxMemoView
          AllowVectorExport = True
          Left = 45.174830000000000000
          Top = 4.779530000000000000
          Width = 467.225480000000000000
          Height = 15.118120000000000000
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Courier New'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            'PRODUTO')
          ParentFont = False
        end
        object Memo16: TfrxMemoView
          AllowVectorExport = True
          Left = 522.657650000000000000
          Top = 4.779530000000000000
          Width = 206.437910000000000000
          Height = 15.118120000000000000
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Courier New'
          Font.Style = []
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            'QUANTIDADE')
          ParentFont = False
        end
        object Line1: TfrxLineView
          AllowVectorExport = True
          Left = 41.040940000000000000
          Height = 23.040000000000000000
          Color = clBlack
          Frame.Typ = []
          Diagonal = True
        end
        object Line50: TfrxLineView
          AllowVectorExport = True
          Left = 517.897650000000000000
          Height = 23.040000000000000000
          Color = clBlack
          Frame.Typ = []
          Diagonal = True
        end
        object Shape2: TfrxShapeView
          AllowVectorExport = True
          Top = 3.779530000000000000
          Width = 737.007874020000000000
          Height = 21.240940000000000000
          Frame.Typ = []
        end
      end
      object Footer1: TfrxFooter
        FillType = ftBrush
        Frame.Typ = []
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -7
        Font.Name = 'COlonna MT'
        Font.Style = []
        Height = 151.181136540000000000
        ParentFont = False
        Top = 321.260050000000000000
        Width = 737.008350000000000000
        OnAfterCalcHeight = 'Footer1OnAfterCalcHeight'
        OnBeforePrint = 'Footer1OnBeforePrint'
        object Shape1: TfrxShapeView
          AllowVectorExport = True
          Width = 737.007874020000000000
          Height = 17.461410000000000000
          Frame.Typ = []
        end
        object Memo7: TfrxMemoView
          AllowVectorExport = True
          Left = 7.559060000000000000
          Top = 37.179530000000000000
          Width = 365.102350000000000000
          Height = 59.036240000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Courier New'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            
              'Declaro que recebi os itens descritos acima, [frxDBEmpresa."CIDA' +
              'DE"]-[frxDBEmpresa."UF"], [DATE]')
          ParentFont = False
          Formats = <
            item
            end
            item
            end>
        end
        object Memo9: TfrxMemoView
          AllowVectorExport = True
          Left = 400.379530000000000000
          Top = 112.872480000000000000
          Width = 326.400000000000000000
          Height = 28.800000000000000000
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Courier New'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            '___________________________________________'
            'VISTO OU CARIMBO DA LOJA')
          ParentFont = False
        end
        object Shape3: TfrxShapeView
          AllowVectorExport = True
          Left = 404.409710000000000000
          Top = 26.456710000000000000
          Width = 321.260050000000000000
          Height = 102.047310000000000000
          Frame.Typ = []
          Shape = skRoundRectangle
        end
      end
      object Header4: TfrxHeader
        FillType = ftBrush
        Frame.Typ = []
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -7
        Font.Name = 'COlonna MT'
        Font.Style = []
        Height = 81.370130000000000000
        ParentFont = False
        Top = 18.897650000000000000
        Width = 737.008350000000000000
        object Memo85: TfrxMemoView
          AllowVectorExport = True
          Left = 101.663080000000000000
          Top = 0.774830000000000000
          Width = 578.268090000000000000
          Height = 18.897650000000000000
          DataSetName = 'frxDBEmp'
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -19
          Font.Name = 'Courier New'
          Font.Style = [fsBold]
          Frame.Typ = []
          Memo.UTF8W = (
            '[frxDBEmpresa."FANTASIA"]')
          ParentFont = False
        end
        object Memo88: TfrxMemoView
          AllowVectorExport = True
          Left = 101.959060000000000000
          Top = 21.518120000000000000
          Width = 624.000000000000000000
          Height = 19.200000000000000000
          AutoWidth = True
          DataSet = frxDBEmpresa
          DataSetName = 'frxDBEmpresa'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -15
          Font.Name = 'Courier New'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            
              'END:[frxDBEmpresa."ENDERECO"],[frxDBEmpresa."NUMERO"] - [frxDBEm' +
              'presa."BAIRRO"]')
          ParentFont = False
          Formats = <
            item
            end
            item
            end
            item
            end>
        end
        object Memo89: TfrxMemoView
          AllowVectorExport = True
          Left = 101.559060000000000000
          Top = 61.281880000000000000
          Width = 624.000000000000000000
          Height = 15.420470000000000000
          AutoWidth = True
          DataSet = frxDBEmpresa
          DataSetName = 'frxDBEmpresa'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -15
          Font.Name = 'Courier New'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            'FONE:[frxDBEmpresa."FONE"] EMAIL:[frxDBEmpresa."EMAIL"]')
          ParentFont = False
        end
        object Picture1: TfrxPictureView
          AllowVectorExport = True
          Left = 7.559060000000000000
          Width = 86.400000000000000000
          Height = 76.800000000000000000
          DataField = 'LOGOMARCA'
          DataSet = frxDBEmpresa
          DataSetName = 'frxDBEmpresa'
          Frame.Typ = []
          HightQuality = False
          Transparent = False
          TransparentColor = clWhite
        end
        object Memo90: TfrxMemoView
          AllowVectorExport = True
          Left = 101.559060000000000000
          Top = 41.881880000000000000
          Width = 624.000000000000000000
          Height = 19.200000000000000000
          AutoWidth = True
          DataSet = frxDBEmpresa
          DataSetName = 'frxDBEmpresa'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -15
          Font.Name = 'Courier New'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            '[frxDBEmpresa."CIDADE"]- [frxDBEmpresa."UF"]')
          ParentFont = False
          Formats = <
            item
            end
            item
            end>
        end
      end
    end
  end
  object qryOrdem: TFDQuery
    Connection = Dados.Conexao
    SQL.Strings = (
      'select * from vendas_entrega')
    Left = 472
    Top = 96
  end
  object dsOrdem: TDataSource
    DataSet = qryOrdem
    Left = 472
    Top = 152
  end
  object qryCliente: TFDQuery
    Connection = Dados.Conexao
    SQL.Strings = (
      
        'SELECT codigo, razao, cnpj, endereco, numero, bairro, municipio,' +
        ' uf, cep, fone1, celular1  FROM pessoa'
      'where'
      'codigo = :codigo')
    Left = 824
    Top = 128
    ParamData = <
      item
        Name = 'CODIGO'
        DataType = ftInteger
        ParamType = ptInput
        Value = Null
      end>
  end
  object qryVendedor: TFDQuery
    Connection = Dados.Conexao
    SQL.Strings = (
      'select nome from vendedores where codigo = :codigo')
    Left = 824
    Top = 56
    ParamData = <
      item
        Name = 'CODIGO'
        DataType = ftInteger
        ParamType = ptInput
      end>
  end
  object frxDBVendedor: TfrxDBDataset
    UserName = 'frxDBVendedor'
    CloseDataSource = False
    DataSet = qryVendedor
    BCDToCurrency = False
    Left = 760
    Top = 224
  end
  object qryItemOrdem: TFDQuery
    Connection = Dados.Conexao
    SQL.Strings = (
      'select vei.*,'
      
        'iif (pro.fabricado = '#39'S'#39'  and vei.item_venda = vd.item, pro.desc' +
        'ricao || '#39' '#39' || vd.largura || '#39' X '#39' || vd.comprimento, pro.descr' +
        'icao) as descricao'
      'from produto pro, vendas_entrega_itens vei, vendas_detalhe vd'
      
        'where vei.ve_codigo = :codigo and vd.fkvenda = vei.ven_codigo an' +
        'd pro.codigo = vei.pro_codigo  and vei.item_venda = vd.item')
    Left = 848
    Top = 208
    ParamData = <
      item
        Name = 'CODIGO'
        DataType = ftInteger
        ParamType = ptInput
      end>
    object qryItemOrdemFIL_CODIGO: TIntegerField
      FieldName = 'FIL_CODIGO'
      Origin = 'FIL_CODIGO'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
    end
    object qryItemOrdemVEN_CODIGO: TIntegerField
      FieldName = 'VEN_CODIGO'
      Origin = 'VEN_CODIGO'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
    end
    object qryItemOrdemVE_CODIGO: TIntegerField
      FieldName = 'VE_CODIGO'
      Origin = 'VE_CODIGO'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
    end
    object qryItemOrdemPEI_CODIGO: TIntegerField
      FieldName = 'PEI_CODIGO'
      Origin = 'PEI_CODIGO'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
    end
    object qryItemOrdemPRO_CODIGO: TStringField
      FieldName = 'PRO_CODIGO'
      Origin = 'PRO_CODIGO'
    end
    object qryItemOrdemVEI_QTD: TFloatField
      FieldName = 'VEI_QTD'
      Origin = 'VEI_QTD'
    end
    object qryItemOrdemVEI_QTD_RESTANTE: TFloatField
      FieldName = 'VEI_QTD_RESTANTE'
      Origin = 'VEI_QTD_RESTANTE'
    end
    object qryItemOrdemVEI_QTD_FALTA: TFloatField
      FieldName = 'VEI_QTD_FALTA'
      Origin = 'VEI_QTD_FALTA'
    end
    object qryItemOrdemDESCRICAO: TStringField
      AutoGenerateValue = arDefault
      FieldName = 'DESCRICAO'
      Origin = 'DESCRICAO'
      ProviderFlags = []
      ReadOnly = True
      Size = 100
    end
  end
end
