unit PrincipalServidorFV;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, FireDAC.Stan.Intf, FireDAC.Stan.Option,
  FireDAC.Stan.Param, FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf,
  FireDAC.DApt.Intf, FireDAC.Stan.Async, FireDAC.DApt, Data.DB,
  FireDAC.Comp.DataSet, FireDAC.Comp.Client, Vcl.DBCtrls, Vcl.StdCtrls,
  Vcl.ExtCtrls, Vcl.AppEvnts, Vcl.Menus, Vcl.Mask;

type
  TPrincipal_FV = class(TForm)
    Label27: TLabel;
    BtnSicronizar: TButton;
    BtnPedidoWeb: TButton;
    DBText27: TDBText;
    Label28: TLabel;
    DBText20: TDBText;
    qrySincro_FV: TFDQuery;
    qrySincro_FVINTERVALO: TIntegerField;
    qrySincro_FVSINCRO_PEDIDO: TSQLTimeStampField;
    qrySincro_FVSINCRO_BD: TSQLTimeStampField;
    dsSincro_FV: TDataSource;
    dsConfigAcesso: TDataSource;
    qryConfigAcesso: TFDQuery;
    TimerFV: TTimer;
    ApplicationEvents1: TApplicationEvents;
    ctiPrincipal: TTrayIcon;
    pmMenu: TPopupMenu;
    RestaurarAplicao1: TMenuItem;
    SairdaAplicao1: TMenuItem;
    qryConfigAcessoCODIGO: TIntegerField;
    qryConfigAcessoBLOQ_CAD_EMPRESA: TStringField;
    qryConfigAcessoBLOQ_CAD_USUARIO: TStringField;
    qryConfigAcessoBLOQ_PERMISSOES: TStringField;
    qryConfigAcessoBLOQ_CONFIG: TStringField;
    qryConfigAcessoBLOQ_CONFIG_TERM: TStringField;
    qryConfigAcessoBLOQ_CAD_CONTADOR: TStringField;
    qryConfigAcessoBLOQ_IBPT: TStringField;
    qryConfigAcessoBLOQ_ICMS: TStringField;
    qryConfigAcessoBLOQ_CFOP: TStringField;
    qryConfigAcessoBLOQ_CAD_CAIXAS: TStringField;
    qryConfigAcessoBLOQ_CAD_MESAS: TStringField;
    qryConfigAcessoRETAGUARDA_BLOQUEADO: TStringField;
    qryConfigAcessoPDV_BLOQUEADO: TStringField;
    qryConfigAcessoPREVENDA_BLOQUEADO: TStringField;
    qryConfigAcessoZAP_BLOQUEADO: TStringField;
    qryConfigAcessoBLOQ_NFE: TStringField;
    qryConfigAcessoBLOQ_CTE: TStringField;
    qryConfigAcessoBLOQ_MDFE: TStringField;
    qryConfigAcessoBLOQ_ORCAMENTO: TStringField;
    qryConfigAcessoPRODUTO_BLOQ_IMPOSTOS: TStringField;
    qryConfigAcessoCLIENTE_BLOQ_ADICIONAIS: TStringField;
    qryConfigAcessoCLIENTE_BLOQ_FOTO: TStringField;
    qryConfigAcessoCLIENTE_BLOQ_CONTATOS: TStringField;
    qryConfigAcessoMENU_BLOQ_BOLETO: TStringField;
    qryConfigAcessoMENU_BLOQ_FISCAL: TStringField;
    qryConfigAcessoMENU_BLOQ_TRANSP: TStringField;
    qryConfigAcessoMENU_BLOQ_OS: TStringField;
    qryConfigAcessoMENU_BLOQ_REL_FISCAL: TStringField;
    qryConfigAcessoBLOQ_SCRIPT: TStringField;
    qryConfigAcessoCHEC_MODULOS: TStringField;
    DBRadioGroup1: TDBRadioGroup;
    Label1: TLabel;
    DBEdit1: TDBEdit;
    qryConfigAcessoSINCRONIZA_FV: TStringField;
    procedure BtnPedidoWebClick(Sender: TObject);
    procedure BtnSicronizarClick(Sender: TObject);
    procedure TimerFVTimer(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure ApplicationEvents1Minimize(Sender: TObject);
    procedure RestaurarAplicao1Click(Sender: TObject);
    procedure SairdaAplicao1Click(Sender: TObject);
  private
    function GetHandleOnTaskBar: THandle;
    procedure HideApplication;
    procedure ShowApplication;
    procedure ChangeStatusWindow;
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Principal_FV: TPrincipal_FV;

implementation

{$R *.dfm}

uses
udados, uPedidoWeb, uSincronizar, uDadosWeb, uConexaoBD;

Function TPrincipal_FV.GetHandleOnTaskBar: THandle;
Begin
{$IFDEF COMPILER11_UP}
  If Application.MainFormOnTaskBar And Assigned(Application.MainForm) Then
    Result := Application.MainForm.Handle
  Else
{$ENDIF COMPILER11_UP}
    Result := Application.Handle;
End;

procedure TPrincipal_FV.HideApplication;
Begin
  ctiPrincipal.Visible := true;
  Application.ShowMainForm := false;
  If Self <> Nil Then
    Self.Visible := false;
  Application.Minimize;
  ShowWindow(GetHandleOnTaskBar, SW_HIDE);
  ChangeStatusWindow;
End;

procedure TPrincipal_FV.ShowApplication;
Begin
  ctiPrincipal.Visible := false;
  Application.ShowMainForm := true;
  If Self <> Nil Then
  Begin
    Self.Visible := true;
    Self.WindowState := WsNormal;
  End;
  ShowWindow(GetHandleOnTaskBar, SW_SHOW);
  ChangeStatusWindow;
End;

procedure TPrincipal_FV.RestaurarAplicao1Click(Sender: TObject);
begin
 ShowApplication;
end;

procedure TPrincipal_FV.SairdaAplicao1Click(Sender: TObject);
begin
 Application.Terminate;
end;

procedure TPrincipal_FV.ChangeStatusWindow;
Begin
  If Self.Visible Then
    SairdaAplicao1.Caption := 'Minimizar para a bandeja'
  Else
    SairdaAplicao1.Caption := 'Sair da Aplica��o';
  Application.ProcessMessages;
End;

procedure TPrincipal_FV.ApplicationEvents1Minimize(Sender: TObject);
begin
   HideApplication;
end;

procedure TPrincipal_FV.BtnPedidoWebClick(Sender: TObject);
begin
  FrmPedidoWeb.btnAtualizar.Click;
FrmPedidoWeb.btnSalvar.Click;

if not(qrySincro_FV.State in dsEditModes) then
      qrySincro_FV.Edit;
    qrySincro_FVSINCRO_PEDIDO.AsDateTime := now;
    qrySincro_FV.Post;

  qrySincro_FV.close;
  qrySincro_FV.Open;
end;

procedure TPrincipal_FV.BtnSicronizarClick(Sender: TObject);
begin
 FrmSincronizar.btnSalvar.Click;

if not(qrySincro_FV.State in dsEditModes) then
      qrySincro_FV.Edit;
    qrySincro_FVSINCRO_BD.AsDateTime := now;
    qrySincro_FV.Post;

  qrySincro_FV.close;
  qrySincro_FV.Open;
end;

procedure TPrincipal_FV.FormShow(Sender: TObject);
begin
qryConfigAcesso.Close;
qryConfigAcesso.Open;

qrySincro_FV.Close;
qrySincro_FV.Open;

if qryConfigAcessoSINCRONIZA_FV.AsString = 'S' then
begin
  TimerFV.Interval := (qrySincro_FVINTERVALO.AsInteger *60)* 1000;
  TimerFV.Enabled := true;
  end else begin
  TimerFV.Enabled := false;
end;
end;

procedure TPrincipal_FV.TimerFVTimer(Sender: TObject);
begin
 if TimerFV.Enabled = true then
begin
  BtnPedidoWeb.Click;
end;
BtnSicronizar.Click;
end;

end.
