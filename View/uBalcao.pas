unit uBalcao;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants,
  System.Classes, Vcl.Graphics, math, ACBrDevice, shellapi,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.ExtCtrls, Vcl.StdCtrls, Vcl.Grids,
  Vcl.DBGrids, Vcl.Buttons, Vcl.ComCtrls, FireDAC.UI.Intf, FireDAC.VCLUI.Wait,
  FireDAC.Comp.UI, FireDAC.Stan.Intf, FireDAC.Phys, FireDAC.Phys.IBBase,
  FireDAC.Phys.FB, FireDAC.Stan.Option, FireDAC.Stan.Param, FireDAC.Stan.Error,
  FireDAC.DatS, FireDAC.Phys.Intf, FireDAC.DApt.Intf, FireDAC.Stan.Async,
  FireDAC.DApt, Data.DB, FireDAC.Comp.DataSet, FireDAC.Comp.Client,
  FireDAC.Stan.Def, FireDAC.Stan.Pool, Vcl.DBCtrls, Vcl.Mask, ACBrDFeSSL,
  System.Actions, ACBrDeviceSerial, pcnConversao, pcnConversaoNFe, ACBrUtil,
  Vcl.ActnList, Vcl.Imaging.pngimage, Vcl.Imaging.jpeg, Vcl.Tabs, blcksock,
  ACBrPosPrinter, ACBrBase, ACBrDFe, ACBrNFe, Vcl.Menus, DBGridEhGrouping,
  ToolCtrlsEh, DBGridEhToolCtrls, DynVarsEh, EhLibVCL, GridsEh, DBAxisGridsEh,
  DBGridEh, ACBrNFeDANFEClass, ACBrNFeDANFeESCPOS, DBCtrlsEh, DBLookupEh,
  ACBrBAL, Vcl.ExtDlgs, ACBrDFeReport, ACBrDFeDANFeReport, ACBrDANFCeFortesFrA4,
  ACBrEnterTab, ACBrTroco, JvComponentBase, JvXPCore, Vcl.DBCGrids, frxClass,
  frxDBSet, frxExportBaseDialog, frxExportPDF, ACBrTEFD, cxGraphics, cxControls,
  cxLookAndFeels, cxLookAndFeelPainters, cxContainer, cxEdit, cxImage, cxDBEdit,
  dxSkinsCore, dxSkinsDefaultPainters, ACBrInStore, acPNG, dxGDIPlusClasses,
  IdBaseComponent, IdComponent, IdIPWatch;

type
  TFrmBalcao = class(TForm)
    ActionList1: TActionList;
    actLerPeso: TAction;
    dsPesqProd: TDataSource;
    qryPesqProd: TFDQuery;
    qryVenda: TFDQuery;
    dsVenda: TDataSource;
    qryVendaCODIGO: TIntegerField;
    qryVendaDATA_EMISSAO: TDateField;
    qryVendaDATA_SAIDA: TDateField;
    qryVendaID_CLIENTE: TIntegerField;
    qryVendaFK_USUARIO: TIntegerField;
    qryVendaFK_CAIXA: TIntegerField;
    qryVendaFK_VENDEDOR: TIntegerField;
    qryVendaCPF_NOTA: TStringField;
    qryVendaTIPO_DESCONTO: TStringField;
    qryVendaOBSERVACOES: TMemoField;
    qryVendaSITUACAO: TStringField;
    qryItem: TFDQuery;
    dsItem: TDataSource;
    qryItemCODIGO: TIntegerField;
    qryItemFKVENDA: TIntegerField;
    qryItemID_PRODUTO: TIntegerField;
    qryItemSITUACAO: TStringField;
    qryItemUNIDADE: TStringField;
    qryCliente: TFDQuery;
    dsCliente: TDataSource;
    qryProd: TFDQuery;
    qryProdCODIGO: TIntegerField;
    qryProdDESCRICAO: TStringField;
    qryItemDESCRICAO_SL: TStringField;
    qryVendaVIRTUAL_CLIENTE: TStringField;
    qryVendaVIRTUAL_VENDEDOR: TStringField;
    qryItemTTOTAL: TAggregateField;
    qryVendaFKEMPRESA: TIntegerField;
    qryProdEFISCAL: TStringField;
    qryVendaTIPO: TStringField;
    qryVendaFKORCAMENTO: TIntegerField;
    qryVendaNECF: TIntegerField;
    qryItemITEM: TSmallintField;
    qryItemCOD_BARRA: TStringField;
    qryItemEFISCAL: TStringField;
    qrySoma: TFDQuery;
    qryConta_Movimento: TFDQuery;
    qryConta_MovimentoID_CONTA_CAIXA: TIntegerField;
    qryConta_MovimentoDATA: TDateField;
    qryConta_MovimentoHORA: TTimeField;
    qryConta_MovimentoFKVENDA: TIntegerField;
    qryConta_MovimentoLOTE: TIntegerField;
    qryConta_MovimentoID_USUARIO: TIntegerField;
    qryVendaLOTE: TIntegerField;
    qryBuscaVenda: TFDQuery;
    dsEmpresa: TDataSource;
    qryVendaVirtualEmpresa: TStringField;
    qryConta_MovimentoCODIGO: TIntegerField;
    qryVendaGERA_FINANCEIRO: TStringField;
    qryVendaFK_TABELA: TIntegerField;
    qryTabela: TFDQuery;
    qryTabelaCODIGO: TIntegerField;
    qryTabelaDESCRICAO: TStringField;
    qryTabelaFKEMPRESA: TIntegerField;
    qryTabelaATIVO: TStringField;
    qryVendaVIRTUAL_TABELA: TStringField;
    qryVendaVIRTUAL_TX_ACRESC: TFloatField;
    qryComposicao: TFDQuery;
    qryComposicaoID_PRODUTO: TIntegerField;
    qryConta_MovimentoHISTORICO: TStringField;
    qryVendaVIRTUAL_CNPJ: TStringField;
    qryBuscaVendaCODIGO: TIntegerField;
    qryVendaSUBTOTAL: TFMTBCDField;
    qryVendaDESCONTO: TFMTBCDField;
    qryVendaTROCO: TFMTBCDField;
    qryVendaDINHEIRO: TFMTBCDField;
    qryVendaTOTAL: TFMTBCDField;
    qryVendaPERCENTUAL: TFMTBCDField;
    qryVendaPERCENTUAL_ACRESCIMO: TFMTBCDField;
    qryVendaACRESCIMO: TFMTBCDField;
    qryVendaPEDIDO: TStringField;
    qryItemPRECO: TFMTBCDField;
    qryItemVALOR_ITEM: TFMTBCDField;
    qryItemVDESCONTO: TFMTBCDField;
    qryItemTOTAL: TFMTBCDField;
    qryItemACRESCIMO: TFMTBCDField;
    qryProdE_MEDIO: TFMTBCDField;
    qryConta_MovimentoENTRADA: TFMTBCDField;
    qryConta_MovimentoSAIDA: TFMTBCDField;
    qrySomaTOTAL: TFMTBCDField;
    qryTabelaACRESCIMO: TFMTBCDField;
    qryItemQTD: TFMTBCDField;
    qryItemE_MEDIO: TFMTBCDField;
    qryItemQTD_DEVOLVIDA: TFMTBCDField;
    qryClienteCODIGO: TIntegerField;
    qryClienteRAZAO: TStringField;
    qryClienteCNPJ: TStringField;
    qryClienteENDERECO: TStringField;
    qryClienteNUMERO: TStringField;
    qryClienteBAIRRO: TStringField;
    qryClienteMUNICIPIO: TStringField;
    qryClienteUF: TStringField;
    qryClienteCEP: TStringField;
    qryClienteFONE1: TStringField;
    qryClienteCELULAR1: TStringField;
    qryComposicaoQUANTIDADE: TFMTBCDField;
    qryVendaTOTAL_TROCA: TFMTBCDField;
    qryContas: TFDQuery;
    qryContasCODIGO: TIntegerField;
    qryContasDESCRICAO: TStringField;
    qryContasTIPO: TStringField;
    qryContasDATA_ABERTURA: TDateField;
    qryContasID_USUARIO: TIntegerField;
    qryContasEMPRESA: TIntegerField;
    qryContasLOTE: TIntegerField;
    qryContasSITUACAO: TStringField;
    qryItemFK_GRADE: TIntegerField;
    qryGrade: TFDQuery;
    qryGradeFK_PRODUTO: TIntegerField;
    qryGradeDESCRICAO: TStringField;
    qryGradeQTD: TFMTBCDField;
    dsGrade: TDataSource;
    qryGradeCODIGO: TIntegerField;
    actBusca: TAction;
    qryConta_MovimentoTROCA: TFMTBCDField;
    qryConta_MovimentoSALDO: TFMTBCDField;
    qryGradePRECO: TFMTBCDField;
    qryVendaOS: TStringField;
    qryVendaFK_OS: TIntegerField;
    actReceber: TAction;
    Timer1: TTimer;
    qryVendaFORMA_PAGAMENTO: TStringField;
    Panel6: TPanel;
    Panel2: TPanel;
    imgLogo: TImage;
    qryPesqConta: TFDQuery;
    qryPesqContaCODIGO: TIntegerField;
    qryPesqContaDESCRICAO: TStringField;
    qryPesqContaTIPO: TStringField;
    qryPesqContaDATA_ABERTURA: TDateField;
    qryPesqContaID_USUARIO: TIntegerField;
    qryPesqContaEMPRESA: TIntegerField;
    qryPesqContaLOTE: TIntegerField;
    qryPesqContaSITUACAO: TStringField;
    qryItemOS: TStringField;
    OpenPicture: TOpenPictureDialog;
    Label1: TLabel;
    qryPesqProdCODIGO: TIntegerField;
    qryPesqProdDESCRICAO: TStringField;
    qryPesqProdCFOP: TStringField;
    qryPesqProdCODBARRA: TStringField;
    qryPesqProdNCM: TStringField;
    qryPesqProdREFERENCIA: TStringField;
    qryPesqProdPR_VENDA: TFMTBCDField;
    qryPesqProdPRECO_ATACADO: TFMTBCDField;
    qryPesqProdQTD_ATACADO: TFMTBCDField;
    qryPesqProdQTD_ATUAL: TFMTBCDField;
    qryPesqProdUNIDADE: TStringField;
    qryPesqProdEFISCAL: TStringField;
    qryPesqProdE_MEDIO: TFMTBCDField;
    qryPesqProdLOCALIZACAO: TStringField;
    qryPesqProdPRECO_PROMO_VAREJO: TFMTBCDField;
    qryPesqProdPRECO_PROMO_ATACADO: TFMTBCDField;
    qryPesqProdPRECO_VARIAVEL: TStringField;
    qryPesqProdDESCONTO: TCurrencyField;
    qryPesqProdINICIO_PROMOCAO: TDateField;
    qryPesqProdFIM_PROMOCAO: TDateField;
    qryPesqProdSERVICO: TStringField;
    qryPesqProdREMEDIO: TStringField;
    qryPesqProdGRADE: TStringField;
    qryPesqProdPREFIXO_BALANCA: TStringField;
    qryPesqProdVIRTUAL_PRECO: TExtendedField;
    actAbrir: TAction;
    ACBrBAL1: TACBrBAL;
    StatusBar1: TStatusBar;
    qryPesqProdPRODUTO_PESADO: TStringField;
    qryPesqProdQTD_FISCAL: TFMTBCDField;
    qryProdQTD_FISCAL: TFMTBCDField;
    qryItemQTD_FISCAL: TExtendedField;
    actCliente: TAction;
    actReimprimir: TAction;
    qryPesqProdSERIAL: TStringField;
    Timer2: TTimer;
    qtdAtacado: TFDQuery;
    Panel1: TPanel;
    dsMesas: TDataSource;
    frxReport: TfrxReport;
    frxDBEmpresa: TfrxDBDataset;
    frxDBPedido: TfrxDBDataset;
    frxDBItens: TfrxDBDataset;
    qryVendaFK_MESA: TIntegerField;
    qryItemDESCRICAO_OBS: TStringField;
    qryItemOBSERVACAO: TStringField;
    qryBuscaFone: TFDQuery;
    qryBuscaFoneCODIGO: TIntegerField;
    qryBuscaFoneFANTASIA: TStringField;
    qryBuscaFoneENDERECO: TStringField;
    qryBuscaFoneNUMERO: TStringField;
    qryBuscaFoneBAIRRO: TStringField;
    qryBuscaFoneMUNICIPIO: TStringField;
    qryBuscaFoneUF: TStringField;
    qryBuscaFoneCEP: TStringField;
    qryBuscaFoneCOMPLEMENTO: TStringField;
    dsBuscaFone: TDataSource;
    qryVendaFK_ENTREGADOR: TIntegerField;
    qryEntregador: TFDQuery;
    qryEntregadorCODIGO: TIntegerField;
    qryEntregadorNOME: TStringField;
    dsEntregador: TDataSource;
    qryVendaVIRTUAL_ENTREGADOR: TStringField;
    qryVendaNOME: TStringField;
    qryBuscaFoneCELULAR1: TStringField;
    grpTotalGeralP: TGroupBox;
    DBText1: TDBText;
    PopupMenu1: TPopupMenu;
    actTEF: TAction;
    actDesconto: TAction;
    Panel7: TPanel;
    qryVendaVIRTUAL_CELULAR: TStringField;
    qryVendaVIRTUAL_ENDERECO: TStringField;
    qryVendaVIRTUAL_NUMERO: TStringField;
    qryVendaVIRTUAL_BAIRRO: TStringField;
    qryClienteCOMPLEMENTO: TStringField;
    qryVendaVIRTUAL_COMPLEMENTO: TStringField;
    qryPedido: TFDQuery;
    actImprimeItem: TAction;
    actConsultaPedido: TAction;
    actTranfereMesa: TAction;
    actAtualizaMesa: TAction;
    qryPedidoCODIGO: TIntegerField;
    qryProdTIPO_ALIMENTO: TStringField;
    qryItemVIRTUAL_TIPO_ALIMENTO: TStringField;
    actAbrirMesa: TAction;
    qryProdULTFORN: TIntegerField;
    qryPesqProdULTFORN: TIntegerField;
    qryProdFOTO: TBlobField;
    qryPesqProdFOTO: TBlobField;
    qryProdCOMISSAO: TCurrencyField;
    qryProdVALORCOMISSAO: TFMTBCDField;
    actMenu: TAction;
    Panel9: TPanel;
    lbCaixaIni: TLabel;
    pnAbrirCaixa: TPanel;
    PageControl2: TPageControl;
    TabPDV: TTabSheet;
    PanelPDV: TPanel;
    grpSelecaoP: TGroupBox;
    EdtProdutoP: TEdit;
    Panel10: TPanel;
    Label4: TLabel;
    grpTotalP: TGroupBox;
    lblTotalP: TLabel;
    GrpQtdP: TGroupBox;
    edtQtdP: TEdit;
    GrpPrecoP: TGroupBox;
    edtPrecoP: TEdit;
    DBGridP: TDBGridEh;
    DBGridBuscaP: TDBGridEh;
    Label3: TLabel;
    Label10: TLabel;
    Label11: TLabel;
    btnMenu: TSpeedButton;
    Panel5: TPanel;
    Panel4: TPanel;
    Panel8: TPanel;
    btnCancelVenda: TSpeedButton;
    Panel11: TPanel;
    btnVendedor: TSpeedButton;
    Panel12: TPanel;
    btnDelItem: TSpeedButton;
    Panel13: TPanel;
    btnImportar: TSpeedButton;
    Panel14: TPanel;
    btnDesconto: TSpeedButton;
    Panel22: TPanel;
    btnReceber: TSpeedButton;
    Panel23: TPanel;
    btnConsultarPedido: TSpeedButton;
    Panel26: TPanel;
    btnFinaliza: TSpeedButton;
    SpeedButton2: TSpeedButton;
    Label12: TLabel;
    Panel27: TPanel;
    btnBuscarCliente: TSpeedButton;
    qryConsultaCliente: TFDQuery;
    dsConsultaCliente: TDataSource;
    qryConsultaClienteRAZAO: TStringField;
    Panel28: TPanel;
    pnPessoa: TPanel;
    DBGridPes: TDBGridEh;
    btnBuscaAvancada: TSpeedButton;
    s1: TMenuItem;
    R1: TMenuItem;
    A1: TMenuItem;
    R2: TMenuItem;
    S2: TMenuItem;
    R3: TMenuItem;
    N1: TMenuItem;
    T1: TMenuItem;
    C1: TMenuItem;
    spbNovaVenda: TSpeedButton;
    btnOrdemEntrega: TSpeedButton;
    actNovaVenda: TAction;
    actOrcamento: TAction;
    actFinalizar: TAction;
    Panel15: TPanel;
    btnConsultarPreco: TSpeedButton;
    btnOS: TSpeedButton;
    Panel3: TPanel;
    Panel25: TPanel;
    Panel30: TPanel;
    actFiscal: TAction;
    Panel35: TPanel;
    Panel36: TPanel;
    pnFecharCaixa: TPanel;
    Label5: TLabel;
    Image1: TImage;
    Panel29: TPanel;
    qryVendasTerminal: TFDQuery;
    Panel32: TPanel;
    btnOrcamento: TSpeedButton;
    Panel33: TPanel;
    btnSalvar: TSpeedButton;
    actOS: TAction;
    actResumo: TAction;
    actSangria: TAction;
    dsTabelaPreco: TDataSource;
    qryTabelaPreco: TFDQuery;
    qryTabelaPrecoCODIGO: TIntegerField;
    qryTabelaPrecoDESCRICAO: TStringField;
    qryTabelaPrecoPRECO: TFMTBCDField;
    qryTabelaPrecoFK_PRODUTO: TIntegerField;
    qryPesqProdTABELA_PRECO: TStringField;
    DBGridTabelaPreco: TDBGridEh;
    Panel34: TPanel;
    btnDevolucao: TSpeedButton;
    Image2: TImage;
    DBImage1: TDBImage;
    Panel37: TPanel;
    btnAlterarQtd: TSpeedButton;
    Panel38: TPanel;
    Label2: TLabel;
    edtQtd: TEdit;
    lbDescricao: TLabel;
    lbPreco: TLabel;
    actSuprimento: TAction;
    IdIPWatch1: TIdIPWatch;
    edtBuscarCliente: TDBEdit;
    actAlteraQtd: TAction;
    Panel39: TPanel;
    btnNFe: TSpeedButton;
    Panel16: TPanel;
    edtLargura: TEdit;
    edtProfundidade: TEdit;
    edtTamanho: TEdit;
    Label6: TLabel;
    Label7: TLabel;
    Label8: TLabel;
    dsModelo: TDataSource;
    qryModelo: TFDQuery;
    qryModeloCODIGO: TIntegerField;
    qryModeloDESCRICAO: TStringField;
    qryModeloATIVO: TStringField;
    Label9: TLabel;
    qryItemLARGURA: TFMTBCDField;
    qryItemCOMPRIMENTO: TFMTBCDField;
    BitBtn1: TBitBtn;
    BitBtn2: TBitBtn;
    qryItemQTD_MODELO: TFMTBCDField;
    Label13: TLabel;
    edtPrecoMateriaPrima: TEdit;
    DBGridEh1: TDBGridEh;
    edtPesqMateriaPrima: TEdit;
    qryPesqMateriaPrima: TFDQuery;
    dsPesqMateriaPrima: TDataSource;
    qryPesqMateriaPrimaCODIGO: TIntegerField;
    qryPesqMateriaPrimaDESCRICAO: TStringField;
    qryPesqMateriaPrimaCFOP: TStringField;
    qryPesqMateriaPrimaCODBARRA: TStringField;
    qryPesqMateriaPrimaNCM: TStringField;
    qryPesqMateriaPrimaREFERENCIA: TStringField;
    qryPesqMateriaPrimaPR_VENDA: TFMTBCDField;
    qryPesqMateriaPrimaPRECO_ATACADO: TFMTBCDField;
    qryPesqMateriaPrimaQTD_ATACADO: TFMTBCDField;
    qryPesqMateriaPrimaQTD_ATUAL: TFMTBCDField;
    qryPesqMateriaPrimaQTD_FISCAL: TFMTBCDField;
    qryPesqMateriaPrimaUNIDADE: TStringField;
    qryPesqMateriaPrimaEFISCAL: TStringField;
    qryPesqMateriaPrimaE_MEDIO: TFMTBCDField;
    qryPesqMateriaPrimaLOCALIZACAO: TStringField;
    qryPesqMateriaPrimaPRODUTO_PESADO: TStringField;
    qryPesqMateriaPrimaPRECO_PROMO_VAREJO: TFMTBCDField;
    qryPesqMateriaPrimaPRECO_PROMO_ATACADO: TFMTBCDField;
    qryPesqMateriaPrimaPRECO_VARIAVEL: TStringField;
    qryPesqMateriaPrimaDESCONTO: TCurrencyField;
    qryPesqMateriaPrimaINICIO_PROMOCAO: TDateField;
    qryPesqMateriaPrimaFIM_PROMOCAO: TDateField;
    qryPesqMateriaPrimaSERVICO: TStringField;
    qryPesqMateriaPrimaREMEDIO: TStringField;
    qryPesqMateriaPrimaGRADE: TStringField;
    qryPesqMateriaPrimaSERIAL: TStringField;
    qryPesqMateriaPrimaPREFIXO_BALANCA: TStringField;
    qryPesqMateriaPrimaFOTO: TBlobField;
    qryPesqMateriaPrimaULTFORN: TIntegerField;
    qryPesqMateriaPrimaTABELA_PRECO: TStringField;
    qryPesqMateriaPrimaVIRTUAL_PRECO: TFloatField;
    txtDescricaoMateriaPrima: TLabel;
    Label14: TLabel;
    edtAcrescimoPorcentagemMateriaPrima: TEdit;
    edtAcrescimoValorMateriaPrima: TEdit;
    Label15: TLabel;
    Label16: TLabel;
    edtTotalMateriaPrima: TEdit;
    Label17: TLabel;
    qryItemID_MODELO: TIntegerField;
    qryItemDESCRICAO_M: TStringField;
    qryPesqProdFABRICADO: TStringField;
    qryPesqMateriaPrimaFABRICADO: TStringField;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure EdtProdutoPChange(Sender: TObject);
    procedure btnDelIteClick(Sender: TObject);
    //procedure btnFinalizClick(Sender: TObject);
    procedure qryVendaDESCONTOValidate(Sender: TField);
    procedure EdtProdutoPKeyPress(Sender: TObject; var Key: Char);
    procedure EdtProdutoPKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure qryVendaAfterOpen(DataSet: TDataSet);
    procedure qryItemBeforeOpen(DataSet: TDataSet);
    procedure qryVendaBeforeOpen(DataSet: TDataSet);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);

    procedure edtQtdPEnter(Sender: TObject);
    procedure edtPrecoPExit(Sender: TObject);

    procedure dsVendaDataChange(Sender: TObject; Field: TField);
    procedure qryConta_MovimentoBeforePost(DataSet: TDataSet);
    procedure edtPrecoPKeyPress(Sender: TObject; var Key: Char);
    procedure edtQtdPExit(Sender: TObject);
    procedure DBGridBuscaPDblClick(Sender: TObject);
    procedure DBGridBuscaPKeyPress(Sender: TObject; var Key: Char);
    procedure qryPesqProdCalcFields(DataSet: TDataSet);
    procedure edtQtdPKeyPress(Sender: TObject; var Key: Char);
    //procedure edtNumeroKeyPress(Sender: TObject; var Key: Char);
    procedure qryItemAfterDelete(DataSet: TDataSet);
    procedure qryItemBeforeDelete(DataSet: TDataSet);
    procedure DBGridPDblClick(Sender: TObject);
    //procedure BtnSuprimentClick(Sender: TObject);
    procedure qryItemQTDValidate(Sender: TField);
    procedure qryItemVALOR_ITEMValidate(Sender: TField);
    procedure actReceberExecute(Sender: TObject);
    procedure Timer1Timer(Sender: TObject);
    procedure qryVendaAfterDelete(DataSet: TDataSet);
    procedure qryVendaAfterPost(DataSet: TDataSet);
    procedure qryItemBeforePost(DataSet: TDataSet);
    procedure qryItemAfterPost(DataSet: TDataSet);
    procedure actAbrirExecute(Sender: TObject);
    procedure ACBrBAL1LePeso(Peso: Double; Resposta: AnsiString);
    //procedure actClienteExecute(Sender: TObject);
    procedure Timer2Timer(Sender: TObject);
    procedure btnImportaClick(Sender: TObject);
    procedure btnVendedClick(Sender: TObject);
    //procedure btnCaixClick(Sender: TObject);
    procedure btnCancVendaClick(Sender: TObject);
    procedure DBCtrlGrid1PaintPanel(DBCtrlGrid: TDBCtrlGrid; Index: Integer);
    procedure edtProdutoREnter(Sender: TObject);
    procedure edtProdutoDEnter(Sender: TObject);
    procedure dsItemDataChange(Sender: TObject; Field: TField);
    procedure ImgOcupadoClick(Sender: TObject);
    procedure btnFecharMesaClick(Sender: TObject);
    procedure qryItemCalcFields(DataSet: TDataSet);
    procedure edtFoneChange(Sender: TObject);
    procedure btnEntregadorClick(Sender: TObject);
    //procedure btnTeClick(Sender: TObject);
    //procedure actTEFExecute(Sender: TObject);
    procedure btnDescontClick(Sender: TObject);
    procedure actDescontoExecute(Sender: TObject);
    procedure edtProdutoRChange(Sender: TObject);
    procedure qryVendaCalcFields(DataSet: TDataSet);
    procedure PageControl2Change(Sender: TObject);
    procedure btnAbrirMesaClick(Sender: TObject);
    procedure actAbrirMesaExecute(Sender: TObject);
    //procedure btnReimprimiClick(Sender: TObject);
    //procedure actReimprimirExecute(Sender: TObject);
    procedure imgLogoDblClick(Sender: TObject);
    procedure btnMenuClick(Sender: TObject);
    procedure Label9Click(Sender: TObject);
    procedure Image1Click(Sender: TObject);
    procedure btnMenuMouseEnter(Sender: TObject);
    procedure btnMenuMouseLeave(Sender: TObject);
    procedure spbNovaVendaClick(Sender: TObject);
    procedure btnConsultaPrecoMouseEnter(Sender: TObject);
    procedure btnConsultaPrecoMouseLeave(Sender: TObject);
    procedure btnConsultaPrecoClick(Sender: TObject);
    procedure btnCancelVendaMouseEnter(Sender: TObject);
    procedure btnCancelVendaMouseLeave(Sender: TObject);
    procedure btnCancelVendaClick(Sender: TObject);
    procedure btnVendedorClick(Sender: TObject);
    procedure btnVendedorMouseEnter(Sender: TObject);
    procedure btnVendedorMouseLeave(Sender: TObject);
    procedure btnDelItemClick(Sender: TObject);
    procedure btnDelItemMouseEnter(Sender: TObject);
    procedure btnDelItemMouseLeave(Sender: TObject);
    procedure btnImportarClick(Sender: TObject);
    procedure btnImportarMouseEnter(Sender: TObject);
    procedure btnImportarMouseLeave(Sender: TObject);
    procedure btnDescontoClick(Sender: TObject);
    procedure btnDescontoMouseEnter(Sender: TObject);
    procedure btnDescontoMouseLeave(Sender: TObject);
    procedure btnNovaVendaClick(Sender: TObject);
    //procedure btnCaixaMouseEnter(Sender: TObject);
    //procedure btnCaixaMouseLeave(Sender: TObject);
    //procedure btnSangriaMouseEnter(Sender: TObject);
    //procedure btnSangriaMouseLeave(Sender: TObject);
    //procedure btnSangriaClick(Sender: TObject);
    //procedure btnCaixaClick(Sender: TObject);
    //procedure btnSuprimentoMouseEnter(Sender: TObject);
    //procedure btnSuprimentoMouseLeave(Sender: TObject);
    //procedure btnSuprimentoClick(Sender: TObject);
    procedure btnReceberMouseEnter(Sender: TObject);
    procedure btnReceberMouseLeave(Sender: TObject);
    procedure btnReceberClick(Sender: TObject);
    procedure btnFinalizaMouseEnter(Sender: TObject);
    procedure btnFinalizaMouseLeave(Sender: TObject);
    procedure btnFinalizaClick(Sender: TObject);
    //procedure btnResumoMouseEnter(Sender: TObject);
    //procedure btnResumoMouseLeave(Sender: TObject);
    //procedure btnResumoClick(Sender: TObject);
    //procedure btnTefMouseEnter(Sender: TObject);
    //procedure btnTefMouseLeave(Sender: TObject);
    //procedure btnTefClick(Sender: TObject);
    //procedure btnReimprimirMouseEnter(Sender: TObject);
    //procedure btnReimprimirMouseLeave(Sender: TObject);
    //procedure btnReimprimirClick(Sender: TObject);
    procedure btnBuscarClienteMouseEnter(Sender: TObject);
    procedure btnBuscarClienteMouseLeave(Sender: TObject);
    procedure btnBuscarClienteClick(Sender: TObject);
    procedure DBEdit27KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure DBGridPesDblClick(Sender: TObject);
    procedure DBGridPesKeyPress(Sender: TObject; var Key: Char);
    procedure edtBuscarClienteChange(Sender: TObject);
    procedure edtBuscarClienteKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure btnBuscaAvancadaClick(Sender: TObject);
    procedure btnBuscaAvancadaMouseEnter(Sender: TObject);
    procedure btnBuscaAvancadaMouseLeave(Sender: TObject);
    procedure btnConsultarPedidoClick(Sender: TObject);
    procedure btnConsultarPedidoMouseEnter(Sender: TObject);
    procedure btnConsultarPedidoMouseLeave(Sender: TObject);
    //procedure btnClientesClick(Sender: TObject);
    //procedure btnClientesMouseEnter(Sender: TObject);
    //procedure btnClientesMouseLeave(Sender: TObject);
    procedure actFinalizarExecute(Sender: TObject);
    procedure actNovaVendaExecute(Sender: TObject);
    procedure btnConsultarPrecoClick(Sender: TObject);
    procedure btnConsultarPrecoMouseEnter(Sender: TObject);
    procedure btnConsultarPrecoMouseLeave(Sender: TObject);
    //procedure Panel30Click(Sender: TObject);
    //procedure Panel30MouseEnter(Sender: TObject);
    //procedure Panel30MouseLeave(Sender: TObject);
    //procedure Panel29MouseLeave(Sender: TObject);
    //procedure Panel31MouseLeave(Sender: TObject);
    //procedure Panel34MouseEnter(Sender: TObject);
    procedure spbNovaVendaMouseEnter(Sender: TObject);
    procedure spbNovaVendaMouseLeave(Sender: TObject);
    procedure btnOSMouseEnter(Sender: TObject);
    procedure btnOSMouseLeave(Sender: TObject);
    procedure btnOrdemEntregaMouseEnter(Sender: TObject);
    procedure btnOrdemEntregaMouseLeave(Sender: TObject);
    procedure btnOSClick(Sender: TObject);
    procedure actConsultaPedidoExecute(Sender: TObject);
    procedure actOrcamentoExecute(Sender: TObject);
    procedure actFiscalExecute(Sender: TObject);
    //procedure FormClick(Sender: TObject);
    procedure Panel31MouseEnter(Sender: TObject);
    procedure btnOrcamentoMouseEnter(Sender: TObject);
    procedure btnOrcamentoMouseLeave(Sender: TObject);
    procedure btnOrcamentoClick(Sender: TObject);
    procedure btnSalvarMouseEnter(Sender: TObject);
    procedure btnSalvarMouseLeave(Sender: TObject);
    procedure btnSalvarClick(Sender: TObject);
    procedure btnOrdemEntregaClick(Sender: TObject);
    procedure actOSExecute(Sender: TObject);
    //procedure actResumoExecute(Sender: TObject);
    //procedure actSangriaExecute(Sender: TObject);
    procedure btnOrcamentoVendaClick(Sender: TObject);
    procedure DBGridBuscaPEnter(Sender: TObject);
    procedure Panel26Resize(Sender: TObject);
    procedure btnDevolucaoClick(Sender: TObject);
    procedure btnDevolucaoMouseEnter(Sender: TObject);
    procedure btnDevolucaoMouseLeave(Sender: TObject);
    procedure btnAlterarQtdClick(Sender: TObject);
    procedure edtQtdKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure btnAlterarQtdMouseEnter(Sender: TObject);
    procedure btnAlterarQtdMouseLeave(Sender: TObject);
    //procedure actSuprimentoExecute(Sender: TObject);
    procedure IdIPWatch1StatusChanged(Sender: TObject);
    procedure DBGridBuscaPCellClick(Column: TColumnEh);
    procedure actAlteraQtdExecute(Sender: TObject);
    procedure btnNFeClick(Sender: TObject);
    procedure btnNFeMouseEnter(Sender: TObject);
    procedure btnNFeMouseLeave(Sender: TObject);
    procedure edtLarguraChange(Sender: TObject);
    procedure edtProfundidadeChange(Sender: TObject);
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure DBComboBox1Enter(Sender: TObject);
    procedure edtLarguraEnter(Sender: TObject);
    procedure DBComboBox1KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure edtLarguraKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure BitBtn1Click(Sender: TObject);
    procedure BitBtn2Click(Sender: TObject);
    procedure edtProfundidadeKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure edtPrecoMateriaPrimaClick(Sender: TObject);
    procedure edtPrecoMateriaPrimaKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure edtPesqMateriaPrimaChange(Sender: TObject);
    procedure edtPesqMateriaPrimaKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure edtPesqMateriaPrimaKeyPress(Sender: TObject; var Key: Char);
    procedure DBGridEh1CellClick(Column: TColumnEh);
    procedure qryPesqMateriaPrimaCalcFields(DataSet: TDataSet);
    procedure DBGridEh1DblClick(Sender: TObject);
    procedure edtAcrescimoPorcentagemMateriaPrimaKeyDown(Sender: TObject;
      var Key: Word; Shift: TShiftState);
    procedure edtAcrescimoValorMateriaPrimaKeyDown(Sender: TObject;
      var Key: Word; Shift: TShiftState);
    procedure edtProfundidadeExit(Sender: TObject);
    procedure edtAcrescimoPorcentagemMateriaPrimaExit(Sender: TObject);
    procedure edtAcrescimoValorMateriaPrimaExit(Sender: TObject);
    procedure edtLarguraKeyPress(Sender: TObject; var Key: Char);
    procedure edtProfundidadeKeyPress(Sender: TObject; var Key: Char);
    //procedure Panel44Click(Sender: TObject);


  private
    vSql, vSQLVenda, vSqlBusca: string;
    ehCaixaRapido: string;
    vSerie: String;
    PesquisaProduto, nImportar: Boolean;
    FGrade: Integer;

    vPosicao, vItem: Integer;
    vPesquisa: string;

    procedure BuscaPreco(Descricao: String);
    procedure BuscaPrecoMateriaPrima(Descricao: String);
    procedure ItemDesconhecido;
    procedure InsereItem(Descricao, OBSERVACAO: String;
      Preco, Qtd, Total: Extended);
    function GerouNFCe(operacao: string): string;
    function ValidaItem(Qtd, vPreco: Extended): Boolean;
    procedure PrecoVariavel;
    procedure tamanho;
    function UltimaVenda: Integer;
    procedure InsereComposicao(produto: Integer);
    procedure PesquisaCodBarra(FPesquisa, Descricao: String);
    procedure PesquisaDescricao(FPesquisa: String);
    procedure PesquisaDescricaoMateriaPrima(FPesquisa: String);
    procedure AtualizaVendedor;
    procedure AtualziaPrecoGrade(Qtd: Extended);
    procedure ChamaGrade;
    procedure PesquisaCodBarraBalanca(FPesquisa: string);
    procedure PesquisaCodBarraGeral(FPesquisa: string);
    procedure FinalizaVenda;
    procedure CalculaTotalVenda(idVenda: Integer);
    function RetornaMaiorItem(idVenda: Integer): Integer;
    procedure CarregaImagem;
    procedure GeraSqlProduto;
    procedure AtualizaDescricao(Descricao: String);
    procedure FechaVenda(Descricao: String);
    function validaComposicao(produto: Integer): Boolean;
    procedure BuscaPeso;
    procedure ConfiguraBalanca;
    procedure ChamaSerial;
    function BaixaSerial(idProduto: Integer; idVenda: Integer; valor: Extended;
      Serie: String): Integer;
    procedure ProdutoCalcFieldGenerico(Descricao: string; Qtd: Extended);
    procedure ProdutoCalcFieldGenericoMateriaPrima(Descricao: string; Qtd: Extended);
    procedure DescricaoSetFocus(Op: String);
    procedure DescricaoMateriaPrimaSetFocus(Op: String);
    procedure DBGridSetFocus;
    procedure PreencheBuscaPreco;
    procedure PreencheBuscaPrecoMateriaPrima;
    procedure PesquisaCliente;
    procedure AtualizaEntregador;
    procedure SituacaoPedido;
    procedure ChamaLogin;
    procedure ConfiguraTipodecaixa;
    procedure PesquisaTipoTerminal;
    procedure HabilitaPreVenda;
    procedure ChamaAbertura;
    procedure CaixaHoje;
    procedure ChecaATACADO(produto, venda: Integer);
    procedure Restaurante_Status_Mesa;
    procedure Retaurante_abre_mesa;
    procedure AbreVenda(Codigo: Integer; tela: String);
    procedure AtualizaPreco(NovoPreco: Extended);
    procedure CadPessoaRapido(Codigo: Integer);
    procedure VerificaFone;
    procedure ImprimeRestaurante;
    procedure ReabrirPedido;
    procedure ImprimeDelivery;
    procedure ImprimeItem;
    procedure GetIdCliente;
    procedure ImprimeRestaurantePadrao;
    procedure ImprimeRestauranteBar;
    procedure ImprimeRestauranteCozinha;
    procedure HabilitaBotoes;
    procedure Carregaimagem2(Field : TField; Img : TImage);
    procedure ConfiguraBotoes;
    procedure MakeRounded(Control: TWinControl);
    procedure AtualizaTabelaPreco;
    procedure GetCliente;


    { Private declarations }
  public
    CodigoGrade, CodigoFKFornecedor, vendaFinalizada, tipoFinalizacao, pedidoImportado: Integer;
    PodeAtualizarEstoque, caixaUnico, CaixaAberto, vFinalizar: Boolean;
    valor, valorcomissao, percentual : Real;
    vPessoa: String;
    ocultaNaoFiscal, codigoVendedor, idCaixaUnico, idLoteCaixaUnico, idUsuarioCaixaUnico, caixaUsado: Integer;
    procedure habilitacampos(campos: Boolean);
    procedure inserevenda;
    procedure FecharPrevenda;
    procedure MostraCaixa;
    procedure VendaExiste;
    procedure CalcTotalMesa;
    procedure ChamaTabelaPreco;
    procedure FecharCaixa;
    procedure AbreCaixa;
    function  SituacaoCaixa : Boolean;
    function BuscaNumeroVenda: Integer;

    { Public declarations }
  end;

var
  FrmBalcao: TFrmBalcao;

implementation

{$R *.dfm}

uses Udados, uAbreCaixa, uSuprimento_Sangria,
  uFormaPagamentoR, uImportar, uResumoCaixa, uSupervisor, uCadProduto,
  uBuscaPreco, uConsReceber, uPesquisaPrincipio, uSplash, uAcesso,
  uMenuImportarPDV, uConsVendedor, uGrade, uRemoveProduto, uCadPessoa, uResumo,
  uChave, uTransfComanda, uConsEntregador, uTef, uDmNFe, uDmPDV, uDesconhecido,
  uCadPessoaRapido, udmImpressao, uReimprimir, uDMEstoque, uBuscaClienteR,
  uAbreCaixaR, uPedidoVenda, uMenuImportarR, uImportarR, uCadOrcamento, uCadOS,
  uConsOS, uOrcamento, uFormaPagamento, uOrdemEntrega, uFechamentoVendaBalcao,
  uTabelaPrecoProdutoR, uDevolucao, uGeraEntrega, uConsNFe, uMenu;

function TFrmBalcao.BaixaSerial(idProduto: Integer; idVenda: Integer;
  valor: Extended; Serie: String): Integer;
begin
  Dados.qryConsulta.close;
  Dados.qryConsulta.SQL.Clear;
  Dados.qryConsulta.SQL.Add(' select codigo from produto_serial');
  Dados.qryConsulta.SQL.Add(' where');
  Dados.qryConsulta.SQL.Add(' id_produto=:id and numero_serie=:serie');
  Dados.qryConsulta.Params[0].Value := idProduto;
  Dados.qryConsulta.Params[1].Value := Serie;
  Dados.qryConsulta.Open;

  if not Dados.qryConsulta.IsEmpty then
  begin
    result := Dados.qryConsulta.Fields[0].AsInteger;
  end
  else
  begin
    result := 0;
    raise Exception.Create('Serial n�o foi encontrado!');
  end;

end;

procedure TFrmBalcao.BitBtn1Click(Sender: TObject);
begin
  edtPrecoP.Text := edtTotalMateriaPrima.Text;
  panel16.Visible := false;

  edtPrecoP.SetFocus;

end;

procedure TFrmBalcao.BitBtn2Click(Sender: TObject);
begin
  Panel16.Visible := false;

  EdtProdutoP.SetFocus;
end;

procedure TFrmBalcao.tamanho;
begin
  DBGridBuscaP.Left := DBGridP.Left;
  DBGridBuscaP.Width := DBGridP.Width;
  DBGridBuscaP.Top := DBGridP.Top;

  if Screen.Width = 1024 then
  begin
    if (Dados.qryEmpresa.FieldByName('MARMORARIA').AsString <> 'S') or (Dados.qryEmpresa.FieldByName('MARMORARIA').AsString = null) then
      begin
        DBGridP.Columns[0].Width := round(Screen.Width * 0.04);
        DBGridP.Columns[1].Width := round(Screen.Width * 0.05);
        DBGridP.Columns[2].Width := round(Screen.Width * 0.35);
        DBGridP.Columns[3].Visible := false;
        DBGridP.Columns[4].Visible := false;
        DBGridP.Columns[5].Visible := false;
        DBGridP.Columns[6].Visible := false;
        DBGridP.Columns[7].Width := round(Screen.Width * 0.06);
        DBGridP.Columns[8].Width := round(Screen.Width * 0.03);
        DBGridP.Columns[9].Width := round(Screen.Width * 0.07);
        DBGridP.Columns[10].Width := round(Screen.Width * 0.07);
      end
    else
      begin
        DBGridP.Columns[0].Width := round(Screen.Width * 0.03);
        DBGridP.Columns[1].Width := round(Screen.Width * 0.03);
        DBGridP.Columns[2].Width := round(Screen.Width * 0.28);
        DBGridP.Columns[3].Width := round(Screen.Width * 0.02);
        DBGridP.Columns[4].Width := round(Screen.Width * 0.10);
        DBGridP.Columns[5].Width := round(Screen.Width * 0.05);
        DBGridP.Columns[6].Width := round(Screen.Width * 0.05);
        DBGridP.Columns[7].Width := round(Screen.Width * 0.05);
        DBGridP.Columns[7].Title.Caption := 'Metro�/Qtd';
        DBGridP.Columns[8].Width := round(Screen.Width * 0.02);
        DBGridP.Columns[9].Width := round(Screen.Width * 0.06);
        DBGridP.Columns[10].Width := round(Screen.Width * 0.06);
      end;

  end
  else
  begin
    if (Dados.qryEmpresa.FieldByName('MARMORARIA').AsString <> 'S') or (Dados.qryEmpresa.FieldByName('MARMORARIA').AsString = null) then
      begin

        DBGridP.Columns[0].Width := round(Screen.Width * 0.05);
        DBGridP.Columns[1].Width := round(Screen.Width * 0.06);
        DBGridP.Columns[2].Width := round(Screen.Width * 0.40);
        DBGridP.Columns[3].Visible := false;
        DBGridP.Columns[4].Visible := false;
        DBGridP.Columns[5].Visible := false;
        DBGridP.Columns[6].Visible := false;
        DBGridP.Columns[7].Width := round(Screen.Width * 0.07);
        DBGridP.Columns[8].Width := round(Screen.Width * 0.02);
        DBGridP.Columns[9].Width := round(Screen.Width * 0.08);
        DBGridP.Columns[10].Width := round(Screen.Width * 0.09);
      end
    else
      begin
        DBGridP.Columns[0].Width := round(Screen.Width * 0.03);
        DBGridP.Columns[1].Width := round(Screen.Width * 0.03);
        DBGridP.Columns[2].Width := round(Screen.Width * 0.31);
        DBGridP.Columns[3].Width := round(Screen.Width * 0.02);
        DBGridP.Columns[4].Width := round(Screen.Width * 0.10);
        DBGridP.Columns[5].Width := round(Screen.Width * 0.05);
        DBGridP.Columns[6].Width := round(Screen.Width * 0.05);
        DBGridP.Columns[7].Width := round(Screen.Width * 0.05);
        DBGridP.Columns[7].Title.Caption := 'Metro�/Qtd';
        DBGridP.Columns[8].Width := round(Screen.Width * 0.02);
        DBGridP.Columns[9].Width := round(Screen.Width * 0.07);
        DBGridP.Columns[10].Width := round(Screen.Width * 0.07);
      end;

  end;

  GrpQtdP.Width := round(Panel10.Width * 0.21);
  GrpPrecoP.Width := round(Panel10.Width * 0.21);
  grpTotalP.Width := round(Panel10.Width * 0.22);
  grpTotalGeralP.Width := round(Panel10.Width * 0.36);

end;

procedure TFrmBalcao.Timer1Timer(Sender: TObject);
begin
  StatusBar1.Panels[3].Text := DateTimeToStr(now);
end;

procedure TFrmBalcao.Timer2Timer(Sender: TObject);
begin
  {
    Timer2.Enabled := false;

    if not Dados.vRetaguarda then
    begin
    if Dados.BloqueiaValidade then
    begin
    try
    frmChave := TfrmChave.Create(Application);
    frmChave.ShowModal;
    finally
    frmChave.Release;
    Application.Terminate;
    end;
    end;
    end; }
end;

procedure TFrmBalcao.btnFecharMesaClick(Sender: TObject);
begin
  btnFinalizaClick(self);
end;

function TiraPontos(Str: string): string;
var
  i, Count: Integer;
begin
  SetLength(result, Length(Str));
  Count := 0;
  for i := 1 to Length(Str) do
  begin
    if not CharInSet(Str[i], ['/', ',', '-', '.', ')', '(', ' ', '_']) then
    begin
      inc(Count);
      result[Count] := Str[i];
    end;
  end;
  SetLength(result, Count);
end;

procedure TFrmBalcao.btnVendedClick(Sender: TObject);
begin

  if not btnVendedor.Visible then
    exit;

  try
    frmconsVendedor := TfrmconsVendedor.Create(Application);
    frmconsVendedor.idVendedor := qryVendaFK_VENDEDOR.Value;
    frmconsVendedor.vNome := qryVendaVIRTUAL_VENDEDOR.Value;
    frmconsVendedor.ShowModal;
  finally
    AtualizaVendedor;
    StatusBar1.Panels[2].Text := 'Vendedor:' + frmconsVendedor.vNome;
    frmconsVendedor.Release;
  end;
end;

procedure TFrmBalcao.btnVendedorClick(Sender: TObject);
begin
if not btnVendedor.Visible then
    exit;

  try
    frmconsVendedor := TfrmconsVendedor.Create(Application);
    frmconsVendedor.idVendedor := qryVendaFK_VENDEDOR.Value;
    frmconsVendedor.vNome := qryVendaVIRTUAL_VENDEDOR.Value;
    frmconsVendedor.ShowModal;
  finally
    if BuscaNumeroVenda <> 0 then
      begin
        AtualizaVendedor;
      end;
    StatusBar1.Panels[2].Text := 'Vendedor:' + frmconsVendedor.vNome;
    frmconsVendedor.Release;
  end;
end;

procedure TFrmBalcao.btnVendedorMouseEnter(Sender: TObject);
begin
Panel11.Color := $00B90000;
//btnVendedor.Font.Color := clWhite;
end;

procedure TFrmBalcao.btnVendedorMouseLeave(Sender: TObject);
begin
  Panel11.Color := $0080552D;
//btnVendedor.Font.Color := clBlack;
end;

procedure TFrmBalcao.BuscaPreco(Descricao: String);
var
  vQtd, vPreco, vTotal: real;
  sTotal: string;
  vPrefixo, vProduto: String;
begin

  if (qryPesqProdPREFIXO_BALANCA.AsString = '') or
    (qryPesqProdPREFIXO_BALANCA.IsNull) then
  begin
    vProduto := '0';
    vProduto := '1';

  end
  else
  begin
    vProduto := copy(Descricao, 1, 1);
    vPrefixo := Dados.qryConfigPREFIXO_BALANCA.Value;
  end;

  if vPrefixo = vProduto then
  begin
    if Pos('*', Descricao) = 0 then
    begin
      case Dados.qryConfigMODELO_BALANCA.Value of
        3:
          begin

            sTotal := copy(Descricao, 8, 5);
            vTotal := StrToFloatDef(sTotal, 0);
            vTotal := RoundABNT((vTotal / 100), 2);
            vQtd := 1;

            if qryPesqProdVIRTUAL_PRECO.Value > 0 then
              vQtd := vTotal / qryPesqProdVIRTUAL_PRECO.AsFloat;

            if PageControl2.ActivePage = TabPDV then
            begin
              edtPrecoP.Text := FormatFloat('0.00',
                qryPesqProdVIRTUAL_PRECO.AsFloat);
              lblTotalP.Caption := FloatToStr(vTotal);
              edtQtdP.Text := FloatToStr(SimpleRoundTo(vQtd, -3));
            end;

          end;

        1, 2, 4:
          begin

            sTotal := copy(Descricao, 8, 5);
            vQtd := (StrToFloatDef(sTotal, 1));

            if (qryPesqProdUNIDADE.Value = 'UN') or
              (qryPesqProdUNIDADE.Value = 'PC') then
              vQtd := Dados.truncar((vQtd), 3)
            else
              vQtd := Dados.truncar((vQtd / 1000), 3);

            vTotal := vQtd * qryPesqProdVIRTUAL_PRECO.AsFloat;
            vTotal := RoundABNT(vTotal, 2);


            if PageControl2.ActivePage = TabPDV then
            begin
              edtPrecoP.Text := FormatFloat('0.00',
                qryPesqProdVIRTUAL_PRECO.AsFloat);
              lblTotalP.Caption := FormatFloat('0.00', vTotal);
              edtQtdP.Text := FormatFloat('0.000', vQtd);
            end;

          end;
      end;
    end
    else
    begin
      try
        vQtd := StrToFloatDef((copy(Descricao, 1, Pos('*', Descricao) - 1)), 1);


        if PageControl2.ActivePage = TabPDV then
        begin
          edtQtdP.Text := copy(Descricao, 1, Pos('*', Descricao) - 1);
          edtPrecoP.Text := FormatFloat('0.00',
            qryPesqProdVIRTUAL_PRECO.AsFloat);

          lblTotalP.Caption := FormatFloat('0.00',
            RoundABNT(qryPesqProdVIRTUAL_PRECO.AsFloat * vQtd, 2));

        end;

        if qryPesqProdGRADE.Value = 'S' then
        begin
          if qryGradePRECO.AsFloat > 0 then
          begin
            if PageControl2.ActivePage = TabPDV then // PDV
            begin
              edtPrecoP.Text := FormatFloat('0.00', qryGradePRECO.AsFloat);
              lblTotalP.Caption :=
                FormatFloat('0.00', RoundABNT(qryGradePRECO.AsFloat * vQtd, 2));
            end;

          end;
        end;

      except
        raise Exception.Create('N�o foi poss�vel Selecionar Produto!');
      end;
    end;
  end
  else
  begin
    if Pos('*', Descricao) > 1 then
    begin
      try
        vQtd := StrToFloatDef((copy(Descricao, 1, Pos('*', Descricao) - 1)), 1);

        if PageControl2.ActivePage = TabPDV then // PDV
        begin
          edtQtdP.Text := copy(Descricao, 1, Pos('*', Descricao) - 1);
          edtPrecoP.Text := FormatFloat('0.00',
            qryPesqProdVIRTUAL_PRECO.AsFloat);
          lblTotalP.Caption := FormatFloat('0.00',
            RoundABNT(qryPesqProdVIRTUAL_PRECO.AsFloat * vQtd, 2));
        end;

      except
        raise Exception.Create('N�o foi poss�vel Selecionar Produto!');
      end;
    end
    else
    begin
      if PageControl2.ActivePage = TabPDV then // PDV
      begin
        vQtd := StrToFloatDef(edtQtdP.Text, 1);
        edtPrecoP.Text := FormatFloat('0.00', qryPesqProdVIRTUAL_PRECO.AsFloat);
        lblTotalP.Caption := FormatFloat('0.00',
          RoundABNT(qryPesqProdVIRTUAL_PRECO.AsFloat * vQtd, 2));
      end;



    end;
  end;
end;

procedure TFrmBalcao.BuscaPrecoMateriaPrima(Descricao: String);
var
  vQtd, vPreco, vTotal: real;
  sTotal: string;
  vPrefixo, vProduto: String;
begin

  if (qryPesqMateriaPrimaPREFIXO_BALANCA.AsString = '') or
    (qryPesqMateriaPrimaPREFIXO_BALANCA.IsNull) then
  begin
    vProduto := '0';
    vProduto := '1';

  end
  else
  begin
    vProduto := copy(Descricao, 1, 1);
    vPrefixo := Dados.qryConfigPREFIXO_BALANCA.Value;
  end;

  if vPrefixo = vProduto then
  begin
    if Pos('*', Descricao) = 0 then
    begin
      case Dados.qryConfigMODELO_BALANCA.Value of
        3:
          begin

            sTotal := copy(Descricao, 8, 5);
            vTotal := StrToFloatDef(sTotal, 0);
            vTotal := RoundABNT((vTotal / 100), 2);
            vQtd := 1;

            if qryPesqMateriaPrimaVIRTUAL_PRECO.Value > 0 then
              vQtd := vTotal / qryPesqMateriaPrimaVIRTUAL_PRECO.AsFloat;

            if PageControl2.ActivePage = TabPDV then
            begin
              edtPrecoP.Text := FormatFloat('0.00',
                qryPesqMateriaPrimaVIRTUAL_PRECO.AsFloat);
              lblTotalP.Caption := FloatToStr(vTotal);
              edtQtdP.Text := FloatToStr(SimpleRoundTo(vQtd, -3));
            end;

          end;

        1, 2, 4:
          begin

            sTotal := copy(Descricao, 8, 5);
            vQtd := (StrToFloatDef(sTotal, 1));

            if (qryPesqMateriaPrimaUNIDADE.Value = 'UN') or
              (qryPesqMateriaPrimaUNIDADE.Value = 'PC') then
              vQtd := Dados.truncar((vQtd), 3)
            else
              vQtd := Dados.truncar((vQtd / 1000), 3);

            vTotal := vQtd * qryPesqMateriaPrimaVIRTUAL_PRECO.AsFloat;
            vTotal := RoundABNT(vTotal, 2);


            if PageControl2.ActivePage = TabPDV then
            begin
              edtPrecoP.Text := FormatFloat('0.00',
                qryPesqMateriaPrimaVIRTUAL_PRECO.AsFloat);
              lblTotalP.Caption := FormatFloat('0.00', vTotal);
              edtQtdP.Text := FormatFloat('0.000', vQtd);
            end;

          end;
      end;
    end
    else
    begin
      try
        vQtd := StrToFloatDef((copy(Descricao, 1, Pos('*', Descricao) - 1)), 1);


        if PageControl2.ActivePage = TabPDV then
        begin
          edtQtdP.Text := copy(Descricao, 1, Pos('*', Descricao) - 1);
          edtPrecoP.Text := FormatFloat('0.00',
            qryPesqMateriaPrimaVIRTUAL_PRECO.AsFloat);

          lblTotalP.Caption := FormatFloat('0.00',
            RoundABNT(qryPesqMateriaPrimaVIRTUAL_PRECO.AsFloat * vQtd, 2));

        end;

        if qryPesqMateriaPrimaGRADE.Value = 'S' then
        begin
          if qryGradePRECO.AsFloat > 0 then
          begin
            if PageControl2.ActivePage = TabPDV then // PDV
            begin
              edtPrecoP.Text := FormatFloat('0.00', qryGradePRECO.AsFloat);
              lblTotalP.Caption :=
                FormatFloat('0.00', RoundABNT(qryGradePRECO.AsFloat * vQtd, 2));
            end;

          end;
        end;

      except
        raise Exception.Create('N�o foi poss�vel Selecionar Produto!');
      end;
    end;
  end
  else
  begin
    if Pos('*', Descricao) > 1 then
    begin
      try
        vQtd := StrToFloatDef((copy(Descricao, 1, Pos('*', Descricao) - 1)), 1);

        if PageControl2.ActivePage = TabPDV then // PDV
        begin
          edtQtdP.Text := copy(Descricao, 1, Pos('*', Descricao) - 1);
          edtPrecoP.Text := FormatFloat('0.00',
            qryPesqMateriaPrimaVIRTUAL_PRECO.AsFloat);
          lblTotalP.Caption := FormatFloat('0.00',
            RoundABNT(qryPesqMateriaPrimaVIRTUAL_PRECO.AsFloat * vQtd, 2));
        end;

      except
        raise Exception.Create('N�o foi poss�vel Selecionar Produto!');
      end;
    end
    else
    begin
      if PageControl2.ActivePage = TabPDV then // PDV
      begin
        vQtd := StrToFloatDef(edtQtdP.Text, 1);
        edtPrecoP.Text := FormatFloat('0.00', qryPesqMateriaPrimaVIRTUAL_PRECO.AsFloat);
        lblTotalP.Caption := FormatFloat('0.00',
          RoundABNT(qryPesqMateriaPrimaVIRTUAL_PRECO.AsFloat * vQtd, 2));
      end;



    end;
  end;
end;

procedure TFrmBalcao.DBGridPDblClick(Sender: TObject);       // AINDA SERA CRIADO CONDI��O PRA EDI��O PRODUTO
begin
  Dados.vAutorizar := true;
      if not Dados.eSupervisor then
      begin

        try
          frmSupervisor := TFrmSupervisor.Create(Application);
          Dados.vAutorizar := false;
          frmSupervisor.ShowModal;
        finally
          frmSupervisor.Release;
        end;
      end;

      if not Dados.vAutorizar then
      begin
        exit;
      end;

  if not qryItem.IsEmpty then
  begin

    try
      FrmCadProduto := TFrmCadProduto.Create(Application);
      FrmCadProduto.qryProdutos.close;
      FrmCadProduto.qryProdutos.Params[0].Value := qryItemID_PRODUTO.Value;
      FrmCadProduto.qryProdutos.Open;
      FrmCadProduto.qryProdutos.Edit;

      FrmCadProduto.ShowModal;

    finally
      FrmCadProduto.Release;
      qryPesqProd.close;
      qryPesqProd.Open;
    end;

  end;
end;

procedure TFrmBalcao.DBGridPesDblClick(Sender: TObject);
begin
  qryVendaNOME.AsString := qryClienteRAZAO.Value;
end;

procedure TFrmBalcao.DBGridPesKeyPress(Sender: TObject; var Key: Char);
begin
if Key = #13 then

    qryVendaNOME.AsString := qryClienteRAZAO.Value;

end;

procedure TFrmBalcao.DBComboBox1Enter(Sender: TObject);
begin
  edtLargura.SetFocus;
end;

procedure TFrmBalcao.DBComboBox1KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key = VK_RETURN then
    edtLargura.SetFocus;
end;

procedure TFrmBalcao.DBCtrlGrid1PaintPanel(DBCtrlGrid: TDBCtrlGrid;
  Index: Integer);
begin

//  if PageControl2.ActivePage = tabRestaurante then
//  begin
//    if Dados.qryMesasSITUACAO.Value = 'L' then
//    begin
//      imgLivre.Visible := true;
//      ImgOcupado.Visible := false;
//    end
//    else
//    begin
//      imgLivre.Visible := false;
//      ImgOcupado.Visible := true;
//    end;
//  end;
end;

procedure TFrmBalcao.DBEdit27KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  //if Key = VK_UP then
    //qryCliente.Prior;
   //qryCliente.Next;

  if Key = VK_DOWN then
    begin
    DBGridPes.DataSource.DataSet.First;
    DBGridPes.SetFocus;
    //DBEdit27.Text := qryClienteRAZAO.Value;
    end;
end;

procedure TFrmBalcao.DBGridBuscaPCellClick(Column: TColumnEh);
begin

  PreencheBuscaPreco;

end;

procedure TFrmBalcao.DBGridBuscaPDblClick(Sender: TObject);
var
  vQtd: String;
begin
  try
    PesquisaProduto := false;

     vQtd := copy(EdtProdutoP.Text, 1, Pos('*', EdtProdutoP.Text));

      EdtProdutoP.Text := vQtd + qryPesqProdDESCRICAO.Value;
      EdtProdutoP.SetFocus;
      PreencheBuscaPreco;
      Panel16.Visible := true;
  finally
    PesquisaProduto := true;
  end;
end;

procedure TFrmBalcao.DBGridBuscaPEnter(Sender: TObject);
begin

  if qryPesqProdTABELA_PRECO.Value = 'S' then
        begin

          DBGridTabelaPreco.Visible := true;
        end
  else
        begin
          DBGridTabelaPreco.Visible := false;
        end;
end;

procedure TFrmBalcao.DBGridBuscaPKeyPress(Sender: TObject; var Key: Char);
begin
  if Key = #13 then
    DBGridBuscaPDblClick(Sender);

end;

procedure TFrmBalcao.DBGridEh1CellClick(Column: TColumnEh);
begin
  PreencheBuscaPrecoMateriaPrima;
end;

procedure TFrmBalcao.DBGridEh1DblClick(Sender: TObject);
var
  vQtd: String;
begin
  try
    PesquisaProduto := false;

     vQtd := copy(edtPesqMateriaPrima.Text, 1, Pos('*', edtPesqMateriaPrima.Text));

      edtPesqMateriaPrima.Text := vQtd + qryPesqMateriaPrimaDESCRICAO.Value;
      edtPesqMateriaPrima.SetFocus;
      PreencheBuscaPrecoMateriaPrima;
      Panel16.Visible := true;
  finally
    PesquisaProduto := true;
  end;
end;

procedure TFrmBalcao.dsItemDataChange(Sender: TObject; Field: TField);
begin

  if qryItemTTOTAL.Value > 0 then
//    lblGeral.Caption := FormatFloat('0.00', qryItemTTOTAL.Value)
  else
//    lblGeral.Caption := FormatFloat('0.00', 0);

  if qryItemTTOTAL.Value > 0 then
//    lblGeralD.Caption := FormatFloat('0.00', qryItemTTOTAL.Value)
  else
//    lblGeralD.Caption := FormatFloat('0.00', 0);

end;

procedure TFrmBalcao.dsVendaDataChange(Sender: TObject; Field: TField);
begin
  StatusBar1.Panels[2].Text := 'Vendedor:' + qryVendaVIRTUAL_VENDEDOR.Value;
end;


procedure TFrmBalcao.edtPesqMateriaPrimaChange(Sender: TObject);
var
  Qtd: Integer;
  Descricao: string;
begin

  if PageControl2.ActivePage = TabPDV then
    Descricao := edtPesqMateriaPrima.Text;

  if Length(Descricao) = 13 then
    Qtd := 1;

  if PageControl2.ActivePage = TabPDV then // PDV
  begin
    if Pos('*', Descricao) = 0 then
      edtQtdP.Text := '1';

    //DBGridBuscaP.Visible := false;
  end;

  if not PesquisaProduto then
    exit;

  vPosicao := Pos('*', trim(Descricao)) + 1;
  vPesquisa := trim(copy((Descricao), vPosicao, 1000));

  if (trim(Descricao) <> '') then
  begin

    if PageControl2.ActivePage = TabPDV then
      //DBGridBuscaP.Visible := true;


    if not Dados.EhNumero(vPesquisa) then
    begin
      if Descricao <> '' then
        PesquisaDescricaoMateriaPrima(vPesquisa);
    end;
  end
  else
  begin
     DBGridTabelaPreco.Visible := false;
  end;
end;

procedure TFrmBalcao.edtPesqMateriaPrimaKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if (DBGridEh1.Visible)
  then
  begin
    if Key = VK_UP then
    begin

      qryPesqMateriaPrima.Prior;
      if qryPesqMateriaPrima.FieldByName('FOTO').Value <> null then
        begin
          DBImage1.Visible := true;
        end
      else
        begin
          DBImage1.Visible := false;
        end;
      PreencheBuscaPrecoMateriaPrima;


      if qryPesqMateriaPrimaTABELA_PRECO.Value = 'S' then
        begin
          DBGridTabelaPreco.Visible := true;
          qryTabelaPreco.Close;
          qryTabelaPreco.Params[0].Value := qryPesqMateriaPrimaCODIGO.Value;
          qryTabelaPreco.Open;
        end
      else
        begin
          DBGridTabelaPreco.Visible := false;
        end;
      DescricaoMateriaPrimaSetFocus('I')
    end;
    if Key = VK_DOWN then
    begin

      qryPesqMateriaPrima.Next;
      if qryPesqMateriaPrima.FieldByName('FOTO').Value <> null then
        begin
          DBImage1.Visible := true;
        end
      else
        begin
          DBImage1.Visible := false;
        end;
      PreencheBuscaPrecoMateriaPrima;

      if qryPesqMateriaPrimaTABELA_PRECO.Value = 'S' then
        begin
          DBGridTabelaPreco.Visible := true;
          qryTabelaPreco.Close;
          qryTabelaPreco.Params[0].Value := qryPesqMateriaPrimaCODIGO.Value;
          qryTabelaPreco.Open;
        end
      else
        begin
          DBGridTabelaPreco.Visible := false;
        end;
      DescricaoMateriaPrimaSetFocus('I')
    end;
  end
  else
  begin
    if Key = VK_UP then
    begin
      qryItem.Prior;
      if qryPesqMateriaPrimaTABELA_PRECO.Value = 'S' then
        begin
          DBGridTabelaPreco.Visible := true;
        end
      else
        begin
          DBGridTabelaPreco.Visible := false;
        end;
  //---------------------------------------------------
      DescricaoMateriaPrimaSetFocus('I')

    end;
    if Key = VK_DOWN then
    begin
      qryItem.Next;
      if qryPesqMateriaPrimaTABELA_PRECO.Value = 'S' then
        begin
          DBGridTabelaPreco.Visible := true;
        end
      else
        begin
          DBGridTabelaPreco.Visible := false;
        end;
      DescricaoMateriaPrimaSetFocus('I')
    end;
  end;
end;

procedure TFrmBalcao.edtPesqMateriaPrimaKeyPress(Sender: TObject;
  var Key: Char);


begin

  if (Key = #13) then
    begin
      txtDescricaoMateriaPrima.Caption := qryPesqMateriaPrimaDESCRICAO.Value;
      edtPrecoMateriaPrima.Text        := FormatFloat('0.00', RoundABNT(qryPesqMateriaPrimaVIRTUAL_PRECO.Value, 2));
      edtLargura.SetFocus;
      edtLargura.SelectAll;
    end;

end;

procedure TFrmBalcao.PageControl2Change(Sender: TObject);
begin

  if PageControl2.ActivePage = TabPDV then
  begin
    Dados.FTIpoPDV := 'PDV';
  end;

  FormShow(Sender);

end;

procedure TFrmBalcao.Panel26Resize(Sender: TObject);
begin
  panel26.Width := panel7.Width div 2
end;

procedure TFrmBalcao.Panel31MouseEnter(Sender: TObject);
begin
  //panel31.Visible := true;
end;

procedure TFrmBalcao.PesquisaCliente;
begin
//  qryBuscaFone.close;
//  qryBuscaFone.ParamByName('fone').Value := '%' + edtFone.Text + '%';
//  qryBuscaFone.Open;

end;

procedure TFrmBalcao.edtAcrescimoPorcentagemMateriaPrimaExit(Sender: TObject);
var
  Total, TotalComAcrescimo : real;
begin

  if (edtAcrescimoPorcentagemMateriaPrima.Text <> '') then
    begin
      Total := StrToFloatDef(edtTamanho.Text, 1) * StrToFloatDef(edtPrecoMateriaPrima.Text, 0);
      TotalComAcrescimo := Total + ((Total / 100) * StrToFloat(edtAcrescimoPorcentagemMateriaPrima.Text));
      edtTotalMateriaPrima.Text := FormatFloat('0.00', RoundABNT(TotalComAcrescimo, 2));
    end
  else
    begin
      edtAcrescimoPorcentagemMateriaPrima.Text := '0,00';
      edtAcrescimoPorcentagemMateriaPrima.SelectAll;
    end;
end;

procedure TFrmBalcao.edtAcrescimoPorcentagemMateriaPrimaKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
  if Key = VK_RETURN then
    begin
      edtAcrescimoValorMateriaPrima.SetFocus;
      edtAcrescimoValorMateriaPrima.SelectAll;
    end;
end;

procedure TFrmBalcao.edtAcrescimoValorMateriaPrimaExit(Sender: TObject);
var
  Total, TotalComAcrescimo : real;
begin
  if edtAcrescimoValorMateriaPrima.Text = '' then
    begin
      edtAcrescimoValorMateriaPrima.Text := '0,00';
    end;

  if StrToFloat(edtAcrescimoValorMateriaPrima.Text) > 0 then
    begin
      Total := StrToFloatDef(edtTamanho.Text, 1) * StrToFloatDef(edtPrecoMateriaPrima.Text, 0);
      TotalComAcrescimo := Total + StrToFloat(edtAcrescimoValorMateriaPrima.Text);

      edtTotalMateriaPrima.Text := FormatFloat('0.00', RoundABNT(TotalComAcrescimo, 2));
    end
  else
    begin
      edtAcrescimoValorMateriaPrima.Text := '0,00';
      edtAcrescimoValorMateriaPrima.SelectAll;
    end;

end;

procedure TFrmBalcao.edtAcrescimoValorMateriaPrimaKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
  if Key = VK_RETURN then
    begin
      BitBtn1.SetFocus;

    end;
end;

procedure TFrmBalcao.edtBuscarClienteChange(Sender: TObject);
begin
if ActiveControl = edtBuscarCliente then
  begin
    if vPessoa <> edtBuscarCliente.Text then
    begin
      DBGridPes.Visible := True;
      pnPessoa.Height := 200;
    end;
  end;

  qryCliente.Close;
  if dados.qryEmpresaPESQUISA_POR_PARTE.Value = 'S' then
  begin
    qryCliente.Params[0].Value := '%' + edtBuscarCliente.Text + '%';
    qryCliente.Params[1].Value := copy(edtBuscarCliente.Text, 1, 14) + '%';
  end
  else
  begin
    qryCliente.Params[0].Value := edtBuscarCliente.Text + '%';
    qryCliente.Params[1].Value := copy(edtBuscarCliente.Text, 1, 14) + '%';
  end;

  qryCliente.Open;
end;

procedure TFrmBalcao.edtBuscarClienteKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
if Key = VK_UP then
    qryCliente.Prior;

  if Key = VK_DOWN then
    qryCliente.Next;

  if Key = VK_RETURN then
    BEGIN
    if not qryCliente.IsEmpty then
      begin
        if vPessoa <> edtBuscarCliente.Text then
          begin
            GetCliente;
          end;
      end;


      DBGridPes.Visible := False;
      pnPessoa.Height := 37;

      EdtProdutoP.SetFocus;
    END;
end;

procedure TFrmBalcao.edtFoneChange(Sender: TObject);
begin
//  PesquisaCliente;
//  if TiraPontos(edtFone.Text) <> '' then
//    DBGridClie.Visible := true;
end;

procedure TFrmBalcao.edtLarguraChange(Sender: TObject);
var
calculo : Real;
begin
  if (edtLargura.Text <> '') then
    begin
      calculo := (StrToFloat(edtLargura.Text) * StrToFloat(edtProfundidade.Text));
      edtTamanho.Text := FloatToStr(calculo);
    end
  else
    begin
      edtLargura.Text := '0,00';
      edtLargura.SelectAll;
    end;
end;

procedure TFrmBalcao.edtLarguraEnter(Sender: TObject);
begin
  edtProfundidade.SetFocus;
end;

procedure TFrmBalcao.edtLarguraKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);

begin
  if Key = VK_RETURN then
    edtProfundidade.SetFocus;


end;

procedure TFrmBalcao.edtLarguraKeyPress(Sender: TObject; var Key: Char);
begin
  if ((key in ['0'..'9'] = false) and (key in [','] = false) and (word(key) <> vk_back)) then
 key := #0;
end;

procedure TFrmBalcao.ReabrirPedido;
begin
//  if not qryBuscaFone.IsEmpty then
//  begin
//
//    qryPedido.close;
//    qryPedido.Params[0].Value := qryBuscaFoneCODIGO.Value;
//    qryPedido.Open;
//
//    If not qryPedido.IsEmpty then
//    begin
//      AbreVenda(qryPedidoCODIGO.Value, Dados.FTIpoPDV);
//      Application.ProcessMessages;
//    end
//    else
//    begin
//      inserevenda;
//      edtFone.SetFocus;
//    end;

  //end;
end;

//procedure TFrmBalcao.edtFoneExit(Sender: TObject);
//begin
//
////  if qryVenda.State in dsEditModes then
////    qryVenda.Post;
////
////  DBGridClie.Visible := false;
////
////  if TiraPontos(edtFone.Text) <> '' then
////  begin
////
////    PesquisaCliente;
////    ReabrirPedido;
////
////    if qryBuscaFone.IsEmpty then
////    begin
////      edtCliente.Caption := '';
////      pnEndereco.Caption := '';
////      CadPessoaRapido(0);
////    end;
//  end;

//end;

//procedure TFrmBalcao.edtFoneKeyPress(Sender: TObject; var Key: Char);
//begin
//  if (Key in [#13]) then
//    edtProdutoD.SetFocus;
//end;

//procedure TFrmBalcao.edtNumeroKeyPress(Sender: TObject; var Key: Char);
//begin
//  if PageControl2.ActivePage = TabPDV then
//  begin
//    if Key = #13 then
//      edtQtdP.SetFocus;
//  end;
//  if PageControl2.ActivePage = tabRestaurante then
//  begin
//    if Key = #13 then
//    begin
//      if PanelRestaurante.Enabled then
//        edtOBSR.SetFocus;
//    end;
//  end;
//  if PageControl2.ActivePage = tabDelivery then
//  begin
//    if Key = #13 then
//      edtObsD.SetFocus;
//  end;

//end;
{
procedure TFrmBalcao.edtObsDExit(Sender: TObject);
begin
  if ehCaixaRapido = 'N' then
    EdtQtdD.SetFocus;
end;

procedure TFrmBalcao.edtObsDKeyPress(Sender: TObject; var Key: Char);
begin
  if Key = #13 then
    EdtQtdD.SetFocus;
end;

procedure TFrmBalcao.edtOBSRExit(Sender: TObject);
begin
  if PanelRestaurante.Enabled then
  begin
    if ehCaixaRapido = 'N' then
      edtQtdR.SetFocus;
  end;
end;

procedure TFrmBalcao.edtOBSRKeyPress(Sender: TObject; var Key: Char);
begin
  if Key = #13 then
    edtQtdR.SetFocus;
end;
}
procedure TFrmBalcao.edtPrecoPExit(Sender: TObject);
var
  Total : real;
begin

  if PageControl2.ActivePage = TabPDV then
  begin
    Total := StrToFloatDef(edtQtdP.Text, 1) * StrToFloatDef(edtPrecoP.Text, 0);

    lblTotalP.Caption := FormatFloat('0.00', RoundABNT(Total, 2));

    EdtProdutoP.SetFocus;

    valor :=  Total;

    InsereItem(EdtProdutoP.Text, '', StrToFloatDef(edtPrecoP.Text, 0), StrToFloatDef(edtQtdP.Text, 1), StrToFloatDef(lblTotalP.Caption, 0));

    qryPesqMateriaPrima.Close;
  end;

end;

procedure TFrmBalcao.edtPrecoPKeyPress(Sender: TObject; var Key: Char);
begin

  if not(Key in ['0' .. '9', ',', '.', #8, #9, #13, #27]) then
    Key := #0;

  if Key = #13 then
  begin
    qryPesqProd.Refresh;
    DescricaoSetFocus('I');
    DBImage1.Visible := false;
  end;


end;

procedure TFrmBalcao.edtProdutoDEnter(Sender: TObject);
begin
  //edtProdutoD.SelectAll;
end;

procedure TFrmBalcao.inserevenda;
var
  codigo1, codigo2: Integer;
begin

  qryConsultaCliente.Close;
  qryConsultaCliente.Params[0].Value := Dados.qryConfigCLIENTE_PADRAO.Value;
  qryConsultaCliente.Open;

  Dados.qryExecute.SQL.Clear;
  Dados.qryExecute.SQL.Add('INSERT INTO VENDAS_MASTER (');
  Dados.qryExecute.SQL.Add('CODIGO,');
  Dados.qryExecute.SQL.Add('DATA_EMISSAO,');
  Dados.qryExecute.SQL.Add('DATA_SAIDA,');
  Dados.qryExecute.SQL.Add('ID_CLIENTE,');
  Dados.qryExecute.SQL.Add('FK_USUARIO,');
  Dados.qryExecute.SQL.Add('FK_CAIXA,');
  Dados.qryExecute.SQL.Add('FK_VENDEDOR,');
  Dados.qryExecute.SQL.Add('CPF_NOTA,');
  Dados.qryExecute.SQL.Add('SUBTOTAL,');
  Dados.qryExecute.SQL.Add('TIPO_DESCONTO,');
  Dados.qryExecute.SQL.Add('DESCONTO,');
  Dados.qryExecute.SQL.Add('TROCO,');
  Dados.qryExecute.SQL.Add('DINHEIRO,');
  Dados.qryExecute.SQL.Add('TOTAL,');
  Dados.qryExecute.SQL.Add('SITUACAO,');
  Dados.qryExecute.SQL.Add('FKEMPRESA,');
  Dados.qryExecute.SQL.Add('PERCENTUAL,');
  Dados.qryExecute.SQL.Add('TIPO,');
  Dados.qryExecute.SQL.Add('NECF,');
  Dados.qryExecute.SQL.Add('FKORCAMENTO,');
  Dados.qryExecute.SQL.Add('FK_MESA,');
  Dados.qryExecute.SQL.Add('LOTE,');
  Dados.qryExecute.SQL.Add('GERA_FINANCEIRO,');
  Dados.qryExecute.SQL.Add('PERCENTUAL_ACRESCIMO,');
  Dados.qryExecute.SQL.Add('ACRESCIMO,');
  Dados.qryExecute.SQL.Add('FK_TABELA,');
  Dados.qryExecute.SQL.Add('PEDIDO,');
  Dados.qryExecute.SQL.Add('OS,');
  Dados.qryExecute.SQL.Add('FK_OS,');
  Dados.qryExecute.SQL.Add('TOTAL_TROCA,');
  Dados.qryExecute.SQL.Add('FORMA_PAGAMENTO,');
  Dados.qryExecute.SQL.Add('FK_ENTREGADOR,');
  Dados.qryExecute.SQL.Add('TELA,');
  Dados.qryExecute.SQL.Add('FLAG_NFCE,');
  Dados.qryExecute.SQL.Add('NOME');
  Dados.qryExecute.SQL.Add(')');
  Dados.qryExecute.SQL.Add('VALUES');
  Dados.qryExecute.SQL.Add('(');
  Dados.qryExecute.SQL.Add(':CODIGO,');
  Dados.qryExecute.SQL.Add(':DATA_EMISSAO,');
  Dados.qryExecute.SQL.Add(':DATA_SAIDA,');
  Dados.qryExecute.SQL.Add(':ID_CLIENTE,');
  Dados.qryExecute.SQL.Add(':FK_USUARIO,');
  Dados.qryExecute.SQL.Add(':FK_CAIXA,');
  Dados.qryExecute.SQL.Add(':FK_VENDEDOR,');
  Dados.qryExecute.SQL.Add(':CPF_NOTA,');
  Dados.qryExecute.SQL.Add(':SUBTOTAL,');
  Dados.qryExecute.SQL.Add(':TIPO_DESCONTO,');
  Dados.qryExecute.SQL.Add(':DESCONTO,');
  Dados.qryExecute.SQL.Add(':TROCO,');
  Dados.qryExecute.SQL.Add(':DINHEIRO,');
  Dados.qryExecute.SQL.Add(':TOTAL,');
  Dados.qryExecute.SQL.Add(':SITUACAO,');
  Dados.qryExecute.SQL.Add(':FKEMPRESA,');
  Dados.qryExecute.SQL.Add(':PERCENTUAL,');
  Dados.qryExecute.SQL.Add(':TIPO,');
  Dados.qryExecute.SQL.Add(':NECF,');
  Dados.qryExecute.SQL.Add(':FKORCAMENTO,');
  Dados.qryExecute.SQL.Add(':FK_MESA,');
  Dados.qryExecute.SQL.Add(':LOTE,');
  Dados.qryExecute.SQL.Add(':GERA_FINANCEIRO,');
  Dados.qryExecute.SQL.Add(':PERCENTUAL_ACRESCIMO,');
  Dados.qryExecute.SQL.Add(':ACRESCIMO,');
  Dados.qryExecute.SQL.Add(':FK_TABELA,');
  Dados.qryExecute.SQL.Add(':PEDIDO,');
  Dados.qryExecute.SQL.Add(':OS,');
  Dados.qryExecute.SQL.Add(':FK_OS,');
  Dados.qryExecute.SQL.Add(':TOTAL_TROCA,');
  Dados.qryExecute.SQL.Add(':FORMA_PAGAMENTO,');
  Dados.qryExecute.SQL.Add(':FK_ENTREGADOR,');
  Dados.qryExecute.SQL.Add(':TELA,');
  Dados.qryExecute.SQL.Add(':FLAG_NFCE,');
  Dados.qryExecute.SQL.Add(':NOME');
  Dados.qryExecute.SQL.Add(');');

  if PageControl2.ActivePage = TabPDV then
  begin
    if Dados.TerminalCaixa then
      Dados.qryExecute.ParamByName('TIPO').Value := 'V'
    else
      Dados.qryExecute.ParamByName('TIPO').Value := 'P';
  end;

  Dados.qryExecute.ParamByName('FKEMPRESA').Value :=
    Dados.qryEmpresaCODIGO.Value;
  if not caixaUnico then
    begin
      Dados.qryExecute.ParamByName('FK_USUARIO').Value := Dados.idUsuario;
    end
  else
    begin
      Dados.qryExecute.ParamByName('FK_USUARIO').Value := idUsuarioCaixaUnico;
    end;
  Dados.qryExecute.ParamByName('ID_CLIENTE').Value :=
    Dados.qryConfigCLIENTE_PADRAO.Value;
  Dados.qryExecute.ParamByName('FK_VENDEDOR').Value := 0;
  Dados.qryExecute.ParamByName('TELA').Value := Dados.FTIpoPDV;
  Dados.qryExecute.ParamByName('FK_TABELA').Value := 1;
  Dados.qryExecute.ParamByName('CPF_NOTA').Value := '';

  if not caixaUnico then
    begin
      Dados.qryExecute.ParamByName('FK_CAIXA').Value := Dados.idCaixa;
      Dados.qryExecute.ParamByName('LOTE').Value := Dados.Lote;

    end
  else
    begin
      Dados.qryExecute.ParamByName('FK_CAIXA').Value := idCaixaUnico;
      Dados.qryExecute.ParamByName('LOTE').Value := idLoteCaixaUnico;

    end;

  Dados.qryExecute.ParamByName('DATA_EMISSAO').Value := date;
  Dados.qryExecute.ParamByName('DATA_SAIDA').Value := date;
  Dados.qryExecute.ParamByName('SUBTOTAL').Value := 0;
  Dados.qryExecute.ParamByName('TIPO_DESCONTO').Value := 'D';
  Dados.qryExecute.ParamByName('DESCONTO').Value := 0;
  Dados.qryExecute.ParamByName('PERCENTUAL').Value := 0;
  Dados.qryExecute.ParamByName('ACRESCIMO').Value := 0;
  Dados.qryExecute.ParamByName('DESCONTO').Value := 0;
  Dados.qryExecute.ParamByName('TOTAL').Value := 0;
  Dados.qryExecute.ParamByName('TROCO').Value := 0;

  Dados.qryExecute.ParamByName('NOME').Value :=
    qryConsultaClienteRAZAO.Value;

  Dados.qryExecute.ParamByName('FK_MESA').Clear;

  Dados.qryExecute.ParamByName('DINHEIRO').Value := 0;
  Dados.qryExecute.ParamByName('SITUACAO').Value := 'X';

  codigo1 := Dados.Numerador('VENDAS_MASTER', 'CODIGO', 'N', '', '');

  Dados.qryExecute.ParamByName('CODIGO').Value := codigo1;
  Dados.qryExecute.ExecSQL;
  Dados.Conexao.CommitRetaining;

  AbreVenda(codigo1, Dados.FTIpoPDV);
  //lbCaixaIni.Caption := 'ABERTA';

  //ConfiguraBalanca;

end;

function TFrmBalcao.GerouNFCe(operacao: string): String;
begin

  if qryVenda.IsEmpty then
    exit;

  result := '';
  Dados.qryConsulta.close;
  Dados.qryConsulta.SQL.Text :=
    'select NUMERO, SITUACAO from nfce_master where fk_venda=:venda';
  Dados.qryConsulta.Params[0].Value := qryVendaCODIGO.Value;
  Dados.qryConsulta.Open;

  if operacao = 'CANCELA VENDA' then
  begin
    if (Dados.qryConsulta.FieldByName('SITUACAO').Value = 'C') or
      (Dados.qryConsulta.FieldByName('SITUACAO').IsNull) then
      // faz nada
    else

      result := 'Existe NFC-e emitida para esta venda!' + sLineBreak +
        'N�mero NFCe...:' + Dados.qryConsulta.FieldByName('NUMERO').AsString;

    if operacao = 'EXCLUIR ITEM' then
    begin
      if (Dados.qryConsulta.FieldByName('SITUACAO').Value = 'C') or
        (Dados.qryConsulta.FieldByName('SITUACAO').Value = 'O') then
        // faz nada
      else
      begin
        result := 'Existe NFC-e emitida para esta venda!' + sLineBreak +
          'N�mero NFCe...:' + Dados.qryConsulta.FieldByName('NUMERO').AsString;;
      end;
    end;

  end;

end;

function TFrmBalcao.BuscaNumeroVenda: Integer;
var
  filtro: string;
begin

  filtro := '';

  vSqlBusca :=
    ' select max(CODIGO)CODIGO from VENDAS_MASTER where SITUACAO = ''X'' and ' +
    ' fk_usuario = :ID and fkempresa = :emp and tela=:tela' + filtro;

  if Dados.TerminalCaixa then
  begin
    qryBuscaVenda.close;
    qryBuscaVenda.SQL.Text := vSqlBusca;
    qryBuscaVenda.Params[0].Value := Dados.idUsuario;
    qryBuscaVenda.Params[1].Value := Dados.qryEmpresaCODIGO.Value;
    qryBuscaVenda.Params[2].Value := Dados.FTIpoPDV;
    qryBuscaVenda.Open;

  end
  else
  begin
    qryBuscaVenda.close;
    qryBuscaVenda.SQL.Text := vSqlBusca;
    qryBuscaVenda.Params[0].Value := Dados.idUsuario;
    qryBuscaVenda.Params[1].Value := Dados.qryEmpresaCODIGO.Value;
    qryBuscaVenda.Params[2].Value := Dados.FTIpoPDV;
    qryBuscaVenda.Open;
  end;

  result := qryBuscaVendaCODIGO.Value;

end;

procedure TFrmBalcao.FinalizaVenda;
begin
  Dados.vEnviarSefa := 'N';

    if (BuscaNumeroVenda = 0) then
      begin
      spbNovaVenda.Enabled := true;
      exit;
      //inserevenda
      end
    else
    begin

      AbreVenda(BuscaNumeroVenda, Dados.FTIpoPDV);

      qryVenda.Edit;
      qryVendaDATA_EMISSAO.Value := now;
      qryVendaDATA_SAIDA.Value := now;
      if Dados.TerminalCaixa then
        qryVendaSITUACAO.Value := 'F';
        qryVendaFK_CAIXA.Value := Dados.idCaixa;
        qryVendaLOTE.Value := Dados.Lote;
        qryVenda.Post;
    end;


end;

procedure TFrmBalcao.FecharPrevenda;
begin

  if not(qryVenda.State in dsEditModes) then
    qryVenda.Edit;
    if not caixaUnico then
      begin
        qryVendaSITUACAO.Value := 'G';
        qryVendaFK_CAIXA.Value := Dados.idCaixa;
        qryVendaLOTE.Value := Dados.Lote;
      end
     else
      begin
        qryVendaSITUACAO.Value := 'F';
        qryVendaFK_CAIXA.Value := idCaixaUnico;
        qryVendaLOTE.Value := idLoteCaixaUnico;
      end;
  qryVendaDATA_EMISSAO.Value := now;
  qryVendaDATA_SAIDA.Value := now;
  qryVenda.Post;
  Dados.Conexao.CommitRetaining;

end;

procedure TFrmBalcao.MostraCaixa;
begin
  qryPesqConta.close;
  qryPesqConta.Params[0].Value := Dados.idUsuario;
  qryPesqConta.Open;

  if not qryPesqConta.IsEmpty then
  begin
    Caption := ' PDV - Venda de Balc�o | ' + qryPesqContaDESCRICAO.Value +
      ' | Op.:' + Dados.vUsuario;
    StatusBar1.Panels[0].Text := 'Conta:' + qryPesqContaDESCRICAO.Value;
    StatusBar1.Panels[1].Text := 'Usu�rio:' + Dados.vUsuario;
    Dados.vConta := qryPesqContaDESCRICAO.Value;
  end
  else
  begin
    Caption := ' ';
    StatusBar1.Panels[0].Text := '';
    StatusBar1.Panels[1].Text := '';
    // StatusBar1.Panels[2].Text := '';
    StatusBar1.Panels[3].Text := '';

    Dados.vConta := '';
  end;
end;

procedure TFrmBalcao.VendaExiste;
begin

  if BuscaNumeroVenda = 0 then
    begin

      PanelPDV.Enabled := false;

      if dados.TerminalCaixa then
        begin

          CaixaAberto := SituacaoCaixa;

          if CaixaAberto then
            begin
//              frmMenu.btnResumo.Enabled         := true;
//              btnSangria.Enabled        := true;
//              btnSuprimento.Enabled     := true;
//
//              btnReimprimir.Enabled     := true;

              Panel36.Visible           := true;
              Panel7.Visible            := true;
              Panel7.Enabled            := true;

              btnReceber.Enabled        := true;
              actNovaVenda.Enabled      := true;
              spbNovaVenda.Enabled      := true;
              actReceber.Enabled        := true;
              actCliente.Enabled        := true;
              actDesconto.Enabled       := true;
              btnDevolucao.Enabled      := true;
              btnImportar.Enabled       := true;
              edtBuscarCliente.Enabled  := false;
            end;
        end
      else
        begin
//          btnResumo.Enabled         := false;
//          btnSangria.Enabled        := false;
//          btnSuprimento.Enabled     := false;
          //btnImportar.Enabled       := false;
          btnReceber.Enabled        := false;
//          btnReimprimir.Enabled     := false;
        end;



    end
  else
    begin

        AbreVenda(BuscaNumeroVenda, Dados.FTIpoPDV);

        qryVenda.Edit;
        qryVendaDATA_EMISSAO.Value    := now;
        qryVendaDATA_SAIDA.Value      := now;
        qryVendaFK_CAIXA.Value        := Dados.idCaixa;
        qryVendaLOTE.Value            := Dados.Lote;
        PesquisaProduto               := true;
        Dados.tela                    := 'PDV';
        Panel7.Enabled                := true;
        habilitacampos(true);
        edtBuscarCliente.Enabled      := true;
        DescricaoSetFocus('');
        lbCaixaIni.Caption            := 'Venda Aberta';

        if Dados.qryEmpresaDESCONTO_ITEM_PDV.Value = 'S' then
          begin
            btnDesconto.Enabled := true;
          end;

        spbNovaVenda.Enabled          := false;
        btnConsultarPreco.Enabled     := false;
        btnImportar.Enabled           := false;
        //btnReceber.Enabled            := false;
        btnOrdemEntrega.Enabled       := false;
        btnDevolucao.Enabled          := false;

        actNovaVenda.Enabled          := false;
        btnSalvar.Enabled             := true;
        //actReceber.Enabled            := false;

        if dados.TerminalCaixa then
        begin
//          btnResumo.Enabled         := true;
//          btnSangria.Enabled        := true;
//          btnSuprimento.Enabled     := true;
        end;
    end;
end;

procedure TFrmBalcao.PreencheBuscaPreco;
begin
  if PageControl2.ActivePage = TabPDV then
    BuscaPreco(EdtProdutoP.Text);
end;

procedure TFrmBalcao.PreencheBuscaPrecoMateriaPrima;
begin
  if PageControl2.ActivePage = TabPDV then
    BuscaPrecoMateriaPrima(edtPesqMateriaPrima.Text);
end;

procedure TFrmBalcao.DBGridSetFocus;
begin
  if PageControl2.ActivePage = TabPDV then
  begin
    DBGridP.Refresh;
    DBGridP.Repaint;
    DBGridP.SetFocus;
  end;
//  if PageControl2.ActivePage = tabRestaurante then
//  begin
//    DBGridR.Refresh;
//    DBGridR.Repaint;
//    if PanelRestaurante.Enabled then
//      DBGridR.SetFocus;
//  end;
//  if PageControl2.ActivePage = tabDelivery then
//  begin
//    DBGridD.Refresh;
//    DBGridD.Repaint;
//    DBGridD.SetFocus;
//  end;
end;

procedure TFrmBalcao.DescricaoMateriaPrimaSetFocus(Op: String);
begin
  if PageControl2.ActivePage = TabPDV then
  begin
    if PanelPDV.Enabled then
      edtPesqMateriaPrima.SetFocus;
  end;
end;

procedure TFrmBalcao.DescricaoSetFocus(Op: String);
begin

    if PageControl2.ActivePage = TabPDV then
  begin
    if PanelPDV.Enabled then
      EdtProdutoP.SetFocus;
  end;


end;

procedure TFrmBalcao.FechaVenda(Descricao: String);
begin
  if (trim(Descricao) = '') then
  begin
    if PageControl2.ActivePage = TabPDV then
    begin
      edtQtdP.Text := '1';
      edtPrecoP.Text := '0,00';
      lblTotalP.Caption := '0,00';
    end;

    //btnFinalizaClick(self);
  end;
end;

procedure TFrmBalcao.AtualizaDescricao(Descricao: String);
begin
  if Descricao <> '' then
  begin
    if ehCaixaRapido <> 'S' then
    begin
      try
        PesquisaProduto := false;

        if PageControl2.ActivePage = TabPDV then
          EdtProdutoP.Text := qryPesqProdDESCRICAO.Value;
      finally

        PesquisaProduto := true;
      end;
    end;
  end;
end;

procedure TFrmBalcao.Carregaimagem2(Field : TField; Img : TImage);

  var
  Bmp: TBitmap;
  vJpeg   : TJPEGImage;
  vStream : TMemoryStream;
begin
  { Verifica se o campo esta v�zio. }
  if not Field.IsNull then
  begin
     try
    { Cria objeto do tipo TJPEG, e objeto do tipo MemoryStream}
    vJpeg   := TJPEGImage.Create;
    vStream := TMemoryStream.Create;

    { Trata o campo como do tipo BLOB e salva o seu conteudo na mem�ria. }
    TBlobField(Field).SaveToStream(vStream);

    { Ajusta a posicao inicial de leitura da mem�ria }
    vStream.Position := 0;

    { Carrega da memoria os dados, para uma estrutura do tipo TJPEG
      (A partir da posicao 0)}
    vJpeg.LoadFromStream(vStream);

    { Exibe o jpg no Timage. }
    //Img.Picture.Assign(vJpeg);
    Bmp := TBitmap.Create;
    Bmp.Assign(vJpeg);

     finally
       { Libera a memoria utilizada pelos componentes de convers�o }
      vJpeg.Free;
      vStream.Free;
      //imgLogo.Imagem := nil;
      //Bmp.Free;
     end;




  end;


//   if not Dados.qryParametro.Active then
//    Dados.qryParametro.Open;
//
//    if FileExists(Dados.qryParametro.FieldByName('TELA_FUNDO_ECF').AsString) then
//  begin
//    imgLogo.Picture.LoadFromFile
//      (Dados.qryParametro.FieldByName('TELA_FUNDO_ECF').Value);
//    Label1.Visible := false;
//  end
//  else
//    Label1.Visible := true;

//  if FileExists(qryPesqProdFOTO.Value) then
//  begin
//    cxDBImage1.Picture.LoadFromStream(qryPesqProdFOTO.Value);
//
//    Label1.Visible := false;
// end
//  else
//    Label1.Visible := true;
end;

procedure TFrmBalcao.PesquisaCodBarraGeral(FPesquisa: string);
begin

  qryPesqProd.close;
  qryPesqProd.IndexFieldNames := 'CODBARRA';
  qryPesqProd.SQL.Text := vSql;
  qryPesqProd.SQL.Text := StringReplace(qryPesqProd.SQL.Text, '/*where*/',
    ' AND ((PRO.CODBARRA=:BARRA) or (PRO.REFERENCIA=:REF) or (PRO.CODIGO =:CODIGO)) ORDER BY PRO.CODBARRA',
    [rfReplaceAll]);
  qryPesqProd.ParamByName('EMP').Value := Dados.qryEmpresaCODIGO.Value;//qryVendaFKEMPRESA.Value;
  qryPesqProd.ParamByName('BARRA').Value := copy(FPesquisa, 1, 14);
  qryPesqProd.ParamByName('REF').Value := copy(FPesquisa, 1, 19);
  qryPesqProd.ParamByName('CODIGO').Value := copy(FPesquisa, 1, 6);
  qryPesqProd.Open;

  if not(qryPesqProd.IsEmpty) then
  begin
    PreencheBuscaPreco;

  end;

end;

procedure TFrmBalcao.PesquisaCodBarraBalanca(FPesquisa: string);
begin

  qryPesqProd.close;
  qryPesqProd.IndexFieldNames := 'DESCRICAO';
  qryPesqProd.SQL.Text := vSql;
  qryPesqProd.SQL.Text := StringReplace(qryPesqProd.SQL.Text, '/*where*/',
    ' AND (PRO.PREFIXO_BALANCA=:PREFIXO) ORDER BY PRO.PREFIXO_BALANCA', []);
  qryPesqProd.ParamByName('EMP').Value := Dados.qryEmpresaCODIGO.Value; //qryVendaFKEMPRESA.Value;
  qryPesqProd.ParamByName('PREFIXO').Value := copy(FPesquisa, 1, 7);
  qryPesqProd.Open;

  if not(qryPesqProd.IsEmpty) then
    PreencheBuscaPreco;

end;

procedure TFrmBalcao.ChamaGrade;
begin
  if qryPesqProdGRADE.Value = 'S' then
  begin
    try
      FGrade := -1;
      frmGrade := TFrmGrade.Create(Application);
      frmGrade.qryGrade.close;
      frmGrade.qryGrade.Params[0].Value := qryPesqProdCODIGO.Value;
      frmGrade.qryGrade.Open;
      frmGrade.ShowModal;
    finally
      FGrade := frmGrade.idGrade;

      if PageControl2.ActivePage = TabPDV then
        AtualziaPrecoGrade(StrToFloatDef(edtQtdP.Text, 1));

      frmGrade.Release;
      if ehCaixaRapido = 'S' then

        if PageControl2.ActivePage = TabPDV then
          InsereItem(EdtProdutoP.Text, '', StrToFloatDef(edtPrecoP.Text, 0),
            StrToFloatDef(edtQtdP.Text, 1),
            StrToFloatDef(lblTotalP.Caption, 0));

    end;
  end;
end;

procedure TFrmBalcao.AtualizaVendedor;
begin
  if qryVenda.Active then
  begin
    //if CaixaAberto then
    //begin
      if not(qryVenda.State in dsEditModes) then
        qryVenda.Edit;
      qryVendaFK_VENDEDOR.Value := frmconsVendedor.idVendedor;
      qryVenda.Post;
      Dados.Conexao.CommitRetaining;
    //end;
  end;
end;

procedure TFrmBalcao.AtualziaPrecoGrade(Qtd: Extended);
var
  vQtd: real;

begin
  qryGrade.close;
  qryGrade.Params[0].Value := FGrade;
  qryGrade.Open;
  if qryGradePRECO.AsFloat > 0 then
  begin
    vQtd := Qtd;

    if PageControl2.ActivePage = TabPDV then
    begin
      edtPrecoP.Text := FormatFloat('0.00', qryGradePRECO.AsFloat);
      lblTotalP.Caption := FormatFloat('0.00',
        RoundABNT(qryGradePRECO.AsFloat * vQtd, 2));
      lbCaixaIni.Caption := qryGradeDESCRICAO.AsString;
    end;
  end;
end;

procedure TFrmBalcao.AbreCaixa;
begin
  Try

    Application.CreateForm(TfrmAbreCaixaR, frmAbreCaixaR);
    frmAbreCaixaR.iTipo := 1;
    Dados.tela := 'PDV';
    frmAbreCaixaR.ShowModal;
  Finally
    frmAbreCaixaR.Release;
  End;
end;





procedure TFrmBalcao.btnCancelVendaClick(Sender: TObject);
var
  vMensagem: String;
begin

  if not btnCancelVenda.Visible then
    exit;


  if PageControl2.ActivePage = TabPDV then

    vMensagem := 'Deseja Cancelar?' + sLineBreak +
      GerouNFCe('CANCELA VENDA');

  if Application.messagebox(pwidechar(vMensagem), 'Confirma��o', mb_yesno) = mrYes
  then
  begin

    //if Dados.qryEmpresaEXCLUI_PDV.Value = 'S' then
    //Begin
      Dados.vAutorizar := true;
      if not Dados.eSupervisor then
      begin

        try
          frmSupervisor := TFrmSupervisor.Create(Application);
          Dados.vAutorizar := false;
          frmSupervisor.ShowModal;
        finally
          frmSupervisor.Release;
        end;
      end;

      if not Dados.vAutorizar then
      begin
        exit;
      end;
    //end;

    try

      if not qryVenda.IsEmpty then
      begin
        Dados.EstornaFinanceiro(qryVendaCODIGO.Value);
        qryVenda.Edit;
        qryVendaSITUACAO.Value := 'C'; // CANCELADA
        qryVenda.Post;
      end;

      qryItem.DisableControls;
      //qryItem.First;
    //---------------------------- DESABILITADO POIS A MUDAN�A DO ESTOQUE � NO FINAL DA VENDA -----------------------------------------------------
//      while not qryItem.eof do
//      begin
//        if not(qryItemOS.Value = 'S') then
//        begin
//
//          DMEstoque.AtualizaEstoque(qryItemID_PRODUTO.Value, qryItemQTD.AsFloat,
//            0, 'E', 'R');
//
//          DMEstoque.AtualizaGrade(qryItemID_PRODUTO.Value,
//            qryItemFK_GRADE.Value, qryItemQTD.AsFloat, 'E', 0);
//
//          DMEstoque.AtualizaComposicao(qryItemID_PRODUTO.Value,
//            qryItemQTD.AsFloat, 0, 'E', 'R');
//
//        end;
//        qryItem.Next;
//      end;
     //--------------------------------------------------------------------------------------------------------------------------
//      if PageControl2.ActivePage <> tabRestaurante then
//        VendaExiste;

      if PageControl2.ActivePage = TabPDV then // PDV
      begin
        if PanelPDV.Enabled then
          EdtProdutoP.SetFocus;
        EdtProdutoP.SelectAll;

        edtPrecoP.Text := '0,00';
        lblTotalP.Caption := '0,00';
        edtQtdP.Text := '1';

      end;
//
    finally
      qryItem.EnableControls;
    end;
    Dados.Conexao.CommitRetaining;
    ShowMessage('Cancelado com sucesso');

    habilitacampos(false);
    spbNovaVenda.Enabled          := true;
    btnConsultarPreco.Enabled     := true;
    if not dados.TerminalCaixa then
        begin
            FrmBalcao.btnImportar.Enabled          := false;
        end
      else
        begin
           FrmBalcao.btnImportar.Enabled           := true;
        end;
    btnReceber.Enabled            := true;
    btnDevolucao.Enabled          := true;
    edtBuscarCliente.Enabled      := false;
    btnDesconto.Enabled           := false;
    actDesconto.Enabled           := false;
    actNovaVenda.Enabled          := true;
    actReceber.Enabled            := true;
    btnOrdemEntrega.Enabled       := true;
    btnConsultarPedido.Enabled    := true;
    btnSalvar.Enabled             := false;
    lbCaixaIni.Caption            := 'Venda Cancelada';

  end;

end;

procedure TFrmBalcao.btnCancelVendaMouseEnter(Sender: TObject);
begin
  Panel8.Color := $00B90000;
//btnCancelVenda.Font.Color := clWhite;
end;

procedure TFrmBalcao.btnCancelVendaMouseLeave(Sender: TObject);
begin
  Panel8.Color := $0080552D;
//btnCancelVenda.Font.Color := clBlack;
end;

procedure TFrmBalcao.btnCancVendaClick(Sender: TObject);
var
  vMensagem: String;
begin

  if not btnCancelVenda.Visible then
    exit;

  SituacaoPedido;

  if PageControl2.ActivePage = TabPDV then
    vMensagem := 'Deseja Cancelar Venda?' + sLineBreak +
      GerouNFCe('CANCELA VENDA');

  if Application.messagebox(pwidechar(vMensagem), 'Confirma��o', mb_yesno) = mrYes
  then
  begin

    if Dados.qryEmpresaEXCLUI_PDV.Value = 'S' then
    Begin
      Dados.vAutorizar := true;
      if not Dados.eSupervisor then
      begin

        try
          frmSupervisor := TFrmSupervisor.Create(Application);
          Dados.vAutorizar := false;
          frmSupervisor.ShowModal;
        finally
          frmSupervisor.Release;
        end;
      end;

      if not Dados.vAutorizar then
      begin
        exit;
      end;
    end;

    try

      if not qryVenda.IsEmpty then
      begin
        Dados.EstornaFinanceiro(qryVendaCODIGO.Value);
        qryVenda.Edit;
        qryVendaSITUACAO.Value := 'C'; // CANCELADA
        qryVenda.Post;
      end;
  //------------------------------------ ATUALIZA ESTOQUE --------------------------------------------------------------------
//      qryItem.DisableControls;
//      qryItem.First;
//      while not qryItem.eof do
//      begin
//        if not(qryItemOS.Value = 'S') then
//        begin
//
//          DMEstoque.AtualizaEstoque(qryItemID_PRODUTO.Value, qryItemQTD.AsFloat,
//            0, 'E', 'R');
//
//          DMEstoque.AtualizaGrade(qryItemID_PRODUTO.Value,
//            qryItemFK_GRADE.Value, qryItemQTD.AsFloat, 'E', 0);
//
//          DMEstoque.AtualizaComposicao(qryItemID_PRODUTO.Value,
//            qryItemQTD.AsFloat, 0, 'E', 'R');
//
//        end;
//        qryItem.Next;
//      end;

//      if PageControl2.ActivePage <> tabRestaurante then
//        VendaExiste;
 //----------------------------------------------------------------------------------------------------------------------------
      if PageControl2.ActivePage = TabPDV then // PDV
      begin
        if PanelPDV.Enabled then
          EdtProdutoP.SetFocus;
        EdtProdutoP.SelectAll;

        edtPrecoP.Text := '0,00';
        lblTotalP.Caption := '0,00';
        edtQtdP.Text := '1';

      end;

    finally
      qryItem.EnableControls;
    end;
    lbCaixaIni.Caption := 'TERMINAL LIVRE'
  end;

  VendaExiste;
  Restaurante_Status_Mesa;

end;

procedure TFrmBalcao.btnDelIteClick(Sender: TObject);
var
  vMensagem: string;
begin
  try
    if btnDelItem.Enabled = false then
      exit;

    if not btnDelItem.Visible then
      exit;

    vMensagem := 'Deseja Excluir Item?' + sLineBreak +
      GerouNFCe('EXCLUIR ITEM');

    if not qryItem.IsEmpty then
    begin
      if Application.messagebox(pwidechar(vMensagem), 'Confirma��o', mb_yesno) = mrYes
      then
      begin

        if Dados.qryEmpresaEXCLUI_PDV.Value = 'S' then
        Begin
          if not Dados.eSupervisor then
          begin

            try
              frmSupervisor := TFrmSupervisor.Create(Application);
              Dados.vAutorizar := false;
              frmSupervisor.ShowModal;
            finally
              frmSupervisor.Release;
            end;
            if not Dados.vAutorizar then
            begin
              exit;
            end;
          end;
        end;

        qryItem.delete;

        qrySoma.close;
        qrySoma.Params[0].Value := qryVendaCODIGO.Value;
        qrySoma.Open;

        qryVenda.Edit;
        qryVendaSUBTOTAL.Value := qrySomaTOTAL.Value;

        if ((qryVendaFK_OS.AsInteger = 0) or (qryVendaFK_OS.IsNull)) and
          ((qryVendaFKORCAMENTO.AsInteger = 0) or (qryVendaFKORCAMENTO.IsNull))
        then
          qryVendaDESCONTO.Value := 0;

        qryVendaSUBTOTAL.Value := qryVendaSUBTOTAL.Value;
        qryVenda.Post;

        Dados.Conexao.CommitRetaining;
      end;
    end;
  finally
    DescricaoSetFocus('I');
  end;
end;

procedure TFrmBalcao.btnDescontClick(Sender: TObject);
begin
  if not btnDesconto.Visible then
    exit;

  if Dados.qryEmpresaDESCONTO_ITEM_PDV.Value = 'S' then
  Begin
    Dados.vAutorizar := true;
    if not Dados.eSupervisor then
    begin

      try
        frmSupervisor := TFrmSupervisor.Create(Application);
        Dados.vAutorizar := false;
        frmSupervisor.ShowModal;
      finally
        frmSupervisor.Release;
      end;
    end;

    if not Dados.vAutorizar then
    begin
      exit;
    end;

    try
      frmDesconhecido := TfrmDesconhecido.Create(Application);
      frmDesconhecido.lblProduto.Caption := qryItemDESCRICAO_SL.Value;
      frmDesconhecido.edtPreco.Text :=
        FormatFloat('0.00', qryItemPRECO.AsFloat);
      frmDesconhecido.edtPrecoAtual.Text :=
        FormatFloat('0.00', qryItemPRECO.AsFloat);
      frmDesconhecido.ShowModal;
    finally
      if (StrToFloatDef(frmDesconhecido.edtPreco.Text, 0) > 0) and
        (StrToFloatDef(frmDesconhecido.edtPreco.Text, 0) <> qryItemPRECO.AsFloat)
      then
        AtualizaPreco(StrToFloatDef(frmDesconhecido.edtPreco.Text, 0));
      frmDesconhecido.Release;

    end;

  end;
end;

procedure TFrmBalcao.btnDescontoClick(Sender: TObject);
begin
if not btnDesconto.Visible then
    exit;

  if Dados.qryEmpresaDESCONTO_ITEM_PDV.Value = 'S' then
  Begin

    try
      frmDesconhecido := TfrmDesconhecido.Create(Application);
      frmDesconhecido.lblProduto.Caption := qryItemDESCRICAO_SL.Value;
      frmDesconhecido.edtPreco.Text :=
        FormatFloat('#,##0.00', qryItemPRECO.AsFloat);
      frmDesconhecido.edtPrecoAtual.Text :=
        FormatFloat('#,##0.00', qryItemPRECO.AsFloat);
      frmDesconhecido.ShowModal;
    finally
      if (StrToFloatDef(frmDesconhecido.edtPreco.Text, 0) > 0) and
        (StrToFloatDef(frmDesconhecido.edtPreco.Text, 0) <> qryItemPRECO.AsFloat)
      then
        AtualizaPreco(StrToFloatDef(frmDesconhecido.edtPreco.Text, 0));
      frmDesconhecido.Release;

    end;

  end;
end;

procedure TFrmBalcao.btnDescontoMouseEnter(Sender: TObject);
begin
Panel14.Color := $00B90000;
//btnDesconto.Font.Color := clWhite;
end;

procedure TFrmBalcao.btnDescontoMouseLeave(Sender: TObject);
begin
  Panel14.Color := $0080552D;
//btnDesconto.Font.Color := clBlack;
end;

procedure TFrmBalcao.btnDevolucaoClick(Sender: TObject);
begin
  try
    frmdevolucao := Tfrmdevolucao.Create(Application);
    frmdevolucao.ShowModal;
  finally
    FreeAndNil(frmdevolucao);
  end;
end;

procedure TFrmBalcao.btnDevolucaoMouseEnter(Sender: TObject);
begin
  Panel34.Color := $00B90000;
end;

procedure TFrmBalcao.btnDevolucaoMouseLeave(Sender: TObject);
begin
  Panel34.Color := $0080552D;
end;

procedure TFrmBalcao.btnEntregadorClick(Sender: TObject);
begin
  try
    FrmConsEntregador := TfrmconsEntregador.Create(Application);
    FrmConsEntregador.idEntregador := qryVendaFK_ENTREGADOR.Value;
    FrmConsEntregador.vNome := qryVendaVIRTUAL_ENTREGADOR.Value;
    FrmConsEntregador.ShowModal;
  finally
    AtualizaEntregador;
    // lblEntregador.Caption := FrmConsEntregador.vNome;
    FrmConsEntregador.Release;
  end;
end;

procedure TFrmBalcao.AtualizaEntregador;
begin
  if not(qryVenda.State in dsEditModes) then
    qryVenda.Edit;
  qryVendaFK_ENTREGADOR.Value := FrmConsEntregador.idEntregador;
  qryVenda.Post;
end;

procedure TFrmBalcao.FecharCaixa;
var
  Data: Tdate;
  Hora: TTime;
  idLote: Integer;
begin

  if Application.messagebox('Tem certeza de que deseja Fechar Caixa?',
    'Confirma��o', mb_yesno) = mrno then
    exit;

  Dados.vPodeFecharCaixa := false;

  try
    frmResumoCaixa := TfrmResumoCaixa.Create(Application);
    frmResumoCaixa.Caption := 'Resumo Caixa - Usu�rio:' + Dados.vUsuario;

    frmResumoCaixa.FUsuario := Dados.idUsuario;
    frmResumoCaixa.FLote := Dados.Lote;
    frmResumoCaixa.Tag := 1;
    frmResumoCaixa.VwResumo := false;

    frmResumoCaixa.qryResumo.Open;

    frmResumoCaixa.ShowModal;
  finally
    frmResumoCaixa.Release;
  end;

  if Dados.vPodeFecharCaixa then
  begin

    AbreVenda(BuscaNumeroVenda, Dados.FTIpoPDV);

    if qryVendaCODIGO.Value > 0 then
      Dados.EstornaFinanceiro(qryVendaCODIGO.Value);

    Dados.qryExecute.close;
    Dados.qryExecute.SQL.Text :=
      'UPDATE CONTAS SET ID_USUARIO=NULL, DATA_ABERTURA=NULL, SITUACAO=''F'', LOTE=0 WHERE CODIGO=:COD';
    Dados.qryExecute.Params[0].Value := Dados.idCaixa;
    Dados.qryExecute.ExecSQL;
    Dados.Conexao.CommitRetaining;

    if qryVendaCODIGO.Value > 0 then
    begin
      Dados.qryExecute.close;
      Dados.qryExecute.SQL.Text :=
        'delete from CONTAS_MOVIMENTO WHERE fkvenda=:COD';
      Dados.qryExecute.Params[0].Value := qryVendaCODIGO.Value;
      Dados.qryExecute.ExecSQL;
      Dados.Conexao.CommitRetaining;
    end;

    close;
  end;

end;

procedure TFrmBalcao.SituacaoPedido;
begin
  Dados.qryConsulta.close;
  Dados.qryConsulta.SQL.Text :=
    'SELECT CODIGO FROM VENDAS_MASTER WHERE SITUACAO=''F'' and CODIGO=:CODIGO';
  Dados.qryConsulta.Params[0].Value := qryVendaCODIGO.Value;
  Dados.qryConsulta.Open;
  if not Dados.qryConsulta.IsEmpty then
  begin
    VendaExiste;
    raise Exception.Create('Venda foi finalizada em outra instancia do sistema!'
      + sLineBreak +
      'Evite abrir o PDV com o mesmo usu�rio em mais de uma inst�ncia do Sistema!');
  end;
end;

procedure TFrmBalcao.spbNovaVendaClick(Sender: TObject);
begin
    if Dados.TerminalCaixa then
    // verifica se o terminal � caixa
    begin
      btnVendedorClick(self);
      inserevenda;
      AtualizaVendedor;
    end
    else
    begin
      if (Dados.qryTerminal.FieldByName('CAIXA_UNICO').AsString <> 'S') then

        begin
           if BuscaNumeroVenda = 0 then
              begin
                btnVendedorClick(self);
                inserevenda;
                AtualizaVendedor;
              end;
        end
     else
      begin
        // verifica se caixa � o correto

          Dados.qryConsulta.close;
          Dados.qryConsulta.SQL.Text :=
          'select LOTE from contas where codigo = :id AND LOTE = :idLote';
          Dados.qryConsulta.ParamByName('id').AsString := Dados.qryTerminal.FieldByName('CAIXA_USADO').AsString;
          Dados.qryConsulta.ParamByName('idLote').AsInteger := idLoteCaixaUnico;
          Dados.qryConsulta.Open;

          if Dados.qryConsulta.FieldByName('LOTE').AsInteger <> idLoteCaixaUnico then
            begin

                  MessageBox(FrmBalcao.Handle,'Login efetuado no dia anterior, sistema ser� fechado', 'Balc�o', MB_ICONINFORMATION + MB_OK);
                  FrmBalcao.Close;

            end
          else
            begin

              if BuscaNumeroVenda = 0 then
                begin
                  btnVendedorClick(self);
                  inserevenda;
                  AtualizaVendedor;
                end;


            end;
      end;

    end;

    btnDesconto.Enabled := false;
  if Dados.qryEmpresaDESCONTO_ITEM_PDV.Value = 'S' then
    begin
      btnDesconto.Enabled := true;
    end;

  Dados.tela := 'PDV';
  PesquisaProduto               := true;
  EdtProdutoP.Enabled           := true;
  edtPrecoP.Enabled             := true;
  edtBuscarCliente.Enabled      := true;
  btnSalvar.Enabled             := true;
  habilitacampos(true);
  spbNovaVenda.Enabled          := false;
  btnConsultarPreco.Enabled     := false;
  btnImportar.Enabled           := false;
  //btnReceber.Enabled            := false;
  //btnOrdemEntrega.Enabled       := false;
  btnDevolucao.Enabled          := false;


  actNovaVenda.Enabled          := false;
  //actReceber.Enabled            := false;
  DescricaoSetFocus('');
  lbCaixaIni.Caption            := 'Venda Aberta';
  DBGridP.DataSource.DataSet.Close;
  edtQtdP.Text                  := '1';
  edtPrecoP.Text                := '0,00';
  lblTotalP.Caption             := '0,00';
  DBText1.Caption               := '0,00';
  qryItem.Open();

end;

procedure TFrmBalcao.spbNovaVendaMouseEnter(Sender: TObject);
begin
  Panel3.Color := $00B90000;
//  spbNovaVenda.Font.Color := clWhite;
end;

procedure TFrmBalcao.spbNovaVendaMouseLeave(Sender: TObject);
begin
  Panel3.Color := $0080552D;
//  spbNovaVenda.Font.Color := clBlack;
end;

procedure TFrmBalcao.SpeedButton1Click(Sender: TObject);
begin
  edtQtdP.Text := edtTamanho.Text;
  panel16.Visible := false;

  edtPrecoP.SetFocus;
end;

procedure TFrmBalcao.SpeedButton3Click(Sender: TObject);
begin
  Panel16.Visible := false;

end;

procedure TFrmBalcao.btnOSClick(Sender: TObject);
begin
  frmConsOS := TfrmConsOS.Create(Application);
  frmConsOS.ShowModal;
end;

procedure TFrmBalcao.btnOSMouseEnter(Sender: TObject);
begin
  Panel25.Color := $00B90000;
//  btnOS.Font.Color := clWhite;
end;

procedure TFrmBalcao.btnOSMouseLeave(Sender: TObject);
begin
  Panel25.Color := $0080552D;
//  btnOS.Font.Color := clBlack;
end;

procedure TFrmBalcao.btnOrcamentoClick(Sender: TObject);
begin
  frmOrcamento := TfrmOrcamento.Create(Application);
  frmOrcamento.ShowModal;

end;

procedure TFrmBalcao.btnOrcamentoMouseEnter(Sender: TObject);
begin
  Panel32.Color := $00B90000;
//  btnOrcamento.Font.Color := clWhite;
end;

procedure TFrmBalcao.btnOrcamentoMouseLeave(Sender: TObject);
begin
  Panel32.Color := $0080552D;
//  btnOrcamento.Font.Color := clBlack;
end;

procedure TFrmBalcao.btnOrcamentoVendaClick(Sender: TObject);
var
  cont, tipoFinalizacao : Integer;
begin

  if not btnFinaliza.Visible then
    exit;

   SituacaoPedido;

  //Dados.BloqueiaCliente;


  if Dados.qryConfigCLIENTE_PADRAO.IsNull then
  begin
    ShowMessage
      ('V� � em configura��es e informe o c�digo do cliente Consumidor Final!');
    exit;
  end;

  if not(qryVenda.State in dsEditModes) then
    qryVenda.Edit;

  if (qryVendaNOME.IsNull) or (qryVendaNOME.AsString = '') then
    qryVendaNOME.AsString := qryVendaVIRTUAL_CLIENTE.AsString;
  qryVenda.Post;

  Dados.Conexao.CommitRetaining;



    if (qryItem.IsEmpty) then
    begin
      ShowMessage('Nenhum item foi inserido na venda!');
      DescricaoSetFocus('');
      exit;
    end;

    try
      btnVendedorClick(self);
      frmFechavendaR := TfrmFechaVendaR.Create(Application);
      frmFechavendaR.vPessoa := qryVendaNOME.Value;

      if Dados.qryEmpresaTABELA_PRECO.Value = 'S' then
        frmFechavendaR.Tag := 2
      else
        frmFechavendaR.Tag := 1;

      frmFechavendaR.PageControl1.ActivePageIndex := 0;
      frmFechavendaR.Caption := 'Forma de Pagamento';
      frmFechavendaR.Height := 600;
      Dados.tela := 'FPG';
      Dados.vEnviarSefa := 'N';

      frmFechavendaR.qryVenda.close;
      frmFechavendaR.qryVenda.Params[0].Value := qryVendaCODIGO.Value;
      frmFechavendaR.qryVenda.Open;

      frmFechavendaR.qryItem.close;
      frmFechavendaR.qryItem.Params[0].Value := qryVendaCODIGO.Value;
      frmFechavendaR.qryItem.Open;

      Dados.qryExecute.close;
      Dados.qryExecute.SQL.Text :=
        'update vendas_fpg set valor=0 where vendas_master=:codigo and FEZ_TEF=''N''';
      Dados.qryExecute.Params[0].Value := qryVendaCODIGO.Value;
      Dados.qryExecute.ExecSQL;
      Dados.Conexao.CommitRetaining;

      frmFechavendaR.JvDBGrid1.Visible := true;
      frmFechavendaR.PageControl2.ActivePageIndex := 0;

      frmFechavendaR.pngeral.Enabled := true;
      frmFechavendaR.Totais.Enabled := true;

      if Dados.checanfce(qryVendaCODIGO.Value) then
      begin
        frmFechavendaR.pngeral.Enabled := false;
        frmFechavendaR.Totais.Enabled := false;
      end;
      //frmFechavendaR.ocultaPreVenda := ocultaNaoFiscal;

      frmFechavendaR.ShowModal;
    finally

      //edtFone.Text := '';

      if frmFechavendaR.vFinalizou then
        FinalizaVenda;

      frmFechavendaR.Release;

      Dados.tela := 'PDV';
      //DBGridSetFocus;
      //DescricaoSetFocus('');

    end;


  end;




procedure TFrmBalcao.btnOrdemEntregaClick(Sender: TObject);
begin
   if btnConsultarPreco.Enabled then
      frmControleEntrega := tfrmControleEntrega.Create(Application);
      frmControleEntrega.ShowModal;
end;

procedure TFrmBalcao.btnOrdemEntregaMouseEnter(Sender: TObject);
begin
  Panel30.Color := $00B90000;
//  btnOrdemEntrega.Font.Color := clWhite;
end;

procedure TFrmBalcao.btnOrdemEntregaMouseLeave(Sender: TObject);
begin
  Panel30.Color := $0080552D;
//  btnOrdemEntrega.Font.Color := clBlack;
end;

procedure TFrmBalcao.btnMenuMouseEnter(Sender: TObject);
begin
//  Panel5.Color := clMaroon;
//  btnMenu.Font.Color := clWhite;
end;

procedure TFrmBalcao.btnMenuMouseLeave(Sender: TObject);
begin
//  Panel5.Color := $00F4DEB0;
//  btnMenu.Font.Color := clBlack;
end;

procedure TFrmBalcao.btnFinalizaClick(Sender: TObject);

var
  cont: Integer;
begin
  nImportar := true;

  if not btnFinaliza.Visible then
    exit;

  try
    if not (pedidoImportado = 1) or (pedidoImportado = null) then
    begin
      frmTipoFinalizacao := TfrmTipoFinalizacao.Create(Application);
      frmTipoFinalizacao.ShowModal;
    end;

    if tipoFinalizacao = 3 then
    begin
      abort;
    end;

    if tipoFinalizacao = 1 then
    begin
      SituacaoPedido;  //Verifica se o pedido j� foi finalizado em outra maquina

      // Dados.BloqueiaCliente;

      if Dados.qryConfigCLIENTE_PADRAO.IsNull then
      begin
        ShowMessage
          ('V� � em configura��es e informe o c�digo do cliente Consumidor Final!');
        exit;
      end;

      if not(qryVenda.State in dsEditModes) then
        qryVenda.Edit;

      if (qryVendaNOME.IsNull) or (qryVendaNOME.AsString = '') then
        qryVendaNOME.AsString := qryVendaVIRTUAL_CLIENTE.AsString;
        qryVendaID_CLIENTE.AsInteger := qryClienteCODIGO.AsInteger;
      qryVenda.Post;

      Dados.Conexao.CommitRetaining;

      if Dados.TerminalCaixa then
      begin

        if (qryItem.IsEmpty) then
        begin
          ShowMessage('Nenhum item foi inserido na venda!');
          DescricaoSetFocus('');
          exit;
        end;

        try
          //btnVendedorClick(self);
          frmFechavendaR := TfrmFechaVendaR.Create(Application);
          frmFechavendaR.vPessoa := qryVendaNOME.Value;

          if Dados.qryEmpresaTABELA_PRECO.Value = 'S' then
            frmFechavendaR.Tag := 2
          else
            frmFechavendaR.Tag := 1;

          frmFechavendaR.PageControl1.ActivePageIndex := 0;
          frmFechavendaR.Caption := 'Forma de Pagamento';
          frmFechavendaR.Height := 600;
          Dados.tela := 'FPG';
          Dados.vEnviarSefa := 'N';

          frmFechavendaR.qryVenda.close;
          frmFechavendaR.qryVenda.Params[0].Value := qryVendaCODIGO.Value;
          frmFechavendaR.qryVenda.Open;

          frmFechavendaR.qryItem.close;
          frmFechavendaR.qryItem.Params[0].Value := qryVendaCODIGO.Value;
          frmFechavendaR.qryItem.Open;

          Dados.qryExecute.close;
          Dados.qryExecute.SQL.Text :=
            'update vendas_fpg set valor=0 where vendas_master=:codigo and FEZ_TEF=''N''';
          Dados.qryExecute.Params[0].Value := qryVendaCODIGO.Value;
          Dados.qryExecute.ExecSQL;
          Dados.Conexao.CommitRetaining;

          frmFechavendaR.JvDBGrid1.Visible := true;
          frmFechavendaR.PageControl2.ActivePageIndex := 0;

          frmFechavendaR.pngeral.Enabled := true;
          frmFechavendaR.Totais.Enabled := true;

          if Dados.checanfce(qryVendaCODIGO.Value) then
          begin
            frmFechavendaR.pngeral.Enabled := false;
            frmFechavendaR.Totais.Enabled := false;
          end;


          frmFechavendaR.ShowModal;
        finally



          if frmFechavendaR.vFinalizou then
            FinalizaVenda;

          frmFechavendaR.Release;
          frmFechavendaR := nil;
          Dados.tela := 'PDV';

          if pedidoImportado = 1
          then
           if Application.messageBox
              (Pwidechar('Deseja importar outro pedido?'), 'Confirma��o',
              mb_Yesno) = mrYes then
               begin
                  try
                      frmImportarR := TfrmImportarR.Create(application);
                      frmImportarR.PageControl1.ActivePageIndex := 0;
                      frmImportarR.ShowModal;


                    finally
                      frmImportarR.Release;


                        if vFinalizar then
                          if pedidoImportado = 1 then
                            btnFinalizaClick(self);

                  end;

               end
           else
              begin
                pedidoImportado := 0;
                nImportar := false;
              end;
          

        end;
      end
      else
      begin

        qryItem.Refresh;

        if (qryItem.IsEmpty) then
        begin
          ShowMessage('Nenhum item foi inserido na venda!');
          DescricaoSetFocus('');
          exit;
        end;

        try


          frmFechavendaR := TfrmFechaVendaR.Create(Application);
          frmFechavendaR.vPessoa := qryVendaNOME.Value;
          if Dados.qryEmpresaTABELA_PRECO.Value = 'S' then
            frmFechavendaR.Tag := 2
          else
            frmFechavendaR.Tag := 1;
          frmFechavendaR.PageControl1.ActivePageIndex := 0;
          frmFechavendaR.Caption := 'Forma de Pagamento';
          frmFechavendaR.Height := 600;
          Dados.tela := 'FPG';
          Dados.vEnviarSefa := 'N';

          frmFechavendaR.qryVenda.close;
          frmFechavendaR.qryVenda.Params[0].Value := qryVendaCODIGO.Value;
          frmFechavendaR.qryVenda.Open;

          if caixaUnico then
          begin
            frmFechavendaR.qryItem.close;
            frmFechavendaR.qryItem.Params[0].Value := qryVendaCODIGO.Value;
            frmFechavendaR.qryItem.Open;
          end;

          Dados.qryExecute.close;
          Dados.qryExecute.SQL.Text :=
            'update vendas_fpg set valor=0  where vendas_master=:codigo and FEZ_TEF=''N''';
          Dados.qryExecute.Params[0].Value := qryVendaCODIGO.Value;
          Dados.qryExecute.ExecSQL;
          Dados.Conexao.CommitRetaining;

          frmFechavendaR.JvDBGrid1.Visible := true;
          frmFechavendaR.PageControl2.ActivePageIndex := 0;

          frmFechavendaR.pngeral.Enabled := true;
          frmFechavendaR.Totais.Enabled := true;
          frmFechavendaR.edtTroco.Visible := false;
          frmFechavendaR.label7.Visible := false;
          frmFechavendaR.ShowModal;
        finally

          if frmFechavendaR.vFinalizou then
          begin
            FecharPrevenda;
            FinalizaVenda;
          end;

          frmFechavendaR.Release;

          Dados.tela := 'PDV';

          Dados.Conexao.CommitRetaining;

          // DBGridSetFocus;
          // DescricaoSetFocus('');

        end;

      end;
    end
    else
    begin
      SituacaoPedido;

      // Dados.BloqueiaCliente;

      if Dados.qryConfigCLIENTE_PADRAO.IsNull then
      begin
        ShowMessage
          ('V� � em configura��es e informe o c�digo do cliente Consumidor Final!');
        exit;
      end;

      if not(qryVenda.State in dsEditModes) then
        qryVenda.Edit;

      if (qryVendaNOME.IsNull) or (qryVendaNOME.AsString = '') then
        qryVendaNOME.AsString := qryVendaVIRTUAL_CLIENTE.AsString;
        qryVendaID_CLIENTE.AsInteger := qryClienteCODIGO.AsInteger;
      qryVenda.Post;

      Dados.Conexao.CommitRetaining;

      if (qryItem.IsEmpty) then
      begin
        ShowMessage('Nenhum item foi inserido!');
        DescricaoSetFocus('');
        exit;
      end;

      try
        //btnVendedorClick(self);
        frmFechavendaR := TfrmFechaVendaR.Create(Application);
        frmFechavendaR.vPessoa := qryVendaNOME.Value;

        if Dados.qryEmpresaTABELA_PRECO.Value = 'S' then
          frmFechavendaR.Tag := 2
        else
          frmFechavendaR.Tag := 1;

        frmFechavendaR.PageControl1.ActivePageIndex := 0;
        frmFechavendaR.Caption := 'Finalizando Or�amento';
        frmFechavendaR.Height := 400;
        Dados.tela := 'FPG';
        Dados.vEnviarSefa := 'N';

        frmFechavendaR.qryVenda.close;
        frmFechavendaR.qryVenda.Params[0].Value := qryVendaCODIGO.Value;
        frmFechavendaR.qryVenda.Open;

        frmFechavendaR.qryItem.close;
        frmFechavendaR.qryItem.Params[0].Value := qryVendaCODIGO.Value;
        frmFechavendaR.qryItem.Open;

        Dados.qryExecute.close;
        Dados.qryExecute.SQL.Text :=
          'update vendas_fpg set valor=0 where vendas_master=:codigo and FEZ_TEF=''N''';
        Dados.qryExecute.Params[0].Value := qryVendaCODIGO.Value;
        Dados.qryExecute.ExecSQL;
        Dados.Conexao.CommitRetaining;

        frmFechavendaR.JvDBGrid1.Visible := false;
        frmFechavendaR.PageControl2.ActivePageIndex := 1;

        frmFechavendaR.pngeral.Enabled := true;

        // frmFechavenda.Totais.TabVisible := false;
        frmFechavendaR.Totais.Enabled := false;

        if Dados.checanfce(qryVendaCODIGO.Value) then
        begin
          frmFechavendaR.pngeral.Enabled := false;
          frmFechavendaR.Totais.Enabled := false;
        end;
        // frmFechavendaR.ocultaPreVenda := ocultaNaoFiscal;

        frmFechavendaR.ShowModal;
      finally

        // edtFone.Text := '';

        if frmFechavendaR.vFinalizou then
          FinalizaVenda;

        frmFechavendaR.Release;

        Dados.tela := 'PDV';
        // DBGridSetFocus;
        // DescricaoSetFocus('');

      end;
    end;
  finally
      if nImportar then
         frmTipoFinalizacao.Release;

  end;
end;

procedure TFrmBalcao.btnFinalizaMouseEnter(Sender: TObject);
begin
  Panel26.Color := $00B90000;
//  btnFinaliza.Font.Color := clWhite;
end;

procedure TFrmBalcao.btnFinalizaMouseLeave(Sender: TObject);
begin
  Panel26.Color := $0080552D;
//  btnFinaliza.Font.Color := clBlack;
end;

//procedure TFrmBalcao.btnFinalizClick(Sender: TObject);
//var
//  cont: Integer;
//begin
//
//  if not btnFinaliza.Visible then
//    exit;
//
//   //SituacaoPedido;
//
//  //Dados.BloqueiaCliente;
//
//  if Dados.qryConfigCLIENTE_PADRAO.IsNull then
//  begin
//    ShowMessage
//      ('V� � em configura��es e informe o c�digo do cliente Consumidor Final!');
//    exit;
//  end;
//
//  if not(qryVenda.State in dsEditModes) then
//    qryVenda.Edit;
//
//  if (qryVendaNOME.IsNull) or (qryVendaNOME.AsString = '') then
//    qryVendaNOME.AsString := qryVendaVIRTUAL_CLIENTE.AsString;
//  qryVenda.Post;
//
//  Dados.Conexao.CommitRetaining;
//
//  if Dados.TerminalCaixa then
//  begin
//    ShowMessage('teste');
//    if (qryItem.IsEmpty) then
//    begin
//      ShowMessage('Nenhum item foi inserido na venda!');
//      DescricaoSetFocus('');
//      exit;
//    end;
//
//    try
//      frmFechavendaR := TfrmFechaVendaR.Create(Application);
//      frmFechavendaR.vPessoa := qryVendaNOME.Value;
//      if Dados.qryEmpresaTABELA_PRECO.Value = 'S' then
//        frmFechavendaR.Tag := 2
//      else
//        frmFechavendaR.Tag := 1;
//
//      frmFechavendaR.PageControl1.ActivePageIndex := 0;
//      frmFechavendaR.Caption := 'Forma de Pagamento';
//      frmFechavendaR.Height := 600;
//      Dados.tela := 'FPG';
//      Dados.vEnviarSefa := 'N';
//
//      frmFechavendaR.qryVenda.close;
//      frmFechavendaR.qryVenda.Params[0].Value := qryVendaCODIGO.Value;
//      frmFechavendaR.qryVenda.Open;
//
//      frmFechavendaR.qryItem.close;
//      frmFechavendaR.qryItem.Params[0].Value := qryVendaCODIGO.Value;
//      frmFechavendaR.qryItem.Open;
//
//      Dados.qryExecute.close;
//      Dados.qryExecute.SQL.Text :=
//        'update vendas_fpg set valor=0 where vendas_master=:codigo and FEZ_TEF=''N''';
//      Dados.qryExecute.Params[0].Value := qryVendaCODIGO.Value;
//      Dados.qryExecute.ExecSQL;
//      Dados.Conexao.CommitRetaining;
//
//      frmFechavendaR.JvDBGrid1.Visible := true;
//      frmFechavendaR.PageControl2.ActivePageIndex := 0;
//
//      frmFechavendaR.pngeral.Enabled := true;
//      frmFechavendaR.Totais.Enabled := true;
//
//      if Dados.checanfce(qryVendaCODIGO.Value) then
//      begin
//        frmFechavendaR.pngeral.Enabled := false;
//        frmFechavendaR.Totais.Enabled := false;
//      end;
//
//      frmFechavendaR.ShowModal;
//    finally
//
//      //edtFone.Text := '';
//
//      if frmFechavendaR.vFinalizou then
//        FinalizaVenda;
//
//      frmFechavendaR.Release;
//
//      Dados.tela := 'PDV';
//      DBGridSetFocus;
//      DescricaoSetFocus('');
//
//    end;
//  end
//  else
//  begin
//
//    qryItem.Refresh;
//    if (qryItem.IsEmpty) then
//    begin
//      ShowMessage('Digite os itens!');
//      DescricaoSetFocus('');
//      exit;
//    end;
//
//    try
//      frmFechavendaR := TfrmFechaVendaR.Create(Application);
//      frmFechavendaR.vPessoa := qryVendaNOME.Value;
//      if Dados.qryEmpresaTABELA_PRECO.Value = 'S' then
//        frmFechavendaR.Tag := 2
//      else
//        frmFechavendaR.Tag := 1;
//      frmFechavendaR.PageControl1.ActivePageIndex := 0;
//      frmFechavendaR.Caption := 'Forma de Pagamento';
//      frmFechavendaR.Height := 350;
//      Dados.tela := 'FPG';
//      Dados.vEnviarSefa := 'N';
//
//      frmFechavendaR.qryVenda.close;
//      frmFechavendaR.qryVenda.Params[0].Value := qryVendaCODIGO.Value;
//      frmFechavendaR.qryVenda.Open;
//
//      Dados.qryExecute.close;
//      Dados.qryExecute.SQL.Text :=
//        'update vendas_fpg set valor=0  where vendas_master=:codigo and FEZ_TEF=''N''';
//      Dados.qryExecute.Params[0].Value := qryVendaCODIGO.Value;
//      Dados.qryExecute.ExecSQL;
//      Dados.Conexao.CommitRetaining;
//
//      frmFechavendaR.JvDBGrid1.Visible := true;
//      frmFechavendaR.PageControl2.ActivePageIndex := 1;
//
//      frmFechavendaR.ShowModal;
//    finally
//
//      if frmFechavendaR.vFinalizou then
//      begin
//        FecharPrevenda;
//        FinalizaVenda;
//      end;
//
//      frmFechavendaR.Release;
//
//      Dados.tela := 'PDV';
//
//      Dados.Conexao.CommitRetaining;
//
//      DBGridSetFocus;
//      DescricaoSetFocus('');
//
//    end;
//
//  end;
//
//end;

procedure TFrmBalcao.ConfiguraBalanca;
begin
  try

    if not(Dados.qryEmpresaLER_PESO.Value = 'S') then
      exit;

    if ACBrBAL1.Ativo then
      ACBrBAL1.Desativar;

    // configura porta de comunica��o
    ACBrBAL1.modelo := TACBrBALModelo
      (strtointdef(Dados.qryTerminalBALANCA_MARCA.Value, 0));
    ACBrBAL1.Device.HandShake :=
      TACBrHandShake(Dados.qryTerminalBALANCA_HANDSHAKING.AsInteger);
    ACBrBAL1.Device.Parity := TACBrSerialParity
      (strtointdef(Dados.qryTerminalBALANCA_PARIDADE.AsString, 0));
    ACBrBAL1.Device.Stop :=
      TACBrSerialStop
      (strtointdef(Dados.qryTerminalBALANCA_STOPBITS.AsString, 0));

    ACBrBAL1.Device.Data :=
      strtointdef(Dados.qryTerminalBALANCA_DATABITS.Value, 5);
    ACBrBAL1.Device.Baud :=
      strtointdef(Dados.qryTerminalBALANCA_VELOCIDADE.Value, 9600);
    ACBrBAL1.Device.Porta := Dados.qryTerminalBALANCA_PORTA.Value;

    // Conecta com a balan�a
    ACBrBAL1.Ativar;

  except
    on e: Exception do
      raise Exception.Create
        ('Balan�a Habilitada! Verifique a configura��o da Balan�a:' + sLineBreak
        + e.Message);
  end;

end;

procedure TFrmBalcao.ConfiguraBotoes;
begin
  btnMenu.Caption           := 'F1' + #13 + 'Menu';
  btnBuscaAvancada.Caption  := 'F4' + #13 + 'Busca Avan�ada';
  btnCancelVenda.Caption    := 'F6' + #13 + 'Canc.Venda';
  btnVendedor.Caption       := 'F3' + #13 + 'Vendedor';
  btnDelItem.Caption        := 'Del' + #13 + 'Deleta Item';
  btnImportar.Caption       := 'F5' + #13 + 'Importar';
  btnDesconto.Caption       := 'Ctrl + D' + #13 + 'Desconto Item';
  spbNovaVenda.Caption      := 'F5' + #13 + 'Nova Venda';
end;



procedure TFrmBalcao.btnBuscaAvancadaClick(Sender: TObject);
begin
if not btnBuscaAvancada.Visible then
    exit;

  try
    frmPesquisa := tfrmPesquisa.Create(Application);
    frmPesquisa.venda := BuscaNumeroVenda;
    frmPesquisa.ShowModal;
    //frmPesquisa.venda := BuscaNumeroVenda;
  finally
    if trim(frmPesquisa.vDescricao) <> '' then
    begin

      if PageControl2.ActivePage = TabPDV then // PDV
        EdtProdutoP.Text := frmPesquisa.vDescricao;
    end;
    frmPesquisa.Release;
    DescricaoSetFocus('I');

  end;
end;

procedure TFrmBalcao.btnBuscaAvancadaMouseEnter(Sender: TObject);
begin
  Panel4.Color := $00B90000;
//  btnBuscaAvancada.Font.Color := clWhite;
end;

procedure TFrmBalcao.btnBuscaAvancadaMouseLeave(Sender: TObject);
begin
  Panel4.Color := $0080552D;
//btnBuscaAvancada.Font.Color := clBlack;
end;

procedure TFrmBalcao.btnBuscarClienteClick(Sender: TObject);
begin

 if not DBGridPes.Visible then
  begin

    edtBuscarCliente.SetFocus;
    //edtBuscarCliente.SelStart := Length( edtBuscarCliente.Text );
    edtBuscarCliente.SelectAll;

  if not(qryVenda.State in dsEditModes) then
    qryVenda.Edit;
  end
 else
  begin
    DBGridPes.Visible := False;
    pnPessoa.Height := 37;
  end;



end;

procedure TFrmBalcao.btnBuscarClienteMouseEnter(Sender: TObject);
begin
  Panel27.Color := clMaroon;
end;

procedure TFrmBalcao.btnBuscarClienteMouseLeave(Sender: TObject);
begin
  Panel27.Color := $0080552D;
end;

procedure TFrmBalcao.btnAbrirMesaClick(Sender: TObject);
var
  numero: Integer;
begin
  numero := strtointdef(InputBox('Mesa', 'N�mero:', ''), 0);
  if not Dados.qryMesas.Locate('codigo', numero, []) then
    raise Exception.Create('Mesa n�o existe!');

  if Dados.qryMesasSITUACAO.Value = 'O' then
    raise Exception.Create('Mesa j� est� ocupada!');



end;

procedure TFrmBalcao.btnAlterarQtdClick(Sender: TObject);
begin
  panel38.Visible := true;

  lbDescricao.Caption := qryItemDESCRICAO_SL.AsString;
  lbPreco.Caption := FloatToStr(qryItemPRECO.AsFloat);
  edtQtd.Text := FloatToStr(qryItemQTD.AsFloat);
  edtQtd.SetFocus;
  edtQtd.SelectAll;
  //edtQtd.SelStart := Length(edtQtd.Text);

end;

procedure TFrmBalcao.btnAlterarQtdMouseEnter(Sender: TObject);
begin
  Panel37.Color := $00B90000;
end;

procedure TFrmBalcao.btnAlterarQtdMouseLeave(Sender: TObject);
begin
  Panel37.Color := $0080552D;
end;

procedure TFrmBalcao.btnSalvarClick(Sender: TObject);
var
  Tipo : String;
begin

    dados.qryUpdate.Close;
    dados.qryUpdate.SQL.Text :=
    'UPDATE VENDAS_MASTER SET SITUACAO=:SIT, FLAG_NFCE=:FLAG, TIPO=:TIPO, LOTE=:LOTE, NECF=:NECF WHERE CODIGO=:CODIGO';
    dados.qryUpdate.ParamByName('SIT').AsString := 'G';
    dados.qryUpdate.ParamByName('TIPO').AsString := 'O';
    dados.qryUpdate.ParamByName('FLAG').AsString :=  'N';
    dados.qryUpdate.ParamByName('LOTE').AsInteger := dados.Lote;
    dados.qryUpdate.ParamByName('NECF').AsInteger := 0;
    dados.qryUpdate.ParamByName('CODIGO').AsInteger := qryVendaCODIGO.Value;
    dados.qryUpdate.ExecSQL;
    dados.Conexao.CommitRetaining;

    btnConsultarPedido.Enabled := true;
    btnSalvar.Enabled := false;

    habilitacampos(false);
    btnOrdemEntrega.Enabled       := true;
    spbNovaVenda.Enabled          := true;
    btnConsultarPreco.Enabled     := true;
    btnImportar.Enabled           := true;
    btnReceber.Enabled            := true;
    btnDevolucao.Enabled          := true;
    btnDesconto.Enabled           := false;
    actDesconto.Enabled           := false;
    actNovaVenda.Enabled          := true;
    actReceber.Enabled            := true;
    edtBuscarCliente.Enabled      := false;

    FrmBalcao.lbCaixaIni.Caption            := 'Or�amento salvo';
    frmBalcao.ActiveControl                 := nil;
end;

procedure TFrmBalcao.btnSalvarMouseEnter(Sender: TObject);
begin
  Panel33.Color := $00B90000;
//  btnConsultaOrcamento.Font.Color := clWhite;
end;

procedure TFrmBalcao.btnSalvarMouseLeave(Sender: TObject);
begin
  Panel33.Color := $0080552D;
  //btnConsultaOrcamento.Font.Color := clBlack;
end;

procedure TFrmBalcao.btnConsultaPrecoClick(Sender: TObject);
begin
if not btnBuscaAvancada.Visible then
    exit;

  try
    frmPesquisa := tfrmPesquisa.Create(Application);
    frmPesquisa.venda := BuscaNumeroVenda;
    frmPesquisa.ShowModal;
    //frmPesquisa.venda := BuscaNumeroVenda;
  finally
    if trim(frmPesquisa.vDescricao) <> '' then
    begin

      if PageControl2.ActivePage = TabPDV then // PDV
        EdtProdutoP.Text := frmPesquisa.vDescricao;
    end;
    frmPesquisa.Release;
    DescricaoSetFocus('I');

  end;
end;

procedure TFrmBalcao.btnConsultaPrecoMouseEnter(Sender: TObject);
begin
Panel4.Color := clMaroon;
end;

procedure TFrmBalcao.btnConsultaPrecoMouseLeave(Sender: TObject);
begin
Panel4.Color := clTeal;
end;

procedure TFrmBalcao.btnConsultarPedidoClick(Sender: TObject);
begin
  frmPedidoVenda := TfrmPedidoVenda.Create(Application);
  frmPedidoVenda.ShowModal;
end;

procedure TFrmBalcao.btnConsultarPedidoMouseEnter(Sender: TObject);
begin
  Panel23.Color := $00B90000;
//  btnConsultarPedido.Font.Color := clWhite;
end;

procedure TFrmBalcao.btnConsultarPedidoMouseLeave(Sender: TObject);
begin
  Panel23.Color := $0080552D;
//  btnConsultarPedido.Font.Color := clBlack;
end;

procedure TFrmBalcao.btnConsultarPrecoClick(Sender: TObject);
begin
  if btnConsultarPreco.Enabled then
      frmPesquisa := tfrmPesquisa.Create(Application);
      frmPesquisa.venda := BuscaNumeroVenda;
      frmPesquisa.ShowModal;
end;

procedure TFrmBalcao.btnConsultarPrecoMouseEnter(Sender: TObject);
begin
  Panel15.Color := $00B90000;
//  btnConsultarPreco.Font.Color := clWhite;
end;

procedure TFrmBalcao.btnConsultarPrecoMouseLeave(Sender: TObject);
begin
  Panel15.Color := $0080552D;
//  btnConsultarPreco.Font.Color := clBlack;
end;

procedure TFrmBalcao.btnDelItemClick(Sender: TObject);
var
  vMensagem: string;
begin
  try
    if btnDelItem.Enabled = false then
      exit;

    if not btnDelItem.Visible then
      exit;

    vMensagem := 'Deseja Excluir Item?' + sLineBreak +
      GerouNFCe('EXCLUIR ITEM');

    if not qryItem.IsEmpty then
    begin
      if Application.messagebox(pwidechar(vMensagem), 'Confirma��o', mb_yesno) = mrYes
      then
      begin

        if Dados.qryEmpresaEXCLUI_PDV.Value = 'S' then
        Begin
          if not Dados.eSupervisor then
          begin

            try
              frmSupervisor := TFrmSupervisor.Create(Application);
              Dados.vAutorizar := false;
              frmSupervisor.ShowModal;
            finally
              frmSupervisor.Release;
            end;
            if not Dados.vAutorizar then
            begin
              exit;
            end;
          end;
        end;

        qryItem.delete;

        qrySoma.close;
        qrySoma.Params[0].Value := qryVendaCODIGO.Value;
        qrySoma.Open;

        qryVenda.Edit;
        qryVendaSUBTOTAL.Value := qrySomaTOTAL.Value;

        if ((qryVendaFK_OS.AsInteger = 0) or (qryVendaFK_OS.IsNull)) and
          ((qryVendaFKORCAMENTO.AsInteger = 0) or (qryVendaFKORCAMENTO.IsNull))
        then
          qryVendaDESCONTO.Value := 0;

        qryVendaSUBTOTAL.Value := qryVendaSUBTOTAL.Value;
        qryVenda.Post;

        Dados.Conexao.CommitRetaining;
      end;
    end;
  finally
    DescricaoSetFocus('I');
  end;
end;

procedure TFrmBalcao.btnDelItemMouseEnter(Sender: TObject);
begin
//Panel12.Color := clMaroon;
Panel12.Color := $00B90000;
end;

procedure TFrmBalcao.btnDelItemMouseLeave(Sender: TObject);
begin
  Panel12.Color := $0080552D;
//btnDelItem.Font.Color := clBlack;
end;


function TFrmBalcao.RetornaMaiorItem(idVenda: Integer): Integer;
begin
  Dados.qryConsulta.close;
  Dados.qryConsulta.SQL.Clear;
  Dados.qryConsulta.SQL.Add
    ('Select coalesce(max(item),0)+1 item from vendas_detalhe');
  Dados.qryConsulta.SQL.Add('where');
  Dados.qryConsulta.SQL.Add('fkvenda=:codigo');

  Dados.qryConsulta.ParamByName('codigo').AsInteger := idVenda;

  Dados.qryConsulta.Open;
  result := Dados.qryConsulta.FieldByName('item').AsInteger;
end;



procedure TFrmBalcao.InsereItem(Descricao, OBSERVACAO: String;
  Preco, Qtd, Total: Extended);
var
  codigoPro, idItem, MaiorItem: Integer;
begin

  //PodeAtualizarEstoque := true;


  // ShowMessage('Perdcentual: '+floattostr(percentual)+' | Valor comiss�o: '+floattostr(valorcomissao));
   //  qryProdVALORCOMISSAO.AsFloat := valorcomissao;


  if (qryVenda.State in dsEditModes) then
    qryVenda.Post;

  Dados.Conexao.CommitRetaining;

  if copy(Descricao, 1, 1) = '*' then
    exit;

  if trim(Descricao) = '' then
    exit;

  if qryPesqProd.IsEmpty then
    exit;

  if Dados.checanfce(qryVendaCODIGO.Value) then
  begin
    if PageControl2.ActivePage = TabPDV then
    begin
      EdtProdutoP.Clear;
      edtPrecoP.Text := '0,00';
      lblTotalP.Caption := '0,00';
      edtQtdP.Text := '1';
      EdtProdutoP.SetFocus;

    end;

    raise Exception.Create
      ('Opera��o n�o permitida, J� existe NFC-e gerada para esta venda!' +
      sLineBreak + 'NFC-e N�mero' + Dados.qryConsulta.FieldByName('NUMERO')
      .AsString);
  end;

  if Preco <= 0 then
  begin
    ShowMessage('Produto est� com o pre�o inv�lido');
    exit;
  end;

  if Qtd <= 0 then
  begin
    ShowMessage('Produto est� com a quantidade inv�lida');
  end;

  if Qtd > 999999 then
  begin
    ShowMessage('Produto est� com a quantidade inv�lida');

    if PageControl2.ActivePage = TabPDV then
    begin
      EdtProdutoP.Clear;
      EdtProdutoP.SetFocus;
      edtQtdP.Text := '1';
    end;

  end;

  if PageControl2.ActivePage = TabPDV then
  begin
    if not ValidaItem(StrToFloatDef(edtQtdP.Text, 1),
      StrToFloatDef(edtPrecoP.Text, 0)) then
    begin
      DescricaoSetFocus('I');
      exit;
    end;
  end;

  if not validaComposicao(qryPesqProdCODIGO.AsInteger) then
  begin

    DescricaoSetFocus('I');
    exit;
  end;

  codigoPro := 0;
  //ShowMessage(IntToStr(qryItem.RecordCount) + ' ' + IntToStr(qryItemID_PRODUTO.Value)  + ' ' + IntToStr(qryPesqProdCODIGO.AsInteger) + ' ' + FloatToStr(qryItemQTD.AsFloat) + ' ' + FloatToStr(qtd) + ' ' + IntToStr(idItem));
  if qryPesqProdFABRICADO.Value <> 'S' then
  begin

    if qryItem.RecordCount > 0 then
      begin
        qryItem.First;
          while not qryItem.Eof do begin
            if qryItemID_PRODUTO.Value = qryPesqProdCODIGO.AsInteger then
              begin
                codigoPro     := 1;
                Qtd           := qryItemQTD.AsVariant + Qtd;
                idItem        := qryItemCODIGO.Value;

                //exit;
              end;
            qryItem.Next;
          end;
      end;

  end;


  if codigoPro = 0 then
  begin

  MaiorItem := RetornaMaiorItem(qryVendaCODIGO.AsInteger);

  try
    Dados.qryExecute.SQL.Clear;

    Dados.qryExecute.SQL.Add('INSERT INTO VENDAS_DETALHE');
    Dados.qryExecute.SQL.Add('(');

    Dados.qryExecute.SQL.Add('CODIGO,');
    Dados.qryExecute.SQL.Add('FKVENDA,');
    Dados.qryExecute.SQL.Add('FK_MESA,');
    Dados.qryExecute.SQL.Add('ID_PRODUTO,');
    Dados.qryExecute.SQL.Add('ITEM,');
    Dados.qryExecute.SQL.Add('COD_BARRA,');
    Dados.qryExecute.SQL.Add('QTD,');
    Dados.qryExecute.SQL.Add('E_MEDIO,');
    Dados.qryExecute.SQL.Add('PRECO,');
    Dados.qryExecute.SQL.Add('VALOR_ITEM,');
    Dados.qryExecute.SQL.Add('VDESCONTO,');
    Dados.qryExecute.SQL.Add('TOTAL,');
    Dados.qryExecute.SQL.Add('SITUACAO,');
    Dados.qryExecute.SQL.Add('UNIDADE,');
    Dados.qryExecute.SQL.Add('QTD_DEVOLVIDA,');
    Dados.qryExecute.SQL.Add('ACRESCIMO,');
    Dados.qryExecute.SQL.Add('OS,');
    Dados.qryExecute.SQL.Add('OBSERVACAO,');
    Dados.qryExecute.SQL.Add('FK_GRADE,');
    //Dados.qryExecute.SQL.Add('VALORCOMISSAOFORNECEDOR,');
   // Dados.qryExecute.SQL.Add('FKFORNECEDOR,');
    Dados.qryExecute.SQL.Add('DATA_VENDA,');
    Dados.qryExecute.SQL.Add('LARGURA,');
    Dados.qryExecute.SQL.Add('COMPRIMENTO,');
    Dados.qryExecute.SQL.Add('ID_MODELO,');
    Dados.qryExecute.SQL.Add('QTD_MODELO,');
    Dados.qryExecute.SQL.Add('ID_SERIAL)');

    Dados.qryExecute.SQL.Add('VALUES (');

    Dados.qryExecute.SQL.Add(':CODIGO,');
    Dados.qryExecute.SQL.Add(':FKVENDA,');
    Dados.qryExecute.SQL.Add(':FK_MESA,');
    Dados.qryExecute.SQL.Add(':ID_PRODUTO,');
    Dados.qryExecute.SQL.Add(':ITEM,');
    Dados.qryExecute.SQL.Add(':COD_BARRA,');
    Dados.qryExecute.SQL.Add(':QTD,');
    Dados.qryExecute.SQL.Add(':E_MEDIO,');
    Dados.qryExecute.SQL.Add(':PRECO,');
    Dados.qryExecute.SQL.Add(':VALOR_ITEM,');
    Dados.qryExecute.SQL.Add(':VDESCONTO,');
    Dados.qryExecute.SQL.Add(':TOTAL,');
    Dados.qryExecute.SQL.Add(':SITUACAO,');
    Dados.qryExecute.SQL.Add(':UNIDADE,');
    Dados.qryExecute.SQL.Add(':QTD_DEVOLVIDA,');
    Dados.qryExecute.SQL.Add(':ACRESCIMO,');
    Dados.qryExecute.SQL.Add(':OS,');
    Dados.qryExecute.SQL.Add(':OBSERVACAO,');
    Dados.qryExecute.SQL.Add(':FK_GRADE,');
    // Dados.qryExecute.SQL.Add(':VALORCOMISSAOFORNECEDOR,');
   // Dados.qryExecute.SQL.Add(':FKFORNECEDOR,');
    Dados.qryExecute.SQL.Add(':DATA_VENDA,');
    Dados.qryExecute.SQL.Add(':LARGURA,');
    Dados.qryExecute.SQL.Add(':COMPRIMENTO,');
    Dados.qryExecute.SQL.Add(':ID_MODELO,');
    Dados.qryExecute.SQL.Add(':QTD_MODELO,');
    Dados.qryExecute.SQL.Add(':ID_SERIAL');

    Dados.qryExecute.SQL.Add(');');

    Dados.qryExecute.ParamByName('ID_PRODUTO').Value :=
      qryPesqProdCODIGO.AsInteger;
    Dados.qryExecute.ParamByName('FKVENDA').Value := qryVendaCODIGO.Value;
    Dados.qryExecute.ParamByName('ITEM').Value :=
      RetornaMaiorItem(qryVendaCODIGO.Value);

    Dados.qryExecute.ParamByName('QTD').AsFloat := Qtd;
    Dados.qryExecute.ParamByName('UNIDADE').AsString :=
      qryPesqProdUNIDADE.Value;

    Dados.qryExecute.ParamByName('OBSERVACAO').AsString := OBSERVACAO;
    Dados.qryExecute.ParamByName('PRECO').AsFloat := Preco;
    Dados.qryExecute.ParamByName('VALOR_ITEM').AsFloat :=
      RoundABNT(Preco * Qtd, 2);
    Dados.qryExecute.ParamByName('COD_BARRA').Value :=
      trim(qryPesqProdCODBARRA.Value);
    Dados.qryExecute.ParamByName('ID_SERIAL').Value := strtointdef(vSerie, 0);
    Dados.qryExecute.ParamByName('E_MEDIO').Value := qryPesqProdE_MEDIO.AsFloat;
    idItem := Dados.Numerador('VENDAS_DETALHE', 'CODIGO', 'N', '', '');

    Dados.qryExecute.ParamByName('FK_MESA').Clear;

    Dados.qryExecute.ParamByName('TOTAL').AsFloat :=
      Dados.qryExecute.ParamByName('VALOR_ITEM').AsFloat;

    Dados.qryExecute.ParamByName('CODIGO').AsFloat := idItem;
    Dados.qryExecute.ParamByName('QTD_DEVOLVIDA').AsFloat := 0;
    Dados.qryExecute.ParamByName('FK_GRADE').AsFloat := FGrade;
    //Dados.qryExecute.ParamByName('VALORCOMISSAOFORNECEDOR').AsFloat := valorcomissao;
   // Dados.qryExecute.ParamByName('FKFORNECEDOR').AsInteger := CodigoFKFornecedor;
    Dados.qryExecute.ParamByName('DATA_VENDA').AsDate := now;
    Dados.qryExecute.ParamByName('LARGURA').AsFloat := StrToFloat(edtLargura.Text);
    Dados.qryExecute.ParamByName('COMPRIMENTO').AsFloat := StrToFloat(edtProfundidade.Text);
    Dados.qryExecute.ParamByName('ID_MODELO').Value := qryPesqMateriaPrimaCODIGO.AsInteger;;
    Dados.qryExecute.ParamByName('QTD_MODELO').AsFloat := StrToFloat(edtTamanho.Text);

    Dados.qryExecute.Prepare;
    Dados.qryExecute.ExecSQL;
    Dados.Conexao.CommitRetaining;

    if (qryPesqProdQTD_ATACADO.AsFloat > 0) and (qryPesqProdPRECO_ATACADO.AsFloat > 0) then
      ChecaATACADO(qryPesqProdCODIGO.AsInteger, qryVendaCODIGO.AsInteger);

    if PageControl2.ActivePage = TabPDV then
    begin
      EdtProdutoP.Clear;
      edtPrecoP.Text := '0,00';
      lblTotalP.Caption := '0,00';
      edtQtdP.Text := '1';
      if PanelPDV.Enabled then
        EdtProdutoP.SetFocus;

    end;

    Dados.Conexao.CommitRetaining;
    qryItem.Refresh;

    qryItem.Locate('codigo', idItem, []);

    CalculaTotalVenda(qryVendaCODIGO.Value);

    InsereComposicao(qryItemID_PRODUTO.Value);
  //------------------------------------------- ATUALIZA ESTOQUE --------------------------------------------------------------
//    if Dados.tela <> 'FPG' then
//    begin
//
//      DMEstoque.AtualizaEstoque(qryItemID_PRODUTO.Value, qryItemQTD.AsFloat, 0,
//        'S', 'R');
//
//      DMEstoque.AtualizaGrade(qryItemID_PRODUTO.Value, qryItemFK_GRADE.Value,
//        qryItemQTD.AsFloat, 'S', 0);
//
//      DMEstoque.AtualizaComposicao(qryItemID_PRODUTO.Value, qryItemQTD.AsFloat,
//        0, 'S', 'R');
//
//    end;
 //---------------------------------------------------------------------------------------------------------------------------
  except
    on e: Exception do
      raise Exception.Create(e.Message + sLineBreak +
        'Entre em contato com o suporte!');
  end;
  end
 else
  begin
    //Atualiza a quantidade do item caso j� esteja lan�ado
   try

    dados.qryUpdate.Close;
    dados.qryUpdate.SQL.Clear;
    dados.qryUpdate.SQL.Text :=
    'UPDATE VENDAS_DETALHE SET QTD=:QTD, VALOR_ITEM=:VALOR_ITEM, TOTAL=:TOTAL WHERE FKVENDA=:FKVENDA AND ID_PRODUTO=:ID_PRODUTO';
    dados.qryUpdate.ParamByName('QTD').AsFloat := Qtd ;
    dados.qryUpdate.ParamByName('FKVENDA').Value := qryVendaCODIGO.Value;;
    dados.qryUpdate.ParamByName('ID_PRODUTO').Value :=
      qryPesqProdCODIGO.AsInteger;;
    Dados.qryUpdate.ParamByName('VALOR_ITEM').AsFloat :=
      RoundABNT(Preco * Qtd, 2);
    Dados.qryUpdate.ParamByName('TOTAL').AsFloat :=
      Dados.qryUpdate.ParamByName('VALOR_ITEM').AsFloat;

    dados.qryUpdate.ExecSQL;
    dados.Conexao.CommitRetaining;

    if (qryPesqProdQTD_ATACADO.AsFloat > 0) and (qryPesqProdPRECO_ATACADO.AsFloat > 0) then
      ChecaATACADO(qryPesqProdCODIGO.AsInteger, qryVendaCODIGO.AsInteger);

    if PageControl2.ActivePage = TabPDV then
    begin
      EdtProdutoP.Clear;
      edtPrecoP.Text := '0,00';
      lblTotalP.Caption := '0,00';
      edtQtdP.Text := '1';
      if PanelPDV.Enabled then
        EdtProdutoP.SetFocus;

    end;

    Dados.Conexao.CommitRetaining;
    qryItem.Refresh;

    qryItem.Locate('codigo', idItem, []);

    CalculaTotalVenda(qryVendaCODIGO.Value);

    InsereComposicao(qryItemID_PRODUTO.Value);
  //------------------------------------------- ATUALIZA ESTOQUE --------------------------------------------------------------
//    if Dados.tela <> 'FPG' then
//    begin
//
//      DMEstoque.AtualizaEstoque(qryItemID_PRODUTO.Value, qryItemQTD.AsFloat, 0,
//        'S', 'R');
//
//      DMEstoque.AtualizaGrade(qryItemID_PRODUTO.Value, qryItemFK_GRADE.Value,
//        qryItemQTD.AsFloat, 'S', 0);
//
//      DMEstoque.AtualizaComposicao(qryItemID_PRODUTO.Value, qryItemQTD.AsFloat,
//        0, 'S', 'R');
//
//    end;
 //---------------------------------------------------------------------------------------------------------------------------
   except
    on e: Exception do
      raise Exception.Create(e.Message + sLineBreak +
        'Entre em contato com o suporte!');
   end;
  end;

end;

procedure TFrmBalcao.ItemDesconhecido;
var
  Descricao: string;
begin
  PrecoVariavel;
  if Dados.qryEmpresaCAIXA_RAPIDO.AsString = 'S' then
  begin
    if PageControl2.ActivePage = TabPDV then
    begin
      Descricao := EdtProdutoP.Text;
      edtPrecoP.ReadOnly := true;
      if ehCaixaRapido = 'N' then
        edtPrecoP.ReadOnly := false;
    end;


  end;

  if qryPesqProdCODIGO.AsInteger = 0 then
    exit;

  if trim(Descricao) <> '' then
  begin

    if (qryPesqProdPRECO_VARIAVEL.Value = 'S') then
    begin
      if PageControl2.ActivePage = TabPDV then
        edtPrecoP.ReadOnly := false;

    end

    else

    begin
      if Dados.qryEmpresaBLOQUEAR_PRECO.Value = 'S' then
      begin
        if PageControl2.ActivePage = TabPDV then
          edtPrecoP.ReadOnly := true;
      end
      else
      begin
        if PageControl2.ActivePage = TabPDV then
          edtPrecoP.ReadOnly := false;

      end;
    end;

  end;
end;

procedure TFrmBalcao.Label9Click(Sender: TObject);
begin

  Panel7.Enabled := True;
  imgLogo.Enabled := True;

  if BuscaNumeroVenda = 0 then
    begin
      //pnTipo.Visible := true;
    end

  //lbCaixaIni.Caption := 'TERMINAL LIVRE';
end;

procedure TfrmBalcao.MakeRounded(Control: TWinControl);
var
R: TRect;
Rgn: HRGN;
begin
with Control do
begin
 R := ClientRect;
 rgn := CreateRoundRectRgn(R.Left, R.Top, R.Right, R.Bottom, 15, 15);
 Perform(EM_GETRECT, 0, lParam(@r));
 InflateRect(r, - 5, - 5);
 Perform(EM_SETRECTNP, 0, lParam(@r));
 SetWindowRgn(Handle, rgn, True);
 Invalidate;
end;
end;

procedure TFrmBalcao.btnImportaClick(Sender: TObject);
begin

  if not btnImportar.Visible then
    exit;

  try
    frmImportarPDV := TfrmImportarPDV.Create(Application);
    frmImportarPDV.ShowModal;
  finally
    frmImportarPDV.Release;
  end;
end;

procedure TFrmBalcao.btnImportarClick(Sender: TObject);
begin
if not btnImportar.Visible then
    exit;

  try
    frmImportarMenuR := TfrmImportarMenuR.Create(Application);
    frmImportarMenuR.ShowModal;

    //vFinalizar := false;

  finally

    if vFinalizar then
      begin
       if pedidoImportado = 1 then
          btnFinalizaClick(self);
      end;

          frmImportarMenuR.Release;
          //vFinalizar := false;


  end;
end;

procedure TFrmBalcao.btnImportarMouseEnter(Sender: TObject);
begin
  Panel13.Color := $00B90000;
  //btnImportar.Font.Color := clWhite;
end;

procedure TFrmBalcao.btnImportarMouseLeave(Sender: TObject);
begin
 Panel13.Color := $0080552D;
// btnImportar.Font.Color := clBlack;
end;

procedure TFrmBalcao.btnMenuClick(Sender: TObject);
begin

   frmMenu:= TfrmMenu.Create(Application);
   frmMenu.ShowModal;

end;


procedure TFrmBalcao.btnNFeClick(Sender: TObject);
begin
  frmConsNFe := tfrmConsNfe.Create(Application);
  frmConsNFe.ShowModal;
end;

procedure TFrmBalcao.btnNFeMouseEnter(Sender: TObject);
begin
  Panel39.Color := $00B90000;
end;

procedure TFrmBalcao.btnNFeMouseLeave(Sender: TObject);
begin
  Panel39.Color := $0080552D;
end;

procedure TFrmBalcao.btnNovaVendaClick(Sender: TObject);
begin


  //btnResumo.Visible := true;
  Dados.tela := 'PDV';
  PesquisaProduto := true;
  spbNovaVenda.Enabled := false;
  habilitacampos(true);
  btnBuscaAvancada.Caption := 'F4' + #13 + 'Busca Avan�ada';
  DescricaoSetFocus('');
  lbCaixaIni.Caption := 'Buscar Produto';
  DBGridP.DataSource.DataSet.Close;
  edtQtdP.Text := '1';
  edtPrecoP.Text := '0,00';
  lblTotalP.Caption := '0,00';
  DBText1.Caption := '0,00';


  btnDesconto.Visible := false;
  if Dados.qryEmpresaDESCONTO_ITEM_PDV.Value = 'S' then
    btnDesconto.Visible := true;
  tamanho;

  DBGridBuscaP.Columns[6].Visible := false;
  if Dados.qryEmpresaEXIBE_ESTOQUE_FISCAL.Value = 'S' then
    DBGridBuscaP.Columns[6].Visible := true;

  if BuscaNumeroVenda = 0 then
    begin
     inserevenda;
    end;

end;

procedure TFrmBalcao.ImprimeItem;
var
  vEndereco, sTexto: String;
  SL: TStringList;
begin

  try
    qryItem.DisableControls;
    SL := TStringList.Create;

    SL.Add('<n>' + Dados.qryEmpresaFANTASIA.AsString + '</n>');

    SL.Add('</linha_simples>');
    sTexto := qryVendaFK_MESA.AsString;
    SL.Add('</ae>Mesa...: ' + sTexto);
    SL.Add('Data/Hora...:' + DateTimeToStr(now));
    SL.Add('</linha_simples>');
    SL.Add(PadRight('PRODUTO', 36) + PadRight('QTD', 6));
    SL.Add('</linha_simples>');

    SL.Add(PadRight(qryItem.FieldByName('DESCRICAO_SL').AsString, 36) +
      PadRight(qryItem.FieldByName('QTD').AsString, 6));
    if trim(qryItem.FieldByName('OBSERVACAO').AsString) <> '' then
      SL.Add(qryItem.FieldByName('OBSERVACAO').AsString);

    SL.Add('</linha_dupla>');
    SL.Add('</corte>');

    DMImpressao.ConfiguraImpressora('');

    SL.SaveToFile(ExtractFilePath(Application.ExeName) + 'Restaurante.txt');

    if DMImpressao.aCBrPosPrinter1.Ativo then
    begin
      DMImpressao.ImprimeLogo;
      DMImpressao.ImprimeTexto(SL.Text);
    end
    else
      ShowMessage('Erro ao imprimir!');
  finally
    SL.Free;
    qryItem.EnableControls;
  end;

end;

procedure TFrmBalcao.ImprimeRestaurante;
begin
  ImprimeRestaurantePadrao;
  ImprimeRestauranteBar;
  ImprimeRestauranteCozinha;

end;

procedure TFrmBalcao.ImprimeRestaurantePadrao;
var
  vEndereco, sTexto: String;
  SL: TStringList;
begin
  if Dados.qryTerminalCAMINHO_BAR.AsString = Dados.qryTerminalCAMINHO_COZINHA.AsString
  then
  begin

    try
      qryItem.DisableControls;
      SL := TStringList.Create;

      SL.Add('<n>' + Dados.qryEmpresaFANTASIA.AsString + '</n>');

      SL.Add('</linha_simples>');

      sTexto := 'Pedido No. ' + FormatFloat('0000', qryVendaCODIGO.AsFloat);
      SL.Add('<n>' + PadCenter(sTexto, Dados.qryTerminalCOLUNAS.AsInteger, '*')
        + '</n>');
      SL.Add('</linha_simples>');
      sTexto := qryVendaFK_MESA.AsString;
      SL.Add('</ae>Mesa...: ' + sTexto);
      SL.Add('Data/Hora...:' + DateTimeToStr(now));
      SL.Add('</linha_simples>');
      SL.Add(PadRight('PRODUTO', 36) + PadRight('QTD', 6));
      SL.Add('</linha_simples>');

      qryItem.First;
      While not qryItem.eof do
      begin
        SL.Add(PadRight(qryItem.FieldByName('DESCRICAO_SL').AsString, 36) +
          PadRight(qryItem.FieldByName('QTD').AsString, 6));
        if trim(qryItem.FieldByName('OBSERVACAO').AsString) <> '' then
          SL.Add(qryItem.FieldByName('OBSERVACAO').AsString);
        SL.Add('</linha_simples>');
        qryItem.Next;
      end;
      SL.Add('</corte>');

      if trim(Dados.qryTerminalCAMINHO_COZINHA.AsString) <> '' then
        DMImpressao.ConfiguraImpressora(Dados.qryTerminalCAMINHO_COZINHA.Value)
      else
        DMImpressao.ConfiguraImpressora('');

      SL.SaveToFile(ExtractFilePath(Application.ExeName) + 'Restaurante.txt');

      if DMImpressao.aCBrPosPrinter1.Ativo then
      begin
        DMImpressao.ImprimeLogo;
        DMImpressao.ImprimeTexto(SL.Text);
      end
      else
        raise Exception.Create('Erro ao imprimir!');
    finally
      SL.Free;
      qryItem.EnableControls;
    end;
  end;

end;

procedure TFrmBalcao.ImprimeRestauranteBar;
var
  vEndereco, sTexto: String;
  SL: TStringList;
  FExisteItem: Boolean;
begin
  FExisteItem := false;
  if Dados.qryTerminalCAMINHO_BAR.AsString <> Dados.qryTerminalCAMINHO_COZINHA.AsString
  then
  begin

    try
      qryItem.DisableControls;
      SL := TStringList.Create;

      SL.Add('<n>' + Dados.qryEmpresaFANTASIA.AsString + '</n>');

      SL.Add('</linha_simples>');

      sTexto := 'Pedido No. ' + FormatFloat('0000', qryVendaCODIGO.AsFloat);
      SL.Add('<n>' + PadCenter(sTexto, Dados.qryTerminalCOLUNAS.AsInteger, '*')
        + '</n>');
      SL.Add('</linha_simples>');
      sTexto := qryVendaFK_MESA.AsString;
      SL.Add('</ae>Mesa...: ' + sTexto);
      SL.Add('Data/Hora...:' + DateTimeToStr(now));
      SL.Add('</linha_simples>');
      SL.Add(PadRight('PRODUTO', 36) + PadRight('QTD', 6));
      SL.Add('</linha_simples>');

      qryItem.First;
      While not qryItem.eof do
      begin
        if qryItemVIRTUAL_TIPO_ALIMENTO.Value = 'B' then
        begin
          FExisteItem := true;
          SL.Add(PadRight(qryItem.FieldByName('DESCRICAO_SL').AsString, 36) +
            PadRight(qryItem.FieldByName('QTD').AsString, 6));
          if trim(qryItem.FieldByName('OBSERVACAO').AsString) <> '' then
            SL.Add(qryItem.FieldByName('OBSERVACAO').AsString);
          SL.Add('</linha_simples>');

        end;
        qryItem.Next;
      end;
      SL.Add('</corte>');

      if FExisteItem then
      begin

        if trim(Dados.qryTerminalCAMINHO_BAR.AsString) <> '' then
          DMImpressao.ConfiguraImpressora(Dados.qryTerminalCAMINHO_BAR.Value)
        else
          DMImpressao.ConfiguraImpressora('');

        SL.SaveToFile(ExtractFilePath(Application.ExeName) + 'Restaurante.txt');

        if DMImpressao.aCBrPosPrinter1.Ativo then
        begin
          DMImpressao.ImprimeLogo;
          DMImpressao.ImprimeTexto(SL.Text);
        end
        else
          raise Exception.Create('Erro ao imprimir!');
      end;

    finally
      SL.Free;
      qryItem.EnableControls;
    end;
  end;

end;

procedure TFrmBalcao.ImprimeRestauranteCozinha;
var
  vEndereco, sTexto: String;
  SL: TStringList;
  FExisteItem: Boolean;
begin
  FExisteItem := false;
  if Dados.qryTerminalCAMINHO_BAR.AsString <> Dados.qryTerminalCAMINHO_COZINHA.AsString
  then
  begin

    try
      qryItem.DisableControls;
      SL := TStringList.Create;

      SL.Add('<n>' + Dados.qryEmpresaFANTASIA.AsString + '</n>');

      SL.Add('</linha_simples>');

      sTexto := 'Pedido No. ' + FormatFloat('0000', qryVendaCODIGO.AsFloat);
      SL.Add('<n>' + PadCenter(sTexto, Dados.qryTerminalCOLUNAS.AsInteger, '*')
        + '</n>');
      SL.Add('</linha_simples>');
      sTexto := qryVendaFK_MESA.AsString;
      SL.Add('</ae>Mesa...: ' + sTexto);
      SL.Add('Data/Hora...:' + DateTimeToStr(now));
      SL.Add('</linha_simples>');
      SL.Add(PadRight('PRODUTO', 36) + PadRight('QTD', 6));
      SL.Add('</linha_simples>');

      qryItem.First;
      While not qryItem.eof do
      begin
        if qryItemVIRTUAL_TIPO_ALIMENTO.Value <> 'B' then
        begin
          FExisteItem := true;
          SL.Add(PadRight(qryItem.FieldByName('DESCRICAO_SL').AsString, 36) +
            PadRight(qryItem.FieldByName('QTD').AsString, 6));
          if trim(qryItem.FieldByName('OBSERVACAO').AsString) <> '' then
            SL.Add(qryItem.FieldByName('OBSERVACAO').AsString);
          SL.Add('</linha_simples>');

        end;
        qryItem.Next;
      end;
      SL.Add('</corte>');

      if FExisteItem then
      begin

        if trim(Dados.qryTerminalCAMINHO_COZINHA.AsString) <> '' then
          DMImpressao.ConfiguraImpressora
            (Dados.qryTerminalCAMINHO_COZINHA.Value)
        else
          DMImpressao.ConfiguraImpressora('');

        SL.SaveToFile(ExtractFilePath(Application.ExeName) + 'Restaurante.txt');

        if DMImpressao.aCBrPosPrinter1.Ativo then
        begin
          DMImpressao.ImprimeLogo;
          DMImpressao.ImprimeTexto(SL.Text);
        end
        else
          raise Exception.Create('Erro ao imprimir!');
      end;

    finally
      SL.Free;
      qryItem.EnableControls;
    end;
  end;

end;

procedure TFrmBalcao.ImprimeDelivery;
var
  vEndereco, sTexto: String;
  SL: TStringList;
begin

  try
    qryItem.DisableControls;
    SL := TStringList.Create;

    SL.Add('<n>' + Dados.qryEmpresaFANTASIA.AsString + '</n>');

    SL.Add('</linha_simples>');

    sTexto := 'Pedido No. ' + FormatFloat('0000', qryVendaCODIGO.AsFloat);
    SL.Add('<n>' + PadCenter(sTexto, Dados.qryTerminalCOLUNAS.AsInteger, '*')
      + '</n>');
    SL.Add('</linha_simples>');
    sTexto := qryVendaFK_MESA.AsString;

    SL.Add('Data:' + qryVendaDATA_EMISSAO.AsString);
    SL.Add('Hora......:' + timetostr(time));
    SL.Add('</linha_simples>');
    SL.Add('Cliente..:' + qryVendaVIRTUAL_CLIENTE.AsString);
    SL.Add('Endereco.: ' + qryVendaVIRTUAL_ENDERECO.AsString + ',' +
      qryVendaVIRTUAL_NUMERO.AsString);
    SL.Add('Comp.....:' + qryVendaVIRTUAL_COMPLEMENTO.AsString);
    SL.Add('Bairro...: ' + qryVendaVIRTUAL_BAIRRO.AsString);
    SL.Add('Fone.....: ' + qryVendaVIRTUAL_CELULAR.AsString);
    SL.Add('</linha_simples>');

    SL.Add('<c>' + PadRight('COD', 5) + PadRight('PRODUTO', 26) +
      PadRight('QTD', 6) + PadRight('VALOR', 8) + PadLeft('TOTAL', 10)
      + '</c>');
    SL.Add('</linha_simples>');

    qryItem.First;
    While not qryItem.eof do
    begin
      SL.Add('<c>' + PadRight(qryItem.FieldByName('ID_PRODUTO').AsString, 5) +
        PadRight(qryItem.FieldByName('DESCRICAO_SL').AsString, 24) +
        PadLeft(qryItem.FieldByName('QTD').AsString, 6) +
        PadLeft(FormatFloat('0.00', qryItem.FieldByName('PRECO').AsFloat), 8) +
        PadLeft(FormatFloat('0.00', qryItem.FieldByName('VALOR_ITEM').AsFloat),
        10) + '</c>');

      if trim(qryItemOBSERVACAO.AsString) <> '' then
        SL.Add('<c>' + qryItemOBSERVACAO.Value + '</c>');

      SL.Add('</linha_simples>');
      qryItem.Next;
    end;
    if trim(qryVendaOBSERVACOES.AsString) <> '' then
      SL.Add('Observa��es:' + qryVendaOBSERVACOES.Value);
    SL.Add('</linha_dupla>');
    SL.Add('</corte>');

    DMImpressao.ConfiguraImpressora('');

    SL.SaveToFile(ExtractFilePath(Application.ExeName) + 'delivery.txt');

    if DMImpressao.aCBrPosPrinter1.Ativo then
    begin
      DMImpressao.ImprimeLogo;
      DMImpressao.ImprimeTexto(SL.Text);
    end
    else
      ShowMessage('Erro ao imprimir!');
  finally
    SL.Free;
    qryItem.EnableControls;
  end;

end;

procedure TFrmBalcao.qryConta_MovimentoBeforePost(DataSet: TDataSet);
begin
  if qryConta_Movimento.State = dsInsert then
    qryConta_MovimentoCODIGO.AsFloat := Dados.Numerador('CONTAS_MOVIMENTO',
      'CODIGO', 'N', '', '');
end;

procedure TFrmBalcao.CalcTotalMesa;
begin

  try

    qryItem.DisableControls;

    qryItem.First;

    while not qryItem.eof do
    begin

      CalculaTotalVenda(qryVendaCODIGO.Value);
      InsereComposicao(qryItemID_PRODUTO.Value);

      qryItem.Next;

    end;
  finally
    qryItem.EnableControls;
  end;
end;

procedure TFrmBalcao.GetCliente;
begin
  if (qryVenda.State in dsEditModes) then
    qryVenda.Edit;

  if not qryCliente.IsEmpty then
  begin
    qryVendaID_CLIENTE.AsInteger := qryClienteCODIGO.AsInteger;
    qryVendaNOME.AsString := qryClienteRAZAO.AsString;
    qryVenda.Post;

      Dados.Conexao.CommitRetaining;
  end
  else
  begin
    raise Exception.Create('Pessoa n�o foi encontrada!');
    qryVendaID_CLIENTE.Clear;
    qryVendaNOME.Clear;
  end;
end;

procedure TFrmBalcao.GetIdCliente;
begin
  if not(qryVenda.State in dsEditModes) then
    qryVenda.Edit;
  qryVendaID_CLIENTE.Value := qryBuscaFoneCODIGO.Value;
  qryVenda.Post;
  Dados.Conexao.CommitRetaining;

end;

procedure TFrmBalcao.HabilitaBotoes;
begin
 btnMenu.Enabled := true;
end;

procedure TFrmBalcao.VerificaFone;
begin
//  if Length(TiraPontos(edtFone.EditText)) <> 9 then
//  begin
//    if pnDelivery.Enabled then
//      edtFone.SetFocus;
//    raise Exception.Create('N�mero de Telefone Inv�lido!');
//  end;
end;

procedure TFrmBalcao.AtualizaPreco(NovoPreco: Extended);
var
  FPreco, FPrecoMinimo: Extended;
begin
  FPrecoMinimo := 0;
  FPreco := 0;

  Dados.qryConsulta.close;
  Dados.qryConsulta.SQL.Text :=
    'select QTD_ATACADO, DESCONTO, PRECO_ATACADO, PR_VENDA from produto where codigo=:codigo';
  Dados.qryConsulta.Params[0].Value := qryItemID_PRODUTO.Value;
  Dados.qryConsulta.Open;

  if Dados.qryConsulta.FieldByName('QTD_ATACADO').AsFloat > 0 then
  // checa se quantidade atacado
  Begin
    if qryItemQTD.AsFloat > Dados.qryConsulta.FieldByName('QTD_ATACADO').AsFloat
    then
      FPreco := Dados.qryConsulta.FieldByName('PRECO_ATACADO').AsFloat
    else
      FPreco := Dados.qryConsulta.FieldByName('PR_VENDA').AsFloat
  End
  else
    FPreco := Dados.qryConsulta.FieldByName('PR_VENDA').AsFloat;

  if Dados.qryConsulta.FieldByName('DESCONTO').AsFloat > 0 then
  begin
    // checa se existe desconto

    FPrecoMinimo := FPreco -
      ((FPreco * Dados.qryConsulta.FieldByName('DESCONTO').AsFloat) / 100);

    if NovoPreco < FPrecoMinimo then
      raise Exception.Create('Desconto maior que o permitido!');

  end;

  qryItem.Edit;
  qryItemPRECO.AsFloat := NovoPreco;
  qryItemVALOR_ITEM.AsFloat := RoundABNT(NovoPreco * qryItemQTD.AsFloat, 2);
  qryItem.Post;
  Dados.Conexao.CommitRetaining;

end;

procedure TFrmBalcao.AtualizaTabelaPreco;
var
  vQtd: real;

begin
  qryTabelaPreco.Close;
  qryTabelaPreco.Params[0].Value := frmTabelaPrecoProdutoR.idPrecoProduto;
  qryTabelaPreco.Open;
  if qryTabelaPrecoPRECO.AsFloat > 0 then
  begin
    vQtd := StrToFloatDef(edtQtdP.Text, 1);
    edtPrecoP.Text := FormatFloat('0.00', qryTabelaPrecoPRECO.AsFloat);
    lblTotalP.Caption := FormatFloat('0.00', qryTabelaPrecoPRECO.AsFloat * vQtd);
  end;


end;

procedure TFrmBalcao.AbreVenda(Codigo: Integer; tela: String);
begin
  qryVenda.close;
  qryVenda.Params[0].AsInteger := Codigo;
  qryVenda.Params[1].AsString := tela;
  qryVenda.Open;
end;

procedure TFrmBalcao.Retaurante_abre_mesa;
begin
  Dados.qryMesas.close;
  Dados.qryMesas.Open;
end;

procedure TFrmBalcao.Restaurante_Status_Mesa;
begin
//  if Dados.qryMesasSITUACAO.Value = 'O' then
//    PanelRestaurante.Enabled := true
//  else
//    PanelRestaurante.Enabled := false;
end;

procedure TFrmBalcao.CaixaHoje;
begin
  if Dados.qryConsulta.FieldByName('DATA_ABERTURA').Value < date then
  begin
    //btnCaixa.Caption            := 'F2' + #13 + 'Fechar Caixa';
    //btnCaixa.Tag                := 2;
    btnImportar.Enabled         := false;
    //BtnSuprimento.Enabled       := false;
    //btnSangria.Enabled          := false;
    btnReceber.Enabled          := false;
    PanelPDV.Enabled            := false;
    spbNovaVenda.Enabled        := false;
    //btnTef.Enabled              := false;
    btnBuscarCliente.Enabled    := false;
    pnPessoa.Visible            := false;
    panel36.Visible             := false;
    panel2.Visible              := false;
    pnFecharCaixa.Visible       := true;
    label5.Caption              := 'Caixa n�o � de hoje, aperte a tecla F2 para efetuar o fechamento.';
    pnFecharCaixa.Align         := alClient;
    Panel7.Visible              := false;
    habilitacampos(false);

    raise Exception.Create('Caixa n�o � de hoje, aperte a tecla F1 Menu e depois F2 Fechar Caixa!');

  end
//  else
//  begin
//    btnCaixa.Caption := 'F2' + #13 + 'Fechar Caixa';
//    btnCaixa.Tag := 2;
//  end;
end;

function TFrmBalcao.SituacaoCaixa: Boolean;
begin
  // verifica se caixa est� aberto
  result := false;
  Dados.qryConsulta.close;
  Dados.qryConsulta.SQL.Text :=
    'select CODIGO, DATA_ABERTURA, LOTE, SITUACAO from contas where id_usuario=:id AND SITUACAO=''A''';
  Dados.qryConsulta.Params[0].Value := Dados.idUsuario;
  Dados.qryConsulta.Open;
  if Dados.qryConsulta.FieldByName('SITUACAO').AsString = 'A' then
    result := true;
end;

procedure TFrmBalcao.ChamaAbertura;
begin
  if not Dados.vRetaguarda then
  begin
    try
      frmSplash := TfrmSplash.Create(Application);
      frmSplash.ShowModal;
    finally
      frmSplash.Release;
    end;
  end;
end;

procedure TFrmBalcao.HabilitaPreVenda;
begin
  Caption := 'Bal��o - Pedido de Venda';
  StatusBar1.Panels[0].Text := 'PR�-VENDA';
  StatusBar1.Panels[1].Text := 'Usu�rio:' + Dados.vUsuario;
  StatusBar1.Panels[2].Text := 'Vendedor:' + qryVendaVIRTUAL_VENDEDOR.Value;
  habilitacampos(false);

//  btnCaixa.Enabled      := false;
//  btnResumo.Enabled     := false;
//  btnSangria.Enabled    := false;
//  BtnSuprimento.Enabled := false;
  btnNfe.Enabled        := false;
  //btnTef.Enabled        := false;
  btnReceber.Visible    := false;
  btnSalvar.Enabled     := false;
  btnConsultarPreco.Enabled   := true;
  btnImportar.Enabled         := false;
  btnConsultarPedido.Enabled  := true;
  Panel22.Visible             := false;
end;

procedure TFrmBalcao.IdIPWatch1StatusChanged(Sender: TObject);
begin
//   if IdIPWatch1.ForceCheck then
//    ShowMessage('Rede Conectada! ')
//  else
//    ShowMessage('Rede Caiu. Sistema ser� finalizado! ');
end;

procedure TFrmBalcao.Image1Click(Sender: TObject);
begin

Panel7.Enabled := False;
imgLogo.Enabled := False;
lbCaixaIni.Caption := 'MENU CONFIGURA��ES DO CAIXA';
end;

procedure TFrmBalcao.PesquisaTipoTerminal;
begin
  Dados.qryTerminal.close;
  Dados.qryTerminal.Params[0].Value := Dados.Getcomputer;
  Dados.qryTerminal.Open;
  if not Dados.qryTerminal.Locate('nome', Dados.Getcomputer, []) then
  begin
    ShowMessage('Terminal n�o cadastrado!');
    close;
  end;
  Dados.TerminalCaixa := Dados.qryTerminalEH_CAIXA.AsString = 'S';
end;

procedure TFrmBalcao.ConfiguraTipodecaixa;
begin

  if ehCaixaRapido = 'S' then
    begin

    edtQtdP.ReadOnly := true;
    edtPrecoP.ReadOnly := true;
    edtQtdP.Color := $E0E0C2;
    edtPrecoP.Color := $E0E0C2;

    end
  else
    begin
    if Dados.qryEmpresaBLOQUEAR_PRECO.Value = 'S' then
      begin
        edtPrecoP.ReadOnly := true;
      end
      else
      begin
         edtPrecoP.ReadOnly := false;

      end;
  end;
end;

procedure TFrmBalcao.ChamaLogin;
begin
  if not Dados.vRetaguarda then
  begin
    try
      frmAcesso := TfrmAcesso.Create(Application);
      frmAcesso.ShowModal;
    finally
      frmAcesso.Release;
    end;
  end;
end;

procedure TFrmBalcao.qryItemAfterDelete(DataSet: TDataSet);
begin
  Dados.Conexao.CommitRetaining;
  CalculaTotalVenda(qryVendaCODIGO.Value);
end;

procedure TFrmBalcao.qryItemAfterPost(DataSet: TDataSet);
begin

  CalculaTotalVenda(qryVendaCODIGO.Value);

  InsereComposicao(qryItemID_PRODUTO.Value);

  if Dados.FTIpoPDV = 'RESTAURANTE' then
    ImprimeItem;

end;
//--------------------------------- ATUALIZA ESTOQUE -----------------------------------------------------------------------
procedure TFrmBalcao.qryItemBeforeDelete(DataSet: TDataSet);
begin

  if Dados.tela <> 'FPG' then
  begin

    if Dados.checanfce(qryVendaCODIGO.Value) then
    begin
      raise Exception.Create
        ('Opera��o n�o permitida, J� existe NFC-e gerada para esta venda!' +
        sLineBreak + 'NFC-e n�mero' + Dados.qryConsulta.FieldByName('NUMERO')
        .AsString);
    end;

//    DMEstoque.AtualizaEstoque(qryItemID_PRODUTO.Value, qryItemQTD.AsFloat, 0,
//      'E', 'R');
//
//    DMEstoque.AtualizaGrade(qryItemID_PRODUTO.Value, qryItemFK_GRADE.Value,
//      qryItemQTD.AsFloat, 'E', 0);
//
//    DMEstoque.AtualizaComposicao(qryItemID_PRODUTO.Value, qryItemQTD.AsFloat, 0,
//      'E', 'R');

  end;
end;
//----------------------------------------------------------------------------------------------------------------------------
procedure TFrmBalcao.qryItemBeforeOpen(DataSet: TDataSet);
begin
  qryProd.close;
  qryProd.Open;
end;

procedure TFrmBalcao.qryItemBeforePost(DataSet: TDataSet);
var
 valorcomissao, valor, percentual : Real;
begin
  qryItemTOTAL.AsFloat := qryItemVALOR_ITEM.AsFloat - qryItemVDESCONTO.AsFloat +
    qryItemACRESCIMO.AsFloat;

  valor :=  qryItemVALOR_ITEM.AsFloat;
  percentual := qryProdCOMISSAO.AsFloat;

  valorcomissao := valor * percentual / 100;

  //ShowMessage('Valor comiss�o: '+FloatToIntStr(valorcomissao));

 //------------------------------------ ATUALIZA ESTOQUE --------------------------------------------------------------------
//  if Dados.tela <> 'FPG' then
//  begin
//    DMEstoque.AtualizaEstoque(qryItemID_PRODUTO.Value, qryItemQTD.AsFloat, 0,
//      'S', 'R');
//    DMEstoque.AtualizaGrade(qryItemID_PRODUTO.Value, qryItemFK_GRADE.Value,
//      qryItemQTD.AsFloat, 'S', 0);
//
//    DMEstoque.AtualizaComposicao(qryItemID_PRODUTO.Value, qryItemQTD.AsFloat, 0,
//      'S', 'R');
//
//    DMEstoque.AtualizaComposicao(qryItemID_PRODUTO.Value, qryItemQTD.AsFloat, 0,
//      'S', 'R');
//
//  end;
//----------------------------------------------------------------------------------------------------------------------------
end;

procedure TFrmBalcao.qryItemCalcFields(DataSet: TDataSet);
begin
  qryItemDESCRICAO_OBS.AsString := qryItemDESCRICAO_SL.AsString +
    qryItemOBSERVACAO.AsString;
end;

procedure TFrmBalcao.qryItemQTDValidate(Sender: TField);
begin

  if qryItemQTD.Value < 0 then
    raise Exception.Create('Quantidade Inv�lida!');

  if qryItemQTD.Value > 9999 then
    raise Exception.Create('Quantidade maior que o permitido!!');

end;

procedure TFrmBalcao.qryItemVALOR_ITEMValidate(Sender: TField);
begin

  if qryItemVALOR_ITEM.Value < 0 then
    raise Exception.Create('Valor Inv�lido!');

  if qryItemVALOR_ITEM.Value > 999999 then
    raise Exception.Create('Valor maior que o permitido!!');

end;

function TFrmBalcao.ValidaItem(Qtd, vPreco: Extended): Boolean;
var
  Preco, percentual: real;
begin
  if Dados.tela <> 'PDV' then
    exit;

  result := true;

  if Dados.qryEmpresaBLOQUEAR_ESTOQUE_NEGATIVO.Value = 'S' then
  begin

    if qryPesqProdQTD_ATUAL.Value < 0 then
    begin
      if (qryPesqProdSERVICO.Value <> 'S') then
      // se n�o for servico
      begin
        if PageControl2.ActivePage = TabPDV then
        begin
          EdtProdutoP.Clear;
          edtQtdP.Text := '1';
          edtPrecoP.Text := '0,00';
          lblTotalP.Caption := '0,00';
        end;

        result := false;
        raise Exception.Create('Estoque Negativo!');
      end;
    end
    else
    begin
      if qryPesqProdQTD_ATUAL.Value < Qtd then
      begin
        if (qryPesqProdSERVICO.Value <> 'S') then
        // se n�o for servico
        begin
          if PageControl2.ActivePage = TabPDV then
          begin
            EdtProdutoP.Clear;
            edtQtdP.Text := '1';
            edtPrecoP.Text := '0,00';
            lblTotalP.Caption := '0,00';
          end;

          result := false;
          raise Exception.Create('Estoque insuficiente!');
        end;
      end;
    end;

    if qryPesqProd.FieldByName('GRADE').AsString = 'S' then
    begin

      Dados.qryConsulta.close;
      Dados.qryConsulta.SQL.Text :=
        ' select qtd from PRODUTO_GRADE where CODIGO=:cod ';
      Dados.qryConsulta.ParamByName('cod').Value := FGrade;
      Dados.qryConsulta.Open;
      if Dados.qryConsulta.FieldByName('qtd').AsFloat < Qtd then
        raise Exception.Create('Quantidade Grade Insuficiente');
    end;
  end;

  Preco := vPreco;

  if not((qryPesqProdINICIO_PROMOCAO.Value >= date) and
    (date <= qryPesqProdFIM_PROMOCAO.Value)) then
  // n�o est� em promo��o
  begin //

    if qryPesqProdDESCONTO.Value > 0 then
    begin
      percentual := 100 - ((Preco / qryPesqProdPR_VENDA.AsFloat) * 100);
      if percentual > qryPesqProdDESCONTO.AsFloat then
      begin
        ShowMessage('Desconto m�ximo para este produto � de' +
          qryPesqProdDESCONTO.AsString + '%');
        result := false;
        exit;
      end;
    end;

  end
  else
  begin
    if Dados.qryEmpresaDESCONTO_PROD_PROMO.Value = 'S' then
    begin
      if qryPesqProdDESCONTO.Value > 0 then
      begin
        percentual := 100 - ((Preco / qryPesqProdVIRTUAL_PRECO.Value) * 100);
        if percentual > qryPesqProdDESCONTO.AsFloat then
        begin
          ShowMessage('Desconto m�ximo para este produto � de' +
            qryPesqProdDESCONTO.AsString + '%');
          result := false;
          exit;
        end;
      end;
    end
    else
    begin
      if qryPesqProdVIRTUAL_PRECO.AsFloat < Preco then
      begin
        ShowMessage('N�o � permitido descontos para produtos em promo��o!');
        result := false;
        exit;
      end;
    end;
  end;

end;

procedure TFrmBalcao.ProdutoCalcFieldGenerico(Descricao: string; Qtd: Extended);
var
  Qtde: real;
  valida1, valida2: Boolean;
begin

  if Pos('*', Descricao) > 1 then
  begin

    Qtde := StrToFloatDef((copy(Descricao, 1, Pos('*', Descricao) - 1)), 1);
  end
  else
  begin
    Qtde := 0;
    if trim(Qtd.ToString) <> '' then
      Qtde := StrToFloatDef(Qtd.ToString, 1);
  end;

  if (date >= qryPesqProdINICIO_PROMOCAO.AsDateTime) and
    (date <= qryPesqProdFIM_PROMOCAO.AsDateTime) then
  begin
    // produto em promo��o
    qryPesqProdVIRTUAL_PRECO.AsFloat := qryPesqProdPRECO_PROMO_VAREJO.AsFloat;
    if (Qtde >= qryPesqProdQTD_ATACADO.AsFloat) and
      (qryPesqProdQTD_ATACADO.AsFloat > 0) and
      (qryPesqProdPRECO_PROMO_ATACADO.AsFloat > 0) then
      qryPesqProdVIRTUAL_PRECO.AsFloat :=
        qryPesqProdPRECO_PROMO_ATACADO.AsFloat;
  end
  else
  begin

    qryPesqProdVIRTUAL_PRECO.AsFloat := qryPesqProdPR_VENDA.AsFloat;
    if ((Qtde >= qryPesqProdQTD_ATACADO.AsFloat) and
      (qryPesqProdQTD_ATACADO.AsFloat > 0) and (qryPesqProdPRECO_ATACADO.AsFloat
      > 0)) then
      qryPesqProdVIRTUAL_PRECO.AsFloat := qryPesqProdPRECO_ATACADO.AsFloat;

  end;
end;

procedure TFrmBalcao.ProdutoCalcFieldGenericoMateriaPrima(Descricao: string;
  Qtd: Extended);
var
  Qtde: real;
  valida1, valida2: Boolean;
begin

  if Pos('*', Descricao) > 1 then
  begin

    Qtde := StrToFloatDef((copy(Descricao, 1, Pos('*', Descricao) - 1)), 1);
  end
  else
  begin
    Qtde := 0;
    if trim(Qtd.ToString) <> '' then
      Qtde := StrToFloatDef(Qtd.ToString, 1);
  end;

  if (date >= qryPesqMateriaPrimaINICIO_PROMOCAO.AsDateTime) and
    (date <= qryPesqMateriaPrimaFIM_PROMOCAO.AsDateTime) then
  begin
    // produto em promo��o
    qryPesqMateriaPrimaVIRTUAL_PRECO.AsFloat := qryPesqMateriaPrimaPRECO_PROMO_VAREJO.AsFloat;
    if (Qtde >= qryPesqMateriaPrimaQTD_ATACADO.AsFloat) and
      (qryPesqMateriaPrimaQTD_ATACADO.AsFloat > 0) and
      (qryPesqMateriaPrimaPRECO_PROMO_ATACADO.AsFloat > 0) then
      qryPesqMateriaPrimaVIRTUAL_PRECO.AsFloat :=
        qryPesqMateriaPrimaPRECO_PROMO_ATACADO.AsFloat;
  end
  else
  begin

    qryPesqMateriaPrimaVIRTUAL_PRECO.AsFloat := qryPesqMateriaPrimaPR_VENDA.AsFloat;
    if ((Qtde >= qryPesqMateriaPrimaQTD_ATACADO.AsFloat) and
      (qryPesqMateriaPrimaQTD_ATACADO.AsFloat > 0) and (qryPesqMateriaPrimaPRECO_ATACADO.AsFloat
      > 0)) then
      qryPesqMateriaPrimaVIRTUAL_PRECO.AsFloat := qryPesqMateriaPrimaPRECO_ATACADO.AsFloat;

  end;
end;

procedure TFrmBalcao.qryPesqMateriaPrimaCalcFields(DataSet: TDataSet);
begin
  if PageControl2.ActivePage = TabPDV then
    ProdutoCalcFieldGenericoMateriaPrima(edtPesqMateriaPrima.Text, StrToFloatDef(edtQtdP.Text, 1));
end;

procedure TFrmBalcao.qryPesqProdCalcFields(DataSet: TDataSet);
begin
  if PageControl2.ActivePage = TabPDV then
    ProdutoCalcFieldGenerico(edtProdutoP.Text, StrToFloatDef(edtQtdP.Text, 1));
//  if PageControl2.ActivePage = tabRestaurante then
//    ProdutoCalcFieldGenerico(edtProdutoR.Text, StrToFloatDef(edtQtdR.Text, 1));
//  if PageControl2.ActivePage = tabDelivery then
//    ProdutoCalcFieldGenerico(edtProdutoD.Text, StrToFloatDef(EdtQtdD.Text, 1));
end;

procedure TFrmBalcao.CalculaTotalVenda(idVenda: Integer);
begin

  qrySoma.close;
  qrySoma.Params[0].Value := idVenda;
  qrySoma.Open;

  if qrySomaTOTAL.AsFloat = 0 then
  begin
    Dados.qryExecute.close;
    Dados.qryExecute.SQL.Text :=
      'UPDATE VENDAS_MASTER SET SUBTOTAL=:SUBTOTAL,TOTAL=:TOTAL, ACRESCIMO=:ACRESCIMO, DESCONTO=:DESCONTO, PERCENTUAL=:PERCENTUAL, PERCENTUAL_ACRESCIMO=:PERCENTUAL_ACRESCIMO WHERE CODIGO=:CODIGO';
    Dados.qryExecute.ParamByName('TOTAL').Value := 0;
    Dados.qryExecute.ParamByName('SUBTOTAL').Value := 0;
    Dados.qryExecute.ParamByName('ACRESCIMO').Value := 0;

    if ((qryVendaFK_OS.AsInteger = 0) or (qryVendaFK_OS.IsNull)) and
      ((qryVendaFKORCAMENTO.AsInteger = 0) or (qryVendaFKORCAMENTO.IsNull)) then
    begin
      Dados.qryExecute.ParamByName('DESCONTO').Value := 0;
      Dados.qryExecute.ParamByName('PERCENTUAL').Value := 0;
    end;

    Dados.qryExecute.ParamByName('PERCENTUAL_ACRESCIMO').Value := 0;
    Dados.qryExecute.ParamByName('CODIGO').Value := qryVendaCODIGO.Value;
    Dados.qryExecute.ExecSQL;
  end
  else
  begin

    Dados.qryExecute.close;
    Dados.qryExecute.SQL.Text :=
      'UPDATE VENDAS_MASTER SET SUBTOTAL=:SUBTOTAL, TOTAL=:TOTAL,  PERCENTUAL=:PERCENTUAL, PERCENTUAL_ACRESCIMO=:PERCENTUAL_ACRESCIMO WHERE CODIGO=:CODIGO';
    Dados.qryExecute.ParamByName('SUBTOTAL').AsFloat := qrySomaTOTAL.AsFloat;
    Dados.qryExecute.ParamByName('TOTAL').AsFloat :=
      Dados.qryExecute.ParamByName('SUBTOTAL').AsFloat -
      qryVendaDESCONTO.AsFloat + qryVendaACRESCIMO.AsFloat;
    Dados.qryExecute.ParamByName('PERCENTUAL').AsFloat :=
      (FrmBalcao.qryVendaDESCONTO.AsFloat / Dados.qryExecute.ParamByName
      ('SUBTOTAL').AsFloat) * 100;
    Dados.qryExecute.ParamByName('PERCENTUAL_ACRESCIMO').AsFloat :=
      (FrmBalcao.qryVendaACRESCIMO.AsFloat / Dados.qryExecute.ParamByName
      ('SUBTOTAL').AsFloat) * 100;
    Dados.qryExecute.ParamByName('CODIGO').Value := qryVendaCODIGO.Value;
    Dados.qryExecute.ExecSQL;
  end;

  Dados.Conexao.CommitRetaining;
  qryVenda.Refresh;
end;

procedure TFrmBalcao.qryVendaAfterDelete(DataSet: TDataSet);
begin
  CalculaTotalVenda(qryVendaCODIGO.Value);
end;

procedure TFrmBalcao.qryVendaAfterOpen(DataSet: TDataSet);
begin
  qryItem.close;
  qryItem.Open;

end;

procedure TFrmBalcao.qryVendaAfterPost(DataSet: TDataSet);
begin
  CalculaTotalVenda(qryVendaCODIGO.Value);
end;

procedure TFrmBalcao.qryVendaBeforeOpen(DataSet: TDataSet);
begin
  Dados.qryVendedor.close;
  Dados.qryVendedor.Open;

  Dados.qryClientes.close;
  Dados.qryClientes.Params[0].Value := '%';
  Dados.qryClientes.Open;

  qryCliente.close;
  qryCliente.Params[0].Value := '%';
  qryCliente.Params[1].Value := '%';
  qryCliente.Open;

end;

procedure TFrmBalcao.qryVendaCalcFields(DataSet: TDataSet);
begin
//  if (qryVendaID_CLIENTE.AsInteger <> Dados.qryConfigCLIENTE_PADRAO.Value) then
//  begin
//
//     edtCliente.Caption := qryVendaVIRTUAL_CLIENTE.Value + ' | ' +
//      qryVendaVIRTUAL_CELULAR.AsString;
//    pnEndereco.Caption := qryVendaVIRTUAL_ENDERECO.Value + ',' +
//      qryVendaVIRTUAL_NUMERO.Value + ' | ' + qryVendaVIRTUAL_BAIRRO.Value +
//      ' | ' + qryVendaVIRTUAL_COMPLEMENTO.Value;
//  end
//  else
//  begin
//    edtCliente.Caption := 'CLIENTE N�O FOI LOCALIZADO';
//    pnEndereco.Caption := '';
//    edtFone.Text := '';
//  end;
end;

procedure TFrmBalcao.qryVendaDESCONTOValidate(Sender: TField);
begin
  qryVendaTOTAL.Value := qryVendaSUBTOTAL.Value - qryVendaDESCONTO.Value +
    qryVendaACRESCIMO.Value;
end;

procedure TFrmBalcao.btnReceberClick(Sender: TObject);
begin
  if not btnReceber.Visible then
    exit;
  try
    frmConsReceber := TfrmConsReceber.Create(Application);
    Dados.vLancamentoCaixa := true;
    Dados.vidLote := Dados.Lote;
    frmConsReceber.ShowModal;
  finally
    Dados.vLancamentoCaixa := false;
    frmConsReceber.Release;
  end;
end;

procedure TFrmBalcao.btnReceberMouseEnter(Sender: TObject);
begin
  Panel22.Color := $00B90000;
//  btnReceber.Font.Color := clWhite;
end;

procedure TFrmBalcao.btnReceberMouseLeave(Sender: TObject);
begin
  Panel22.Color := $0080552D;
//  btnReceber.Font.Color := clBlack;
end;

procedure TFrmBalcao.PrecoVariavel;
begin
  if Dados.qryEmpresaCAIXA_RAPIDO.AsString = 'S' then
  begin
    if qryPesqProdPRECO_VARIAVEL.Value = 'S' then
    begin
      ehCaixaRapido := 'N';
      edtPrecoP.ReadOnly := false;
    end
    else
      ehCaixaRapido := 'S';
  end;
end;

procedure TFrmBalcao.PesquisaCodBarra(FPesquisa, Descricao: String);
begin
  if copy(Descricao, 1, 1) = Dados.qryConfigPREFIXO_BALANCA.Value then
  begin
    if (Length(FPesquisa) >= 13) then
    begin
      if Pos('*', Descricao) = 0 then
        PesquisaCodBarraBalanca(FPesquisa)
      else

        PesquisaCodBarraGeral(FPesquisa);
    end
    else
      PesquisaCodBarraGeral(FPesquisa);
  end
  else
    PesquisaCodBarraGeral(FPesquisa);

end;

procedure TFrmBalcao.PesquisaDescricao(FPesquisa: String);
// pesquisa
begin

  qryPesqProd.close;
  qryPesqProd.IndexFieldNames := 'DESCRICAO';
  qryPesqProd.SQL.Text := vSql;
  qryPesqProd.SQL.Text := StringReplace(vSql, '/*where*/',
    ' AND ((PRO.DESCRICAO LIKE :DESCRICAO) or (PRO.REFERENCIA LIKE :REF)) ORDER BY DESCRICAO',
    [rfReplaceAll]);
  qryPesqProd.ParamByName('EMP').Value := Dados.qryEmpresaCODIGO.Value; //qryVendaFKEMPRESA.Value;
  if Dados.qryEmpresaPESQUISA_POR_PARTE.Value = 'S' then
    qryPesqProd.ParamByName('DESCRICAO').Value := '%' + FPesquisa + '%'
  else
    qryPesqProd.ParamByName('DESCRICAO').Value := FPesquisa + '%';
  qryPesqProd.ParamByName('REF').Value := copy(FPesquisa, 1, 19) + '%';

  qryPesqProd.Open;

  if not(qryPesqProd.IsEmpty) then
  begin
    PrecoVariavel;
    PreencheBuscaPreco;
    if qryPesqProdTABELA_PRECO.Value = 'S' then
        begin
          DBGridTabelaPreco.Visible := true;
          qryTabelaPreco.Close;
          qryTabelaPreco.Params[0].Value := qryPesqProdCODIGO.Value;
          qryTabelaPreco.Open;
        end
      else
        begin
          DBGridTabelaPreco.Visible := false;
        end;

    ItemDesconhecido;
  end;


end;

procedure TFrmBalcao.PesquisaDescricaoMateriaPrima(FPesquisa: String);
// pesquisa
begin

  qryPesqMateriaPrima.close;
  qryPesqMateriaPrima.IndexFieldNames := 'DESCRICAO';
  qryPesqMateriaPrima.SQL.Text := vSql;
  qryPesqMateriaPrima.SQL.Text := StringReplace(vSql, '/*where*/',
    ' AND ((PRO.DESCRICAO LIKE :DESCRICAO) or (PRO.REFERENCIA LIKE :REF)) AND (PRO.MATERIA_PRIMA=''S'') ORDER BY DESCRICAO',
    [rfReplaceAll]);
  qryPesqMateriaPrima.ParamByName('EMP').Value := Dados.qryEmpresaCODIGO.Value; //qryVendaFKEMPRESA.Value;
  if Dados.qryEmpresaPESQUISA_POR_PARTE.Value = 'S' then
    qryPesqMateriaPrima.ParamByName('DESCRICAO').Value := '%' + FPesquisa + '%'
  else
    qryPesqMateriaPrima.ParamByName('DESCRICAO').Value := FPesquisa + '%';
  qryPesqMateriaPrima.ParamByName('REF').Value := copy(FPesquisa, 1, 19) + '%';

  qryPesqMateriaPrima.Open;

  if not(qryPesqMateriaPrima.IsEmpty) then
  begin
    PrecoVariavel;
    PreencheBuscaPrecoMateriaPrima;
    if qryPesqMateriaPrimaTABELA_PRECO.Value = 'S' then
        begin
          DBGridTabelaPreco.Visible := true;
          qryTabelaPreco.Close;
          qryTabelaPreco.Params[0].Value := qryPesqMateriaPrimaCODIGO.Value;
          qryTabelaPreco.Open;
        end
      else
        begin
          DBGridTabelaPreco.Visible := false;
        end;

    ItemDesconhecido;
  end;


end;

procedure TFrmBalcao.GeraSqlProduto;
begin

  if Dados.qryEmpresaCHECA_ESTOQUE_FISCAL.Value = 'N' then
  begin
    vSql := ' SELECT PRO.CODIGO,' + ' PRO.DESCRICAO,' + ' PRO.CFOP,' +
      ' PRO.CODBARRA,' + ' PRO.NCM, PRO.REFERENCIA,' + ' PRO.PR_VENDA,' +
      ' PRO.PRECO_ATACADO, ' + 'PRO.QTD_ATACADO,' + ' PRO.QTD_ATUAL,' + ' PRO.FOTO,' +
      ' PRO.QTD_FISCAL,' +
      ' PRO.UNIDADE, PRO.EFISCAL,PRO.E_MEDIO, PRO.LOCALIZACAO, PRO.PRODUTO_PESADO,'
      + ' PRO.PRECO_PROMO_VAREJO,PRO.PRECO_PROMO_ATACADO, PRO.PRECO_VARIAVEL, PRO.DESCONTO,'
      + ' PRO.INICIO_PROMOCAO,PRO.FIM_PROMOCAO, PRO.SERVICO, PRO.REMEDIO, PRO.GRADE, PRO.SERIAL, PRO.ULTFORN, PRO.TABELA_PRECO, PRO.FABRICADO, PRO.PREFIXO_BALANCA'
      + ' FROM PRODUTO PRO' +
      ' WHERE (PRO.EMPRESA=:EMP) and (PRO.QTD_ATUAL>0) AND (PRO.ATIVO=''S'') ' +
      '/*where*/';
  end
  else
  begin
    vSql := ' SELECT PRO.CODIGO,' + ' PRO.DESCRICAO,' + ' PRO.CFOP,' +
      ' PRO.CODBARRA,' + ' PRO.NCM, PRO.REFERENCIA,' + ' PRO.PR_VENDA,' +
      ' PRO.PRECO_ATACADO, ' + 'PRO.QTD_ATACADO,' + ' PRO.QTD_ATUAL,' +  ' PRO.FOTO,' +
      ' PRO.QTD_FISCAL,' +
      ' PRO.UNIDADE, PRO.EFISCAL,PRO.E_MEDIO, PRO.LOCALIZACAO, PRO.PRODUTO_PESADO, '
      + ' PRO.PRECO_PROMO_VAREJO,PRO.PRECO_PROMO_ATACADO, PRO.PRECO_VARIAVEL, PRO.DESCONTO,'
      + ' PRO.INICIO_PROMOCAO,PRO.FIM_PROMOCAO, PRO.SERVICO, PRO.REMEDIO, PRO.GRADE, PRO.SERIAL, PRO.ULTFORN, PRO.TABELA_PRECO, PRO.FABRICADO, PRO.PREFIXO_BALANCA '
      + ' FROM PRODUTO PRO' + ' WHERE (PRO.EMPRESA=:EMP) AND (PRO.ATIVO=''S'') '
      + ' /*where*/';
  end;

  qryPesqProd.close;
  qryPesqProd.SQL.Text := vSql;
  qryPesqProd.ParamByName('EMP').Value := qryVendaFKEMPRESA.Value;
  qryPesqProd.Open;

end;

procedure TFrmBalcao.EdtProdutoPChange(Sender: TObject);
var
  Qtd: Integer;
  Descricao: string;
begin

  if PageControl2.ActivePage = TabPDV then
    Descricao := EdtProdutoP.Text;

  if Length(Descricao) = 13 then
    Qtd := 1;

  if PageControl2.ActivePage = TabPDV then // PDV
  begin
    if Pos('*', Descricao) = 0 then
      edtQtdP.Text := '1';

    DBGridBuscaP.Visible := false;
  end;

  if not PesquisaProduto then
    exit;

  vPosicao := Pos('*', trim(Descricao)) + 1;
  vPesquisa := trim(copy((Descricao), vPosicao, 1000));

  if (trim(Descricao) <> '') then
  begin

    if PageControl2.ActivePage = TabPDV then
      DBGridBuscaP.Visible := true;


    if not Dados.EhNumero(vPesquisa) then
    begin
      if Descricao <> '' then
        PesquisaDescricao(vPesquisa);
    end;
  end
  else
  begin
     DBGridTabelaPreco.Visible := false;
  end;
end;

procedure TFrmBalcao.EdtProdutoPKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin

  if (DBGridEh1.Visible)
  then
  begin
    if Key = VK_UP then
    begin

      qryPesqProd.Prior;
      if qryPesqProd.FieldByName('FOTO').Value <> null then
        begin
          DBImage1.Visible := true;
        end
      else
        begin
          DBImage1.Visible := false;
        end;
      PreencheBuscaPreco;


      if qryPesqProdTABELA_PRECO.Value = 'S' then
        begin
          DBGridTabelaPreco.Visible := true;
          qryTabelaPreco.Close;
          qryTabelaPreco.Params[0].Value := qryPesqProdCODIGO.Value;
          qryTabelaPreco.Open;
        end
      else
        begin
          DBGridTabelaPreco.Visible := false;
        end;
      DescricaoSetFocus('I')
    end;
    if Key = VK_DOWN then
    begin

      qryPesqProd.Next;
      if qryPesqProd.FieldByName('FOTO').Value <> null then
        begin
          DBImage1.Visible := true;
        end
      else
        begin
          DBImage1.Visible := false;
        end;
      PreencheBuscaPreco;

      if qryPesqProdTABELA_PRECO.Value = 'S' then
        begin
          DBGridTabelaPreco.Visible := true;
          qryTabelaPreco.Close;
          qryTabelaPreco.Params[0].Value := qryPesqProdCODIGO.Value;
          qryTabelaPreco.Open;
        end
      else
        begin
          DBGridTabelaPreco.Visible := false;
        end;
      DescricaoSetFocus('I')
    end;
  end
  else
  begin
    if Key = VK_UP then
    begin
      qryItem.Prior;
      if qryPesqProdTABELA_PRECO.Value = 'S' then
        begin
          DBGridTabelaPreco.Visible := true;
        end
      else
        begin
          DBGridTabelaPreco.Visible := false;
        end;
  //---------------------------------------------------
      DescricaoSetFocus('I')

    end;
    if Key = VK_DOWN then
    begin
      qryItem.Next;
      if qryPesqProdTABELA_PRECO.Value = 'S' then
        begin
          DBGridTabelaPreco.Visible := true;
        end
      else
        begin
          DBGridTabelaPreco.Visible := false;
        end;
      DescricaoSetFocus('I')
    end;
  end;

end;

procedure TFrmBalcao.BuscaPeso;
begin

  if qryPesqProd.FieldByName('PRODUTO_PESADO').AsString = 'S' then
  begin
    if Dados.qryEmpresaLER_PESO.Value = 'S' then
    begin
      try
        ACBrBAL1.LePeso(2000);
      except
        on e: Exception do
          raise Exception.Create(e.Message);

      end;
    end;

  end;

end;

procedure TFrmBalcao.ChamaSerial;
begin
  vSerie := '0';
  if qryPesqProdSERIAL.Value = 'S' then
  begin
    try
      frmSerial := TfrmSerial.Create(Application);
      frmSerial.ShowModal;
    finally
      vSerie := IntToStr(BaixaSerial(qryPesqProdCODIGO.AsInteger,
        qryVendaCODIGO.AsInteger, StrToFloatDef(edtPrecoP.Text, 0),
        frmSerial.edtSerial.Text));
      frmSerial.Release;
      if ehCaixaRapido = 'S' then
      begin
        if PageControl2.ActivePage = TabPDV then
          InsereItem(EdtProdutoP.Text, '', StrToFloatDef(edtPrecoP.Text, 0),
            StrToFloatDef(edtQtdP.Text, 1),
            StrToFloatDef(lblTotalP.Caption, 0));

      end;
    end;
  end;

end;

procedure TFrmBalcao.ChamaTabelaPreco;
begin
  try
      frmTabelaPrecoProdutoR := TfrmTabelaPrecoProdutoR.Create(Application);
      frmTabelaPrecoProdutoR.qryTabelaPrecoProduto.Close;
      frmTabelaPrecoProdutoR.qryTabelaPrecoProduto.Params[0].Value := qryPesqProdCODIGO.Value;
      frmTabelaPrecoProdutoR.qryTabelaPrecoProduto.Open;
      frmTabelaPrecoProdutoR.ShowModal;
    finally
      AtualizaTabelaPreco;
      frmTabelaPrecoProdutoR.Release;
      if ehCaixaRapido = 'S' then
        edtPrecoP.SetFocus
    end;
end;

procedure TFrmBalcao.EdtProdutoPKeyPress(Sender: TObject; var Key: Char);

var
  Qtd: Integer;
  Descricao: String;

begin
  // produtos

  if PageControl2.ActivePage = TabPDV then
  begin
    Descricao := EdtProdutoP.Text;
    if Length(EdtProdutoP.Text) = 13 then
      Qtd := 1;

    vPosicao := Pos('*', trim(EdtProdutoP.Text)) + 1;
    vPesquisa := trim(copy((EdtProdutoP.Text), vPosicao, 1000));

    if EdtProdutoP.ReadOnly then
      exit;

    if copy(EdtProdutoP.Text, 1, 1) = '*' then
    begin
      if not(Key in ['1' .. '9', #8, #9, #13, #27]) then
        Key := #0;
      exit;
    end;


  end;


  if (Key = #13) then
    begin

      try
      if (Dados.EhNumero(vPesquisa)) and (vPesquisa <> '') then
      begin
        PesquisaCodBarra(vPesquisa, Descricao);
        if not qryPesqProd.IsEmpty then
        begin
          BuscaPeso;
          PreencheBuscaPreco;
          EdtProdutoP.Text := qryPesqProd.FieldByName('descricao').AsString;
          edtQtdP.SetFocus;
        end;
      end
      else
      begin
        if PageControl2.ActivePage = TabPDV then
          AtualizaDescricao(EdtProdutoP.Text);

      if PageControl2.ActivePage = TabPDV then
        FechaVenda(EdtProdutoP.Text);

      //BuscaPeso;
      //PreencheBuscaPreco;
      //EdtProdutoP.Text := qryPesqProd.FieldByName('descricao').AsString;
      //edtQtdP.SetFocus;

      PrecoVariavel;

      if ehCaixaRapido = 'S' then
      begin
        if qryPesqProdFABRICADO.Value = 'S' then
          begin
            Panel16.Visible := true;
            edtLargura.Focused;
          end;
        if qryPesqProdTabela_Preco.Value = 'S' then
          begin
            DBGridTabelaPreco.Visible := false;
            ChamaTabelaPreco;
          end;
        if qryPesqProdGRADE.Value = 'S' then
        begin
          ChamaGrade;
        end
        else if qryPesqProdSERIAL.Value = 'S' then
        begin
          if PageControl2.ActivePage = TabPDV then
            ChamaSerial;
        end
        else
        begin
          if copy(Descricao, 1, 1) = '*' then
          begin
            if not(Key in ['1' .. '9', #8, #9, #13, #27]) then
              Key := #0;
            if PageControl2.ActivePage = TabPDV then
            begin
              EdtProdutoP.Clear;
              EdtProdutoP.SetFocus;
              edtQtdP.Text := '1';

            end;

          end
          else
          begin
            if PageControl2.ActivePage = TabPDV then
            begin
              if ActiveControl = EdtProdutoP then
                EdtProdutoP.SetFocus;
            end;

            BuscaPeso;
            ItemDesconhecido;

            if PageControl2.ActivePage = TabPDV then
              InsereItem(EdtProdutoP.Text, '', StrToFloatDef(edtPrecoP.Text, 0),
                StrToFloatDef(edtQtdP.Text, 1),
                StrToFloatDef(lblTotalP.Caption, 0));
          end;
        end;

      end
      else if trim(Descricao) <> '' then
      begin

        if PageControl2.ActivePage = TabPDV then
        begin

          if qryPesqProdGRADE.Value = 'S' then
            begin
              ChamaGrade;
            end
          else if qryPesqProdSERIAL.Value = 'S' then
            begin
              if PageControl2.ActivePage = TabPDV then
                ChamaSerial;
            end;
          if qryPesqProdTabela_Preco.Value = 'S' then
            begin
              DBGridTabelaPreco.Visible := false;
              ChamaTabelaPreco;
            end;
              edtQtdP.SetFocus;

            if (Dados.qryEmpresa.FieldByName('MARMORARIA').AsString = 'S') then
          begin
            if qryPesqProdFABRICADO.Value = 'S' then
            begin
              Panel16.Visible := true;
              edtPesqMateriaPrima.SetFocus;
              edtPrecoMateriaPrima.Text := '';
              edtPrecoMateriaPrima.Text := '0,00';
              edtLargura.Text := '0,00';
              edtProfundidade.Text := '0,00';
              edtTotalMateriaPrima.Text := '0,00';
              edtAcrescimoValorMateriaPrima.Text := '0,00';
              edtAcrescimoPorcentagemMateriaPrima.Text := '0,00';
            end
            else
            begin
              edtPrecoMateriaPrima.Text := '0,00';
              edtLargura.Text := '0,00';
              edtProfundidade.Text := '0,00';
            end;
          end;

        end;

      end
      else
      begin
        if PageControl2.ActivePage = TabPDV then
          EdtProdutoP.SetFocus;
      end;
   end;
    finally
      if (qryPesqProdGRADE.AsString = 'N') and (ehCaixaRapido = 'S') then
      begin
        if PageControl2.ActivePage = TabPDV then
          EdtProdutoP.Clear;
      end;
    end;
  end;

end;

procedure TFrmBalcao.edtProdutoRChange(Sender: TObject);
var
  Qtd: Integer;
  Descricao: string;
begin

  if Length(Descricao) = 13 then
    Qtd := 1;

  if not PesquisaProduto then
    exit;

  vPosicao := Pos('*', trim(Descricao)) + 1;
  vPesquisa := trim(copy((Descricao), vPosicao, 1000));

  if (trim(Descricao) <> '') then
  begin

    if not Dados.EhNumero(vPesquisa) then
    begin
      if Descricao <> '' then
        PesquisaDescricao(vPesquisa);
    end;
  end;

end;

procedure TFrmBalcao.edtProdutoREnter(Sender: TObject);
begin
  //edtProdutoR.SelectAll;
end;

procedure TFrmBalcao.edtProfundidadeChange(Sender: TObject);
var
calculo : Real;
begin
  if (edtProfundidade.Text <> '') then
    begin
      calculo := (StrToFloat(edtLargura.Text) * StrToFloat(edtProfundidade.Text));
      edtTamanho.Text := FloatToStr(calculo);
    end
  else
    begin
      edtProfundidade.Text := '0,00';
      edtProfundidade.SelectAll;
    end;
  
end;

procedure TFrmBalcao.edtProfundidadeExit(Sender: TObject);
 var
  Total : real;
begin
    Total := StrToFloatDef(edtTamanho.Text, 1) * StrToFloatDef(edtPrecoMateriaPrima.Text, 0);

    edtTotalMateriaPrima.Text := FormatFloat('0.00', RoundABNT(Total, 2));

    edtAcrescimoPorcentagemMateriaPrima.SetFocus;
    edtAcrescimoPorcentagemMateriaPrima.SelectAll;


end;

procedure TFrmBalcao.edtProfundidadeKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key = vk_return then
  BitBtn1.SetFocus;
end;

procedure TFrmBalcao.edtProfundidadeKeyPress(Sender: TObject; var Key: Char);
begin
  if ((key in ['0'..'9'] = false) and (key in [','] = false) and (word(key) <> vk_back)) then
 key := #0;
end;

procedure TFrmBalcao.edtQtdKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
  var
  idItem: Integer;
begin
if Key = vk_return then
  begin
    begin

  if (qryVenda.State in dsEditModes) then
    qryVenda.Post;

  Dados.Conexao.CommitRetaining;

  if StrToFloat(edtQtd.Text) <= 0 then
  begin
    ShowMessage('Produto est� com a quantidade inv�lida');

    edtQtd.SetFocus;
    edtQtd.SelStart := Length(edtQtd.Text);
    exit;
  end;

  if StrToFloat(edtQtd.Text) > 999999 then
  begin
    ShowMessage('Produto est� com a quantidade inv�lida');

    edtQtd.SetFocus;
    edtQtd.SelStart := Length(edtQtd.Text);
    exit;
  end;

  if PageControl2.ActivePage = TabPDV then
  begin
    if not ValidaItem(StrToFloatDef(edtQtdP.Text, 1),
      StrToFloatDef(lbPreco.Caption, 0)) then
    begin
      edtQtd.SetFocus;
      exit;
    end;
  end;

  if not validaComposicao(qryPesqProdCODIGO.AsInteger) then
  begin

    edtQtd.SetFocus;
    exit;
  end;

   idItem        := qryItemCODIGO.Value;
    //Atualiza a quantidade do item caso j� esteja lan�ado
   try

    dados.qryUpdate.Close;
    dados.qryUpdate.SQL.Clear;
    dados.qryUpdate.SQL.Text :=
    'UPDATE VENDAS_DETALHE SET QTD=:QTD, VALOR_ITEM=:VALOR_ITEM, TOTAL=:TOTAL WHERE FKVENDA=:FKVENDA AND ID_PRODUTO=:ID_PRODUTO AND ITEM=:ITEM';
    dados.qryUpdate.ParamByName('QTD').AsFloat := StrToFloat(edtQtd.Text) ;
    dados.qryUpdate.ParamByName('FKVENDA').Value := qryVendaCODIGO.Value;;
    dados.qryUpdate.ParamByName('ID_PRODUTO').Value :=
      qryItemID_PRODUTO.AsInteger;
    dados.qryUpdate.ParamByName('ITEM').Value :=
      qryItemITEM.AsInteger;
    Dados.qryUpdate.ParamByName('VALOR_ITEM').AsFloat :=
      RoundABNT(StrToFloat(lbPreco.Caption) * StrToFloat(edtQtd.Text), 2);
    Dados.qryUpdate.ParamByName('TOTAL').AsFloat :=
      Dados.qryUpdate.ParamByName('VALOR_ITEM').AsFloat;

    dados.qryUpdate.ExecSQL;
    dados.Conexao.CommitRetaining;

    if (qryPesqProdQTD_ATACADO.AsFloat > 0) and (qryPesqProdPRECO_ATACADO.AsFloat > 0) then
      ChecaATACADO(qryPesqProdCODIGO.AsInteger, qryVendaCODIGO.AsInteger);

    Dados.Conexao.CommitRetaining;
    qryItem.Refresh;

    qryItem.Locate('codigo', idItem, []);

    CalculaTotalVenda(qryVendaCODIGO.Value);

    InsereComposicao(qryItemID_PRODUTO.Value);

   except
    on e: Exception do
      raise Exception.Create(e.Message + sLineBreak +
        'Entre em contato com o suporte!');
   end;
  end;

    panel38.Visible := false;
  end;
end;



procedure TFrmBalcao.edtPrecoMateriaPrimaClick(Sender: TObject);
begin
  edtPrecoMateriaPrima.SelectAll;
end;

procedure TFrmBalcao.edtPrecoMateriaPrimaKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key = VK_RETURN then
    edtLargura.SetFocus;
end;

procedure TFrmBalcao.edtQtdPEnter(Sender: TObject);
begin

  if ehCaixaRapido = 'S' then
  begin

    if PageControl2.ActivePage = TabPDV then
      EdtProdutoP.SetFocus;
  end;
end;

procedure TFrmBalcao.edtQtdPExit(Sender: TObject);
var
  Total: real;
begin
  if PageControl2.ActivePage = TabPDV then
  begin
    Total := StrToFloatDef(edtQtdP.Text, 1) * StrToFloatDef(edtPrecoP.Text, 0);
    lblTotalP.Caption := FormatFloat('0.00', RoundABNT(Total, 2));
  end;


end;

procedure TFrmBalcao.edtQtdPKeyPress(Sender: TObject; var Key: Char);
begin
  if not(Key In ['0' .. '9', ',', #8, #9, #13, #27]) then
    Key := #0;

  if Key = #13 then
  begin
     edtPrecoP.SetFocus;
  end;
end;

//procedure TFrmBalcao.FormClick(Sender: TObject);
//begin
//  if Panel31.Visible then
//     Panel31.Visible := false;
//end;

procedure TFrmBalcao.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  if (PageControl2.ActivePage = TabPDV) then
  begin
    if (qryVenda.State in dsEditModes) then
      qryVenda.Post;
    Dados.Conexao.CommitRetaining;
  end
  else
  begin
    if (qryVenda.State = dsInsert) then
      qryVenda.Cancel;
  end;

  qryVenda.close;
  qryItem.close;
  qryPesqProd.close;

end;

procedure TFrmBalcao.CarregaImagem;
begin


  if not Dados.qryParametro.Active then
    Dados.qryParametro.Open;

  if FileExists(Dados.qryParametro.FieldByName('TELA_FUNDO_ECF').AsString) then
  begin
    imgLogo.Picture.LoadFromFile
      (Dados.qryParametro.FieldByName('TELA_FUNDO_ECF').Value);
    Label1.Visible := false;
    imgLogo.BringToFront;
  end
  else
    Label1.Visible := true;

end;

procedure TFrmBalcao.ChecaATACADO(produto, venda: Integer);
begin
  Dados.qryConsulta.close;
  Dados.qryConsulta.SQL.Clear;
  Dados.qryConsulta.SQL.Add
    ('select  vd.id_produto, pro.qtd_atacado, pro.preco_atacado, vd.qtd, SUM(vd.qtd) QTD  from vendas_detalhe vd');
  Dados.qryConsulta.SQL.Add
    ('left join produto pro on pro.codigo=vd.id_produto');
  Dados.qryConsulta.SQL.Add('where');
  Dados.qryConsulta.SQL.Add('vd.fkvenda=:vd and vd.id_produto=:id');
  Dados.qryConsulta.SQL.Add('group by 1,2,3,4');
  Dados.qryConsulta.SQL.Add('having sum(vd.qtd)>=pro.qtd_atacado');
  Dados.qryConsulta.SQL.Add('order by 1');
  Dados.qryConsulta.Params[0].Value := venda;
  Dados.qryConsulta.Params[1].Value := produto;
  Dados.qryConsulta.Open;

  Dados.qryConsulta.First;

  if not Dados.qryConsulta.IsEmpty then
  begin
    Dados.qryExecute.close;
    Dados.qryExecute.SQL.Text :=
      'update vendas_detalhe set preco=:preco,  valor_item= qtd*:preco where ID_PRODUTO=:ID';
    Dados.qryExecute.Params[0].Value := Dados.qryConsulta.Fields[2].AsFloat;
    Dados.qryExecute.Params[1].Value := produto;
    Dados.qryExecute.ExecSQL;
    Dados.Conexao.CommitRetaining;

    { Dados.qryExecute.close;
      Dados.qryExecute.SQL.Text :=
      'update vendas_detalhe set valor_item= qtd*preco where ID_PRODUTO=:ID';
      Dados.qryExecute.Params[0].Value := produto;
      Dados.qryExecute.ExecSQL;
      Dados.Conexao.CommitRetaining; }
  end;

end;

procedure TFrmBalcao.FormCreate(Sender: TObject);
begin
//ShowMessage('Entrei create');
  Left := 0;
  Top := 0;
  Width := Screen.Width;
  Height := Screen.Height;
  TabPDV.TabVisible := false;

  GeraSqlProduto;

  Dados.qryTerminal.close;
  Dados.qryTerminal.Params[0].Value := Dados.Getcomputer;
  Dados.qryTerminal.Open;

  Dados.qryTerminal.Locate('NOME', Dados.Getcomputer, []);

  if not Dados.vRetaguarda then
    Dados.ChecaPDV;

  if Dados.FTIpoPDV = 'PDV' then
    PageControl2.ActivePage := TabPDV;

  qryTabela.close;
  qryTabela.Params[0].Value := Dados.qryEmpresaCODIGO.Value;
  qryTabela.Open;

  Dados.qryConfig.close;
  Dados.qryConfig.Params[0].Value := Dados.qryEmpresaCODIGO.AsInteger;
  Dados.qryConfig.Open;
  ehCaixaRapido := Dados.qryEmpresaCAIXA_RAPIDO.AsString;

  CarregaImagem;

  ChamaAbertura;
  ChamaLogin;

  btnBuscaAvancada.Caption          := 'F4' + #13 + 'Busca Avan�ada';
  btnCancelVenda.Caption            := 'F8' + #13 + 'Cancelar Venda';
  btnVendedor.Caption               := 'F3' + #13 + 'Vendedor';
  btnDelItem.Caption                := 'Del' + #13 + 'Deletar Item';
  btnAlterarQtd.Caption             := 'F10' + #13 + 'Alterar Qtd';
  btnDesconto.Caption               := 'F9' + #13 + 'Desc/Acres F9';
  btnFinaliza.Caption               := 'Finalizar - F12';

  IdIPWatch1.HistoryEnabled := FALSE;
  IdIPWatch1.Active := TRUE;

end;

procedure TFrmBalcao.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin

  if Key = VK_escape then
  begin
    if DBGridPes.Visible then
      begin
        if DBGridPes.Focused then
          begin
            edtBuscarCliente.SetFocus;
            exit;
          end
        else if edtBuscarCliente.Focused then
          begin
            DBGridPes.Visible := False;
            pnPessoa.Height := 37;
            EdtProdutoP.SetFocus;
            exit;
          end;
      end;


    if not DBGridBuscaP.Visible then
    begin
      if Application.messagebox('Tem certeza de que deseja sair?',
        'Confirma��o', mb_yesno) = mrYes then
        close;
    end;


  end;

  if Key = VK_F2 then
  begin
      if pnAbrirCaixa.Visible then
        begin
        //if btnCaixa.Tag = 1 then
          //begin
            AbreCaixa;
            exit;
          //end;
        end;
      if pnFecharCaixa.Visible then
        begin
        //if btnCaixa.Tag = 2 then
          //begin
            FecharCaixa;
            exit;
          //end;
        end
      else
        begin
          if not spbNovaVenda.Enabled then
             btnBuscarClienteClick(self);
        end;

  end;

  if Key = VK_F3 then
  begin
    if btnVendedor.Enabled then
      btnVendedorClick(self);
  end;

  if Key = VK_F4 then
  begin
    if btnBuscaAvancada.Enabled or btnConsultarPreco.Enabled then
      btnBuscaAvancadaClick(self);
  end;

  if Key = VK_F5 then
  begin
    if btnImportar.Enabled then
      btnImportarClick(self);
  end;



  if Key = VK_F8 then
  begin
    if btnCancelVenda.Enabled then
      btnCancelVendaClick(self);
  end;


//  if Key = VK_F10 then
//  begin
//    if btnAlterarQtd.Enabled then
//      btnAlterarQtdClick(self);
//  end;

  if Key = vk_delete then
  begin
    if btnDelItem.Enabled then
    begin

      if (ActiveControl = DBGridP) then
      begin
        btnDelItemClick(self);
        exit;
      end
      else if (ActiveControl = EdtProdutoP) then
      begin
        if EdtProdutoP.Text = '' then
        begin
          btnDelItemClick(self);
          exit;
        end;
      end;
    end;
  end;
end;

procedure TFrmBalcao.ImgOcupadoClick(Sender: TObject);
begin

  if Dados.qryMesas.IsEmpty then
    exit;

  AbreVenda(BuscaNumeroVenda, Dados.FTIpoPDV);

  Restaurante_Status_Mesa;

//  if PanelRestaurante.Enabled then
//    edtProdutoR.SetFocus;

end;

procedure TFrmBalcao.InsereComposicao(produto: Integer);
var
  FQuantidade: Extended;
  ehComposicao: String;

begin

  if Dados.tela = 'FPG' then
    exit;

  Dados.qryConsulta.close;
  Dados.qryConsulta.SQL.Clear;
  Dados.qryConsulta.SQL.Text :=
    'select composicao from produto where codigo=:id';
  Dados.qryConsulta.Params[0].Value := produto;
  Dados.qryConsulta.Open;
  ehComposicao := Dados.qryConsulta.Fields[0].AsString;

  if ehComposicao = 'S' then
  begin

    qryComposicao.close;
    qryComposicao.Params[0].Value := produto;
    qryComposicao.Open;

    qryComposicao.First;
    while not qryComposicao.eof do
    begin

      FQuantidade := qryItemQTD.AsFloat * qryComposicao.FieldByName
        ('QUANTIDADE').AsFloat;

      Dados.qryExecute.SQL.Clear;
      Dados.qryExecute.SQL.Add('INSERT INTO VENDAS_COMPOSICAO (');
      Dados.qryExecute.SQL.Add('CODIGO,');
      Dados.qryExecute.SQL.Add('FK_VENDA_DETALHE,');
      Dados.qryExecute.SQL.Add('ID_PRODUTO,');
      Dados.qryExecute.SQL.Add('QTD,');
      Dados.qryExecute.SQL.Add('QTD_DEVOLVIDA)');
      Dados.qryExecute.SQL.Add('VALUES (');
      Dados.qryExecute.SQL.Add(':CODIGO,');
      Dados.qryExecute.SQL.Add(':FK_VENDA_DETALHE,');
      Dados.qryExecute.SQL.Add(':ID_PRODUTO,');
      Dados.qryExecute.SQL.Add(':QTD,');
      Dados.qryExecute.SQL.Add(':QTD_DEVOLVIDA');
      Dados.qryExecute.SQL.Add(');');

      Dados.qryExecute.ParamByName('CODIGO').Value :=
        Dados.Numerador('VENDAS_COMPOSICAO', 'CODIGO', 'N', '', '');
      Dados.qryExecute.ParamByName('FK_VENDA_DETALHE').Value :=
        qryItemCODIGO.Value;
      Dados.qryExecute.ParamByName('ID_PRODUTO').Value :=
        qryComposicaoID_PRODUTO.Value;
      Dados.qryExecute.ParamByName('QTD').Value := qryItemQTD.AsFloat *
        qryComposicao.FieldByName('QUANTIDADE').AsFloat;

      Dados.qryExecute.ExecSQL;
      Dados.Conexao.CommitRetaining;

      qryComposicao.Next;
    end;
  end;

end;

function TFrmBalcao.validaComposicao(produto: Integer): Boolean;
var
  FQuantidade: Extended;
  ehComposicao: Boolean;

begin

  result := true;

  if Dados.tela = 'FPG' then
    exit;

  Dados.qryConsulta.close;
  Dados.qryConsulta.SQL.Clear;
  Dados.qryConsulta.SQL.Text :=
    'select composicao from produto where codigo=:id and composicao=''S''';
  Dados.qryConsulta.Params[0].Value := produto;
  Dados.qryConsulta.Open;

  ehComposicao := not Dados.qryConsulta.IsEmpty;

  if ehComposicao then
  begin

    qryComposicao.close;
    qryComposicao.Params[0].Value := produto;
    qryComposicao.Open;

    qryComposicao.First;
    while not qryComposicao.eof do
    begin

      FQuantidade := qryItemQTD.AsFloat * qryComposicao.FieldByName
        ('QUANTIDADE').AsFloat;

      if Dados.qryEmpresaBLOQUEAR_ESTOQUE_NEGATIVO.Value = 'S' then
      begin
        Dados.qryConsulta.close;
        Dados.qryConsulta.SQL.Clear;
        Dados.qryConsulta.SQL.Text :=
          'select qtd_atual from produto where codigo=:codigo';
        Dados.qryConsulta.Params[0].AsInteger := qryComposicaoID_PRODUTO.Value;
        Dados.qryConsulta.Open;

        if Dados.qryConsulta.FieldByName('qtd_atual').AsFloat < FQuantidade then
        begin
          result := false;
          raise Exception.Create('Estoque Insuficinete dos Itens deste KIT !');
        end;
      end;
      qryComposicao.Next;
    end;
  end;

end;

procedure TFrmBalcao.ACBrBAL1LePeso(Peso: Double; Resposta: AnsiString);
var
  valid: Integer;
  vPeso: Extended;
begin

  if Peso > 0 then
  begin
    if PageControl2.ActivePage = TabPDV then
      edtQtdP.Text := FormatFloat('0.0000', Peso);
//    if PageControl2.ActivePage = tabRestaurante then
//      edtQtdR.Text := FormatFloat('0.0000', Peso);
//    if PageControl2.ActivePage = tabDelivery then
//      EdtQtdD.Text := FormatFloat('0.0000', Peso);
  end
  else
  begin
    valid := Trunc(ACBrBAL1.UltimoPesoLido);
    case valid of
      0:
        begin
          raise Exception.Create('TimeOut !' + sLineBreak +
            'Coloque o produto sobre a Balan�a!');
          if PageControl2.ActivePage = TabPDV then
            EdtProdutoP.SetFocus;

        end;
      -1:
        begin
          raise Exception.Create('Peso Instavel ! ' + sLineBreak +
            'Tente Nova Leitura');
          if PageControl2.ActivePage = TabPDV then
            EdtProdutoP.SetFocus;


        end;
      -2:
        begin
          raise Exception.Create('Peso Negativo !');
          if PageControl2.ActivePage = TabPDV then
            EdtProdutoP.SetFocus;
//          if PageControl2.ActivePage = tabRestaurante then
//          begin
//            if PanelRestaurante.Enabled then
//              edtProdutoR.SetFocus;
//          end;
//
//          if PageControl2.ActivePage = tabDelivery then
//            edtProdutoD.SetFocus;

        end;
      -10:
        begin
          raise Exception.Create('Sobrepeso !');
          if PageControl2.ActivePage = TabPDV then
            EdtProdutoP.SetFocus;
//          if PageControl2.ActivePage = tabRestaurante then
//          begin
//            if PanelRestaurante.Enabled then
//              edtProdutoR.SetFocus;
//          end;
//          if PageControl2.ActivePage = tabDelivery then
//            edtProdutoD.SetFocus;

        end;
    end;
  end;
end;

procedure TFrmBalcao.actAbrirExecute(Sender: TObject);
begin

  dmnfe.ImpressoraBobina('NFCe');
  if dmnfe.aCBrPosPrinter1.Ativo then
    dmnfe.aCBrPosPrinter1.AbrirGaveta(1);
end;

procedure TFrmBalcao.actAbrirMesaExecute(Sender: TObject);
begin
  btnAbrirMesaClick(self);
end;

procedure TFrmBalcao.actAlteraQtdExecute(Sender: TObject);
begin
  if btnAlterarQtd.Enabled then
      btnAlterarQtdClick(self);
end;

procedure TFrmBalcao.CadPessoaRapido(Codigo: Integer);
begin

  VerificaFone;

  try
    frmCadPessoaRapido := TfrmCadPessoaRapido.Create(Application);
    frmCadPessoaRapido.qryPessoas.close;
    frmCadPessoaRapido.qryPessoas.Params[0].Value := Codigo;
    frmCadPessoaRapido.qryPessoas.Open;
    if frmCadPessoaRapido.qryPessoas.IsEmpty then
    begin
      frmCadPessoaRapido.qryPessoas.Insert;
    end
    else
      frmCadPessoaRapido.qryPessoas.Edit;
    frmCadPessoaRapido.ShowModal;
  finally
    frmCadPessoaRapido.Release;
    qryCliente.close;
    qryCliente.Open;
    //edtFone.SetFocus;
  end;

end;

procedure TFrmBalcao.actConsultaPedidoExecute(Sender: TObject);
begin
  btnConsultarPedidoClick(self);
end;

procedure TFrmBalcao.actDescontoExecute(Sender: TObject);
begin
  if btnDesconto.Enabled then
    btnDescontoClick(self);
end;

procedure TFrmBalcao.actFinalizarExecute(Sender: TObject);
begin
  if btnFinaliza.Enabled then
      btnFinalizaClick(self);
end;

procedure TFrmBalcao.actFiscalExecute(Sender: TObject);
begin
 if ocultaNaoFiscal = 0 then
    begin
      //panel7.Color := $00B6680A;
      ocultaNaoFiscal := 1;
    end
  else
    begin
      //panel7.Color := $003B3B3B;
      ocultaNaoFiscal := 0;
    end;
end;

procedure TFrmBalcao.actNovaVendaExecute(Sender: TObject);
begin

  spbNovaVendaClick(self);
end;

procedure TFrmBalcao.actOrcamentoExecute(Sender: TObject);
begin
  btnOrcamentoClick(self);
end;

procedure TFrmBalcao.actOSExecute(Sender: TObject);
begin

      btnOsClick(self);

end;

procedure TFrmBalcao.actReceberExecute(Sender: TObject);
begin
  if not btnReceber.Visible then
    exit;
  try
    frmConsReceber := TfrmConsReceber.Create(Application);
    Dados.vLancamentoCaixa := true;
    Dados.vidLote := Dados.Lote;
    frmConsReceber.ShowModal;
  finally
    Dados.vLancamentoCaixa := false;
    frmConsReceber.Release;
  end;
end;

function TFrmBalcao.UltimaVenda: Integer;
begin
  result := 0;
  Dados.qryConsulta.close;
  Dados.qryConsulta.SQL.Text :=
    'select ultima_venda, ultimo_pedido from usuarios where codigo=:id';
  Dados.qryConsulta.Params[0].Value := Dados.idUsuario;
  Dados.qryConsulta.Open;
  if Dados.TerminalCaixa then
    result := Dados.qryConsulta.Fields[0].AsInteger
  else
    result := Dados.qryConsulta.Fields[1].AsInteger;
end;

procedure TFrmBalcao.FormShow(Sender: TObject);
begin
  vendaFinalizada := 0;
  Dados.tela := 'PDV';
  PesquisaProduto := true;

  DBGridBuscaP.Columns[6].Visible := false;
  if Dados.qryEmpresaEXIBE_ESTOQUE_FISCAL.Value = 'S' then
    begin
      DBGridBuscaP.Columns[6].Visible := true;
    end;



//    btnDesconto.Enabled := false;
//  if Dados.qryEmpresaDESCONTO_ITEM_PDV.Value = 'S' then
//    begin
//      btnDesconto.Enabled := true;
//    end;



  tamanho;

  ConfiguraTipodecaixa;
  PesquisaTipoTerminal;

  try

    if not Dados.TerminalCaixa then
    // verifica se o terminal � caixa
    begin

//      qryVendasTerminal.close;
//      qryVendasTerminal.SQL.Clear;
//      qryVendasTerminal.SQL.Text :=
//        'select caixa_unico, caixa_usado from vendas_terminais where nome = :terminal';
//      qryVendasTerminal.Params[0].Value := Dados.Getcomputer;
//      qryVendasTerminal.Open;

      if (Dados.qryTerminal.FieldByName('CAIXA_UNICO').AsString <> 'S') then
        begin
         HabilitaPreVenda;
         actFiscal.Enabled := false;

        end
      else
        begin
          // verifica se caixa est� aberto
          Dados.qryConsulta.close;
          Dados.qryConsulta.SQL.Text :=
          'select CODIGO, DATA_ABERTURA, ID_USUARIO, LOTE, SITUACAO from contas where codigo =:id AND SITUACAO=''A''';
          Dados.qryConsulta.Params[0].Value := Dados.qryTerminal.FieldByName('CAIXA_USADO').AsString;
          Dados.qryConsulta.Open;

          if Dados.qryConsulta.FieldByName('SITUACAO').AsString = 'A' then
            begin
              if Dados.qryConsulta.FieldByName('DATA_ABERTURA').Value < date then
                begin

                  MessageBox(FrmBalcao.Handle,'Caixa aberto n�o � de hoje, aguarde a abertura do caixa', 'Caixa', MB_ICONINFORMATION + MB_OK);
                  FrmBalcao.Close;
                end
              else
                begin
                  HabilitaPreVenda;
                  actFiscal.Enabled := false;
                  caixaUnico := true;
                  idCaixaUnico := Dados.qryConsulta.FieldByName('CODIGO').Value;
                  idLoteCaixaUnico := Dados.qryConsulta.FieldByName('LOTE').Value;
                  idUsuarioCaixaUnico := dados.idUsuario; //Dados.qryConsulta.FieldByName('ID_USUARIO').Value;
                  caixaUsado := Dados.qryConsulta.FieldByName('ID_USUARIO').Value;
                end;
            end
          else
            begin

              MessageBox(FrmBalcao.Handle,'Caixa fechado, aguarde a abertura do caixa', 'Caixa', MB_ICONINFORMATION + MB_OK);
              FrmBalcao.Close;
            end;

        end;
    end
    else
    begin

      CaixaAberto := SituacaoCaixa;

      if CaixaAberto then
      begin

        Dados.idCaixa := Dados.qryConsulta.Fields[0].Value;
        Dados.Lote := Dados.qryConsulta.Fields[2].Value;
        CaixaHoje;
        DescricaoSetFocus('');


      end
      else
      begin
        // n�o existe caixa aberto para o usuario
        Panel2.Visible            := false;
        pnAbrirCaixa.Enabled      := True;
        pnAbrirCaixa.Visible      := True;
        pnAbrirCaixa.Align        := alClient;
        Panel7.Enabled            := false;
        Panel7.Visible            := false;
        habilitacampos(false);
        btnImportar.Enabled       := false;
        btnNfe.Enabled            := false;
        spbNovaVenda.Enabled      := false;
        btnReceber.Enabled        := false;
//        btnResumo.Enabled         := false;
//        btnSangria.Enabled        := false;
//        btnSuprimento.Enabled     := false;
        btnBuscarCliente.Enabled  := false;
        pnPessoa.Visible          := false;
        panel36.Visible           := false;
        lbCaixaIni.Caption        := '';
        pnFecharCaixa.Visible     := false;
        btnSalvar.Enabled         := false;
        btnDevolucao.Enabled      := false;

        actNovaVenda.Enabled      := false;
        actReceber.Enabled        := false;
        actCliente.Enabled        := false;
        actDesconto.Enabled       := false;

        label5.Caption            := 'Caixa fechado, aperte a tecla F2 para efetuar a abertura.';
//        btnCaixa.Caption          := 'F2 Abrir Caixa';
//        btnCaixa.Tag              := 1;



      end;
    end;

    VendaExiste;

    // Close;

  finally
    MostraCaixa;
    Application.ProcessMessages;
    //DescricaoSetFocus('');
    ConfiguraBalanca;
    Timer2.Enabled        := true;
  end;

end;

procedure TFrmBalcao.habilitacampos(campos: Boolean);
begin

  btnBuscaAvancada.Enabled  := campos;
  btnDelItem.Enabled        := campos;
  btnCancelVenda.Enabled    := campos;
  btnFinaliza.Enabled       := campos;
  btnVendedor.Enabled       := campos;
  btnBuscarCliente.Enabled  := campos;
  btnAlterarQtd.Enabled     := campos;
  PanelPDV.Enabled          := campos;



end;

procedure TFrmBalcao.imgLogoDblClick(Sender: TObject);
begin

  if Dados.qryParametroBLOQUEAR_PERSONALIZACAO.Value = 'S' then
    exit;

  OpenPicture.Execute;
  if trim(OpenPicture.FileName) <> '' then
  begin

    Dados.qryParametro.Edit;
    Dados.qryParametro.FieldByName('TELA_FUNDO_ECF').AsString :=
      OpenPicture.FileName;
    Dados.qryParametro.Post;
    Dados.Conexao.CommitRetaining;
    CarregaImagem;
  end;
end;

end.
