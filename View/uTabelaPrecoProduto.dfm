object FormTabelaPrecoProduto: TFormTabelaPrecoProduto
  Left = 0
  Top = 0
  ActiveControl = DBGrid1
  BorderStyle = bsDialog
  Caption = 'Tabela de Pre'#231'o'
  ClientHeight = 289
  ClientWidth = 459
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poMainFormCenter
  OnKeyDown = FormKeyDown
  PixelsPerInch = 96
  TextHeight = 13
  object DBGrid1: TDBGrid
    Left = 0
    Top = 0
    Width = 459
    Height = 289
    Align = alClient
    DataSource = dsTabelaPrecoProduto
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -21
    Font.Name = 'Segoe UI'
    Font.Style = []
    Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgAlwaysShowSelection, dgConfirmDelete, dgCancelOnExit, dgTitleClick, dgTitleHotTrack]
    ParentFont = False
    TabOrder = 0
    TitleFont.Charset = ANSI_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -13
    TitleFont.Name = 'Segoe UI'
    TitleFont.Style = []
    OnDblClick = DBGrid1DblClick
    OnKeyPress = DBGrid1KeyPress
    Columns = <
      item
        Expanded = False
        FieldName = 'DESCRICAO'
        Title.Caption = 'Descri'#231#227'o'
        Width = 252
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'PRECO'
        Title.Caption = 'Pre'#231'o'
        Width = 151
        Visible = True
      end>
  end
  object dsTabelaPrecoProduto: TDataSource
    DataSet = qryTabelaPrecoProduto
    OnDataChange = dsTabelaPrecoProdutoDataChange
    Left = 136
    Top = 192
  end
  object qryTabelaPrecoProduto: TFDQuery
    Connection = Dados.Conexao
    SQL.Strings = (
      
        'select CODIGO, DESCRICAO, PRECO, FK_PRODUTO from tabela_preco_un' +
        'idade'
      'where'
      'fk_produto=:produto'
      'order by descricao')
    Left = 224
    Top = 104
    ParamData = <
      item
        Name = 'PRODUTO'
        DataType = ftInteger
        ParamType = ptInput
        Value = Null
      end>
    object qryTabelaPrecoProdutoCODIGO: TIntegerField
      FieldName = 'CODIGO'
      Origin = 'CODIGO'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
    end
    object qryTabelaPrecoProdutoDESCRICAO: TStringField
      FieldName = 'DESCRICAO'
      Origin = 'DESCRICAO'
      Size = 40
    end
    object qryTabelaPrecoProdutoPRECO: TFMTBCDField
      FieldName = 'PRECO'
      Origin = 'PRECO'
      Precision = 18
      Size = 2
    end
    object qryTabelaPrecoProdutoFK_PRODUTO: TIntegerField
      FieldName = 'FK_PRODUTO'
      Origin = 'FK_PRODUTO'
    end
  end
end
