unit uFecharMes;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics, System.zip,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, DBGridEh, Vcl.StdCtrls, Vcl.Mask,
  DBCtrlsEh, DBLookupEh, Vcl.ComCtrls, Vcl.Buttons, Vcl.ExtCtrls,
  FireDAC.Stan.Intf, FireDAC.Stan.Option, FireDAC.Stan.Param,
  FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf, FireDAC.DApt.Intf,
  FireDAC.Stan.Async, FireDAC.DApt, Data.DB, FireDAC.Comp.DataSet, Winapi.ShellAPI, ACBrDFeUtil, acbrUtil,
  FireDAC.Comp.Client;

type
  TfrmFecharMes = class(TForm)
    PagUtilidade: TPageControl;
    TabGerar: TTabSheet;
    pnGerar: TPanel;
    BitBtn1: TBitBtn;
    BitBtn2: TBitBtn;
    ProgressBar1: TProgressBar;
    memLista: TListBox;
    GroupBox2: TGroupBox;
    Label14: TLabel;
    Label15: TLabel;
    Label16: TLabel;
    edtArquivo: TEdit;
    edtRelatorio: TEdit;
    edtEmail: TDBLookupComboboxEh;
    Button1: TButton;
    Panel1: TPanel;
    LblPeriodo: TLabel;
    maskInicio: TMaskEdit;
    maskFim: TMaskEdit;
    qryXML: TFDQuery;
    qryXMLNUMERO: TIntegerField;
    qryXMLDATA_EMISSAO: TDateField;
    qryXMLCHAVE: TStringField;
    qryXMLTRIB_FED: TFMTBCDField;
    qryXMLTRIB_EST: TFMTBCDField;
    qryXMLTRIB_MUN: TFMTBCDField;
    qryXMLTRIB_IMP: TFMTBCDField;
    qryXMLSITUACAO: TStringField;
    qryXMLSERIE: TStringField;
    qryXMLXML: TMemoField;
    qryXMLTOTAL: TFMTBCDField;
    qryXMLXML_CANCELAMENTO: TMemoField;
    procedure BitBtn1Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
    procedure GerarXml;
    procedure ListarArquivos(Diretorio: string; Sub: Boolean);
    procedure compactanfce(Caminho, pasta: string);
    function TemAtributo(Attr, Val: Integer): Boolean;

  public
    { Public declarations }
  end;

var
  frmFecharMes: TfrmFecharMes;

implementation

{$R *.dfm}

uses Udados, uDmNFe, uNFe, uDMEstoque, uCadProduto, ufrmStatus, uEmail,
  uDadosWeb;

procedure TfrmFecharMes.BitBtn1Click(Sender: TObject);
var
  dia, mes, ano, dia1, mes1, ano1: Word;
begin

  DecodeDate(strtodate(maskInicio.Text), ano, mes, dia);
  DecodeDate(strtodate(maskFim.Text), ano1, mes1, dia1);

  if (mes + ano) <> (mes1 + ano1) then
  begin
    ShowMessage('Per�odo Inv�lido');
    exit;
  end;

  Dados.qryConsulta.Close;
  Dados.qryConsulta.SQL.Text := ' SELECT NUMERO FROM NFCE_MASTER' +
    ' WHERE SITUACAO IN (''D'',''G'',''O'') AND ' +
    ' DATA_EMISSAO between :DATA1 AND :DATA2 AND FKEMPRESA=:EMPRESA ORDER BY numero';
  Dados.qryConsulta.Params[0].AsDate := strtodate(maskInicio.EditText);
  Dados.qryConsulta.Params[1].AsDate := strtodate(maskFim.EditText);
  Dados.qryConsulta.Params[2].Value := Dados.qryEmpresaCODIGO.Value;
  Dados.qryConsulta.Open;

  if not Dados.qryConsulta.IsEmpty then
  begin
    ShowMessage('Existem NFC-E com DUPLICIDADE, GRAVADO E/OU OFFLINE!' +
      sLineBreak + 'N�o � Possivel Continuar!');
    exit;
  end;

  try
    BitBtn1.Enabled := false;
    qryXML.Close;
    qryXML.Params[0].AsDate := strtodate(maskInicio.EditText);
    qryXML.Params[1].AsDate := strtodate(maskFim.EditText);
    qryXML.Params[2].Value := Dados.qryEmpresaCODIGO.Value;
    qryXML.Open;
    if not qryXML.IsEmpty then
    begin
      ProgressBar1.Position := 1;
      ProgressBar1.Min := 1;
      ProgressBar1.Max := qryXML.RecordCount;
      qryXML.First;
      while not qryXML.Eof do
      begin
        GerarXml;
        ProgressBar1.Position := ProgressBar1.Position + 1;
        qryXML.Next;
        Application.ProcessMessages;
      end;
    end;
    //GerarRelatorio;

    Sleep(5000);

    memLista.Items.Clear;
    ListarArquivos(dmnfe.ACBrNFe.Configuracoes.Arquivos.GetPathNFe
      (qryXMLDATA_EMISSAO.Value, TiraPontos(Dados.qryEmpresaCNPJ.AsString),
      '65'), false);
    compactanfce(dmnfe.ACBrNFe.Configuracoes.Arquivos.PathNFe + '\' +
      TiraPontos(Dados.qryEmpresaCNPJ.AsString), 'Xml ' + Dados.qryEmpresaFANTASIA.AsString + ' - ' + FormatDateTime('yyyymm',
      qryXMLDATA_EMISSAO.Value));
  finally
    BitBtn1.Enabled := true;
    ShellExecute(Application.HANDLE, 'open',
    PChar(ExtractFilePath(dmnfe.ACBrNFe.Configuracoes.Arquivos.PathNFe + '\' +
      TiraPontos(Dados.qryEmpresaCNPJ.AsString) + '\' + TiraPontos(Dados.qryEmpresaCNPJ.AsString))),
    nil,nil,SW_SHOWNORMAL)
  end;

end;


procedure TfrmFecharMes.compactanfce(Caminho, pasta: string);
var
  ZipFile: TZipFile;
  arquivo: string;
begin
  // Cria uma inst�ncia da classe TZipFile
  ZipFile := TZipFile.Create;
  try
    // Indica o diret�rio e nome do arquivo Zip que ser� criado
    ZipFile.Open(Caminho + '\' + pasta + '.zip', zmWrite);
    for arquivo in memLista.Items do
      ZipFile.Add(arquivo);

    MessageDlg('Compacta��o conclu�da!', mtInformation, [mbOK], 0);
  finally
    // Libera o objeto da mem�ria
    ZipFile.Free;
  end;

end;

procedure TfrmFecharMes.FormCreate(Sender: TObject);
var
  dia, mes, ano: Word;
begin
DecodeDate(DATE, ano, mes, dia);
  maskInicio.EditText := '01' + '/' + FormatFloat('00', mes) + '/' +
    IntToStr(ano);
  maskFim.EditText := datetostr(DATE);

  edtEmail.Text := Dados.qryEmpresaEMAIL_CONTADOR.AsString;
  edtArquivo.Text := dmnfe.ACBrNFe.Configuracoes.Arquivos.PathNFe + '\' +
    TiraPontos(Dados.qryEmpresaCNPJ.AsString) + '\' + 'Xml ' + Dados.qryEmpresaFANTASIA.AsString + ' - ' + FormatDateTime('yyyymm',
    strtodate(maskInicio.Text)) + '.ZIP';

  edtRelatorio.Text := dmnfe.ACBrNFe.Configuracoes.Arquivos.PathNFe + '\' +
    TiraPontos(Dados.qryEmpresaCNPJ.AsString) + '\' + FormatDateTime('yyyymm',
    strtodate(maskInicio.Text)) + '\';
end;

procedure TfrmFecharMes.GerarXml;
var
  dia, mes, ano: Word;
begin
  dmnfe.ACBrNFeDANFCeFortesA41.MostraPreview := false;
  dmnfe.ACBrNFeDANFCeFortesA41.MostraStatus := false;
  try

    if (Trim(qryXMLXML.AsString) = '') or (qryXMLXML.IsNull) then
      raise Exception.Create('ERRO XML n�o encontrado NFCe.' +
        qryXMLNUMERO.AsString);

    dmnfe.ACBrNFe.NotasFiscais.Clear;
    if (qryXMLSITUACAO.Value = 'T') then
      dmnfe.ACBrNFe.NotasFiscais.LoadFromString(qryXMLXML.AsString);

    if (qryXMLSITUACAO.Value = 'C') then
    begin
      if not(qryXMLXML_CANCELAMENTO.IsNull) then
        dmnfe.ACBrNFe.NotasFiscais.LoadFromString
          (qryXMLXML_CANCELAMENTO.AsString)
      else
        dmnfe.ACBrNFe.NotasFiscais.LoadFromString(qryXMLXML.AsString);
    end;

    dmnfe.ACBrNFe.DANFE := dmnfe.ACBrNFeDANFCeFortesA41;
    dmnfe.ACBrNFe.DANFE.Cancelada := false;
    if qryXMLSITUACAO.Value = 'C' then
      dmnfe.ACBrNFe.DANFE.Cancelada := true;
    dmnfe.ACBrNFe.DANFE.vTribFed := qryXMLTRIB_FED.AsFloat +
      qryXML.FieldByName('TRIB_IMP').AsFloat;
    dmnfe.ACBrNFe.DANFE.vTribEst := qryXMLTRIB_EST.AsFloat;
    dmnfe.ACBrNFe.DANFE.vTribMun := qryXMLTRIB_MUN.AsFloat;
    dmnfe.ACBrNFe.DANFE.PathPDF := Dados.qryConfigPATHPDF.Value;
    dmnfe.ACBrNFe.NotasFiscais.ImprimirPDF;

    if (qryXMLSITUACAO.Value = 'T') or (qryXMLSITUACAO.Value = 'C') then
      dmnfe.ACBrNFe.NotasFiscais.GravarXML();

  finally
    dmnfe.ACBrNFeDANFCeFortesA41.MostraPreview := true;
    dmnfe.ACBrNFeDANFCeFortesA41.MostraStatus := true;
  end;

end;

procedure TfrmFecharMes.ListarArquivos(Diretorio: string; Sub: Boolean);
var
  F: TSearchRec;
  Ret: Integer;
  TempNome: string;
begin
  Ret := FindFirst(Diretorio + '\*.*xml', faAnyFile, F);
  try
    while Ret = 0 do
    begin
      if TemAtributo(F.Attr, faDirectory) then
      begin
        if (F.Name <> '.') And (F.Name <> '..') then
          if Sub = true then
          begin
            TempNome := Diretorio + '\' + F.Name;
            ListarArquivos(TempNome, true);
          end;
      end
      else
      begin
        memLista.Items.Add(Diretorio + '\' + F.Name);
      end;
      Ret := FindNext(F);
    end;
  finally
    begin
      FindClose(F);
    end;
  end;

end;

function TfrmFecharMes.TemAtributo(Attr, Val: Integer): Boolean;
begin
  Result := Attr and Val = Val;
end;

end.
