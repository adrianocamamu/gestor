unit uBuscaClienteR;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, DBGridEhGrouping, ToolCtrlsEh,
  DBGridEhToolCtrls, DynVarsEh, EhLibVCL, GridsEh, DBAxisGridsEh, DBGridEh,
  Vcl.StdCtrls, Vcl.Mask, Vcl.DBCtrls, Vcl.ExtCtrls, FireDAC.Stan.Intf,
  FireDAC.Stan.Option, FireDAC.Stan.Param, FireDAC.Stan.Error, FireDAC.DatS,
  FireDAC.Phys.Intf, FireDAC.DApt.Intf, FireDAC.Stan.Async, FireDAC.DApt,
  Data.DB, FireDAC.Comp.DataSet, FireDAC.Comp.Client;

type
  TfrmBuscarCliente = class(TForm)
    Button1: TButton;
    Button2: TButton;
    Button3: TButton;
    Panel1: TPanel;
    dsCliente: TDataSource;
    qryCliente: TFDQuery;
    qryClienteCODIGO: TIntegerField;
    qryClienteRAZAO: TStringField;
    qryClienteCNPJ: TStringField;
    qryClienteENDERECO: TStringField;
    qryClienteNUMERO: TStringField;
    qryClienteBAIRRO: TStringField;
    qryClienteMUNICIPIO: TStringField;
    qryClienteUF: TStringField;
    qryClienteCEP: TStringField;
    qryClienteFONE1: TStringField;
    qryClienteCELULAR1: TStringField;
    qryClienteCOMPLEMENTO: TStringField;
    DBGridCliente: TDBGridEh;
    edtBuscarCliente: TEdit;
    procedure edtBuscarClienteChange(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure edtBuscarClienteKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure DBGridClienteKeyPress(Sender: TObject; var Key: Char);
  private
    { Private declarations }
    vPessoa: String;
  public
    { Public declarations }
  end;

var
  frmBuscarCliente: TfrmBuscarCliente;

implementation

{$R *.dfm}

uses Udados, uBalcao;

procedure TfrmBuscarCliente.DBGridClienteKeyPress(Sender: TObject;
  var Key: Char);
begin
  if Key = #13 then
    FrmBalcao.edtBuscarCliente.Text := qryClienteRAZAO.AsString;
    frmBuscarCliente.Close;
end;

procedure TfrmBuscarCliente.edtBuscarClienteChange(Sender: TObject);
begin

  qryCliente.Close;
  if dados.qryEmpresaPESQUISA_POR_PARTE.Value = 'S' then
  begin
    qryCliente.Params[0].Value := '%' + edtBuscarCliente.Text + '%';
    qryCliente.Params[1].Value := copy(edtBuscarCliente.Text, 1, 14) + '%';
  end
  else
  begin
    qryCliente.Params[0].Value := edtBuscarCliente.Text + '%';
    qryCliente.Params[1].Value := copy(edtBuscarCliente.Text, 1, 14) + '%';
  end;

  qryCliente.Open;
end;

procedure TfrmBuscarCliente.edtBuscarClienteKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
  if Key = VK_UP then
    qryCliente.Prior;

  if Key = VK_DOWN then
    qryCliente.Next;
end;

procedure TfrmBuscarCliente.FormShow(Sender: TObject);
begin
  qryCliente.Close;
  qryCliente.Params[0].Value := '%' + edtBuscarCliente.Text + '%';
  qryCliente.open;
end;

end.
