unit uConsVendedor;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants,
  System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls, Vcl.Buttons,
  ACBrBase, ACBrEnterTab, Data.DB, FireDAC.Stan.Intf, FireDAC.Stan.Option,
  FireDAC.Stan.Param, FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf,
  FireDAC.DApt.Intf, FireDAC.Stan.Async, FireDAC.DApt, FireDAC.Comp.DataSet,
  FireDAC.Comp.Client, Vcl.Grids, Vcl.DBGrids, Vcl.ExtCtrls;

type
  TFrmConsVendedor = class(TForm)
    edtLoc: TEdit;
    DBGrid1: TDBGrid;
    dsVendedor: TDataSource;
    qryVendedor: TFDQuery;
    qryVendedorNOME: TStringField;
    qryVendedorCODIGO: TIntegerField;
    Label1: TLabel;
    Panel1: TPanel;
    edtSenha: TEdit;
    qryVendedorSENHA: TStringField;
    Label2: TLabel;
    procedure edtLocChange(Sender: TObject);
    procedure edtLocKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure FormShow(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure edtSenhaKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure DBGrid1KeyPress(Sender: TObject; var Key: Char);
  private
    procedure Localiza;
    { Private declarations }
  public
    idVendedor: integer;
    vNome: String;
    selecionado : boolean;
    { Public declarations }
  end;

var
  FrmConsVendedor: TFrmConsVendedor;

implementation

{$R *.dfm}

uses Udados, uBalcao;

procedure TFrmConsVendedor.edtSenhaKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin

  if Key = VK_ESCAPE then
  begin
    Panel1.Visible := false;
    edtLoc.Text;
    Localiza;

  end;

 try
  if Key = VK_RETURN then
  begin

       if (edtSenha.Text = '') then
    begin
      ShowMessage('Informe a senha');
      exit;
    end;

    if edtSenha.Text <> Dados.crypt('D', qryVendedorSENHA.Value) then
    begin
      Application.ProcessMessages;
      ShowMessage(qryVendedorSENHA.Value);
      ShowMessage('Senha incorreta!');
      edtSenha.SetFocus;
      edtSenha.Clear;
      exit;
    end
    else
    begin
      selecionado := true;
//      idVendedor := qryVendedorCODIGO.Value;
//      vNome := qryVendedorNOME.Value;
//      close;
    end;
  end;
    finally

    end;

end;

procedure TFrmConsVendedor.DBGrid1KeyPress(Sender: TObject; var Key: Char);
begin
//  if Key = #13 then
//  begin
//    panel1.Visible := true;
//    edtSenha.SetFocus;
//  end;
end;

procedure TFrmConsVendedor.edtLocChange(Sender: TObject);
begin
  Localiza;
end;

procedure TFrmConsVendedor.edtLocKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin

  if Key = vk_up then
    qryVendedor.Prior;


  if Key = VK_DOWN then
    qryVendedor.Next;


end;

procedure TFrmConsVendedor.FormActivate(Sender: TObject);
begin
  dados.vForm := nil;
  dados.vForm := self;
  dados.GetComponentes;
end;

procedure TFrmConsVendedor.FormClose(Sender: TObject; var Action: TCloseAction);
begin
//  idVendedor := qryVendedorCODIGO.Value;
//  vNome := qryVendedorNOME.Value;
end;



procedure TFrmConsVendedor.FormKeyPress(Sender: TObject; var Key: Char);
begin

    if Key = #13 then
      begin
      if not selecionado then
        begin

          panel1.Visible := true;
          edtSenha.SetFocus;

        end
      else
        begin
          //selecionado := true;
          idVendedor := qryVendedorCODIGO.Value;
          vNome := qryVendedorNOME.Value;
          close;
        end;
   end;


end;

procedure TFrmConsVendedor.FormShow(Sender: TObject);
begin
  Localiza;
end;

procedure TFrmConsVendedor.Localiza;
begin
  qryVendedor.close;
  qryVendedor.Params[0].Value := edtLoc.Text + '%';
  qryVendedor.Open;
  qryVendedor.Locate('codigo', idVendedor, []);

  if dados.qryEmpresaSENHA_VENDEDOR.value = 'S' then
    begin
      selecionado := false;
    end
  else
    begin
      selecionado := true;
    end;
end;

end.
