unit uFechamentoVendaBalcao;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.Buttons, Vcl.StdCtrls, Data.DB,
  DBGridEhGrouping, ToolCtrlsEh, DBGridEhToolCtrls, DynVarsEh,
  FireDAC.Stan.Intf, FireDAC.Stan.Option, FireDAC.Stan.Param,
  FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf, FireDAC.DApt.Intf,
  FireDAC.Stan.Async, FireDAC.DApt, frxClass, frxExportBaseDialog, frxExportXLS,
  frxDBSet, FireDAC.Comp.DataSet, FireDAC.Comp.Client, Vcl.ComCtrls,
  Vcl.ExtCtrls, EhLibVCL, GridsEh, DBAxisGridsEh, DBGridEh, Vcl.Grids,
  Vcl.DBGrids, Vcl.DBCtrls, Vcl.Mask;

type
  TfrmTipoFinalizacao = class(TForm)
    Button1: TButton;
    Button3: TButton;
    Button2: TButton;
    procedure btnRetornarClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure Button3Click(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure Button2Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmTipoFinalizacao: TfrmTipoFinalizacao;

implementation

{$R *.dfm}

uses uBalcao, uFormaPagamentoR, Udados;

procedure TfrmTipoFinalizacao.btnRetornarClick(Sender: TObject);
begin
  close;
end;

procedure TfrmTipoFinalizacao.Button1Click(Sender: TObject);
begin
  FrmBalcao.tipoFinalizacao := 1;

  close;
end;

procedure TfrmTipoFinalizacao.Button2Click(Sender: TObject);
begin
  FrmBalcao.tipoFinalizacao := 3;
  close;
end;

procedure TfrmTipoFinalizacao.Button3Click(Sender: TObject);
begin
  FrmBalcao.tipoFinalizacao := 0;
  close;
end;

procedure TfrmTipoFinalizacao.FormActivate(Sender: TObject);
begin
  dados.vForm := nil;
  dados.vForm := self;
  dados.GetComponentes;
end;

procedure TfrmTipoFinalizacao.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key = vk_escape then
  begin
    Button2Click(self);
  end;

end;

end.
