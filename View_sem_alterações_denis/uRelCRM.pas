unit uRelCRM;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants,
  System.Classes, Vcl.Graphics, dateutils,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, DBGridEh, Vcl.Buttons, Vcl.ExtCtrls,
  Vcl.StdCtrls, Vcl.ComCtrls, Vcl.Mask, DBCtrlsEh, DBLookupEh,
  FireDAC.Stan.Intf, FireDAC.Stan.Option, FireDAC.Stan.Param,
  FireDAC.Stan.Error, FireDAC.Phys.Intf, FireDAC.DApt.Intf,
  FireDAC.Stan.Async, FireDAC.DApt, Data.DB, FireDAC.Comp.DataSet,
  FireDAC.Comp.Client, frxClass, frxDBSet, frxExportPDF, frxChart,
  frxExportBaseDialog, FireDAC.DatS;

type
  TfrmRelCRM = class(TForm)
    Panel3: TPanel;
    btnImp: TSpeedButton;
    bbSair: TSpeedButton;
    qryUsuario: TFDQuery;
    dsUsuario: TDataSource;
    frxPDFExport1: TfrxPDFExport;
    frxReport: TfrxReport;
    frxDBEmpresa: TfrxDBDataset;
    frxPlanoR: TfrxDBDataset;
    qryEmpresa: TFDQuery;
    qryEmpresaCODIGO: TIntegerField;
    qryEmpresaFANTASIA: TStringField;
    dsEmpresa: TDataSource;
    qryFinanceiroD: TFDQuery;
    qryUsuarioCODIGO: TIntegerField;
    qryUsuarioLOGIN: TStringField;
    Label2: TLabel;
    cbEmpresa: TDBLookupComboboxEh;
    chkPeriodo: TCheckBox;
    MaskInicio: TDateTimePicker;
    MaskFim: TDateTimePicker;
    qryPlano: TFDQuery;
    IntegerField1: TIntegerField;
    qryPlanoDESCRICAO: TStringField;
    dsPlano: TDataSource;
    qryFinanceiroDCODIGO: TIntegerField;
    qryFinanceiroDDOC: TStringField;
    qryFinanceiroDEMISSAO: TDateField;
    qryFinanceiroDPLANO: TStringField;
    qryFinanceiroDHISTORICO: TStringField;
    qryFinanceiroDENTRADA: TFMTBCDField;
    qryFinanceiroDSAIDA: TFMTBCDField;
    qryFinanceiroR: TFDQuery;
    qryFinanceiroRCODIGO: TIntegerField;
    qryFinanceiroRPLANO: TStringField;
    qryFinanceiroRENTRADA: TFMTBCDField;
    qryFinanceiroRSAIDA: TFMTBCDField;
    cbTipo: TComboBox;
    lblTitulo: TLabel;
    frxPlanoD: TfrxDBDataset;
    qryFinanceiroDespesas: TFDQuery;
    qryFinanceiroCompras: TFDQuery;
    qryFinanceiroEntradas: TFDQuery;
    frxFinanceiroDespesas: TfrxDBDataset;
    frxFinanceiroCompras: TfrxDBDataset;
    frxFinanceiroEntradas: TfrxDBDataset;
    qryFinanceiroDespesasCODIGO: TIntegerField;
    qryFinanceiroDespesasPLANO: TStringField;
    qryFinanceiroDespesasENTRADA: TFMTBCDField;
    qryFinanceiroDespesasSAIDA: TFMTBCDField;
    qryFinanceiroComprasCODIGO: TIntegerField;
    qryFinanceiroComprasPLANO: TStringField;
    qryFinanceiroComprasENTRADA: TFMTBCDField;
    qryFinanceiroComprasSAIDA: TFMTBCDField;
    qryFinanceiroEntradasCODIGO: TIntegerField;
    qryFinanceiroEntradasPLANO: TStringField;
    qryFinanceiroEntradasENTRADA: TFMTBCDField;
    qryFinanceiroEntradasSAIDA: TFMTBCDField;
    qryFinanceiroTaxaJuros: TFDQuery;
    IntegerField2: TIntegerField;
    StringField1: TStringField;
    FMTBCDField1: TFMTBCDField;
    FMTBCDField2: TFMTBCDField;
    frxFinanceiroTaxaJuros: TfrxDBDataset;
    procedure frxReportGetValue(const VarName: string; var Value: Variant);
    procedure bbSairClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure btnImpClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
  private
    vSqlD, vSqlR: string;
    vSaldoMes: Extended;
    //WS
    vSqlDespesas, vSqlCompras, vSqlEntradas, vSqlTaxaJuros: string;
    procedure Detalhado;
    procedure Resumido;
    { Private declarations }
  public
    { Public declarations }
    DTIni, DTFim: TDate;
  end;

var
  frmRelCRM: TfrmRelCRM;

implementation

{$R *.dfm}

uses Udados;

procedure TfrmRelCRM.bbSairClick(Sender: TObject);
begin
  close;
end;

procedure TfrmRelCRM.Resumido;
var
  filtro: string;
begin
  filtro := '';

  if vSqlDespesas = '' then
    vSqlDespesas  :=  qryFinanceiroDespesas.SQL.Text;
  if vSqlCompras = '' then
    vSqlCompras  :=  qryFinanceiroCompras.SQL.Text;
  if vSqlEntradas = '' then
    vSqlEntradas  :=  qryFinanceiroEntradas.SQL.Text;
  if vSqlTaxaJuros = '' then
    vSqlTaxaJuros  :=  qryFinanceiroTaxaJuros.SQL.Text;

  if cbEmpresa.KeyValue <> null then
    if cbEmpresa.KeyValue <> 0 then
      filtro := filtro + ' and cx.empresa=' + inttostr(cbEmpresa.KeyValue);

  DTIni := MaskInicio.Date;
  DTFim := MaskFim.Date;

  if not chkPeriodo.Checked then
  begin
    DTIni := strtodate('01/01/1900');
    DTFim := strtodate('01/12/9999');
  end;
  //DESPESAS
  qryFinanceiroDespesas.close;
  qryFinanceiroDespesas.SQL.Text := vSqlDespesas;
  qryFinanceiroDespesas.SQL.Text := StringReplace(qryFinanceiroDespesas.SQL.Text, '/*where*/',
    filtro, [rfReplaceAll]);
  qryFinanceiroDespesas.Params[0].AsDate := DTIni;
  qryFinanceiroDespesas.Params[1].AsDate := DTFim;
  qryFinanceiroDespesas.Open;
  //COMPRAS
  qryFinanceiroCompras.close;
  qryFinanceiroCompras.SQL.Text := vSqlCompras;
  qryFinanceiroCompras.SQL.Text := StringReplace(qryFinanceiroCompras.SQL.Text, '/*where*/',
    filtro, [rfReplaceAll]);
  qryFinanceiroCompras.Params[0].AsDate := DTIni;
  qryFinanceiroCompras.Params[1].AsDate := DTFim;
  qryFinanceiroCompras.Open;
  //TAXAS E JUROS
  qryFinanceiroTaxaJuros.close;
  qryFinanceiroTaxaJuros.SQL.Text := vSqlTaxaJuros;
  qryFinanceiroTaxaJuros.SQL.Text := StringReplace(qryFinanceiroTaxaJuros.SQL.Text, '/*where*/',
    filtro, [rfReplaceAll]);
  qryFinanceiroTaxaJuros.Params[0].AsDate := DTIni;
  qryFinanceiroTaxaJuros.Params[1].AsDate := DTFim;
  qryFinanceiroTaxaJuros.Open;
  //ENTRADAS
  qryFinanceiroEntradas.close;
  qryFinanceiroEntradas.SQL.Text := vSqlEntradas;
  qryFinanceiroEntradas.SQL.Text := StringReplace(qryFinanceiroEntradas.SQL.Text, '/*where*/',
    filtro, [rfReplaceAll]);
  qryFinanceiroEntradas.Params[0].AsDate := DTIni;
  qryFinanceiroEntradas.Params[1].AsDate := DTFim;
  qryFinanceiroEntradas.Open;

  frxReport.LoadFromFile(ExtractFilePath(application.ExeName) +
    '\Relatorio\RelFinanceiroPlanoDRE.fr3');
  frxReport.ShowReport;
end;

procedure TfrmRelCRM.btnImpClick(Sender: TObject);
begin
  Resumido;
  {$REGION 'MODOS'}
  {
  case cbTipo.ItemIndex of
    0:
      Resumido;
    1:
      Detalhado;
  end;
  }
  {$ENDREGION}
end;

procedure TfrmRelCRM.Detalhado;
var
  filtro: string;
begin
  filtro := '';

  if vSqlDespesas = '' then
    vSqlDespesas  :=  qryFinanceiroDespesas.SQL.Text;
  if vSqlCompras = '' then
    vSqlCompras  :=  qryFinanceiroCompras.SQL.Text;
  if vSqlEntradas = '' then
    vSqlEntradas  :=  qryFinanceiroEntradas.SQL.Text;
  if vSqlTaxaJuros = '' then
    vSqlTaxaJuros :=  qryFinanceiroTaxaJuros.SQL.Text;

  //if vSqlD = '' then
  //  vSqlD := qryFinanceiroD.SQL.Text;

  if cbEmpresa.KeyValue <> null then
    if cbEmpresa.KeyValue <> 0 then
      filtro := filtro + ' and cx.empresa=' + inttostr(cbEmpresa.KeyValue);

  DTIni := MaskInicio.Date;
  DTFim := MaskFim.Date;

  if not chkPeriodo.Checked then
  begin
    DTIni := strtodate('01/01/1900');
    DTFim := strtodate('01/12/9999');
  end;
  //Despesas
  qryFinanceiroDespesas.close;
  qryFinanceiroDespesas.SQL.Text := vSqlDespesas;
  qryFinanceiroDespesas.SQL.Text := StringReplace(qryFinanceiroDespesas.SQL.Text, '/*where*/',
    filtro, [rfReplaceAll]);
  qryFinanceiroDespesas.Params[0].AsDate := DTIni;
  qryFinanceiroDespesas.Params[1].AsDate := DTFim;
  qryFinanceiroDespesas.Open;
  //Compras
  qryFinanceiroCompras.close;
  qryFinanceiroCompras.SQL.Text := vSqlCompras;
  qryFinanceiroCompras.SQL.Text := StringReplace(qryFinanceiroCompras.SQL.Text, '/*where*/',
    filtro, [rfReplaceAll]);
  qryFinanceiroCompras.Params[0].AsDate := DTIni;
  qryFinanceiroCompras.Params[1].AsDate := DTFim;
  qryFinanceiroCompras.Open;
  //Entradas
  qryFinanceiroEntradas.close;
  qryFinanceiroEntradas.SQL.Text := vSqlEntradas;
  qryFinanceiroEntradas.SQL.Text := StringReplace(qryFinanceiroEntradas.SQL.Text, '/*where*/',
    filtro, [rfReplaceAll]);
  qryFinanceiroEntradas.Params[0].AsDate := DTIni;
  qryFinanceiroEntradas.Params[1].AsDate := DTFim;
  qryFinanceiroEntradas.Open;

  frxReport.LoadFromFile(ExtractFilePath(application.ExeName) +
    '\Relatorio\RelFinanceiroCRM.fr3');
  frxReport.ShowReport;
end;

procedure TfrmRelCRM.FormActivate(Sender: TObject);
begin
  dados.vForm := nil;
  dados.vForm := self; dados.GetComponentes;
end;

procedure TfrmRelCRM.FormCreate(Sender: TObject);
begin
  qryUsuario.close;
  qryUsuario.Open;

  qryEmpresa.close;
  qryEmpresa.Open;

  qryPlano.close;
  qryPlano.Open;

  MaskInicio.Date := StartOfTheMonth(Date);

end;

procedure TfrmRelCRM.FormShow(Sender: TObject);
begin
  cbEmpresa.KeyValue := 0;
  MaskInicio.Date := StartOfTheMonth(Date);
  MaskFim.Date := Date;
end;

procedure TfrmRelCRM.frxReportGetValue(const VarName: string;
  var Value: Variant);
begin
  if VarName = 'PARAMETRO' then
  begin
    Value := 'Empresa:' + cbEmpresa.Text + ' | Periodo de :' + datetostr(DTIni)
      + ' at� ' + datetostr(DTFim);
  end;
end;

end.
