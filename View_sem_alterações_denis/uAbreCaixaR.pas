unit uAbreCaixaR;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants,
  System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls, Vcl.Mask, Vcl.ExtCtrls,
  Vcl.Buttons, DBGridEh, DBCtrlsEh, DBLookupEh, FireDAC.Stan.Intf,
  FireDAC.Stan.Option, FireDAC.Stan.Param, FireDAC.Stan.Error, FireDAC.DatS,
  FireDAC.Phys.Intf, FireDAC.DApt.Intf, FireDAC.Stan.Async, FireDAC.DApt,
  Data.DB, FireDAC.Comp.DataSet, FireDAC.Comp.Client, Vcl.Imaging.pngimage;

type
  TfrmAbreCaixaR = class(TForm)
    Panel1: TPanel;
    dsCaixa: TDataSource;
    qryCaixa: TFDQuery;
    Image1: TImage;
    Panel4: TPanel;
    btnAbrir: TSpeedButton;
    btnCancelar: TSpeedButton;
    Label2: TLabel;
    cbCaixa: TDBLookupComboboxEh;
    Label1: TLabel;
    edtValor: TEdit;
    procedure FormShow(Sender: TObject);
    procedure btnAbrirClick(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure btnCancelarClick(Sender: TObject);
    procedure edtValorKeyPress(Sender: TObject; var Key: Char);
    procedure FormCreate(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
  private
    AtivaTempo: Boolean;
    procedure AbrePDV;
    procedure GeraCaixa;
    { Private declarations }
  public
    iTipo: Integer;
    { Public declarations }
  end;

var
  frmAbreCaixaR: TfrmAbreCaixaR;

implementation

{$R *.dfm}

uses Udados, uBalcao, uMenu;

procedure TfrmAbreCaixaR.GeraCaixa;
var
  vValor: Extended;
begin

  vValor := StrToFloatDef(edtValor.Text, 0);

  if vValor > 0 then
  begin

    dados.Conexao.CommitRetaining;

    dados.qryCaixa.Close;
    dados.qryCaixa.Open;
    if not(dados.qryconsulta.IsEmpty) then
    begin
      dados.qryCaixa.Insert;
      dados.qryCaixaEMISSAO.Value := Date;
      dados.qryCaixaEMPRESA.Value := dados.qryEmpresaCODIGO.Value;
      dados.qryCaixaEMISSAO.Value := Date;

      dados.qryCaixaDOC.Value := 'ABRE';

      dados.qryCaixaENTRADA.Value := 0;
      dados.qryCaixaSAIDA.AsFloat := vValor;

      dados.qryCaixaHISTORICO.Value := 'ABERTURA DE CAIXA -' + cbCaixa.Text;

      if dados.qryEmpresaID_PLANO_ABERTURA.IsNull then
        raise Exception.Create('Plano de contas n�o foi encontrado!');

      dados.qryCaixaHORA_EMISSAO.Value := time;
      dados.qryCaixaID_USUARIO.Value := dados.idUsuario;
      dados.qryCaixaFKPLANO.Value :=
        dados.qryEmpresaID_PLANO_ABERTURA.AsInteger;
      dados.qryCaixaFKCONTA.Value := dados.qryEmpresaID_CAIXA_GERAL.Value;
      dados.qryCaixaTIPO_MOVIMENTO.Value := 'VA';
      dados.qryCaixaFKVENDA.Value := 0;
      dados.qryCaixaTRANSFERENCIA.Value := 0;
      dados.qryCaixaFPG.Value := dados.buscafpg('D');
      dados.qryCaixaCODIGO.Value := dados.Numerador('CAIXA', 'CODIGO',
        'N', '', '');
      dados.qryCaixa.Post;
      dados.Conexao.CommitRetaining;
    end;
  end;
end;

procedure TfrmAbreCaixaR.AbrePDV;
begin
  try
    if cbCaixa.KeyValue = Null then
    begin
      ShowMessage('Selecione caixa!');
      exit;
    end;

    if Trim(edtValor.Text) = '' then
      edtValor.Text := '0';

    dados.qryconsulta.Close;
    dados.qryconsulta.SQL.Text := 'SELECT MAX(LOTE) FROM CONTAS_MOVIMENTO';
    dados.qryconsulta.Open;

    dados.Lote := dados.qryconsulta.Fields[0].AsInteger + 1;

    dados.qryExecute.Close;
    dados.qryExecute.SQL.Text :=
      'UPDATE CONTAS SET DATA_ABERTURA=CURRENT_DATE, SITUACAO=''A'', LOTE=:LOTE, ID_USUARIO=:ID WHERE CODIGO=:COD';
    dados.qryExecute.Params[0].Value := dados.Lote;
    dados.qryExecute.Params[1].Value := dados.idUsuario;
    dados.qryExecute.Params[2].Value := cbCaixa.KeyValue;
    dados.qryExecute.ExecSQL;

    dados.Conexao.CommitRetaining;

    FrmBalcao.qryConta_Movimento.Close;
    FrmBalcao.qryConta_Movimento.Params[0].Value := dados.Lote;
    FrmBalcao.qryConta_Movimento.Params[1].Value := cbCaixa.KeyValue;
    FrmBalcao.qryConta_Movimento.Open;

    FrmBalcao.qryConta_Movimento.Insert;
    FrmBalcao.qryConta_MovimentoID_CONTA_CAIXA.Value := cbCaixa.KeyValue;
    FrmBalcao.qryConta_MovimentoHISTORICO.Value := 'ABERTURA DE CAIXA ';
    FrmBalcao.qryConta_MovimentoDATA.Value := Date;
    FrmBalcao.qryConta_MovimentoHORA.Value := now;
    FrmBalcao.qryConta_MovimentoENTRADA.Value := strtofloat(edtValor.Text);
    FrmBalcao.qryConta_MovimentoSAIDA.Value := 0;
    FrmBalcao.qryConta_MovimentoFKVENDA.Value := 0;
    FrmBalcao.qryConta_MovimentoLOTE.Value := dados.Lote;
    FrmBalcao.qryConta_MovimentoID_USUARIO.Value := dados.idUsuario;
    FrmBalcao.qryConta_Movimento.Post;
    dados.Conexao.CommitRetaining;

    GeraCaixa;

    dados.idCaixa := cbCaixa.KeyValue;

    AtivaTempo := false;
    //FrmMenu.btnCaixa.Caption := 'F2' + #13 + 'Fechar Caixa';
    //FrmMenu.btnCaixa.Tag := 2;

    FrmBalcao.MostraCaixa;
    Close;

    ShowMessage('Caixa aberto com sucesso');

    FrmBalcao.VendaExiste;

    FrmBalcao.pnAbrirCaixa.Enabled      := False;
    FrmBalcao.pnAbrirCaixa.Visible      := False;
    FrmBalcao.Panel2.Visible            := True;
    FrmBalcao.Panel7.Visible            := true;
    FrmBalcao.Panel7.Enabled            := true;
    FrmBalcao.panel36.Visible           := true;
    FrmBalcao.pnPessoa.Visible          := true;
    FrmBalcao.pnPessoa.Height           := 37;
  except
    on e: Exception do
      raise Exception.Create('Erro ao abrir caixa!' + #13 + e.Message);
  end;

end;

procedure TfrmAbreCaixaR.btnAbrirClick(Sender: TObject);
begin
  if dados.Tela = 'PDV' then
    AbrePDV;

end;

procedure TfrmAbreCaixaR.btnCancelarClick(Sender: TObject);
begin

  if application.messagebox('Deseja Sair da Tela?', 'Confirma��o', mb_yesno) = mrYes
  then
  begin
    AtivaTempo := true;
    Close;
  end;
end;

procedure TfrmAbreCaixaR.edtValorKeyPress(Sender: TObject; var Key: Char);
begin
  if not(Key in ['0' .. '9', ',', #8, #9, #13, #27]) then
    Key := #0;


end;

procedure TfrmAbreCaixaR.FormActivate(Sender: TObject);
begin
  dados.vForm := nil;
  dados.vForm := self;
  dados.GetComponentes;
end;

procedure TfrmAbreCaixaR.FormCreate(Sender: TObject);
begin
  qryCaixa.Close;
  qryCaixa.Open;
end;

procedure TfrmAbreCaixaR.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key = vk_F10 then
    btnAbrir.Click;
  if Key = vk_escape then
    btnCancelar.Click;
end;

procedure TfrmAbreCaixaR.FormKeyPress(Sender: TObject; var Key: Char);
begin
  if Key = #13 then
    Perform(Wm_NextDlgCtl, 0, 0);
end;

procedure TfrmAbreCaixaR.FormShow(Sender: TObject);
begin
  AtivaTempo := true;
end;

end.
