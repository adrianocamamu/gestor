unit uImportarR;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants,
  System.Classes, Vcl.Graphics, Data.db, math,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls, Vcl.Buttons,
  DBGridEhGrouping, ToolCtrlsEh, DBGridEhToolCtrls, DynVarsEh, EhLibVCL,
  GridsEh, DBAxisGridsEh, DBGridEh, Vcl.ComCtrls, FireDAC.Stan.Intf,
  FireDAC.Stan.Option, FireDAC.Stan.Param, FireDAC.Stan.Error, FireDAC.DatS,
  FireDAC.Phys.Intf, FireDAC.DApt.Intf, FireDAC.Stan.Async, FireDAC.DApt,
  FireDAC.Comp.DataSet, FireDAC.Comp.Client, Vcl.Grids, Vcl.DBGrids, Vcl.Mask,
  ACBrBase, ACBrEnterTab;

type
  TfrmImportarR = class(TForm)
    qryPedido: TFDQuery;
    dsPedido: TDataSource;
    qryOrcamento: TFDQuery;
    dsOrcamento: TDataSource;
    qryOS_Detalhe: TFDQuery;
    qryOS_DetalheCODIGO: TIntegerField;
    qryOS_DetalheFK_OS_MASTER: TIntegerField;
    qryOS_DetalheFK_FUNCIONARIO: TIntegerField;
    qryOS_DetalheFK_PRODUTO: TIntegerField;
    qryOS_DetalheDATA_INICIO: TDateField;
    qryOS_DetalheHORA_INICIO: TTimeField;
    qryOS_DetalheDATA_TERMINO: TDateField;
    qryOS_DetalheHORA_TERMINO: TTimeField;
    qryOS_DetalheDISCRIMINACAO: TStringField;
    qryOS_DetalheFK_USUARIO: TIntegerField;
    qryOS_DetalheFK_EMPRESA: TIntegerField;
    qryOS_DetalheTIPO: TStringField;
    qryOS_DetalheSITUACAO: TStringField;
    qryPedidoCODIGO: TIntegerField;
    qryPedidoDATA_EMISSAO: TDateField;
    qryPedidoDATA_SAIDA: TDateField;
    qryPedidoID_CLIENTE: TIntegerField;
    qryPedidoFK_USUARIO: TIntegerField;
    qryPedidoFK_CAIXA: TIntegerField;
    qryPedidoFK_VENDEDOR: TIntegerField;
    qryPedidoCPF_NOTA: TStringField;
    qryPedidoSUBTOTAL: TFMTBCDField;
    qryPedidoTIPO_DESCONTO: TStringField;
    qryPedidoDESCONTO: TFMTBCDField;
    qryPedidoTROCO: TFMTBCDField;
    qryPedidoDINHEIRO: TFMTBCDField;
    qryPedidoTOTAL: TFMTBCDField;
    qryPedidoOBSERVACOES: TMemoField;
    qryPedidoSITUACAO: TStringField;
    qryPedidoFKEMPRESA: TIntegerField;
    qryPedidoPERCENTUAL: TFMTBCDField;
    qryPedidoTIPO: TStringField;
    qryPedidoNECF: TIntegerField;
    qryPedidoFKORCAMENTO: TIntegerField;
    qryPedidoLOTE: TIntegerField;
    qryPedidoGERA_FINANCEIRO: TStringField;
    qryPedidoPERCENTUAL_ACRESCIMO: TFMTBCDField;
    qryPedidoACRESCIMO: TFMTBCDField;
    qryPedidoFK_TABELA: TIntegerField;
    qryPedidoPEDIDO: TStringField;
    qryPedidoFK_OS: TIntegerField;
    qryOS_DetalheQTD: TFMTBCDField;
    qryOS_DetalhePRECO: TFMTBCDField;
    qryOS_DetalheTOTAL: TFMTBCDField;
    qryOs_Master: TFDQuery;
    qryOs_MasterCODIGO: TIntegerField;
    qryOs_MasterDATA_INICIO: TDateField;
    qryOs_MasterHORA_INICIO: TTimeField;
    qryOs_MasterPREVISAO_ENTREGA: TDateField;
    qryOs_MasterDATA_TERMINO: TDateField;
    qryOs_MasterHORA_TERMINO: TTimeField;
    qryOs_MasterDATA_ENTREGA: TDateField;
    qryOs_MasterHORA_ENTREGA: TTimeField;
    qryOs_MasterFK_ATENDENDE: TIntegerField;
    qryOs_MasterPROBLEMA: TMemoField;
    qryOs_MasterOBSERVACOES: TMemoField;
    qryOs_MasterFK_EMPRESA: TIntegerField;
    qryOs_MasterFK_USUARIO: TIntegerField;
    qryOs_MasterDOCUMENTO: TStringField;
    qryOs_MasterNOME: TStringField;
    qryOs_MasterFONE1: TStringField;
    qryOs_MasterFONE2: TStringField;
    qryOs_MasterSITUACAO: TStringField;
    qryOs_MasterNUMERO_SERIE: TStringField;
    qryOs_MasterDESCRICAO: TStringField;
    qryOs_MasterMODELO: TStringField;
    qryOs_MasterMARCA: TStringField;
    qryOs_MasterANO: TIntegerField;
    qryOs_MasterPLACA: TStringField;
    qryOs_MasterKM: TFMTBCDField;
    qryOs_MasterSUBTOTAL: TFMTBCDField;
    qryOs_MasterSUBTOTAL_PECAS: TFMTBCDField;
    qryOs_MasterSUBTOTAL_SERVICOS: TFMTBCDField;
    qryOs_MasterVL_DESC_PECAS: TFMTBCDField;
    qryOs_MasterVL_DESC_SERVICOS: TFMTBCDField;
    qryOs_MasterDESC_PERC_PECAS: TFMTBCDField;
    qryOs_MasterDESC_PERC_SERVICOS: TFMTBCDField;
    qryOs_MasterTOTAL_SERVICOS: TFMTBCDField;
    qryOs_MasterTOTAL_PRODUTOS: TFMTBCDField;
    qryOs_MasterTOTAL_GERAL: TFMTBCDField;
    qryOs_MasterENDERECO: TStringField;
    qryOs_MasterBAIRRO: TStringField;
    qryOs_MasterCIDADE: TStringField;
    qryOs_MasterUF: TStringField;
    qryOs_MasterDATA_EMISSAO: TDateField;
    qryOs_MasterNUMERO: TStringField;
    qryOs_MasterNOME_TIME: TStringField;
    qryOs_MasterTIPO_SERVICO: TStringField;
    qryOs_MasterFK_TIPO_TECIDO: TIntegerField;
    qryOs_MasterQUANTIDADE: TIntegerField;
    qryOs_MasterFK_PRODUTO: TIntegerField;
    qryOs_MasterFOTO: TBlobField;
    qryOs_MasterFK_CLIENTE: TIntegerField;
    DsOs_Master: TDataSource;
    GroupBox2: TGroupBox;
    Label2: TLabel;
    maskInicio: TMaskEdit;
    maskFim: TMaskEdit;
    btnFiltrar: TBitBtn;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    Label3: TLabel;
    btnPedido: TBitBtn;
    DBGrid2: TDBGrid;
    TabSheet2: TTabSheet;
    Label4: TLabel;
    btnOrcamento: TBitBtn;
    DBGrid1: TDBGrid;
    TabSheet3: TTabSheet;
    Label5: TLabel;
    btnOS: TBitBtn;
    DBGrid3: TDBGrid;
    qryItensO: TFDQuery;
    qryItensOCODIGO: TIntegerField;
    qryItensOFK_ORCAMENTO: TIntegerField;
    qryItensOFK_PRODUTO: TIntegerField;
    qryItensOQTD: TFMTBCDField;
    qryItensOPRECO: TFMTBCDField;
    qryItensOTOTAL: TFMTBCDField;
    qryItensOITEM: TSmallintField;
    qryItensOVIRTUAL_PRODUTO: TStringField;
    qryItensOVIRTUAL_PRECO: TFMTBCDField;
    qryItensOVIRTUAL_UNIDADE: TStringField;
    qryItensOVIRTUAL_LOCAL: TStringField;
    qryItensOVIRTUAL_REFERENCIA: TStringField;
    qryItensOFK_GRADE: TIntegerField;
    qryItensOGRADE: TStringField;
    ACBrEnterTab1: TACBrEnterTab;
    qryItens: TFDQuery;
    qryItensCODIGO: TIntegerField;
    qryItensFKVENDA: TIntegerField;
    qryItensID_PRODUTO: TIntegerField;
    qryItensITEM: TSmallintField;
    qryItensCOD_BARRA: TStringField;
    qryItensQTD: TFMTBCDField;
    qryItensE_MEDIO: TFMTBCDField;
    qryItensPRECO: TFMTBCDField;
    qryItensVALOR_ITEM: TFMTBCDField;
    qryItensVDESCONTO: TFMTBCDField;
    qryItensTOTAL: TFMTBCDField;
    qryItensSITUACAO: TStringField;
    qryItensUNIDADE: TStringField;
    qryItensQTD_DEVOLVIDA: TFMTBCDField;
    qryItensACRESCIMO: TFMTBCDField;
    qryItensOS: TStringField;
    qryItensFK_GRADE: TIntegerField;
    qryItensID_PRODUTO_SIMILAR: TIntegerField;
    qryItensID_SERIAL: TIntegerField;
    qryOrcamentoCODIGO: TIntegerField;
    qryOrcamentoDATA: TDateField;
    qryOrcamentoTOTAL: TFMTBCDField;
    qryOrcamentoNOME: TStringField;
    qryOrcamentoFKVENDEDOR: TIntegerField;
    qryOrcamentoFK_CLIENTE: TIntegerField;
    qryOrcamentoCLIENTE: TStringField;
    qryOrcamentoTELEFONE: TStringField;
    qryOrcamentoCELULAR: TStringField;
    qryOrcamentoENDERECO: TStringField;
    qryOrcamentoNUMERO: TStringField;
    qryOrcamentoBAIRRO: TStringField;
    qryOrcamentoCIDADE: TStringField;
    qryOrcamentoUF: TStringField;
    qryOrcamentoCNPJ: TStringField;
    qryOrcamentoFORMA_PAGAMENTO: TStringField;
    qryOrcamentoVALIDADE: TSmallintField;
    qryOrcamentoOBS: TMemoField;
    qryOrcamentoSITUACAO: TStringField;
    qryOrcamentoCEP: TStringField;
    qryOrcamentoFKEMPRESA: TIntegerField;
    qryOrcamentoSUBTOTAL: TFMTBCDField;
    qryOrcamentoPERCENTUAL: TFMTBCDField;
    qryOrcamentoDESCONTO: TFMTBCDField;
    qryOrcamentoCODIGO_WEB: TIntegerField;
    qryOrcamentoNCONTROLE: TIntegerField;
    qryOrcamentoFK_TRANSP: TIntegerField;
    qryOrcamentoFK_FPG: TIntegerField;
    qryOrcamentoGID: TIntegerField;
    qryOrcamentoCODIGO_1: TIntegerField;
    qryOrcamentoCMA: TFMTBCDField;
    qryOrcamentoCMP: TFMTBCDField;
    qryOrcamentoATIVO: TStringField;
    qryOrcamentoEMPRESA: TIntegerField;
    qryOrcamentoFLAG: TStringField;
    qryOrcamentoGID_1: TIntegerField;
    qryPedidoNOME_1: TStringField;
    qryPedidoVIRTUAL_SITUACAO: TStringField;
    procedure btnPedidoClick(Sender: TObject);
    procedure btnOrcamentoClick(Sender: TObject);
    procedure BtnSairClick(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure DBGridEh1DblClick(Sender: TObject);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure FormCreate(Sender: TObject);
    procedure btnOSClick(Sender: TObject);
    procedure DBGrid2DblClick(Sender: TObject);
    procedure DBGrid1DblClick(Sender: TObject);
    procedure DBGrid3DblClick(Sender: TObject);
    procedure btnFiltrarClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure PageControl1Change(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure Button3Click(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure DBGrid2DrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumn; State: TGridDrawState);
    procedure qryPedidoCalcFields(DataSet: TDataSet);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
  private
    procedure ApagaItensVenda;
    procedure ImportaItensOrcamento;
    procedure ImportaOS;

    { Private declarations }
  public
    { Public declarations }
    var vFormaPagamento : boolean;
  end;

var
  frmImportarR: TfrmImportarR;


implementation

{$R *.dfm}

uses Udados, uBalcao, uSupervisor;

procedure TfrmImportarR.btnPedidoClick(Sender: TObject);
begin

  If qryPedido.IsEmpty then
    exit;

  FrmBalcao.PodeAtualizarEstoque := false;

  if qryPedidoSITUACAO.Value = 'X' then
  begin
    ShowMessage('N�o � possivel importar Pedido em Aberto');
    exit;
  end;

  if qryPedidoSITUACAO.Value = 'C' then
  begin
    ShowMessage('N�o � possivel importar Pedido Cancelado');
    exit;
  end;

  if qryPedidoSITUACAO.Value = 'F' then
  begin
  if Application.messageBox
    (Pwidechar('Pedido j� FECHADO. Deseja reabrir o pedido?'), 'Confirma��o',
      mb_Yesno) = mrYes then
   begin
    if not Dados.eSupervisor then
          begin

            try
              frmSupervisor := TFrmSupervisor.Create(Application);
              Dados.vAutorizar := false;
              frmSupervisor.ShowModal;
            finally
              frmSupervisor.Release;
            end;
            if not Dados.vAutorizar then
            begin
              exit;
            end;
          end;
   end
 else
    begin
      exit;  //nada faz
    end;
  end;


  Dados.qryExecute.Close;
  Dados.qryExecute.SQL.Clear;
  Dados.qryExecute.SQL.Add('Select SITUACAO from nfce_master');
  Dados.qryExecute.SQL.Add('where');
  Dados.qryExecute.SQL.Add('fk_venda=:codigo');
  Dados.qryExecute.Params[0].Value := qryPedidoCODIGO.Value;
  Dados.qryExecute.Open;

  if (trim(Dados.qryExecute.FieldByName('SITUACAO').AsString) <> 'O') and
    (trim(Dados.qryExecute.FieldByName('SITUACAO').AsString) <> '') and
    (trim(Dados.qryExecute.FieldByName('SITUACAO').AsString) <> 'G') then
  begin
    ShowMessage('J� existe NFC-e gerada para esta venda!');
    exit;
  end;

  try

    Dados.EstornaFinanceiro(qryPedidoCODIGO.Value);          //FrmBalcao.qryVendaCODIGO.Value

    Dados.qryExecute.Close;
    Dados.qryExecute.SQL.Text :=
      'update vendas_master set SITUACAO=''X'', FLAG_NFCE=''N'', TIPO_DESCONTO=''D'', FK_USUARIO=:USU, FK_CAIXA=:CAIXA, LOTE=:LT WHERE CODIGO=:CODIGO';
    Dados.qryExecute.Params[0].Value := Dados.idUsuario;
    Dados.qryExecute.Params[1].Value := Dados.idCaixa;
    Dados.qryExecute.Params[2].Value := Dados.Lote;
    Dados.qryExecute.Params[3].Value := qryPedidoCODIGO.Value;
    Dados.qryExecute.ExecSQL;
    Dados.Conexao.CommitRetaining;

    FrmBalcao.qryVenda.Close;
    FrmBalcao.qryVenda.Params[0].Value := qryPedidoCODIGO.Value;
    FrmBalcao.qryVenda.Open;

    Application.ProcessMessages;
    ShowMessage('Importa��o realizada com sucesso!');
    FrmBalcao.pedidoImportado := 1;
    frmBalcao.tipoFinalizacao := 1;
    FrmBalcao.VendaExiste;

    Close;


  finally
    FrmBalcao.vFinalizar := true;
    qryPedido.Close;
    qryPedido.Open;
    //frmbalcao.btnFinalizaClick(self);
  end;

end;

procedure TfrmImportarR.BtnSairClick(Sender: TObject);
begin
  Close;
end;

procedure TfrmImportarR.Button1Click(Sender: TObject);
begin

  PageControl1.ActivePage := TabSheet1;
end;

procedure TfrmImportarR.Button2Click(Sender: TObject);
begin
  PageControl1.ActivePage := TabSheet2;
end;

procedure TfrmImportarR.Button3Click(Sender: TObject);
begin
  PageControl1.ActivePage := TabSheet3;
end;

procedure TfrmImportarR.DBGrid1DblClick(Sender: TObject);
begin
  btnOrcamento.Click;
end;

procedure TfrmImportarR.DBGrid2DblClick(Sender: TObject);
begin
  btnPedido.Click;
end;

procedure TfrmImportarR.DBGrid2DrawColumnCell(Sender: TObject;
  const Rect: TRect; DataCol: Integer; Column: TColumn; State: TGridDrawState);
begin
if not(qryPedido.State in dsEditModes) then
        qryPedido.Edit;

if odd((Sender as TDBGrid).DataSource.DataSet.RecNo) then
    (Sender as TDBGrid).Canvas.Brush.Color := $00F1ECE7
  else
    (Sender as TDBGrid).Canvas.Brush.Color := clWhite;

  if (gdSelected in State) then
  begin
    (Sender as TDBGrid).Canvas.Font.Color := clBlack;
    (Sender as TDBGrid).Canvas.Brush.Color := clSilver;
    DBGrid2.Canvas.Font.Style := [fsbold];

//    if qryPVSITUACAO.Value = 'G' then
//    begin
//      btnNFCe.Visible := false;
//    end
//  else
//    begin
//      btnNFCe.Visible := true;
//    end;
  end
  else
  begin
    DBGrid2.Canvas.Font.Style := [];


  end;

  (Sender as TDBGrid).DefaultDrawColumnCell(Rect, DataCol, Column, State);

    if (qryPedidoSITUACAO.Value = 'C') Then
      DBGrid2.Canvas.Font.Color := clRed;
      //qryPedidoSITUACAO.Value := 'CANCELADO';

    if (qryPedidoSITUACAO.Value = 'G') Then
      DBGrid2.Canvas.Font.Color := clBlue;
      //qryPedidoSITUACAO.Value := 'GRAVADO';

    if (qryPedidoSITUACAO.Value = 'F') Then
      DBGrid2.Canvas.Font.Color := clGreen;
      //qryPedidoSITUACAO.Value := 'FECHADO';

    (Sender as TDBGrid).DefaultDrawColumnCell(Rect, DataCol, Column, State);
end;

procedure TfrmImportarR.DBGrid3DblClick(Sender: TObject);
begin
  btnOS.Click;
end;

procedure TfrmImportarR.DBGridEh1DblClick(Sender: TObject);
begin
  btnPedido.Click;
end;

procedure TfrmImportarR.FormActivate(Sender: TObject);
begin
  Dados.vForm := nil;
  Dados.vForm := self;
  Dados.GetComponentes;
end;

procedure TfrmImportarR.ApagaItensVenda;
begin
  Dados.qryExecute.Close;
  Dados.qryExecute.SQL.Text :=
    'delete from VENDAS_DETALHE where fkvenda=:codigo';
  Dados.qryExecute.Params[0].Value := FrmBalcao.qryVendaCODIGO.Value;
  Dados.qryExecute.ExecSQL;
  Dados.Conexao.CommitRetaining;
end;

procedure TfrmImportarR.ImportaItensOrcamento;
var
  qtd: Real;
  vitem: Integer;
begin
  Dados.qryConsulta.Close;
  Dados.qryConsulta.SQL.Text :=
    ' select pr.codigo, pr.codbarra, pr.unidade, pr.cfop,  pr.ncm,  pr.qtd_atual, pr.servico from produto pr '
    + ' where ' + ' pr.codigo=:id ';
  Dados.qryConsulta.Params[0].Value := qryItensOFK_PRODUTO.AsInteger;
  Dados.qryConsulta.Open;
  qtd := Dados.qryConsulta.FieldByName('QTD_ATUAL').AsFloat;
  if not(Dados.qryEmpresaBLOQUEAR_ESTOQUE_NEGATIVO.Value = 'S') then
    qtd := 1;
  if Dados.qryConsulta.FieldByName('servico').AsString = 'S' then
    qtd := 1;
  if qtd > 0 then
  begin
    FrmBalcao.qryItem.Last;
    vitem := FrmBalcao.qryItemITEM.AsInteger + 1;
    FrmBalcao.qryItem.Append;
    FrmBalcao.qryItemCODIGO.AsFloat := Dados.Numerador('VENDAS_DETALHE', 'CODIGO',
      'N', '', '');
    FrmBalcao.qryItemID_PRODUTO.Value := qryItensOFK_PRODUTO.Value;
    FrmBalcao.qryItemFKVENDA.Value := FrmBalcao.qryVendaCODIGO.Value;
    FrmBalcao.qryItemITEM.Value := vitem;
    FrmBalcao.qryItemQTD.Value := qryItensOQTD.Value;
    FrmBalcao.qryItemFK_GRADE.Value := qryItensOFK_GRADE.Value;
    if Dados.qryEmpresaBLOQUEAR_ESTOQUE_NEGATIVO.Value = 'S' then
    begin
      if qtd < qryItensOQTD.Value then
        FrmBalcao.qryItemQTD.Value := qtd;
    end;
    FrmBalcao.qryItemUNIDADE.AsString := Dados.qryConsulta.FieldByName
      ('UNIDADE').AsString;
    FrmBalcao.qryItemPRECO.Value := qryItensOPRECO.AsFloat;
    FrmBalcao.qryItemVALOR_ITEM.AsFloat := FrmBalcao.qryItemQTD.AsFloat *
      FrmBalcao.qryItemPRECO.AsFloat;
    FrmBalcao.qryItemCOD_BARRA.Value := Dados.qryConsulta.FieldByName
      ('CODBARRA').AsString;
    FrmBalcao.qryItemTOTAL.AsFloat := FrmBalcao.qryItemVALOR_ITEM.AsFloat -
      FrmBalcao.qryItemVDESCONTO.AsFloat;
    FrmBalcao.qryItem.Post;
    Dados.Conexao.CommitRetaining;
  end;
end;

procedure TfrmImportarR.ImportaOS;
var
  qtd: Real;
  vitem: Integer;
begin
  Dados.qryConsulta.Close;
  Dados.qryConsulta.SQL.Text :=
    ' select pr.codigo, pr.codbarra, pr.unidade, pr.cfop,  pr.ncm,  pr.qtd_atual from produto pr '
    + ' where ' + ' pr.codigo=:id ';
  Dados.qryConsulta.Params[0].Value := qryOS_DetalheFK_PRODUTO.AsInteger;
  Dados.qryConsulta.Open;
  qtd := Dados.qryConsulta.FieldByName('QTD_ATUAL').AsFloat;
  FrmBalcao.qryItem.Last;
  vitem := FrmBalcao.qryItemITEM.AsInteger + 1;
  FrmBalcao.qryItem.Append;
  Frmbalcao.qryItemCODIGO.AsFloat := Dados.Numerador('VENDAS_DETALHE', 'CODIGO',
    'N', '', '');
  FrmBalcao.qryItemID_PRODUTO.Value := qryOS_DetalheFK_PRODUTO.Value;
  FrmBalcao.qryItemFKVENDA.Value := FrmBalcao.qryVendaCODIGO.Value;
  FrmBalcao.qryItemITEM.Value := vitem;
  FrmBalcao.qryItemQTD.Value := qryOS_DetalheQTD.Value;
  FrmBalcao.qryItemUNIDADE.AsString := Dados.qryConsulta.FieldByName
    ('UNIDADE').AsString;
  FrmBalcao.qryItemPRECO.Value := qryOS_DetalhePRECO.Value;
  FrmBalcao.qryItemVALOR_ITEM.AsFloat := FrmBalcao.qryItemQTD.AsFloat *
    FrmBalcao.qryItemPRECO.AsFloat;
  FrmBalcao.qryItemCOD_BARRA.Value := Dados.qryConsulta.FieldByName
    ('CODBARRA').AsString;
  FrmBalcao.qryItemTOTAL.AsFloat := FrmBalcao.qryItemVALOR_ITEM.AsFloat -
    FrmBalcao.qryItemVDESCONTO.AsFloat;
  FrmBalcao.qryItemOS.Value := 'S';
  FrmBalcao.qryItem.Post;
  Dados.Conexao.CommitRetaining;
end;

procedure TfrmImportarR.FormClose(Sender: TObject; var Action: TCloseAction);
begin
//  if not vFormaPagamento then
//    FrmBalcao.vFinalizar := false
//  else
//    FrmBalcao.vFinalizar := true;
end;

procedure TfrmImportarR.FormCreate(Sender: TObject);
begin
  PageControl1.ActivePageIndex := 0;
end;

procedure TfrmImportarR.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin

  if Key = VK_F2 then
    btnPedido.Click;

  if Key = VK_F3 then
    btnOrcamento.Click;

  if Key = VK_F4 then
    btnOS.Click;
  if Key = VK_F9 then
    btnFiltrarClick(self);

  if Key = vk_escape then
    Close;

end;

procedure TfrmImportarR.FormKeyPress(Sender: TObject; var Key: Char);
begin
  if PageControl1.ActivePage = TabSheet1 then
  begin
    if Key = #13 then
      btnPedido.Click;

  end;
end;

procedure TfrmImportarR.FormShow(Sender: TObject);
begin
  maskInicio.EditText := DateToStr(date);
  maskFim.EditText := DateToStr(date);
  btnFiltrarClick(self);
  frmBalcao.vFinalizar := false;
end;

procedure TfrmImportarR.PageControl1Change(Sender: TObject);
begin
  btnFiltrar.Click;

end;

procedure TfrmImportarR.qryPedidoCalcFields(DataSet: TDataSet);
begin
  if (qryPedidoSITUACAO.Value = 'X') Then
      qryPedidoVIRTUAL_SITUACAO.Value := 'EM ABERTO';
  if (qryPedidoSITUACAO.Value = 'C') Then
      qryPedidoVIRTUAL_SITUACAO.Value := 'CANCELADO';
  if (qryPedidoSITUACAO.Value = 'G') Then
      qryPedidoVIRTUAL_SITUACAO.Value := 'GRAVADO';
  if (qryPedidoSITUACAO.Value = 'F') Then
      qryPedidoVIRTUAL_SITUACAO.Value := 'FECHADO';
end;

procedure TfrmImportarR.btnOSClick(Sender: TObject);
var
  numero: string;
begin

  if qryOs_Master.IsEmpty then
    exit;

  numero := qryOs_MasterCODIGO.AsString;

  Dados.qryConsulta.Close;
  Dados.qryConsulta.SQL.Text :=
    'SELECT CODIGO FROM VENDAS_MASTER WHERE FK_OS=:COD and situacao<>''C''';
  Dados.qryConsulta.Params[0].Value := numero;
  Dados.qryConsulta.Open;

  if not Dados.qryConsulta.IsEmpty then
    raise Exception.Create('OS j� foi importada para o Pedido n�' +
      Dados.qryConsulta.Fields[0].AsString);

  FrmBalcao.PodeAtualizarEstoque := true;

  Dados.qryConsulta.Close;
  Dados.qryConsulta.SQL.Text :=
    'select * FROM OS_MASTER WHERE CODIGO=:NUM AND SITUACAO=''F''';
  Dados.qryConsulta.Params[0].AsString := numero;
  Dados.qryConsulta.Open;

  if Dados.qryConsulta.IsEmpty then
  begin
    ShowMessage('OS n�o encontrada!');
    exit;
  end;

  Dados.Conexao.CommitRetaining;

  if not FrmBalcao.qryItem.IsEmpty then
  begin
    If Application.messageBox('Deseja Excluir os Itens?', 'Confirma��o',
      mb_YesNo + mb_iconquestion) = idyes then
    begin
      ApagaItensVenda;
    end;
  end;

  if not(FrmBalcao.qryVenda.State in dsEditModes) then
    FrmBalcao.qryVenda.Edit;
  FrmBalcao.qryVendaFK_VENDEDOR.Value := Dados.qryConfigVENDEDOR_PADRAO.Value;;
  FrmBalcao.qryVendaID_CLIENTE.Value := Dados.qryConfigCLIENTE_PADRAO.Value;
  FrmBalcao.qryVendaFK_OS.Value := qryOs_MasterCODIGO.Value;
  FrmBalcao.qryVenda.Post;
  Dados.Conexao.CommitRetaining;

  qryOS_Detalhe.Close;
  qryOS_Detalhe.Params[0].Value := Dados.qryConsulta.FieldByName
    ('codigo').Value;
  qryOS_Detalhe.Open;

  qryOS_Detalhe.First;

  while not qryOS_Detalhe.Eof do
  begin
    ImportaOS;
    qryOS_Detalhe.Next;
  end;

  if not(FrmBalcao.qryVenda.State in dsEditModes) then
    FrmBalcao.qryVenda.Edit;

  FrmBalcao.qryVendaDESCONTO.AsFloat := qryOs_MasterVL_DESC_SERVICOS.AsFloat +
    qryOs_MasterVL_DESC_PECAS.AsFloat;

  FrmBalcao.qryVendaTOTAL.AsFloat := FrmBalcao.qryVendaSUBTOTAL.AsFloat -
    FrmBalcao.qryVendaDESCONTO.AsFloat;

  FrmBalcao.qryVenda.Post;
  Dados.Conexao.CommitRetaining;

  Application.ProcessMessages;
  ShowMessage('Importa��o realizada com sucesso!');
  Close;

end;

procedure TfrmImportarR.btnFiltrarClick(Sender: TObject);
begin
  case PageControl1.ActivePageIndex of
    0:
      begin
        qryPedido.Close;
        qryPedido.Params[0].Value := maskInicio.EditText;
        qryPedido.Params[1].Value := maskFim.EditText;
        qryPedido.Open;

      end;
    1:
      begin
        qryOrcamento.Close;
        qryOrcamento.Params[0].Value := maskInicio.EditText;
        qryOrcamento.Params[1].Value := maskFim.EditText;
        qryOrcamento.Open;
      end;
    2:
      begin
        qryOs_Master.Close;
        qryOs_Master.Params[0].Value := maskInicio.EditText;
        qryOs_Master.Params[1].Value := maskFim.EditText;
        qryOs_Master.Open;
      end;

  end;
end;

procedure TfrmImportarR.btnOrcamentoClick(Sender: TObject);
var
  numero: string;
begin

  if qryOrcamento.IsEmpty then
    exit;

  if not FrmBalcao.qryItem.IsEmpty then
  begin
    If Application.messageBox('Deseja Excluir os Itens j� lan�ados?', 'Confirma��o',
      mb_YesNo + mb_iconquestion) = idyes then
    begin
      ApagaItensVenda;
    end;
  end;

  Dados.Conexao.CommitRetaining;

  FrmBalcao.inserevenda;

  FrmBalcao.PodeAtualizarEstoque := true;
  numero := qryOrcamentoCODIGO.AsString;

  if not(FrmBalcao.qryVenda.State in dsEditModes) then
    FrmBalcao.qryVenda.Edit;

  FrmBalcao.qryVendaOBSERVACOES.Value := qryOrcamentoOBS.AsString;

  FrmBalcao.qryVendaFK_VENDEDOR.Value := qryOrcamentoFKVENDEDOR.Value;
  if (qryOrcamentoFK_CLIENTE.Value = 0) or (qryOrcamentoFK_CLIENTE.IsNull) then
    FrmBalcao.qryVendaID_CLIENTE.Value := Dados.qryConfigCLIENTE_PADRAO.Value
  else
    FrmBalcao.qryVendaID_CLIENTE.Value := qryOrcamentoFK_CLIENTE.Value;
  FrmBalcao.qryVendaNOME.Value := qryOrcamentoCLIENTE.Value;

  FrmBalcao.qryVenda.Post;
  Dados.Conexao.CommitRetaining;

  qryItensO.Close;
  qryItensO.Params[0].Value := qryOrcamentoCODIGO.AsInteger;
  qryItensO.Open;

  qryItensO.First;
  while not qryItensO.Eof do
  begin
    ImportaItensOrcamento;
    qryItensO.Next;
  end;

  if not(FrmBalcao.qryVenda.State in dsEditModes) then
    FrmBalcao.qryVenda.Edit;

  FrmBalcao.qryVendaDESCONTO.AsFloat := qryOrcamentoDESCONTO.AsFloat;

  FrmBalcao.qryVendaTOTAL.AsFloat := FrmBalcao.qryVendaSUBTOTAL.AsFloat -
    FrmBalcao.qryVendaDESCONTO.AsFloat;

  if FrmBalcao.qryVendaSUBTOTAL.AsFloat > 0 then
    FrmBalcao.qryVendaPERCENTUAL.AsFloat :=
      (FrmBalcao.qryVendaDESCONTO.AsFloat / FrmBalcao.qryVendaSUBTOTAL.AsFloat) * 100;

  FrmBalcao.qryVenda.Post;
  Dados.Conexao.CommitRetaining;

  Application.ProcessMessages;
  ShowMessage('Importa��o realizada com sucesso!');
  Close;


    FrmBalcao.Panel7.Visible := true;
    FrmBalcao.VendaExiste;
end;

end.
{ TODO -oImportar -cCorre��o : 22-07-2019-Corre��o de pedido de vendas e os }
