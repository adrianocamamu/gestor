unit uTabelaPrecoProdutoR;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Data.DB, FireDAC.Stan.Intf,
  FireDAC.Stan.Option, FireDAC.Stan.Param, FireDAC.Stan.Error, FireDAC.DatS,
  FireDAC.Phys.Intf, FireDAC.DApt.Intf, FireDAC.Stan.Async, FireDAC.DApt,
  FireDAC.Comp.DataSet, FireDAC.Comp.Client, Vcl.Grids, Vcl.DBGrids;

type
  TfrmTabelaPrecoProdutoR = class(TForm)
    DBGrid1: TDBGrid;
    dsTabelaPrecoProduto: TDataSource;
    qryTabelaPrecoProduto: TFDQuery;
    qryTabelaPrecoProdutoCODIGO: TIntegerField;
    qryTabelaPrecoProdutoDESCRICAO: TStringField;
    qryTabelaPrecoProdutoPRECO: TFMTBCDField;
    qryTabelaPrecoProdutoFK_PRODUTO: TIntegerField;
    procedure FormKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure DBGrid1KeyPress(Sender: TObject; var Key: Char);
    procedure dsTabelaPrecoProdutoDataChange(Sender: TObject; Field: TField);
    procedure DBGrid1DblClick(Sender: TObject);
  private
    { Private declarations }

  public
    { Public declarations }
    idPrecoProduto : integer;
  end;

var
  frmTabelaPrecoProdutoR: TfrmTabelaPrecoProdutoR;

implementation

{$R *.dfm}

uses Udados, uBalcao;

procedure TfrmTabelaPrecoProdutoR.DBGrid1DblClick(Sender: TObject);
begin
//  FrmPDV. edtPrecoP.Text := FormatFloat(',0.00', StrToFloatDef(qryTabelaPrecoProdutoPRECO.Text,0));
//  FrmPDV.lblTotalP.Caption := FormatFloat(',0.00',
//    StrToFloatDef(FrmPDV.edtQtdP.Text, 1) *
//    StrToFloatDef(FrmPDV.edtPrecoP.Text, 0));

  idPrecoProduto := qryTabelaPrecoProdutoCODIGO.Value;
  close;
end;

procedure TfrmTabelaPrecoProdutoR.DBGrid1KeyPress(Sender: TObject;
  var Key: Char);
begin
if Key = #13 then
  begin
    FrmBalcao. edtPrecoP.Text := FormatFloat(',0.00', StrToFloatDef(qryTabelaPrecoProdutoPRECO.Text,0));
    FrmBalcao.lblTotalP.Caption := FormatFloat(',0.00',
    StrToFloatDef(FrmBalcao.edtQtdP.Text, 1) *
    StrToFloatDef(FrmBalcao.edtPrecoP.Text, 0));

    idPrecoProduto := qryTabelaPrecoProdutoCODIGO.Value;
    close;
  end;
end;

procedure TfrmTabelaPrecoProdutoR.dsTabelaPrecoProdutoDataChange(
  Sender: TObject; Field: TField);
begin
  FrmBalcao. edtPrecoP.Text := FormatFloat(',0.00', StrToFloatDef(qryTabelaPrecoProdutoPRECO.Text,0));
  FrmBalcao.lblTotalP.Caption := FormatFloat(',0.00',
    StrToFloatDef(FrmBalcao.edtQtdP.Text, 1) *
    StrToFloatDef(FrmBalcao.edtPrecoP.Text, 0));
end;

procedure TfrmTabelaPrecoProdutoR.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
 if Key = vk_escape then
  idPrecoProduto := 0;
  close;
end;

end.
