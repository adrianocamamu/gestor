unit uPrincipalM;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants,
  System.Classes, Vcl.Graphics, TDI, acbrUtil, ACBrBase, ACBrDFe, ACBrNFe,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.ExtCtrls, Vcl.Buttons, Vcl.Menus,
  System.ImageList, Vcl.ImgList, System.Actions, Vcl.ActnList, Vcl.ToolWin,
  Vcl.ComCtrls, IdBaseComponent, IdComponent, IdIPWatch, dateutils, serial,
  Vcl.StdCtrls, frxClass, frxDBSet, frxExportPDF, Vcl.Imaging.pngimage,
  shellapi,
  FireDAC.Stan.Intf, FireDAC.Stan.Option, FireDAC.Stan.Param,
  FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf, FireDAC.DApt.Intf,
  FireDAC.Stan.Async, FireDAC.DApt, Data.DB, FireDAC.Comp.DataSet,
  FireDAC.Comp.Client, frxExportBaseDialog, System.Threading, IdTCPConnection,
  IdTCPClient, IdExplicitTLSClientServerBase, IdFTP, Vcl.AppEvnts, JvExControls,
  JvSpeedButton, Vcl.ExtDlgs, JvComponentBase, JvThreadTimer,
  TLHelp32,PsAPI,Registry, JvXPCore, JvXPButtons, dxGDIPlusClasses;

type
  TfrmPrincipalM = class(TForm)
    MmPrincipal: TMainMenu;
    MnEmpresa: TMenuItem;
    MnSair: TMenuItem;
    MnTroca: TMenuItem;
    MnUsuarios: TMenuItem;
    MnMovimento: TMenuItem;
    N6: TMenuItem;
    MnSistema: TMenuItem;
    IdIPWatch1: TIdIPWatch;
    MnConfig: TMenuItem;
    MnAlterarSenha: TMenuItem;
    MMPermisses: TMenuItem;
    qryPermissoes: TFDQuery;
    qryPermissoesVISUALIZAR: TStringField;
    qryPermissoesTELA: TStringField;
    frxPDFExport1: TfrxPDFExport;
    frxReport: TfrxReport;
    frxDBEmpresa: TfrxDBDataset;
    frxDBEstoqueNegativoR: TfrxDBDataset;
    qryEstoqueReal: TFDQuery;
    qryEstoqueFiscal: TFDQuery;
    frxDBEstoqueNegativoF: TfrxDBDataset;
    frxDBEstoqueMinimo: TfrxDBDataset;
    qryEstoqueMinimo: TFDQuery;
    qryClientesRestricoes: TFDQuery;
    qryClientesRestricoesCODIGO: TIntegerField;
    qryClientesRestricoesRAZAO: TStringField;
    qryClientesRestricoesCCF: TStringField;
    qryClientesRestricoesSPC: TStringField;
    qryClientesRestricoesFONE1: TStringField;
    qryClientesRestricoesFONE2: TStringField;
    qryClientesRestricoesCELULAR1: TStringField;
    qryClientesRestricoesCELULAR2: TStringField;
    frxDBRestricoes: TfrxDBDataset;
    qryConferencia: TFDQuery;
    frxDBConferencia: TfrxDBDataset;
    N15: TMenuItem;
    MNNotasFornecedorLerXML: TMenuItem;
    MNAjustaMenu: TMenuItem;
    qryEstoqueRealCODIGO: TIntegerField;
    qryEstoqueRealTIPO: TStringField;
    qryEstoqueRealCODBARRA: TStringField;
    qryEstoqueRealREFERENCIA: TStringField;
    qryEstoqueRealGRUPO: TIntegerField;
    qryEstoqueRealUNIDADE: TStringField;
    qryEstoqueRealULTFORN: TIntegerField;
    qryEstoqueRealLOCALIZACAO: TStringField;
    qryEstoqueRealALIQ_ICM: TCurrencyField;
    qryEstoqueRealALIQ_PIS: TCurrencyField;
    qryEstoqueRealALIQ_COF: TCurrencyField;
    qryEstoqueRealPR_CUSTO: TFMTBCDField;
    qryEstoqueRealMARGEM: TCurrencyField;
    qryEstoqueRealPR_VENDA: TFMTBCDField;
    qryEstoqueRealQTD_ATUAL: TFMTBCDField;
    qryEstoqueRealQTD_MIN: TFMTBCDField;
    qryEstoqueRealE_MEDIO: TFMTBCDField;
    qryEstoqueRealCSTICMS: TStringField;
    qryEstoqueRealCSTE: TStringField;
    qryEstoqueRealCSTS: TStringField;
    qryEstoqueRealCSTIPI: TStringField;
    qryEstoqueRealCSOSN: TStringField;
    qryEstoqueRealNCM: TStringField;
    qryEstoqueRealCOMISSAO: TCurrencyField;
    qryEstoqueRealDESCONTO: TCurrencyField;
    qryEstoqueRealFOTO: TBlobField;
    qryEstoqueRealATIVO: TStringField;
    qryEstoqueRealCFOP: TStringField;
    qryEstoqueRealPR_CUSTO_ANTERIOR: TFMTBCDField;
    qryEstoqueRealPR_VENDA_ANTERIOR: TFMTBCDField;
    qryEstoqueRealULT_COMPRA: TIntegerField;
    qryEstoqueRealULT_COMPRA_ANTERIOR: TIntegerField;
    qryEstoqueRealPRECO_ATACADO: TFMTBCDField;
    qryEstoqueRealQTD_ATACADO: TFMTBCDField;
    qryEstoqueRealCOD_BARRA_ATACADO: TStringField;
    qryEstoqueRealALIQ_IPI: TFMTBCDField;
    qryEstoqueRealEMPRESA: TSmallintField;
    qryEstoqueRealCEST: TStringField;
    qryEstoqueRealGRADE: TStringField;
    qryEstoqueRealEFISCAL: TStringField;
    qryEstoqueRealPAGA_COMISSAO: TStringField;
    qryEstoqueRealPESO: TFMTBCDField;
    qryEstoqueRealCOMPOSICAO: TStringField;
    qryEstoqueRealPRECO_PROMO_ATACADO: TFMTBCDField;
    qryEstoqueRealPRECO_PROMO_VAREJO: TFMTBCDField;
    qryEstoqueRealINICIO_PROMOCAO: TDateField;
    qryEstoqueRealFIM_PROMOCAO: TDateField;
    qryEstoqueRealESTOQUE_INICIAL: TFMTBCDField;
    qryEstoqueRealPR_VENDA_PRAZO: TFMTBCDField;
    qryEstoqueRealPRECO_VARIAVEL: TStringField;
    qryEstoqueRealAPLICACAO: TStringField;
    qryEstoqueRealREDUCAO_BASE: TFMTBCDField;
    qryEstoqueRealMVA: TFMTBCDField;
    qryEstoqueRealFCP: TFMTBCDField;
    qryEstoqueRealPRODUTO_PESADO: TStringField;
    qryEstoqueRealSERVICO: TStringField;
    qryEstoqueRealDT_CADASTRO: TDateField;
    qryEstoqueRealDESCRICAO: TStringField;
    qryEstoqueRealPR_CUSTO2: TFMTBCDField;
    qryEstoqueRealPERC_CUSTO: TFMTBCDField;
    qryEstoqueMinimoCODIGO: TIntegerField;
    qryEstoqueMinimoTIPO: TStringField;
    qryEstoqueMinimoCODBARRA: TStringField;
    qryEstoqueMinimoREFERENCIA: TStringField;
    qryEstoqueMinimoGRUPO: TIntegerField;
    qryEstoqueMinimoUNIDADE: TStringField;
    qryEstoqueMinimoULTFORN: TIntegerField;
    qryEstoqueMinimoLOCALIZACAO: TStringField;
    qryEstoqueMinimoALIQ_ICM: TCurrencyField;
    qryEstoqueMinimoALIQ_PIS: TCurrencyField;
    qryEstoqueMinimoALIQ_COF: TCurrencyField;
    qryEstoqueMinimoPR_CUSTO: TFMTBCDField;
    qryEstoqueMinimoMARGEM: TCurrencyField;
    qryEstoqueMinimoPR_VENDA: TFMTBCDField;
    qryEstoqueMinimoQTD_ATUAL: TFMTBCDField;
    qryEstoqueMinimoQTD_MIN: TFMTBCDField;
    qryEstoqueMinimoE_MEDIO: TFMTBCDField;
    qryEstoqueMinimoCSTICMS: TStringField;
    qryEstoqueMinimoCSTE: TStringField;
    qryEstoqueMinimoCSTS: TStringField;
    qryEstoqueMinimoCSTIPI: TStringField;
    qryEstoqueMinimoCSOSN: TStringField;
    qryEstoqueMinimoNCM: TStringField;
    qryEstoqueMinimoCOMISSAO: TCurrencyField;
    qryEstoqueMinimoDESCONTO: TCurrencyField;
    qryEstoqueMinimoFOTO: TBlobField;
    qryEstoqueMinimoATIVO: TStringField;
    qryEstoqueMinimoCFOP: TStringField;
    qryEstoqueMinimoPR_CUSTO_ANTERIOR: TFMTBCDField;
    qryEstoqueMinimoPR_VENDA_ANTERIOR: TFMTBCDField;
    qryEstoqueMinimoULT_COMPRA: TIntegerField;
    qryEstoqueMinimoULT_COMPRA_ANTERIOR: TIntegerField;
    qryEstoqueMinimoPRECO_ATACADO: TFMTBCDField;
    qryEstoqueMinimoQTD_ATACADO: TFMTBCDField;
    qryEstoqueMinimoCOD_BARRA_ATACADO: TStringField;
    qryEstoqueMinimoALIQ_IPI: TFMTBCDField;
    qryEstoqueMinimoEMPRESA: TSmallintField;
    qryEstoqueMinimoCEST: TStringField;
    qryEstoqueMinimoGRADE: TStringField;
    qryEstoqueMinimoEFISCAL: TStringField;
    qryEstoqueMinimoPAGA_COMISSAO: TStringField;
    qryEstoqueMinimoPESO: TFMTBCDField;
    qryEstoqueMinimoCOMPOSICAO: TStringField;
    qryEstoqueMinimoPRECO_PROMO_ATACADO: TFMTBCDField;
    qryEstoqueMinimoPRECO_PROMO_VAREJO: TFMTBCDField;
    qryEstoqueMinimoINICIO_PROMOCAO: TDateField;
    qryEstoqueMinimoFIM_PROMOCAO: TDateField;
    qryEstoqueMinimoESTOQUE_INICIAL: TFMTBCDField;
    qryEstoqueMinimoPR_VENDA_PRAZO: TFMTBCDField;
    qryEstoqueMinimoPRECO_VARIAVEL: TStringField;
    qryEstoqueMinimoAPLICACAO: TStringField;
    qryEstoqueMinimoREDUCAO_BASE: TFMTBCDField;
    qryEstoqueMinimoMVA: TFMTBCDField;
    qryEstoqueMinimoFCP: TFMTBCDField;
    qryEstoqueMinimoPRODUTO_PESADO: TStringField;
    qryEstoqueMinimoSERVICO: TStringField;
    qryEstoqueMinimoDT_CADASTRO: TDateField;
    qryEstoqueMinimoDESCRICAO: TStringField;
    qryEstoqueMinimoPR_CUSTO2: TFMTBCDField;
    qryEstoqueMinimoPERC_CUSTO: TFMTBCDField;
    qryConferenciaCODIGO: TIntegerField;
    qryConferenciaTIPO: TStringField;
    qryConferenciaCODBARRA: TStringField;
    qryConferenciaREFERENCIA: TStringField;
    qryConferenciaGRUPO: TIntegerField;
    qryConferenciaUNIDADE: TStringField;
    qryConferenciaULTFORN: TIntegerField;
    qryConferenciaLOCALIZACAO: TStringField;
    qryConferenciaALIQ_ICM: TCurrencyField;
    qryConferenciaALIQ_PIS: TCurrencyField;
    qryConferenciaALIQ_COF: TCurrencyField;
    qryConferenciaPR_CUSTO: TFMTBCDField;
    qryConferenciaMARGEM: TCurrencyField;
    qryConferenciaPR_VENDA: TFMTBCDField;
    qryConferenciaQTD_ATUAL: TFMTBCDField;
    qryConferenciaQTD_MIN: TFMTBCDField;
    qryConferenciaE_MEDIO: TFMTBCDField;
    qryConferenciaCSTICMS: TStringField;
    qryConferenciaCSTE: TStringField;
    qryConferenciaCSTS: TStringField;
    qryConferenciaCSTIPI: TStringField;
    qryConferenciaCSOSN: TStringField;
    qryConferenciaNCM: TStringField;
    qryConferenciaCOMISSAO: TCurrencyField;
    qryConferenciaDESCONTO: TCurrencyField;
    qryConferenciaFOTO: TBlobField;
    qryConferenciaATIVO: TStringField;
    qryConferenciaCFOP: TStringField;
    qryConferenciaPR_CUSTO_ANTERIOR: TFMTBCDField;
    qryConferenciaPR_VENDA_ANTERIOR: TFMTBCDField;
    qryConferenciaULT_COMPRA: TIntegerField;
    qryConferenciaULT_COMPRA_ANTERIOR: TIntegerField;
    qryConferenciaPRECO_ATACADO: TFMTBCDField;
    qryConferenciaQTD_ATACADO: TFMTBCDField;
    qryConferenciaCOD_BARRA_ATACADO: TStringField;
    qryConferenciaALIQ_IPI: TFMTBCDField;
    qryConferenciaEMPRESA: TSmallintField;
    qryConferenciaCEST: TStringField;
    qryConferenciaGRADE: TStringField;
    qryConferenciaEFISCAL: TStringField;
    qryConferenciaPAGA_COMISSAO: TStringField;
    qryConferenciaPESO: TFMTBCDField;
    qryConferenciaCOMPOSICAO: TStringField;
    qryConferenciaPRECO_PROMO_ATACADO: TFMTBCDField;
    qryConferenciaPRECO_PROMO_VAREJO: TFMTBCDField;
    qryConferenciaINICIO_PROMOCAO: TDateField;
    qryConferenciaFIM_PROMOCAO: TDateField;
    qryConferenciaESTOQUE_INICIAL: TFMTBCDField;
    qryConferenciaPR_VENDA_PRAZO: TFMTBCDField;
    qryConferenciaPRECO_VARIAVEL: TStringField;
    qryConferenciaAPLICACAO: TStringField;
    qryConferenciaREDUCAO_BASE: TFMTBCDField;
    qryConferenciaMVA: TFMTBCDField;
    qryConferenciaFCP: TFMTBCDField;
    qryConferenciaPRODUTO_PESADO: TStringField;
    qryConferenciaSERVICO: TStringField;
    qryConferenciaDT_CADASTRO: TDateField;
    qryConferenciaDESCRICAO: TStringField;
    qryConferenciaPR_CUSTO2: TFMTBCDField;
    qryConferenciaPERC_CUSTO: TFMTBCDField;
    MMAjuda: TMenuItem;
    BalloonHint1: TBalloonHint;
    Label1: TLabel;
    N16: TMenuItem;
    MnExecuteScript: TMenuItem;
    MnFecharJanelas: TMenuItem;
    ApplicationEvents1: TApplicationEvents;
    OpenPicture: TOpenPictureDialog;
    qryEstoqueGrade: TFDQuery;
    qryEstoqueGradeCODIGO: TIntegerField;
    qryEstoqueGradeDESCRICAO: TStringField;
    qryEstoqueGradeGRADE: TStringField;
    qryEstoqueGradeQTD: TFMTBCDField;
    qryEstoqueGradePRECO: TFMTBCDField;
    frxDBEstoqueGrade: TfrxDBDataset;
    qryComposicao_M: TFDQuery;
    qryComposicao_D: TFDQuery;
    qryComposicao_DID_PRODUTO: TIntegerField;
    qryComposicao_DDESCRICAO: TStringField;
    qryComposicao_DQUANTIDADE: TFMTBCDField;
    qryComposicao_DPRECO: TFMTBCDField;
    qryComposicao_MCODIGO: TIntegerField;
    qryComposicao_MDESCRICAO: TStringField;
    qryComposicao_MQTD_ATUAL: TFMTBCDField;
    qryComposicao_MPR_VENDA: TFMTBCDField;
    frxDBComposicao_M: TfrxDBDataset;
    frxDBComposicao_D: TfrxDBDataset;
    dsComposicao_M: TDataSource;
    Timer1: TTimer;
    StatusBar1: TStatusBar;
    Panel1: TPanel;
    btnBackup: TSpeedButton;
    btnSair: TSpeedButton;
    Timer2: TTimer;
    Timer3: TTimer;
    Regs1: TMenuItem;
    Image1: TImage;
    Label2: TLabel;
    tmrWhatsServer: TTimer;
    tmrCobian: TTimer;
    qryParametro: TFDQuery;
    qryParametroVERSAO: TIntegerField;
    qryParametroDATA_SCRIPT: TDateField;
    qryParametroEMAIL_SUPORTE: TStringField;
    qryParametroTITULO_SISTEMA: TStringField;
    qryParametroSUB_TITULO_SISTEMA: TStringField;
    qryParametroSITE: TStringField;
    qryParametroFONE1: TStringField;
    qryParametroFONE2: TStringField;
    qryParametroCONTATO: TStringField;
    qryParametroESTILO: TStringField;
    qryParametroLINK_TREINAMENTO: TStringField;
    qryParametroICONE: TBlobField;
    qryParametroSPLASH: TBlobField;
    qryParametroSERVIDOR_APP: TStringField;
    qryParametroUSUARIO_APP: TStringField;
    qryParametroSENHA_APP: TStringField;
    qryParametroDATABASE_APP: TStringField;
    qryParametroDATABASE_LI: TStringField;
    qryParametroTELA_FUNDO: TStringField;
    qryParametroMENU_MAXIMIZADO: TStringField;
    qryParametroSENHA_LI: TStringField;
    qryParametroTELA_ABERTURA: TStringField;
    qryParametroTELA_FUNDO_ECF: TStringField;
    qryParametroUSUARIO_LI: TStringField;
    qryParametroFTP_PASTA: TStringField;
    qryParametroFTP_SERVIDOR: TStringField;
    qryParametroFTP_USUARIO: TStringField;
    qryParametroFTP_SENHA: TStringField;
    qryParametroFTP_ARQUIVO: TStringField;
    qryParametroEMPRESA: TStringField;
    qryParametroLINK_VENDA: TStringField;
    qryParametroCAMINHO_LOGO_FPG: TStringField;
    qryParametroTELA_FUNDO_LOGIN: TStringField;
    qryParametroCNPJ: TStringField;
    qryParametroBLOQUEAR_PERSONALIZACAO: TStringField;
    qryParametroVAL_CERTIFICADO: TDateField;
    SpeedButton1: TSpeedButton;








    procedure btnSairClick(Sender: TObject);
    procedure MnTrocaClick(Sender: TObject);
    procedure MnSairClick(Sender: TObject);

    procedure MnUsuariosClick(Sender: TObject);
    procedure MnEmpresaClick(Sender: TObject);
    procedure Permisses1Click(Sender: TObject);



    procedure MnPDVClick(Sender: TObject);
    procedure MnConfigClick(Sender: TObject);


    procedure MNHistoricodeVendas1Click(Sender: TObject);
    procedure MNHistoricodeVendas2Click(Sender: TObject);
    procedure MNHistoricodeCompras1Click(Sender: TObject);



    procedure MMPermissesClick(Sender: TObject);
    procedure MnAlterarSenhaClick(Sender: TObject);

    procedure MNEstoqueMinimoClick(Sender: TObject);
    procedure MNListaSPCCCFClick(Sender: TObject);
    procedure frxReportGetValue(const VarName: string; var Value: Variant);
    procedure MNListaConfEstoqueClick(Sender: TObject);
    procedure MNEstoqueNegativoClick(Sender: TObject);


    procedure MnListagemdeVendasClick(Sender: TObject);
    procedure MnListagemdeComprasClick(Sender: TObject);



    procedure FormCreate(Sender: TObject);





    procedure FormShow(Sender: TObject);

    

    procedure MNNotasFornecedorLerXMLClick(Sender: TObject);



    procedure MNAjustaMenuClick(Sender: TObject);

    procedure MNRegistrarClick(Sender: TObject);
    procedure MNTeinamentosClick(Sender: TObject);
    procedure MNSincronizarClick(Sender: TObject);
    procedure MNPedidosWebClick(Sender: TObject);

    procedure btnPessoasMouseMove(Sender: TObject; Shift: TShiftState;
      X, Y: Integer);
    procedure btnPessoasMouseLeave(Sender: TObject);

    procedure MnFecharJanelasClick(Sender: TObject);

    procedure FormActivate(Sender: TObject);

    procedure mnTabelasCamposClick(Sender: TObject);
    //procedure Configurao1Click(Sender: TObject);

    procedure Image1DblClick(Sender: TObject);
    procedure MNRelatriodeEstoqueGradeClick(Sender: TObject);
    procedure MNRelatriodeEstoqueComposioClick(Sender: TObject);
    procedure dsComposicao_MDataChange(Sender: TObject; Field: TField);

    procedure Timer1Timer(Sender: TObject);
    procedure btnWhatsClick(Sender: TObject);

    procedure MnAjustaCamposClick(Sender: TObject);
    procedure Timer2Timer(Sender: TObject);

    procedure Regs1Click(Sender: TObject);
    procedure mnEnviarMensagemClick(Sender: TObject);


    procedure tmrWhatsServerTimer(Sender: TObject);

    procedure tmrCobianTimer(Sender: TObject);


    procedure SpeedButton1Click(Sender: TObject);
  private
    function ChamaLogin: Boolean;
    function VerificarExisteConexaoComInternet: Boolean;
    function CriaEmpresa: Boolean;
    procedure CarregaTabelas;

    procedure TituloEmpresa;

    procedure VerificaBackup;
    procedure LicencaOnline;
    procedure CarregaImagem;

    function ChecaValidade: Boolean;
    function ProcessExists(exeFileName: string): Boolean;
    { Private declarations }
  public
    procedure ConfiguraSistema;
    procedure CarregaSistema;
    procedure InsereTela;
    procedure HabilitaMenus;
    procedure DesabilitaMenus;
    { Public declarations }
  end;

var
  frmPrincipalM: TfrmPrincipalM;
  FTDI: TTDI;

implementation

{$R *.dfm}

uses WinInet,
   uAcesso, UUsuarios,
  uEmpresa, Udados, uPermissoes, uConfig,
   uParametros, uChave, utrocaSenha, uConsEmpresa,
   uDadosWeb, uManifesto,
   uSincronizar, uPedidoWeb,

  uExecute,
  //ufrmCBRconfig,
  uATZBanco;



function RemoveAcento(Str: string): string;
const
  ComAcento = '����������������������������';
  SemAcento = 'aaeouaoaeioucuAAEOUAOAEIOUCU';
var
  X: Integer;
begin;
  for X := 1 to Length(Str) do
    if Pos(Str[X], ComAcento) <> 0 then
      Str[X] := SemAcento[Pos(Str[X], ComAcento)];
  Result := Str;
end;







procedure TfrmPrincipalM.CarregaImagem;
begin

  if not Dados.qryParametro.Active then
    Dados.qryParametro.Open;

  if FileExists(Dados.qryParametro.FieldByName('TELA_FUNDO').AsString) then
  begin
    Image1.Picture.LoadFromFile(Dados.qryParametro.FieldByName('TELA_FUNDO')
      .AsString);
  end;

end;

procedure TfrmPrincipalM.Image1DblClick(Sender: TObject);
begin
  OpenPicture.Execute;
  if Trim(OpenPicture.FileName) <> '' then
  begin

    Dados.qryParametro.Edit;
    Dados.qryParametro.FieldByName('TELA_FUNDO').AsString :=
      OpenPicture.FileName;
    Dados.qryParametro.Post;
    Dados.Conexao.CommitRetaining;

    CarregaImagem;

  end;
end;



procedure TfrmPrincipalM.InsereTela;
var
  nivel, i, j, k, l, cont, cont1: Integer;

begin
  // esta fun��o percorre menu e sub menus e insere na tela
  try
    Dados.qryUpdate.Close;
    Dados.qryUpdate.SQL.Clear;
    Dados.qryUpdate.SQL.Add('UPDATE OR INSERT INTO TELAS (');
    Dados.qryUpdate.SQL.Add('CODIGO,');
    Dados.qryUpdate.SQL.Add('TELA,');
    Dados.qryUpdate.SQL.Add('NIVEL,');
    Dados.qryUpdate.SQL.Add('PAI,');
    Dados.qryUpdate.SQL.Add('NOME,');
    Dados.qryUpdate.SQL.Add('FLAG');
    Dados.qryUpdate.SQL.Add(')');
    Dados.qryUpdate.SQL.Add('VALUES');
    Dados.qryUpdate.SQL.Add('(');
    Dados.qryUpdate.SQL.Add(':CODIGO,');
    Dados.qryUpdate.SQL.Add(':TELA,');
    Dados.qryUpdate.SQL.Add(':NIVEL,');
    Dados.qryUpdate.SQL.Add(':PAI,');
    Dados.qryUpdate.SQL.Add(':NOME,');
    Dados.qryUpdate.SQL.Add(':FLAG)');
    Dados.qryUpdate.SQL.Add('MATCHING (CODIGO)');

    Dados.QryTelas.Close;
    Dados.QryTelas.Open;

    for i := 0 to MmPrincipal.items.Count - 1 do
    begin // verifica menu principal
      if not(Dados.QryTelas.locate('TELA', MmPrincipal.items.items[i].Name, []))
      then
      begin
        if (MmPrincipal.items.items[i].Caption <> '-') then
        begin
          Dados.qryUpdate.Close;
          Dados.qryUpdate.ParamByName('CODIGO').Value :=
            Dados.Numerador('TELAS', 'CODIGO', 'N', 'N', 'N');
          Dados.qryUpdate.ParamByName('TELA').Value :=
            MmPrincipal.items.items[i].Name;
          Dados.qryUpdate.ParamByName('NOME').Value :=
            RemoveAcento(MmPrincipal.items.items[i].Caption);
          Dados.qryUpdate.ParamByName('NIVEL').Value := 0;
          Dados.qryUpdate.ParamByName('FLAG').Value := 'S';
          Dados.qryUpdate.ParamByName('PAI').Value := i;
          Dados.qryUpdate.ExecSQL;
          Dados.Conexao.CommitRetaining;
        end;
      end
      else
      begin
        Dados.qryUpdate.Close;
        Dados.qryUpdate.ParamByName('CODIGO').Value :=
          Dados.QryTelas.FieldByName('CODIGO').Value;
        Dados.qryUpdate.ParamByName('TELA').Value :=
          MmPrincipal.items.items[i].Name;
        Dados.qryUpdate.ParamByName('NOME').Value :=
          RemoveAcento(MmPrincipal.items.items[i].Caption);
        Dados.qryUpdate.ParamByName('NIVEL').Value := 0;
        Dados.qryUpdate.ParamByName('PAI').Value := i;
        Dados.qryUpdate.ExecSQL;
        Dados.Conexao.CommitRetaining;
      end;

    end;

    for i := 0 to MmPrincipal.items.Count - 1 do
    begin // menu principal
      for j := 0 to MmPrincipal.items.items[i].Count - 1 do
      begin // verifica submenu 1� nivel
        if not(Dados.QryTelas.locate('TELA', MmPrincipal.items.items[i].items[j]
          .Name, [])) then
        begin
          if MmPrincipal.items.items[i].items[j].Caption <> '-' then
          begin

            Dados.qryUpdate.Close;
            Dados.qryUpdate.ParamByName('CODIGO').Value :=
              Dados.Numerador('TELAS', 'CODIGO', 'N', 'N', 'N');
            Dados.qryUpdate.ParamByName('TELA').Value := MmPrincipal.items.items
              [i].items[j].Name;
            Dados.qryUpdate.ParamByName('NOME').Value :=
              RemoveAcento(MmPrincipal.items.items[i].items[j].Caption);
            Dados.qryUpdate.ParamByName('NIVEL').Value := 1;
            Dados.qryUpdate.ParamByName('FLAG').Value := 'S';
            Dados.qryUpdate.ParamByName('PAI').Value := i;
            Dados.qryUpdate.ExecSQL;
            Dados.Conexao.CommitRetaining;

          end;
        end
        else
        begin

          Dados.qryUpdate.Close;
          Dados.qryUpdate.ParamByName('CODIGO').Value :=
            Dados.QryTelas.FieldByName('CODIGO').Value;
          Dados.qryUpdate.ParamByName('TELA').Value := MmPrincipal.items.items
            [i].items[j].Name;
          Dados.qryUpdate.ParamByName('NOME').Value :=
            RemoveAcento(MmPrincipal.items.items[i].items[j].Caption);
          Dados.qryUpdate.ParamByName('NIVEL').Value := 1;
          Dados.qryUpdate.ParamByName('PAI').Value := i;
          Dados.qryUpdate.ExecSQL;
          Dados.Conexao.CommitRetaining;

        end;
      end;
    end;

    for i := 0 to MmPrincipal.items.Count - 1 do
    begin // menu
      for j := 0 to MmPrincipal.items.items[i].Count - 1 do
      begin // submenu 1� nivel
        for k := 0 to MmPrincipal.items.items[i].items[j].Count - 1 do
        begin // submenu 2� nivel
          if not(Dados.QryTelas.locate('TELA', MmPrincipal.items.items[i].items
            [j].items[k].Name, [])) then
          begin
            if MmPrincipal.items.items[i].items[j].items[k].Caption <> '-' then
            begin

              Dados.qryUpdate.Close;
              Dados.qryUpdate.ParamByName('CODIGO').Value :=
                Dados.Numerador('TELAS', 'CODIGO', 'N', 'N', 'N');
              Dados.qryUpdate.ParamByName('TELA').Value :=
                MmPrincipal.items.items[i].items[j].items[k].Name;
              Dados.qryUpdate.ParamByName('NOME').Value :=
                RemoveAcento(MmPrincipal.items.items[i].items[j].items
                [k].Caption);
              Dados.qryUpdate.ParamByName('NIVEL').Value := 2;
              Dados.qryUpdate.ParamByName('FLAG').Value := 'S';
              Dados.qryUpdate.ParamByName('PAI').Value := i;
              Dados.qryUpdate.ExecSQL;
              Dados.Conexao.CommitRetaining;

            end;
          end
          else
          begin

            Dados.qryUpdate.Close;
            Dados.qryUpdate.ParamByName('CODIGO').Value :=
              Dados.QryTelas.FieldByName('CODIGO').Value;
            Dados.qryUpdate.ParamByName('TELA').Value := MmPrincipal.items.items
              [i].items[j].items[k].Name;
            Dados.qryUpdate.ParamByName('NOME').Value :=
              RemoveAcento(MmPrincipal.items.items[i].items[j].items[k]
              .Caption);
            Dados.qryUpdate.ParamByName('NIVEL').Value := 2;
            Dados.qryUpdate.ParamByName('PAI').Value := i;
            Dados.qryUpdate.ExecSQL;
            Dados.Conexao.CommitRetaining;

          end;
        end;
      end;
    end;

    for i := 0 to MmPrincipal.items.Count - 1 do
    begin // menu
      for j := 0 to MmPrincipal.items.items[i].Count - 1 do
      begin // submenu 1� nivel
        for k := 0 to MmPrincipal.items.items[i].items[j].Count - 1 do
        begin // submenu 2� nivel
          for l := 0 to MmPrincipal.items.items[i].items[j].items[k]
            .Count - 1 do
          begin // submenu 3� nivel
            if MmPrincipal.items.items[i].items[j].items[k].items[l].Caption <> '-'
            then
            begin
              if not(Dados.QryTelas.locate('TELA',
                MmPrincipal.items.items[i].items[j].items[k].items[l].Name, []))
              then
              begin

                Dados.qryUpdate.Close;
                Dados.qryUpdate.ParamByName('CODIGO').Value :=
                  Dados.Numerador('TELAS', 'CODIGO', 'N', 'N', 'N');
                Dados.qryUpdate.ParamByName('TELA').Value :=
                  MmPrincipal.items.items[i].items[j].items[k].items[l].Name;
                Dados.qryUpdate.ParamByName('NOME').Value :=
                  RemoveAcento(MmPrincipal.items.items[i].items[j].items[k]
                  .items[l].Caption);
                Dados.qryUpdate.ParamByName('NIVEL').Value := 3;
                Dados.qryUpdate.ParamByName('FLAG').Value := 'S';
                Dados.qryUpdate.ParamByName('PAI').Value := i;
                Dados.qryUpdate.ExecSQL;
                Dados.Conexao.CommitRetaining;

              end
            end
            else
            begin

              Dados.qryUpdate.Close;
              Dados.qryUpdate.ParamByName('CODIGO').Value :=
                Dados.QryTelas.FieldByName('CODIGO').Value;
              Dados.qryUpdate.ParamByName('TELA').Value :=
                MmPrincipal.items.items[i].items[j].items[k].items[l].Name;
              Dados.qryUpdate.ParamByName('NOME').Value :=
                RemoveAcento(MmPrincipal.items.items[i].items[j].items[k].items
                [l].Caption);
              Dados.qryUpdate.ParamByName('NIVEL').Value := 3;
              Dados.qryUpdate.ParamByName('PAI').Value := i;
              Dados.qryUpdate.ExecSQL;
              Dados.Conexao.CommitRetaining;

            end;
          end;
        end;
      end;
    end;
    Dados.QryTelas.Refresh;
  except
    on e: exception do
      raise exception.Create(e.Message);
  end;

end;



procedure TfrmPrincipalM.MNListaSPCCCFClick(Sender: TObject);
begin
  Dados.aMenu := 'MNListaSPCCCF';
  qryClientesRestricoes.Close;
  qryClientesRestricoes.Open;
  frxReport.LoadFromFile(ExtractFilePath(Application.ExeName) +
    '\Relatorio\RelRestricoes.fr3');
  frxReport.ShowReport;

end;





procedure TfrmPrincipalM.MnAjustaCamposClick(Sender: TObject);
begin
  Dados.aMenu := 'MnAjustaCampos';
  Dados.AjustaCamposNovos('orcamento');
  Dados.AjustaCamposNovos('nfe');
  Dados.AjustaCamposNovos('os');
  Dados.AjustaCamposNovos('compra');
end;





procedure TfrmPrincipalM.HabilitaMenus;
var
  i, j, k, l: Integer;
begin

  qryPermissoes.Close;
  qryPermissoes.Params[0].AsFloat := Dados.idUsuario;
  qryPermissoes.Open;
  qryPermissoes.First;

  // nivel 0
  for i := 0 to MmPrincipal.items.Count - 1 do
  begin
    if qryPermissoes.locate('tela', MmPrincipal.items.items[i].Name, []) then
    begin
      if qryPermissoesVISUALIZAR.AsString = 'S' then
        MmPrincipal.items.items[i].Visible := true;
    end
    else
      MmPrincipal.items.items[i].Visible := false;
  end;

  // nivel 1
  for i := 0 to MmPrincipal.items.Count - 1 do
  begin
    for j := 0 to MmPrincipal.items.items[i].Count - 1 do
    begin
      if qryPermissoes.locate('tela', MmPrincipal.items.items[i].items[j]
        .Name, []) then
      begin
        if qryPermissoesVISUALIZAR.AsString = 'S' then
          MmPrincipal.items.items[i].items[j].Enabled := true;
      end
      else
        MmPrincipal.items.items[i].items[j].Enabled := false;
    end;
  end;

  // nivel 3
  for i := 0 to MmPrincipal.items.Count - 1 do
  begin
    for j := 0 to MmPrincipal.items.items[i].Count - 1 do
    begin
      for k := 0 to MmPrincipal.items.items[i].items[j].Count - 1 do
      begin
        if qryPermissoes.locate('tela', MmPrincipal.items.items[i].items[j]
          .items[k].Name, []) then
        begin
          if qryPermissoesVISUALIZAR.AsString = 'S' then
            MmPrincipal.items.items[i].items[j].items[k].Enabled := true;
        end
        else
          MmPrincipal.items.items[i].items[j].items[k].Enabled := false;

      end;
    end;
  end;

  // nivel 4
  for i := 0 to MmPrincipal.items.Count - 1 do
  begin
    for j := 0 to MmPrincipal.items.items[i].Count - 1 do
    begin
      for k := 0 to MmPrincipal.items.items[i].items[j].Count - 1 do
      begin
        for l := 0 to MmPrincipal.items.items[i].items[j].items[k].Count - 1 do
        begin
          if qryPermissoes.locate('tela', MmPrincipal.items.items[i].items[j]
            .items[k].items[l].Name, []) then
          begin
            if qryPermissoesVISUALIZAR.AsString = 'S' then
              MmPrincipal.items.items[i].items[j].items[k].items[l]
                .Enabled := true;
          end
          else
            MmPrincipal.items.items[i].items[j].items[k].items[l]
              .Enabled := false;
        end;
      end;
    end;
  end;

  
end;



procedure TfrmPrincipalM.DesabilitaMenus;
var
  i, j, k, l: Integer;
begin

  qryPermissoes.Close;
  qryPermissoes.Params[0].AsFloat := Dados.idUsuario;
  qryPermissoes.Open;
  qryPermissoes.First;

  // nivel 0
  for i := 0 to MmPrincipal.items.Count - 1 do
  begin
    if qryPermissoes.locate('tela', MmPrincipal.items.items[i].Name, []) then
      MmPrincipal.items.items[i].Visible := false;
  end;

  // nivel 1
  for i := 0 to MmPrincipal.items.Count - 1 do
  begin
    for j := 0 to MmPrincipal.items.items[i].Count - 1 do
      MmPrincipal.items.items[i].items[j].Enabled := false;
  end;

  // nivel 3
  for i := 0 to MmPrincipal.items.Count - 1 do
  begin
    for j := 0 to MmPrincipal.items.items[i].Count - 1 do
    begin
      for k := 0 to MmPrincipal.items.items[i].items[j].Count - 1 do
        MmPrincipal.items.items[i].items[j].items[k].Enabled := false;
    end;
  end;

  // nivel 4
  for i := 0 to MmPrincipal.items.Count - 1 do
  begin
    for j := 0 to MmPrincipal.items.items[i].Count - 1 do
    begin
      for k := 0 to MmPrincipal.items.items[i].items[j].Count - 1 do
      begin
        for l := 0 to MmPrincipal.items.items[i].items[j].items[k].Count - 1 do
          MmPrincipal.items.items[i].items[j].items[k].items[l].Enabled
            := false;
      end;
    end;
  end;
end;



procedure TfrmPrincipalM.dsComposicao_MDataChange(Sender: TObject;
  Field: TField);
begin
  qryComposicao_D.Close;
  qryComposicao_D.Params[0].Value := qryComposicao_MCODIGO.Value;
  qryComposicao_D.Open;

end;












procedure TfrmPrincipalM.btnSairClick(Sender: TObject);
begin
  if Application.messagebox('Deseja Sair do Sistema?', 'Confirma��o', mb_yesno)
    = mrYes then
  begin
    btnBackup.Click;
    Dados.AtualizaTerminal;
    Dados.vFechaPrograma := true;
    Application.Terminate;
  end;

end;

procedure TfrmPrincipalM.btnWhatsClick(Sender: TObject);
var
  caminho: string;
begin
  caminho := ExtractFilePath(Application.ExeName) + 'Whats.exe';
  if FileExists(caminho) then
    ShellExecute(0, 'open', PChar(caminho), '', '', SW_SHOWNORMAL);
end;















procedure TfrmPrincipalM.MNEstoqueNegativoClick(Sender: TObject);
begin
  qryEstoqueReal.Close;
  qryEstoqueReal.Open;
  frxReport.LoadFromFile(ExtractFilePath(Application.ExeName) +
    '\Relatorio\RelEstoqueNegativoR.fr3');
  frxReport.ShowReport;

end;





procedure TfrmPrincipalM.MnConfigClick(Sender: TObject);
begin
  Dados.aMenu := 'MnConfig';
  try
    frmConfig := TfrmConfig.Create(Application);
    frmConfig.ShowModal;
  finally
    FreeAndNil(frmConfig);
  end;
end;



function TfrmPrincipalM.CriaEmpresa: Boolean;

begin
  Result := true;
  Dados.qryConsulta.Close;
  Dados.qryConsulta.SQL.Text := 'select * from empresa';
  Dados.qryConsulta.Open;

  if Dados.qryConsulta.IsEmpty then
    Result := false;

  if not Result then
  begin
    try

      Dados.LimpaTerminais;

      frmempresa := Tfrmempresa.Create(Application);

      frmempresa.qryEmpresa.Close;
      frmempresa.qryEmpresa.Params[0].Value := -1;
      frmempresa.qryEmpresa.Open;

      frmempresa.qryEmpresa.Insert;
      frmempresa.qryEmpresaCODIGO.Value := Dados.Numerador('EMPRESA', 'CODIGO',
        'N', '', '');
      frmempresa.ShowModal;
    finally
      frmempresa.Release;
    end;
  end;

  Dados.qryConsulta.Close;
  Dados.qryConsulta.SQL.Text := 'select * from empresa';
  Dados.qryConsulta.Open;

  if not Dados.qryConsulta.IsEmpty then
    Result := true
  else
  begin
    ShowMessage('N�o existe empresa cadastrada, sistema ser� finalizado!');
    Dados.vFechaPrograma := true;
    Dados.AtualizaTerminal;
    Application.Terminate;
  end;

end;

procedure TfrmPrincipalM.mnTabelasCamposClick(Sender: TObject);
begin
  try
    frmExecute := TfrmExecute.Create(Application);
    ShowMessage('Comandos executados com sucesso!');
  finally
    FreeAndNil(frmExecute);
  end;
end;







function TfrmPrincipalM.ChamaLogin: Boolean;
var
  CriaUsuario: Boolean;
begin
  CriaUsuario := false;

  Dados.qryConsulta.Close;
  Dados.qryConsulta.SQL.Text := 'select * from usuarios';
  Dados.qryConsulta.Open;
  if Dados.qryConsulta.IsEmpty then
    CriaUsuario := true;
  Result := CriaUsuario;
  if not CriaUsuario then
  begin
    try
      frmAcesso := TfrmAcesso.Create(Application);
      frmAcesso.ShowModal;
    finally
      frmAcesso.Release;
    end;
  end
  else
  begin
    try
      frmUsuarios := TfrmUsuarios.Create(Application);
      Dados.aUsuario := 'Novo';
      frmUsuarios.ShowModal;
    finally
      frmUsuarios.Release;
    end;
  end;

end;


procedure TfrmPrincipalM.MNEstoqueMinimoClick(Sender: TObject);
begin
  qryEstoqueMinimo.Close;
  qryEstoqueMinimo.Open;
  frxReport.LoadFromFile(ExtractFilePath(Application.ExeName) +
    '\Relatorio\RelEstoqueMinimo.fr3');
  frxReport.ShowReport;

end;







procedure TfrmPrincipalM.MnFecharJanelasClick(Sender: TObject);
begin
  if FTDI <> nil then
  begin
    FTDI.Fechar(true);
    FreeAndNil(FTDI);
  end;
end;



procedure TfrmPrincipalM.FormActivate(Sender: TObject);
begin
  Dados.vForm := nil;
  Dados.vForm := self;
  Dados.GetComponentes;
end;

procedure TfrmPrincipalM.FormCreate(Sender: TObject);
begin
  Dados.vRetaguarda := true;

  VerificaVersao := true;
  Dados.FVerificouOnline := true;
  Dados.vFechaPrograma := false;
  Dados.FVerificaValidade := true;

  qryParametro.Close;
  qryParametro.Open;

//  if qryParametroVERSAO.Value < 306 then
//  begin
//    CarregaTabelas;
//  end;

  Dados.qryTerminal.Close;
  Dados.qryTerminal.Params[0].Value := Dados.Getcomputer;
  Dados.qryTerminal.Open;

  Dados.CriaTerminal;

  ConfiguraSistema;

end;

procedure TfrmPrincipalM.LicencaOnline;
begin
  DadosWeb.CadastraEmpresa;
  DadosWeb.RetornaSerial;
end;

procedure TfrmPrincipalM.Timer1Timer(Sender: TObject);
var
  Task: ITask;
begin
  try
    Timer1.Enabled := false;
    if not Dados.IsGlobalOffline then
    begin
      Task := TTask.Create(LicencaOnline);
      Task.Start;
    end;
  finally
    tmrWhatsServer.Enabled  :=  true;
    tmrCobian.Enabled  :=  true;
  end;
end;

function TfrmPrincipalM.ChecaValidade: Boolean;
var
  DataValidade: TDate;
begin
  Result := false;
  if (Trim(Dados.qryEmpresaDATA_CADASTRO.AsString) = '') or
    (Dados.qryEmpresaDATA_CADASTRO.IsNull) then
  begin
    Result := true;
    Dados.ChamaContato('Aten��o!' + sLineBreak + 'Chave de registro inv�lida!');
    Dados.qryEmpresa.Edit;
    Dados.qryEmpresaNSERIE.Value := '...';
    Dados.qryEmpresa.Post;
    Dados.Conexao.CommitRetaining;
    Dados.ApagaNumeroSerie;
    exit;
  end;
end;



procedure TfrmPrincipalM.Timer2Timer(Sender: TObject);
var
  DataIni, DataFim, Databloqueio, DataValidade: TDate;
begin
  try
    Timer2.Enabled := false;
    if Dados.FVerificaValidade then
    begin
      if Dados.qryEmpresaCHECA.AsString = Dados.crypt('C', 'DEMOSNTRACAO') then
      begin
        if ChecaValidade then
          MNRegistrarClick(self);
      end
      else if Dados.qryEmpresaCHECA.AsString = Dados.crypt('C', 'TERMINAL_INVALIDO') then
      begin
          ShowMessage('Aten��o!' + sLineBreak + 'Terminal invalido ou n�o encontrado!');
          MNRegistrarClick(self);
          exit;
      end
      else if Dados.qryEmpresaCHECA.AsString <> Dados.crypt('C', 'DEMONSTRACAO')
      then
      begin
        if (Trim(Dados.qryEmpresaDATA_VALIDADE.Value) = '') or
          (Dados.qryEmpresaDATA_VALIDADE.IsNull) then
        begin
          ShowMessage('Aten��o!' + sLineBreak + 'Chave de registro inv�lida!');
          MNRegistrarClick(self);
          exit;
        end;

        if Dados.crypt('D', Dados.qryEmpresaCSENHA.AsString) = 'S' then
        begin
          ShowMessage('Aten��o!' + sLineBreak +
            'Licen�a Bloqueada, entre em contato com o suporte');
          MNRegistrarClick(self);
        end;

        try
          Databloqueio :=
            StrToDateDef(Dados.crypt('D',
            Dados.qryEmpresaDATA_VALIDADE.Value), Date);
        except
          Dados.qryEmpresa.Edit;
          Dados.qryEmpresaNSERIE.Value := '...';
          Dados.qryEmpresaDATA_VALIDADE.Value :=
            Dados.crypt('C', datetostr(Date));
          Dados.qryEmpresa.Post;
        end;

        if (Date > Databloqueio) then
        begin
          ShowMessage
            ('Aten��o, sistema bloqueado, entre em contato com o Suporte!');
          MNRegistrarClick(self);
        end;
      end;
    end;
    Dados.FVerificaValidade := false;
  finally
    Timer3.Enabled := true;
  end;

end;



procedure TfrmPrincipalM.TituloEmpresa;
begin
  if Trim(Dados.qryParametroTITULO_SISTEMA.Value) <> '' then
  begin
    Caption := Dados.qryParametroTITULO_SISTEMA.Value + ' | ' +
      Dados.qryParametroSUB_TITULO_SISTEMA.Value + ' | ' +
      Dados.qryEmpresaFANTASIA.AsString;
  end
  else
    Caption := Caption + ' | ' + Dados.qryEmpresaFANTASIA.AsString;
end;

procedure TfrmPrincipalM.tmrCobianTimer(Sender: TObject);
var
  Dir: string;
begin
  // Verificando se o cobian est� aberto.
  if ProcessExists('cobian.exe') then
    begin
    //Nada faz
    tmrCobian.Enabled  :=  False;
    //ShowMessage('O Processo est� rodando!');
    end
  else
    begin
      tmrCobian.Enabled  :=  False;

      Dir :=  ExtractFilePath(Application.ExeName) + '\CobianBackup\' + 'Cobian.exe';
      if FileExists(Dir) then
      ShellExecute(0, 'open', PChar(Dir), '', '', SW_SHOWNORMAL);
    end;
end;

procedure TfrmPrincipalM.tmrWhatsServerTimer(Sender: TObject);
  function VerficarSeAplicaticoEstarRodandoPeloNomeDoExecutavel(Nome:String):Boolean;
  var rId:array[0..999] of DWord; i,NumProc,NumMod:DWord;
      HProc,HMod:THandle; sNome:String;
      Tamanho, Count:Integer;
      sNomeTratado:String;
  begin
    result:=False;
    SetLength(sNome, 256);
//   Aqui vc pega os IDs dos processos em execu��o
    EnumProcesses(@rId[0], 4000, NumProc);

//   Aqui vc faz um for p/ pegar cada processo
    for i := 0 to NumProc div 4 do
    begin
//   Aqui vc seleciona o processo
      HProc := OpenProcess(Process_Query_Information or Process_VM_Read, FALSE, rId[i]);
      if HProc = 0 then
        Continue;
//   Aqui vc pega os m�dulos do processo
//   Como vc s� quer o nome do programa, ent�o ser� sempre o primeiro
      EnumProcessModules(HProc, @HMod, 4, NumMod);
//   Aqui vc pega o nome do m�dulo; como � o primeiro, � o nome do programa
      GetModuleBaseName(HProc, HMod, @sNome[1], 256);
      sNomeTratado := trim(sNome);
      Tamanho:=Length(SnomeTratado);
       Count:=1;
       While Count <= Tamanho do
         begin
           if SnomeTratado[Count]= '' Then
             Break;
          count:=Count+1;
         end;
       sNomeTratado:=Copy(SnomeTratado,1,Count-1);
      if AnsiUpperCase(sNomeTratado)=AnsiUpperCase(Nome) Then
        Result:=True;
//   Aqui vc libera o handle do processo selecionado
      CloseHandle(HProc);
    end;
  end;
var
  Dir: string;
begin
  tmrWhatsServer.Enabled  :=  False;


//  Dir :=  ExtractFilePath(Application.ExeName) + 'WhatsAppServer.exe';
//  if FileExists(Dir) then
//    ShellExecute(0, 'open', PChar(Dir), '', '', SW_SHOWNORMAL);


end;

procedure TfrmPrincipalM.ConfiguraSistema;
begin

  Dados.qryParametro.Close;
  Dados.qryParametro.Open;

end;

procedure TfrmPrincipalM.CarregaTabelas;
begin
  try
    frmExecute := TfrmExecute.Create(Application);
  finally
    frmExecute.Release;
  end;
end;

//procedure TfrmPrincipalM.Configurao1Click(Sender: TObject);
//begin
//  try
//    frmCBRconfig := TfrmCBRconfig.Create(Application);
//    frmCBRconfig.ShowModal;
//  finally
//    FreeAndNil(frmCBRconfig);
//  end;
//
//end;



procedure TfrmPrincipalM.VerificaBackup;
var
  caminho: string;
begin
  caminho := ExtractFilePath(Application.ExeName) + 'Backup.exe';
  if FileExists(caminho) then
    ShellExecute(0, 'open', PChar(caminho), '', '', SW_SHOWNORMAL);

end;

procedure TfrmPrincipalM.FormShow(Sender: TObject);
begin
  qryParametro.Close;
  qryParametro.Open;

  if qryParametroVERSAO.Value < 308 then
  begin
    frmATZBanco := TfrmATZBanco.Create(Application);
    frmATZBanco.ShowModal;
   // frmExecute.ExecuteScript;
  end;

  CarregaSistema;
  CarregaImagem;
  Timer2.Enabled := true;
end;



function TfrmPrincipalM.VerificarExisteConexaoComInternet: Boolean;
var
  nFlags: Cardinal;
begin
  // retorna True se houver conex�o com a internet
  Result := InternetGetConnectedState(@nFlags, 0);
end;







procedure TfrmPrincipalM.CarregaSistema;
var
  vData: TDateTime;
begin
  if not CriaEmpresa then
  begin
    Dados.AtualizaTerminal;
    Dados.vFechaPrograma := true;
    Application.Terminate;
  end;

  if ChamaLogin then
    exit;

  if Dados.vFechaPrograma then
    exit;

  TituloEmpresa;

  FileAge(ParamStr(0), vData);

  StatusBar1.Panels[0].Text := Application.Title;
  StatusBar1.Panels[1].Text := 'Usu�rio: ' + Dados.vUsuario;
  StatusBar1.Panels[2].Text := 'IP: ' + IdIPWatch1.LocalIP;
  StatusBar1.Panels[3].Text := 'Atualizado Em: ' + datetostr(vData);
  StatusBar1.Panels[4].Text := 'Vers�o: ' + IntToStr(Dados.qryParametroVERSAO.Value);
    //(Application.ExeName);
  StatusBar1.Panels[5].Text := 'Licenciado: ' +
    Dados.crypt('D', Dados.qryEmpresaDATA_VALIDADE.Value);
//  StatusBar1.Panels[5].Text := 'Licenciado: ' +
//    Dados.crypt('D', Dados.qryEmpresaDATA_VALIDADE.Value);

  InsereTela;
  Dados.AjustaPermissoes;
  DesabilitaMenus;
  HabilitaMenus;

  

  Dados.AtualizaTerminal;

end;





procedure TfrmPrincipalM.frxReportGetValue(const VarName: string;
  var Value: Variant);
var
  filtro: string;
begin
  if VarName = 'PARAMETRO' then
  begin
    Value := ' ORDERNADO P/ DESCRI��O ';
  end;
end;



procedure TfrmPrincipalM.MNHistoricodeVendas1Click(Sender: TObject);
begin
  try
    FrmParametros := TFrmParametros.Create(Application);
    FrmParametros.vTipo := '1';
    FrmParametros.ShowModal;
  finally
    FreeAndNil(FrmParametros);
  end;
end;

procedure TfrmPrincipalM.MNHistoricodeCompras1Click(Sender: TObject);
begin
  try
    FrmParametros := TFrmParametros.Create(Application);
    FrmParametros.vTipo := '3';
    FrmParametros.ShowModal;
  finally
    FreeAndNil(FrmParametros);
  end;
end;





procedure TfrmPrincipalM.MNListaConfEstoqueClick(Sender: TObject);
begin
  try
    qryConferencia.Close;
    qryConferencia.Open;
    frxReport.LoadFromFile(ExtractFilePath(Application.ExeName) +
      '\Relatorio\RelEstoqueConf.fr3');
    frxReport.ShowReport;
  finally
    // nada
  end;
end;

procedure TfrmPrincipalM.MNHistoricodeVendas2Click(Sender: TObject);
begin
  try
    FrmParametros := TFrmParametros.Create(Application);
    FrmParametros.vTipo := '2';
    FrmParametros.ShowModal;
  finally
    FreeAndNil(FrmParametros);
  end;
end;

procedure TfrmPrincipalM.MnEmpresaClick(Sender: TObject);
begin
  Dados.aMenu := 'MnEmpresa';
  if FTDI = nil then
  begin
    Align := alClient;
    FTDI := TTDI.Create(self, TfrmConsEmpresa);
  end
  else
    FTDI.MostrarFormulario(TfrmConsEmpresa, false);
end;



procedure TfrmPrincipalM.mnEnviarMensagemClick(Sender: TObject);
var
  caminho: string;
begin
  caminho := ExtractFilePath(Application.ExeName) + 'WhatsAppServer.exe';
  if FileExists(caminho) then
    ShellExecute(0, 'open', PChar(caminho), '', '', SW_SHOWNORMAL);
end;



procedure TfrmPrincipalM.MnListagemdeComprasClick(Sender: TObject);
begin
  try
    FrmParametros := TFrmParametros.Create(Application);
    FrmParametros.vTipo := '3';
    FrmParametros.ShowModal;
  finally
    FreeAndNil(FrmParametros);
  end;
end;

procedure TfrmPrincipalM.MnListagemdeVendasClick(Sender: TObject);
begin
  try
    FrmParametros := TFrmParametros.Create(Application);
    FrmParametros.vTipo := '4';
    FrmParametros.ShowModal;
  finally
    FreeAndNil(FrmParametros);
  end;
end;





procedure TfrmPrincipalM.MnPDVClick(Sender: TObject);
begin
  Dados.FTIpoPDV := 'PDV';
  //btnPDV.Click;
end;





procedure TfrmPrincipalM.MNPedidosWebClick(Sender: TObject);
begin
    try
    FrmPedidoWeb := TFrmPedidoWeb.Create(Application);
    FrmPedidoWeb.ShowModal;
  finally
    FreeAndNil(FrmPedidoWeb);
  end;

end;

procedure TfrmPrincipalM.Permisses1Click(Sender: TObject);
begin
  try
    frmPermissoes := TfrmPermissoes.Create(Application);
    frmPermissoes.ShowModal;
  finally
    frmPermissoes.Release;
  end;

end;

function TfrmPrincipalM.ProcessExists(exeFileName: string): Boolean;
var
  ContinueLoop: BOOL;
  FSnapshotHandle: THandle;
  FProcessEntry32: TProcessEntry32;
begin
  FSnapshotHandle := CreateToolhelp32Snapshot(TH32CS_SNAPPROCESS, 0);
  FProcessEntry32.dwSize := SizeOf(FProcessEntry32);
  ContinueLoop := Process32First(FSnapshotHandle, FProcessEntry32);
  Result := False;
  while Integer(ContinueLoop) <> 0 do
  begin
    if ((UpperCase(ExtractFileName(FProcessEntry32.szExeFile)) =
      UpperCase(ExeFileName)) or (UpperCase(FProcessEntry32.szExeFile) =
      UpperCase(ExeFileName))) then
    begin
      Result := True;
    end;
    ContinueLoop := Process32Next(FSnapshotHandle, FProcessEntry32);
  end;
  CloseHandle(FSnapshotHandle);

end;

procedure TfrmPrincipalM.MNRelatriodeEstoqueComposioClick(Sender: TObject);
begin

  qryComposicao_M.Close;
  qryComposicao_M.Open;

  frxReport.LoadFromFile(ExtractFilePath(Application.ExeName) +
    '\Relatorio\RelEstoqueComposicao.fr3');
  frxReport.ShowReport;
end;

procedure TfrmPrincipalM.MNRelatriodeEstoqueGradeClick(Sender: TObject);
begin
  qryEstoqueGrade.Close;
  qryEstoqueGrade.Open;
  frxReport.LoadFromFile(ExtractFilePath(Application.ExeName) +
    '\Relatorio\RelEstoqueGrade.fr3');
  frxReport.ShowReport;

end;





procedure TfrmPrincipalM.Regs1Click(Sender: TObject);
begin
  try
    frmChave := TfrmChave.Create(Application);
    frmChave.ShowModal;
  finally
    frmChave.Release;
    Application.Terminate;
  end;
end;







procedure TfrmPrincipalM.SpeedButton1Click(Sender: TObject);
begin
  Dados.aMenu := 'MNNotasFornecedorLerXML';
  if FTDI = nil then
  begin
    Align := alClient;
    FTDI := TTDI.Create(self, TfrmManifesto);
  end
  else
    FTDI.MostrarFormulario(TfrmManifesto, false);
end;



procedure TfrmPrincipalM.MNSincronizarClick(Sender: TObject);
begin
  try
    FrmSincronizar := TfrmSincronizar.Create(Application);
    FrmSincronizar.ShowModal;
  finally
    FreeAndNil(FrmSincronizar);
  end;
end;



procedure TfrmPrincipalM.MNRegistrarClick(Sender: TObject);
begin
  try
    frmChave := TfrmChave.Create(Application);
    frmChave.ShowModal;
  finally
    frmChave.Release;
    Dados.AtualizaTerminal;
    Dados.vFechaPrograma := true;
    Application.Terminate;
  end;
end;



procedure TfrmPrincipalM.MNTeinamentosClick(Sender: TObject);
var
  url: string;
  nchrome, nmozila, nie: string;
begin
  nchrome := 'C:\Program Files (x86)\Google\Chrome\Application';
  url := Dados.qryParametroLINK_TREINAMENTO.Value;
  if FileExists(nchrome + '\chrome.exe') then // abre
    ShellExecute(handle, 'Open', 'chrome.exe', PChar(url), PChar(nchrome),
      SW_SHOWMAXIMIZED)
  else
    ShellExecute(Application.handle, nil, PChar(url), nil, nil,
      SW_SHOWMINIMIZED);
end;





















procedure TfrmPrincipalM.MnTrocaClick(Sender: TObject);
begin
  if FTDI <> nil then
  begin
    FTDI.Fechar(true);
    FreeAndNil(FTDI);
  end;
  CarregaSistema;
  ConfiguraSistema;
  Timer1.Enabled := true;
end;




procedure TfrmPrincipalM.MnSairClick(Sender: TObject);
begin
  btnSairClick(self);
end;



procedure TfrmPrincipalM.MnUsuariosClick(Sender: TObject);
begin
  Dados.aMenu := 'MnUsuarios';
  try
    frmUsuarios := TfrmUsuarios.Create(Application);
    Dados.aUsuario := 'Principal';
    frmUsuarios.ShowModal;
  finally
    FreeAndNil(frmUsuarios);
  end;
end;



procedure TfrmPrincipalM.MNNotasFornecedorLerXMLClick(Sender: TObject);
begin
  Dados.aMenu := 'MNNotasFornecedorLerXML';
  if FTDI = nil then
  begin
    Align := alClient;
    FTDI := TTDI.Create(self, TfrmManifesto);
  end
  else
    FTDI.MostrarFormulario(TfrmManifesto, false);
end;







procedure TfrmPrincipalM.MMPermissesClick(Sender: TObject);
begin
  try
    frmPermissoes := TfrmPermissoes.Create(Application);
    frmPermissoes.ShowModal;
  finally
    FreeAndNil(frmPermissoes);
  end;
end;



procedure TfrmPrincipalM.MnAlterarSenhaClick(Sender: TObject);
begin
  try
    frmTrocaSenha := TfrmTrocaSenha.Create(Application);
    frmTrocaSenha.ShowModal;
  finally
    frmTrocaSenha.Release;
  end;
end;





procedure TfrmPrincipalM.MNAjustaMenuClick(Sender: TObject);
var
  nivel, i, j, k, l, cont, cont1: Integer;

begin
  // esta fun��o percorre menu e sub menus e insere na tela

  Dados.QryTelas.Close;
  Dados.QryTelas.Open;

  for i := 0 to MmPrincipal.items.Count - 1 do
  begin
    // verifica menu principal
    if (Dados.QryTelas.locate('TELA', MmPrincipal.items.items[i].Name, [])) then
    begin
      if (MmPrincipal.items.items[i].Caption <> '-') then
      begin
        Dados.QryTelas.Edit;
        Dados.qryTelasFLAG.Value := '*';
        Dados.QryTelas.Post;
      end;
    end
  end;

  for i := 0 to MmPrincipal.items.Count - 1 do
  begin // menu principal
    for j := 0 to MmPrincipal.items.items[i].Count - 1 do
    begin
      // verifica submenu 1� nivel
      if (Dados.QryTelas.locate('TELA', MmPrincipal.items.items[i].items[j]
        .Name, [])) then
      begin
        if MmPrincipal.items.items[i].items[j].Caption <> '-' then
        begin
          Dados.QryTelas.Edit;
          Dados.qryTelasFLAG.Value := '*';
          Dados.QryTelas.Post;
        end;
      end;
    end;
  end;

  for i := 0 to MmPrincipal.items.Count - 1 do
  begin // menu
    for j := 0 to MmPrincipal.items.items[i].Count - 1 do
    begin
      // submenu 1� nivel
      for k := 0 to MmPrincipal.items.items[i].items[j].Count - 1 do
      begin
        // submenu 2� nivel
        if (Dados.QryTelas.locate('TELA', MmPrincipal.items.items[i].items[j]
          .items[k].Name, [])) then
        begin
          if MmPrincipal.items.items[i].items[j].items[k].Caption <> '-' then
          begin
            Dados.QryTelas.Edit;
            Dados.qryTelasFLAG.Value := '*';
            Dados.QryTelas.Post;
          end;
        end;
      end;
    end;
  end;

  for i := 0 to MmPrincipal.items.Count - 1 do
  begin // menu
    for j := 0 to MmPrincipal.items.items[i].Count - 1 do
    begin
      // submenu 1� nivel
      for k := 0 to MmPrincipal.items.items[i].items[j].Count - 1 do
      begin
        // submenu 2� nivel
        for l := 0 to MmPrincipal.items.items[i].items[j].items[k].Count - 1 do
        begin
          // submenu 3� nivel
          if MmPrincipal.items.items[i].items[j].items[k].items[l].Caption <> '-'
          then
          begin
            if (Dados.QryTelas.locate('TELA', MmPrincipal.items.items[i].items
              [j].items[k].items[l].Name, [])) then
            begin
              Dados.QryTelas.Edit;
              Dados.qryTelasFLAG.Value := '*';
              Dados.QryTelas.Post;
            end
          end;
        end;
      end;
    end;
  end;
  Dados.Conexao.CommitRetaining;

  Dados.qryExecute.Close;
  Dados.qryExecute.SQL.Text := 'delete from telas where flag<>''*''';;
  Dados.qryExecute.ExecSQL;

  Dados.Conexao.CommitRetaining;

  Dados.qryExecute.Close;
  Dados.qryExecute.SQL.Text := 'update telas set flag=''S''';;
  Dados.qryExecute.ExecSQL;
  Dados.Conexao.CommitRetaining;

  Application.ProcessMessages;
  ShowMessage('Menu Ajusta com sucesso!');

end;







procedure TfrmPrincipalM.btnPessoasMouseLeave(Sender: TObject);
begin
  (Sender as TSpeedButton).Font.Style := [];
  (Sender as TSpeedButton).Font.Color := clGray;

end;

procedure TfrmPrincipalM.btnPessoasMouseMove(Sender: TObject; Shift: TShiftState;
  X, Y: Integer);
begin
  (Sender as TSpeedButton).Font.Style := [fsBold];
  (Sender as TSpeedButton).Font.Color := clBlue;
end;

end.
