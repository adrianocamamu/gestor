{$A8,B-,C+,D+,E-,F-,G+,H+,I+,J-,K-,L+,M-,N-,O+,P+,Q-,R-,S-,T-,U-,V+,W-,X+,Y+,Z1}
{$MINSTACKSIZE $00004000}
{$MAXSTACKSIZE $00100000}
{$IMAGEBASE $00400000}
{$APPTYPE GUI}
{$WARN SYMBOL_DEPRECATED ON}
{$WARN SYMBOL_LIBRARY ON}
{$WARN SYMBOL_PLATFORM ON}
{$WARN SYMBOL_EXPERIMENTAL ON}
{$WARN UNIT_LIBRARY ON}
{$WARN UNIT_PLATFORM ON}
{$WARN UNIT_DEPRECATED ON}
{$WARN UNIT_EXPERIMENTAL ON}
{$WARN HRESULT_COMPAT ON}
{$WARN HIDING_MEMBER ON}
{$WARN HIDDEN_VIRTUAL ON}
{$WARN GARBAGE ON}
{$WARN BOUNDS_ERROR ON}
{$WARN ZERO_NIL_COMPAT ON}
{$WARN STRING_CONST_TRUNCED ON}
{$WARN FOR_LOOP_VAR_VARPAR ON}
{$WARN TYPED_CONST_VARPAR ON}
{$WARN ASG_TO_TYPED_CONST ON}
{$WARN CASE_LABEL_RANGE ON}
{$WARN FOR_VARIABLE ON}
{$WARN CONSTRUCTING_ABSTRACT ON}
{$WARN COMPARISON_FALSE ON}
{$WARN COMPARISON_TRUE ON}
{$WARN COMPARING_SIGNED_UNSIGNED ON}
{$WARN COMBINING_SIGNED_UNSIGNED ON}
{$WARN UNSUPPORTED_CONSTRUCT ON}
{$WARN FILE_OPEN ON}
{$WARN FILE_OPEN_UNITSRC ON}
{$WARN BAD_GLOBAL_SYMBOL ON}
{$WARN DUPLICATE_CTOR_DTOR ON}
{$WARN INVALID_DIRECTIVE ON}
{$WARN PACKAGE_NO_LINK ON}
{$WARN PACKAGED_THREADVAR ON}
{$WARN IMPLICIT_IMPORT ON}
{$WARN HPPEMIT_IGNORED ON}
{$WARN NO_RETVAL ON}
{$WARN USE_BEFORE_DEF ON}
{$WARN FOR_LOOP_VAR_UNDEF ON}
{$WARN UNIT_NAME_MISMATCH ON}
{$WARN NO_CFG_FILE_FOUND ON}
{$WARN IMPLICIT_VARIANTS ON}
{$WARN UNICODE_TO_LOCALE ON}
{$WARN LOCALE_TO_UNICODE ON}
{$WARN IMAGEBASE_MULTIPLE ON}
{$WARN SUSPICIOUS_TYPECAST ON}
{$WARN PRIVATE_PROPACCESSOR ON}
{$WARN UNSAFE_TYPE OFF}
{$WARN UNSAFE_CODE OFF}
{$WARN UNSAFE_CAST OFF}
{$WARN OPTION_TRUNCATED ON}
{$WARN WIDECHAR_REDUCED ON}
{$WARN DUPLICATES_IGNORED ON}
{$WARN UNIT_INIT_SEQ ON}
{$WARN LOCAL_PINVOKE ON}
{$WARN MESSAGE_DIRECTIVE ON}
{$WARN TYPEINFO_IMPLICITLY_ADDED ON}
{$WARN RLINK_WARNING ON}
{$WARN IMPLICIT_STRING_CAST ON}
{$WARN IMPLICIT_STRING_CAST_LOSS ON}
{$WARN EXPLICIT_STRING_CAST OFF}
{$WARN EXPLICIT_STRING_CAST_LOSS OFF}
{$WARN CVT_WCHAR_TO_ACHAR ON}
{$WARN CVT_NARROWING_STRING_LOST ON}
{$WARN CVT_ACHAR_TO_WCHAR ON}
{$WARN CVT_WIDENING_STRING_LOST ON}
{$WARN NON_PORTABLE_TYPECAST ON}
{$WARN XML_WHITESPACE_NOT_ALLOWED ON}
{$WARN XML_UNKNOWN_ENTITY ON}
{$WARN XML_INVALID_NAME_START ON}
{$WARN XML_INVALID_NAME ON}
{$WARN XML_EXPECTED_CHARACTER ON}
{$WARN XML_CREF_NO_RESOLVE ON}
{$WARN XML_NO_PARM ON}
{$WARN XML_NO_MATCHING_PARM ON}
{$WARN IMMUTABLE_STRINGS OFF}
unit uGeraEntrega;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Data.DB, Vcl.StdCtrls, Vcl.Buttons,
  Vcl.Grids, Vcl.DBGrids, FireDAC.Stan.Intf, FireDAC.Stan.Option,
  FireDAC.Stan.Param, FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf,
  FireDAC.DApt.Intf, FireDAC.Stan.Async, FireDAC.DApt, FireDAC.Comp.DataSet,
  FireDAC.Comp.Client, ACBrBase, ACBrEnterTab, cxGraphics, cxControls,
  cxLookAndFeels, cxLookAndFeelPainters, cxContainer, cxEdit, cxTextEdit,
  cxMaskEdit, cxSpinEdit, cxDBEdit, frxClass, frxDBSet, Vcl.Imaging.pngimage,
  Vcl.ExtCtrls;

type
  TfrmGeraEntrega = class(TForm)
    btnGravarOrdem: TSpeedButton;
    btnSairOrdem: TSpeedButton;
    qryItemVenda: TFDQuery;
    dsItemVenda: TDataSource;
    qryItemVendaFKVENDA: TIntegerField;
    qryItemVendaID_PRODUTO: TIntegerField;
    qryItemVendaITEM: TSmallintField;
    qryItemVendaQTD: TFMTBCDField;
    qryItemVendaUNIDADE: TStringField;
    qryItemVendaDESCRICAO: TStringField;
    qryItemVendaENTREGUE: TFloatField;
    qryItemVendaSALDO: TFloatField;
    DBGrid1: TDBGrid;
    qryVenda: TFDQuery;
    qryVendaCODIGO: TIntegerField;
    qryVendaID_CLIENTE: TIntegerField;
    qryVendaFK_USUARIO: TIntegerField;
    qryVendaFK_VENDEDOR: TIntegerField;
    qryVendaFKEMPRESA: TIntegerField;
    dsVenda: TDataSource;
    frxDBEmpresa: TfrxDBDataset;
    frxDBOrdemItens: TfrxDBDataset;
    frxDBCliente: TfrxDBDataset;
    frxDBOrdem: TfrxDBDataset;
    frxReport: TfrxReport;
    qryOrdem: TFDQuery;
    dsOrdem: TDataSource;
    qryCliente: TFDQuery;
    qryVendedor: TFDQuery;
    frxDBVendedor: TfrxDBDataset;
    qryItemOrdem: TFDQuery;
    qryItemOrdemFIL_CODIGO: TIntegerField;
    qryItemOrdemVEN_CODIGO: TIntegerField;
    qryItemOrdemVE_CODIGO: TIntegerField;
    qryItemOrdemPEI_CODIGO: TIntegerField;
    qryItemOrdemPRO_CODIGO: TStringField;
    qryItemOrdemVEI_QTD: TFloatField;
    qryItemOrdemVEI_QTD_RESTANTE: TFloatField;
    qryItemOrdemVEI_QTD_FALTA: TFloatField;
    qryItemOrdemDESCRICAO: TStringField;
    qryItemVendaLARGURA: TFMTBCDField;
    qryItemVendaCOMPRIMENTO: TFMTBCDField;
    qryItemVendaID_MODELO: TIntegerField;
    qryItemVendaQTD_MODELO: TFMTBCDField;
    procedure DBGrid1KeyPress(Sender: TObject; var Key: Char);
    procedure FormShow(Sender: TObject);
    procedure qryItemVendaAfterOpen(DataSet: TDataSet);
    procedure btnGravarOrdemClick(Sender: TObject);
    procedure btnSairOrdemClick(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);




  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmGeraEntrega: TfrmGeraEntrega;

implementation

{$R *.dfm}

uses uFormaPagamentoR, Udados, uBalcao;


procedure TfrmGeraEntrega.DBGrid1KeyPress(Sender: TObject; var Key: Char);
begin
  if key = #13 then
    begin
      if DBGrid1.Fields[7].Value >= 0 then
       begin
        if DBGrid1.Fields[7].Value <= DBGrid1.Fields[5].Value then
          begin

            DBGrid1.Fields[6].Value := DBGrid1.Fields[5].Value - DBGrid1.Fields[7].Value;

          end
        else
         begin
          ShowMessage('Quantidade a entregar n�o pode ser maior que a vendida');
          DBGrid1.Fields[7].Value := 0;
          exit;
         end;
       end
      else
        begin
          ShowMessage('Quantidade a entregar n�o pode ser menor que 0');
          DBGrid1.Fields[7].Value := 0;
          exit;
        end;
    end;
end;

procedure TfrmGeraEntrega.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if key = vk_f12 then
    begin
      btnGravarOrdemClick(self);
    end;

  if key = vk_escape then
    begin
      close;
    end;
end;

procedure TfrmGeraEntrega.FormShow(Sender: TObject);
begin

    qryItemVenda.close;
    qryItemVenda.Params[0].AsInteger := StrToInt(frmFechaVendaR.DBEdit8.Text);
    qryItemVenda.Open;

end;

procedure TfrmGeraEntrega.qryItemVendaAfterOpen(DataSet: TDataSet);
begin
    if not(qryItemVenda.State in dsEditModes) then
      qryItemVenda.Edit;
   while not qryItemVenda.Eof do begin

   if not(qryItemVenda.State in dsEditModes) then
      qryItemVenda.Edit;

   qryItemVenda.FieldByName('Entregue').Value := qryItemVenda.FieldByName('qtd').Value;

   qryItemVenda.Next;
   end;

   qryItemVenda.First;
   DBGrid1.SelectedIndex := 7;
   DBGrid1.SetFocus;
end;

procedure TfrmGeraEntrega.btnGravarOrdemClick(Sender: TObject);
var codigo_ve, codigo_vei : Integer;
begin

  qryVenda.Close;
  qryVenda.Params[0].Value := StrToInt(frmFechaVendaR.DBEdit8.Text);
  qryVenda.Open;

  Dados.qryExecute.SQL.Clear;
  Dados.qryExecute.SQL.Add('INSERT INTO VENDAS_ENTREGA (');
  Dados.qryExecute.SQL.Add('VE_CODIGO,');
  Dados.qryExecute.SQL.Add('FIL_CODIGO,');
  Dados.qryExecute.SQL.Add('VEN_CODIGO,');
  Dados.qryExecute.SQL.Add('FK_CLIENTE,');
  Dados.qryExecute.SQL.Add('VE_DATA,');
  Dados.qryExecute.SQL.Add('VE_HORA,');
  Dados.qryExecute.SQL.Add('VE_STATUS,');
  Dados.qryExecute.SQL.Add('USR_USUARIO,');
  Dados.qryExecute.SQL.Add('FK_VENDEDOR');
  Dados.qryExecute.SQL.Add(')');
  Dados.qryExecute.SQL.Add('VALUES');
  Dados.qryExecute.SQL.Add('(');
  Dados.qryExecute.SQL.Add(':VE_CODIGO,');
  Dados.qryExecute.SQL.Add(':FIL_CODIGO,');
  Dados.qryExecute.SQL.Add(':VEN_CODIGO,');
  Dados.qryExecute.SQL.Add(':ID_CLIENTE,');
  Dados.qryExecute.SQL.Add(':VE_DATA,');
  Dados.qryExecute.SQL.Add(':VE_HORA,');
  Dados.qryExecute.SQL.Add(':VE_STATUS,');
  Dados.qryExecute.SQL.Add(':USR_USUARIO,');
  Dados.qryExecute.SQL.Add(':FK_VENDEDOR');
  Dados.qryExecute.SQL.Add(');');

  Dados.qryExecute.ParamByName('FIL_CODIGO').Value :=
    Dados.qryEmpresaCODIGO.Value;
  Dados.qryExecute.ParamByName('VEN_CODIGO').Value :=
    StrToInt(frmFechaVendaR.DBEdit8.Text);
  Dados.qryExecute.ParamByName('ID_CLIENTE').Value :=
    qryVendaID_CLIENTE.Value;
  Dados.qryExecute.ParamByName('VE_DATA').Value := date;
  Dados.qryExecute.ParamByName('VE_HORA').Value := time;
  Dados.qryExecute.ParamByName('VE_STATUS').Value := 'ABERTO';
  Dados.qryExecute.ParamByName('USR_USUARIO').Value := Dados.idUsuario;
  Dados.qryExecute.ParamByName('FK_VENDEDOR').Value := qryVendaFK_VENDEDOR.Value;

  codigo_ve := Dados.Numerador('VENDAS_ENTREGA', 'VE_CODIGO', 'N', '', '');

  Dados.qryExecute.ParamByName('VE_CODIGO').Value := codigo_ve;
  Dados.qryExecute.ExecSQL;
  Dados.Conexao.CommitRetaining;

  codigo_vei := 0;

  qryItemVenda.First;
  while not qryItemVenda.Eof do begin
    if qryItemVenda.FieldByName('saldo').Value > 0 then
      begin

        Dados.qryExecute.SQL.Clear;
        Dados.qryExecute.SQL.Add('INSERT INTO VENDAS_ENTREGA_ITENS (');
        Dados.qryExecute.SQL.Add('FIL_CODIGO,');
        Dados.qryExecute.SQL.Add('VEN_CODIGO,');
        Dados.qryExecute.SQL.Add('VE_CODIGO,');
        Dados.qryExecute.SQL.Add('PEI_CODIGO,');
        Dados.qryExecute.SQL.Add('PRO_CODIGO,');
        Dados.qryExecute.SQL.Add('VEI_QTD_RESTANTE,');
        Dados.qryExecute.SQL.Add('ITEM_VENDA');
        Dados.qryExecute.SQL.Add(')');
        Dados.qryExecute.SQL.Add('VALUES');
        Dados.qryExecute.SQL.Add('(');
        Dados.qryExecute.SQL.Add(':FIL_CODIGO,');
        Dados.qryExecute.SQL.Add(':VEN_CODIGO,');
        Dados.qryExecute.SQL.Add(':VE_CODIGO,');
        Dados.qryExecute.SQL.Add(':PEI_CODIGO,');
        Dados.qryExecute.SQL.Add(':PRO_CODIGO,');
        Dados.qryExecute.SQL.Add(':VEI_QTD_RESTANTE,');
        Dados.qryExecute.SQL.Add(':ITEM_VENDA');
        Dados.qryExecute.SQL.Add(');');

        Dados.qryExecute.ParamByName('FIL_CODIGO').Value :=
          Dados.qryEmpresaCODIGO.Value;
        Dados.qryExecute.ParamByName('VEN_CODIGO').Value :=
          StrToInt(frmFechaVendaR.DBEdit8.Text);

        Dados.qryExecute.ParamByName('VE_CODIGO').Value :=
          codigo_ve;
        Dados.qryExecute.ParamByName('PRO_CODIGO').Value :=
          qryItemVenda.FieldByName('ID_PRODUTO').Value;
        Dados.qryExecute.ParamByName('VEI_QTD_RESTANTE').Value :=
          qryItemVenda.FieldByName('SALDO').Value;
        Dados.qryExecute.ParamByName('ITEM_VENDA').Value :=
          qryItemVenda.FieldByName('ITEM').Value;

        codigo_vei := codigo_vei + 1;

        Dados.qryExecute.ParamByName('PEI_CODIGO').Value := codigo_vei;
        Dados.qryExecute.ExecSQL;
        Dados.Conexao.CommitRetaining;


      end;
      qryItemVenda.Next;
    end;

    if codigo_vei <> 0 then
      begin
        if application.messagebox('Deseja imprimir a Ordem de Entgrega?', 'Confirma��o', mb_yesno) = mrYes then
          begin
            qryOrdem.Close;
            qryOrdem.SQL.Text :=
            'select * from VENDAS_ENTREGA VE where VE.ve_codigo=:codigo';
            qryOrdem.Params[0].Value := codigo_ve;
            qryOrdem.Open;

            qryItemOrdem.Close;
            qryItemOrdem.Params[0].Value := codigo_ve;
            qryItemOrdem.Open;

            qryCliente.Close;
            qryCliente.Params[0].Value := qryOrdem.FieldByName('FK_CLIENTE').Value;
            qryCliente.Open;

            qryVendedor.Close;
            qryVendedor.Params[0].Value := qryOrdem.FieldByName('FK_VENDEDOR').Value;
            qryVendedor.Open;


                frxReport.LoadFromFile(ExtractFilePath(Application.ExeName) +
                '\Relatorio\RelOrdemEntrega.fr3');
                frxReport.ShowReport;
                close;
          end
        else
          begin

            close;
          end;

      end
     else
      begin
         ShowMessage('Nenhum produto foi adicionado para entrega.');
      end;



end;

procedure TfrmGeraEntrega.btnSairOrdemClick(Sender: TObject);
begin
  if application.messagebox('Deseja Sair da Tela?', 'Confirma��o', mb_yesno) = mrYes
  then
  begin
    Close;

  end;
end;



end.
