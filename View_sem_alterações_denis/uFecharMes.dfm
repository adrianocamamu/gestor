object frmFecharMes: TfrmFecharMes
  Left = 0
  Top = 0
  Caption = 'Fechar M'#234's'
  ClientHeight = 360
  ClientWidth = 598
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object PagUtilidade: TPageControl
    Left = 0
    Top = 65
    Width = 598
    Height = 295
    ActivePage = TabGerar
    Align = alClient
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Segoe UI'
    Font.Style = []
    ParentFont = False
    TabOrder = 0
    Visible = False
    ExplicitLeft = 27
    ExplicitTop = 42
    ExplicitWidth = 582
    ExplicitHeight = 278
    object TabGerar: TTabSheet
      Caption = 'Gerar PDF / XML'
      ImageIndex = 1
      object pnGerar: TPanel
        Left = 0
        Top = 0
        Width = 590
        Height = 265
        Align = alClient
        BevelOuter = bvNone
        TabOrder = 0
        ExplicitWidth = 574
        ExplicitHeight = 248
        object BitBtn1: TBitBtn
          Left = 33
          Top = 182
          Width = 103
          Height = 60
          Caption = 'Gerar'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentFont = False
          TabOrder = 0
          OnClick = BitBtn1Click
        end
        object BitBtn2: TBitBtn
          Left = 464
          Top = 182
          Width = 108
          Height = 60
          Caption = 'Sair'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentFont = False
          TabOrder = 1
        end
        object ProgressBar1: TProgressBar
          Left = 0
          Top = 248
          Width = 590
          Height = 17
          Align = alBottom
          TabOrder = 2
          ExplicitTop = 231
          ExplicitWidth = 574
        end
        object memLista: TListBox
          Left = 18
          Top = -13
          Width = 111
          Height = 187
          ItemHeight = 15
          TabOrder = 3
          Visible = False
        end
        object GroupBox2: TGroupBox
          Left = 21
          Top = 15
          Width = 551
          Height = 161
          Caption = 'Enviar Email'
          TabOrder = 4
          object Label14: TLabel
            Left = 16
            Top = 21
            Width = 87
            Height = 15
            Caption = 'E-Mail Contador'
          end
          object Label15: TLabel
            Left = 16
            Top = 64
            Width = 42
            Height = 15
            Caption = 'Arquivo'
          end
          object Label16: TLabel
            Left = 16
            Top = 106
            Width = 99
            Height = 15
            Caption = 'Caminho Relat'#243'rio'
          end
          object edtArquivo: TEdit
            Left = 16
            Top = 79
            Width = 514
            Height = 21
            Ctl3D = False
            ParentCtl3D = False
            TabOrder = 1
          end
          object edtRelatorio: TEdit
            Left = 16
            Top = 125
            Width = 514
            Height = 21
            Ctl3D = False
            ParentCtl3D = False
            TabOrder = 2
          end
          object edtEmail: TDBLookupComboboxEh
            Left = 16
            Top = 37
            Width = 514
            Height = 23
            DynProps = <>
            DataField = ''
            EditButtons = <>
            KeyField = 'EMAIL'
            ListField = 'EMAIL'
            TabOrder = 0
            Visible = True
          end
        end
        object Button1: TButton
          Left = 152
          Top = 182
          Width = 105
          Height = 60
          Caption = 'Enviar E-mail'
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Segoe UI'
          Font.Style = [fsBold]
          ParentFont = False
          TabOrder = 5
        end
      end
    end
  end
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 598
    Height = 65
    Align = alTop
    TabOrder = 1
    ExplicitWidth = 641
    object LblPeriodo: TLabel
      Left = 7
      Top = 10
      Width = 119
      Height = 16
      Caption = 'Per'#237'odo de          at'#233
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    object maskInicio: TMaskEdit
      Left = 7
      Top = 35
      Width = 90
      Height = 21
      Color = clWhite
      Ctl3D = False
      EditMask = '!99/99/0000;1;_'
      Font.Charset = ANSI_CHARSET
      Font.Color = clNavy
      Font.Height = -12
      Font.Name = 'Segoe UI'
      Font.Style = []
      MaxLength = 10
      ParentCtl3D = False
      ParentFont = False
      TabOrder = 0
      Text = '  /  /    '
    end
    object maskFim: TMaskEdit
      Left = 106
      Top = 35
      Width = 90
      Height = 21
      Color = clWhite
      Ctl3D = False
      EditMask = '!99/99/0000;1;_'
      Font.Charset = ANSI_CHARSET
      Font.Color = clNavy
      Font.Height = -12
      Font.Name = 'Segoe UI'
      Font.Style = []
      MaxLength = 10
      ParentCtl3D = False
      ParentFont = False
      TabOrder = 1
      Text = '  /  /    '
    end
  end
  object qryXML: TFDQuery
    Connection = Dados.Conexao
    FetchOptions.AssignedValues = [evMode]
    FetchOptions.Mode = fmAll
    SQL.Strings = (
      'select'
      'NM.numero,'
      'NM.data_emissao,'
      'NM.chave,'
      'NM.TRIB_FED,'
      'NM.TRIB_EST,'
      'NM.TRIB_MUN,'
      'NM.TRIB_IMP,'
      'NM.situacao,'
      'NM.serie,'
      'NM.xml,'
      'NM.xml_cancelamento,'
      'NM.total'
      'FROM NFCE_MASTER NM'
      'WHERE'
      'NM.SITUACAO in ('#39'T'#39','#39'C'#39') AND'
      'NM.data_emissao between :DATA1 AND :DATA2 AND'
      'NM.FKEMPRESA=:ID'
      'ORDER BY NM.numero;')
    Left = 264
    Top = 72
    ParamData = <
      item
        Name = 'DATA1'
        DataType = ftDate
        ParamType = ptInput
        Value = Null
      end
      item
        Name = 'DATA2'
        DataType = ftDate
        ParamType = ptInput
      end
      item
        Name = 'ID'
        DataType = ftInteger
        ParamType = ptInput
      end>
    object qryXMLNUMERO: TIntegerField
      FieldName = 'NUMERO'
      Origin = 'NUMERO'
    end
    object qryXMLDATA_EMISSAO: TDateField
      FieldName = 'DATA_EMISSAO'
      Origin = 'DATA_EMISSAO'
    end
    object qryXMLCHAVE: TStringField
      FieldName = 'CHAVE'
      Origin = 'CHAVE'
      Size = 50
    end
    object qryXMLTRIB_FED: TFMTBCDField
      FieldName = 'TRIB_FED'
      Origin = 'TRIB_FED'
      Precision = 18
      Size = 2
    end
    object qryXMLTRIB_EST: TFMTBCDField
      FieldName = 'TRIB_EST'
      Origin = 'TRIB_EST'
      Precision = 18
      Size = 2
    end
    object qryXMLTRIB_MUN: TFMTBCDField
      FieldName = 'TRIB_MUN'
      Origin = 'TRIB_MUN'
      Precision = 18
      Size = 2
    end
    object qryXMLTRIB_IMP: TFMTBCDField
      FieldName = 'TRIB_IMP'
      Origin = 'TRIB_IMP'
      Precision = 18
      Size = 2
    end
    object qryXMLSITUACAO: TStringField
      FieldName = 'SITUACAO'
      Origin = 'SITUACAO'
      Required = True
      Size = 1
    end
    object qryXMLSERIE: TStringField
      FieldName = 'SERIE'
      Origin = 'SERIE'
      Size = 10
    end
    object qryXMLXML: TMemoField
      FieldName = 'XML'
      Origin = 'XML'
      BlobType = ftMemo
    end
    object qryXMLTOTAL: TFMTBCDField
      FieldName = 'TOTAL'
      Origin = 'TOTAL'
      Precision = 18
      Size = 2
    end
    object qryXMLXML_CANCELAMENTO: TMemoField
      FieldName = 'XML_CANCELAMENTO'
      Origin = 'XML_CANCELAMENTO'
      BlobType = ftMemo
    end
  end
end
