program MonitorNFCe;

uses
  System.SysUtils,
  vcl.Forms,
  windows,
  Themes,
  Styles,
  Serial in '..\View\Serial.pas',
  uCadProduto in '..\View\uCadProduto.pas' {FrmCadProduto},
  uMarca in '..\View\uMarca.pas' {frmMarca},
  uAcesso in '..\View\uAcesso.pas' {frmAcesso},
  uChave in '..\View\uChave.pas' {frmChave},
  Udados in '..\Model\Udados.pas' {Dados: TDataModule},
  uConexaoBD in '..\View\uConexaoBD.pas' {frmConexaoBD},
  udadosSped in '..\Model\udadosSped.pas' {DadosSped: TDataModule},
  uSplash in 'uSplash.pas' {frmSplash},
  uDadosWeb in '..\Model\uDadosWeb.pas' {DadosWeb: TDataModule},
  uNFCe_Monitor in '..\View\uNFCe_Monitor.pas' {frmNFCe_Monitor},
  udtmCBR in '..\Model\udtmCBR.pas' {dtmCBR: TDataModule},
  uTef in '..\View\uTef.pas' {FrmTEF},
  udmImpressao in '..\Model\udmImpressao.pas' {DMImpressao: TDataModule},
  uDmNFeMonitor in '..\Model\uDmNFeMonitor.pas' {dmNFeMonitor: TDataModule},
  uDmNFe in '..\Model\uDmNFe.pas' {dmNFe: TDataModule},
  uDMSat in '..\Model\uDMSat.pas' {DMSat: TDataModule},
  uDmMDFE in '..\Model\uDmMDFE.pas' {DmMDFe: TDataModule},
  uDmCTe in '..\Model\uDmCTe.pas' {dmCTe: TDataModule},
  uDmPDV in '..\Model\uDmPDV.pas' {dmPDV: TDataModule},
  uTabelaIcms in '..\View\uTabelaIcms.pas' {FrmTabela},
  uUnidade in '..\View\uUnidade.pas' {frmUnidade},
  uGrupo in '..\View\uGrupo.pas' {frmGrupo},
  uProdutos in '..\View\uProdutos.pas' {frmProdutos},
  uSupervisor in '..\View\uSupervisor.pas' {FrmSupervisor},
  ufrmStatus in '..\View\ufrmStatus.pas' {frmStatus},
  uEmail in '..\View\uEmail.pas' {FrmEmail},
  uDMEstoque in '..\Model\uDMEstoque.pas' {DMEstoque: TDataModule},
  uRotinasComuns in '..\View\uRotinasComuns.pas' {DMRotinas: TDataModule},
  uclassUTIL in '..\Boleto\class\uclassUTIL.pas',
  uclassCBR_REMESSA in '..\Boleto\class\uclassCBR_REMESSA.pas',
  uclassCBR_TITULOS in '..\Boleto\class\uclassCBR_TITULOS.pas',
  ufrmMENSAGEMespera in '..\Boleto\ufrmMENSAGEMespera.pas' {frmMENSAGEMespera},
  ufuncoes in '..\Boleto\unit\ufuncoes.pas',
  uclassLOG in '..\Boleto\class\uclassLOG.pas',
  uclassDB in '..\Boleto\class\uclassDB.pas',
  ufrmDefault in '..\Boleto\ufrmDefault.pas' {frmDefault},
  frExibeMensagem in '..\View\frExibeMensagem.pas' {FormExibeMensagem},
  uStatus in '..\View\uStatus.pas' {FrmStatusGTIN},
  uExecute in '..\View\uExecute.pas' {frmExecute};

{$R *.res}

begin

  Application.Initialize;
  Application.MainFormOnTaskbar := true;
  Application.Title := 'Monitor NFCe';
  Application.CreateForm(TDados, Dados);
  Application.CreateForm(TDadosSped, DadosSped);
  Application.CreateForm(TDadosWeb, DadosWeb);
  Application.CreateForm(TDMRotinas, DMRotinas);
  Application.CreateForm(TdtmCBR, dtmCBR);
  Application.CreateForm(TDMImpressao, DMImpressao);
  Application.CreateForm(TdmNFeMonitor, dmNFeMonitor);
  Application.CreateForm(TDMSat, DMSat);
  Application.CreateForm(TDmMDFe, DmMDFe);
  Application.CreateForm(TdmCTe, dmCTe);
  Application.CreateForm(TdmPDV, dmPDV);
  Application.CreateForm(TDMEstoque, DMEstoque);
  //Dados.ConfiguraEstilo(Dados.qryParametroESTILO.Value);
  Application.CreateForm(TfrmNFCe_Monitor, frmNFCe_Monitor);
  Application.CreateForm(TFrmTEF, FrmTEF);
  Application.Run;
end.
