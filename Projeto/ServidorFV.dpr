program ServidorFV;

uses
  Vcl.Forms,
  PrincipalServidorFV in '..\View\PrincipalServidorFV.pas' {Principal_FV},
  Udados in '..\Model\Udados.pas' {Dados: TDataModule},
  uPedidoWeb in '..\View\uPedidoWeb.pas' {FrmPedidoWeb},
  uSincronizar in '..\View\uSincronizar.pas' {FrmSincronizar},
  uDadosWeb in '..\Model\uDadosWeb.pas' {DadosWeb: TDataModule},
  uConexaoBD in '..\View\uConexaoBD.pas' {frmConexaoBD},
  Serial in '..\View\Serial.pas';

{$R *.res}

begin
  Application.Initialize;
  Application.MainFormOnTaskbar := True;
  Application.CreateForm(TPrincipal_FV, Principal_FV);
  Application.CreateForm(TDados, Dados);
  Application.CreateForm(TFrmPedidoWeb, FrmPedidoWeb);
  Application.CreateForm(TFrmSincronizar, FrmSincronizar);
  Application.CreateForm(TDadosWeb, DadosWeb);
  Application.CreateForm(TfrmConexaoBD, frmConexaoBD);
  Application.Run;
end.
